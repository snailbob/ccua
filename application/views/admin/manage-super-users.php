<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">
   <div class="row">
      <div class="col-lg-12">
         <div class="page-title">
            <h3>Manage <?php echo $title; ?></h3>
            <ol class="breadcrumb">
               <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>
               <li class="active">Manage <?php echo $title; ?></li>
            </ol>
         </div>
      </div>
      <!-- /.col-lg-12 -->
   </div>
   <!-- /.row -->
   <!-- end PAGE TITLE AREA -->
   <!-- Form AREA -->
   <div class="row">
      <div class="col-lg-12">
         <?php if($this->session->flashdata('success')!=""){ ?>
         <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>
         </div>
         <?php } if($this->session->flashdata('error')!=""){ ?>
         <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?>
         </div>
         <?php } ?>
      </div>
      <div class="col-lg-12">
         <p class="text-right">
            <a href="#addAgencyModal" class="btn btn-primary" data-toggle="modal" data-controls-modal="#addAgencyModal">Add <?php echo $singular_title; ?></a>
         </p>
         <div class="panel panel-default">
            <div class="panel-heading">
               <div  class="panel-title">
                  <h4><?php echo $title; ?></h4>
               </div>
            </div>
            <?php if(count($users) > 0) { ?>
            <div class="table-responsivex">
               <table class="table table-striped table-hover table-datatable">
                  <thead>
                     <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Activated</th>
                        <th>User Type</th>
                        <th> </th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                        foreach($users as $u=>$user){
                        	$user['access_rights'] = (@unserialize($user['access_rights'])) ? unserialize($user['access_rights']) : array();

                        ?>
                     <tr>
                        <td><?php echo $this->common->customer_name($user['id']) ?></td>
                        <td><?php echo $user['email'] ?></td>
                        <td>
                           <?php
                              if($user['status'] == 'Y'){
                              	echo '<span class="text-success">Verified</span>';
                              }

                              else{
                              	echo '<span class="text-red">Not Verified</span>';
                              }
                              ?>
                        </td>
                        <td>
                           <?php
                              if($user['enabled'] == 'Y'){
                              	echo '<span class="text-success">Yes</span>';
                              }

                              else{
                              	echo '<span class="text-red">No</span>';
                              }
                              ?>
                        </td>
                        <td>
                           <?php
                              if($user['first_user'] == 'Y'){
                              	echo '<span class="text-success">Super Administrator</span>';
                              }

                              else{
                              	echo '<span class="text-text-muted">Regular User</span>';
                              }
                              ?>
                        </td>
                        <td>
                           <!-- Single button -->
                           <div class="btn-group pull-right">
                              <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                              Action <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu" role="menu">
                                 <li><a href="javascript:;" onclick="customerDetails('<?php echo $user['id']; ?>');">Details</a></li>
                                 <?php if($user['first_user'] == 'N' || $this->session->userdata('logged_admin') == 'admin'){ ?>
                                 <li><a href="javascript:;" class="customer-update" data-id="<?php echo $user['id'] ?>" data-arr='<?php echo json_encode($user) ?>'>Update</a></li>
                                 <?php if($user['enabled'] == 'Y') {?>
                                 <li><a href="javascript:;" onclick="customerActivate('N','<?php echo $user['id']; ?>');">Deactivate</a></li>
                                 <?php } else {?>
                                 <li><a href="javascript:;" onclick="customerActivate('Y','<?php echo $user['id']; ?>');">Activate</a></li>
                                 <?php } ?>
                                 <li class="divider"></li>
                                 <?php if($user['first_user'] == 'N') {?>
                                 <li><a href="javascript:;" class="underwriter-first-user" data-to="Y" data-id="<?php echo $user['id'] ?>" data-agency_id="<?php // echo $user['agency_id'] ?>" data-info='<?php echo json_encode($user) ?>'>Make Super Admin</a></li>
                                 <?php } ?>
                                 <?php if($user['first_user'] != 'N') {?>
                                 <li><a href="javascript:;" class="underwriter-first-user" data-to="N" data-id="<?php echo $user['id'] ?>" data-agency_id="<?php // echo $user['agency_id'] ?>" data-info='<?php echo json_encode($user) ?>'>Unassign as Super Admin</a></li>
                                 <?php } ?>
                                 <li><a href="javascript:;" class="delete_btn" data-id="<?php echo $user['id']?>" data-table="customers">Delete Permanently</a></li>
                                 <?php } else { ?>
                                 <li class="disabled"><a href="javascript:;">Update</a></li>
                                 <li class="divider"></li>
                                 <li class="disabled"><a href="javascript:;">Delete Permanently</a></li>
                                 <?php } ?>
                              </ul>
                           </div>
                        </td>
                     </tr>
                     <?php
                        }
                        ?>
                  </tbody>
               </table>
               <p class="lead" style="padding-bottom: 15px;">&nbsp;</p>
            </div>
            <!--end of table responsive-->
            <?php } else{
               echo '<div class="panel-body"><p class="text-center text-muted">Nothing to show you.</p></div>';
               }?>
            <div class="panel-body hidden">
            </div>
         </div>
      </div>
   </div>
   <!--.row-->
</div>
<!-- Modal -->
<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"><?php echo $singular_title; ?> Information</h4>
         </div>
         <div class="modal-body">
            <div class="customer_info">
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!-- Portfolio Modals -->
<div class="portfolio-modal modal fade" id="addAgencyModal" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-content">
      <div class="close-modal" data-dismiss="modal">
         <div class="lr">
            <div class="rl">
            </div>
         </div>
      </div>
      <div class="container">
         <div class="row">
            <div class="col-md-6 col-md-offset-3">
               <div class="modal-body">
                  <h1><?php echo $singular_title; ?></h1>
                  <hr />
                  <form id="customer_form" class="text-left">
                     <input type="hidden" name="id"/>
                     <input type="hidden" name="super_admin" value="Y"/>
                     <div class="row">
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label>First Name</label>
                              <input type="text" class="form-control" name="first" required="required"/>
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label>Last Name</label>
                              <input type="text" class="form-control" name="last" required="required" />
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <label>Business Name</label>
                        <input type="text" class="form-control" name="bname"/>
                     </div>
                     <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" name="email" required="required"/>
                     </div>
                     <div class="form-group">
                        <label>Access rights</label>
                        <div class="row row-sm">
                           <?php foreach($access_rights as $r=>$value) {?>
                           <div class="col-md-4 col-sm-6">
                              <div class="form-group">
                                 <div class="btn-group btn-group-justified" data-toggle="buttons">
                                    <label class="btn btn-primary active" style="padding-top: 25px;padding-bottom: 25px;">
                                    <input type="checkbox" class="access_rights" name="access_rights[]" checked="checked" value="<?php echo $value ?>">
                                    <?php echo $value ?>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <?php } ?>
                        </div>
                     </div>
                     <div class="form-group hidden">
                        <label><?php echo $singular_title; ?> Type</label><br />
                        <label>
                        <input type="checkbox" name="ctype" <?php if($this->uri->segment(2) != 'agency') { echo 'checked="checked"'; } ?>/>
                        </label>
                     </div>
                     <div class="form-group">
                        <button class="btn btn-primary" type="submit">Submit</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!--portfolio-modal-->
