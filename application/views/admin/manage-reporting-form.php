<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-title">
                <h3>Manage
                    <?php echo $title ?>
                </h3>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>
                        <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a>
                    </li>
                    <li class="active">Manage
                        <?php echo $title ?>
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- end PAGE TITLE AREA -->
    <!-- Form AREA -->
    <div class="row">
        <div class="col-lg-12">
            <?php if($this->session->flashdata('success')!=""){ ?>
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <strong>Success!</strong>
                <?php echo $this->session->flashdata('success');   ?>
            </div>
            <?php } if($this->session->flashdata('error')!=""){ ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <strong>Error:</strong>
                <?php echo $this->session->flashdata('error');   ?>
            </div>
            <?php } ?>
        </div>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        <h4>
                            <?php echo $title; ?>
                        </h4>
                    </div>
                </div>
                <div class="panel-body">
                    <form class="generate_report_form">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label for="shipment_date">From</label>
                                    <input type="text" class="form-control date_from" id="date_from" name="date_from" value="<?php // echo date(" d-m-Y", strtotime("-1 months ")); ?>" />
                                    <input type="hidden" class="report-type" name="type" value="bordereaux" />
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="date_to">To</label>
                                    <input type="text" class="form-control date_to" id="date_to" name="date_to" value="<?php // echo date(" d-m-Y "); ?>" />
                                </div>
                            </div>
                            <div class="row row-bordereaux hiddenx">
                                <div class="col-sm-12">
                                    <div class="form-group hidden">
                                        <label>Select template</label>
                                        <select class="form-control" name="report_template">
                                            <option value="" data-name="" data-inputs='[]'>New Template</option>
                                            <?php
                                                if(count($report_templates) > 0){
                                                    foreach($report_templates as $r=>$value){
                                                        $tdata = unserialize($value['details']);

                                                        echo '<option value="'.$value['id'].'" data-inputs=\''.json_encode($tdata).'\' data-name="'.$value['name'].'">'.$value['name'].'</option>';
                                                    }
                                                }
                                                ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group hidden">
                                        <label>Template Name</label>
                                        <input type="text" class="form-control" name="template_name" value="" />
                                    </div>
                                    <div class="form-group report-attributes">
                                        <button type="button" class="toggle_check btn btn-link btn-xs pull-right" data-name="report_header[]">
                                          Select/Deselect All
                                        </button>
                                        <label>Select Data</label>
                                        <div class="row">
                                            <?php foreach($report_header as $r=>$value) {?>
                                            <div class="col-md-3 col-sm-6" style="min-height: 50px">
                                                <div class="form-group">
                                                    <div class="btn-group btn-group-justified" data-toggle="buttons">
                                                        <label class="btn btn-default">
                                                            <input type="checkbox" name="report_header[]" value="<?php echo $r ?>">
                                                            <!-- <i class="fa <?php // echo $report_icons[$r] ?> fa-2x"></i>
                                                            <br /> -->
                                                            <?php echo $value ?>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                        <?php /*?>
                                        <?php foreach($report_header as $r=>$value) { ?>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="report_header[]" value="<?php echo $r ?>">
                                                <?php echo $value ?>
                                            </label>
                                        </div>
                                        <?php } ?>
                                        <?php */?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Generate Report</button>
                        </div>
                    </form>



                </div>

            </div>
        </div>
    </div>
    <!--.row-->
</div>
