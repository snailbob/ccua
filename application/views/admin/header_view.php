<?php if($this->session->userdata('logged_admin') == '') { redirect(base_url().'webmanager'); }
	$access_rights = $this->session->userdata('access_rights');
	$first_user = $this->session->userdata('first_user');
	$logged_admin_id = $this->session->userdata('logged_admin_id');
	$admin_first = ($first_user == 'Y' || $logged_admin_id == '') ? 'Y' : 'N';
	$access_rights = (!empty($access_rights) && $admin_first == 'N') ? $access_rights : $this->common->access_rights();
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">


    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="twitter:widgets:csp" content="on">


    <title>Cargo &amp; Carriers Underwriting Agency</title>

<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/summernote/css/summernote.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/summernote/css/summernote-bs3.css" type="text/css">

<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/admin-style.css" type="text/css">


<link href="<?php echo base_url()?>assets/frontpage/corporate/css/bootstrap.css" rel="stylesheet">

<!-- Plugin CSS -->
<?php /*?><link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.min.css" type="text/css"><?php */?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/dataTables/css/dataTables.bootstrap.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/dataTables/css/datatables.css" type="text/css">

<!-- Plugin CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/cropper/dist/cropper.min.css" type="text/css">
<?php /*?><link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/cropper/css/crop-avatar.css" type="text/css"><?php */?>


<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css" type="text/css">

<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bs-switch/css/bootstrap3/bootstrap-switch.min.css" type="text/css">

<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/silviomoreto-bootstrap-select/css/bootstrap-select.min.css" type="text/css">


<link href="<?php echo base_url()?>assets/frontpage/corporate/css/main.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" type="text/css">

<link href="<?php echo base_url()?>assets/frontpage/corporate/css/animate.min.css" rel="stylesheet">
<link rel="shortcut icon" href="<?php echo base_url()?>assets/frontpage/images/favicon.ico" type="image/x-icon" />
<link type="text/css" rel="stylesheet" media="all" href="<?php echo base_url()?>assets/frontpage/corporate/css/jquery.mmenu.css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/js/libs/head.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/corporate/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/corporate/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/us/variablesCorporate"></script>
<script>
	var base_url = '<?php echo base_url() ?>';
	var uri_1 = '<?php echo $this->uri->segment(1) ?>';
	var uri_2 = '<?php echo $this->uri->segment(2) ?>';
	var uri_3 = '<?php echo $this->uri->segment(3) ?>';
	var uri_4 = '<?php echo $this->uri->segment(4) ?>';
	var user_name = '<?php echo $this->session->userdata('name') ?>';
	var user_location = '<?php echo $this->session->userdata('location') ?>';
	var user_address = '<?php echo $this->session->userdata('address') ?>';
	var user_type = '<?php echo $this->session->userdata('type') ?>';
	var stripe_id = '<?php echo $this->session->userdata('stripe_id') ?>';
</script>


<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

<![endif]-->

</head>



 <body id="skrollr-body" class="root home landing">



	<?php
		//vars
		$colors = $this->common->colors();
		$usertype = $this->session->userdata('type');
		$opposite_type = 'agency';
		if($usertype != 'customer'){
			$opposite_type = 'customer';
		}
	?>



    <div id="browser-detection" class="alert alert-warning" style="display:none; position:relative;">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		Your browser is not supported by Transit Insurance. For a better experience, use one of our <a class='preventDefault' data-toggle='modal' data-target='#modal-browser-options'>supported browsers.</a>
	</div>
	<div id="cookies-detection" class="alert alert-danger" style="display:none; position:relative;">
		*__browser_cookies_disabled*
	</div>
    <nav id="my-menu">
       <ul>
          <li class="hidden"><a href="<?php echo base_url()?>">Home</a></li>
          <?php /*?><li><a href="<?php echo base_url()?>about">About Us</a></li>
          <li><a href="<?php echo base_url()?>contact">Contact</a></li><?php */?>
       </ul>
    </nav>

    <div id="main-container">
    <div class="mobile-spacer visible-xs hidden-sm hidden-md hidden-lg" data-0="margin-top:120px" data-1="margin-top:60px"></div>

      <!-- Static navbar -->
      <div class="navbar navbar-default navbar-static-top" role="navigation">
       	<div class="container-fluid">
            <div class="navbar-header">
              <button type="button" id="open-slider" class="navbar-toggle" data-0="top:0" data-1="top:-3px">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <div class="navbar-brand">
              	<a href="<?php echo base_url(); if($this->session->userdata('logged_admin') != '') { echo 'webmanager'; }?>"><img id="nav-logo" class="animated fadeIn animated-top" data-0="display: inline-block" data-50="display:none" src="<?php echo base_url()?>assets/frontpage/corporate/images/crisisflo-logo-medium.png" style="height: 90px;"/></a>
                <div class="mobile-menu visible-xs">

					<?php if($this->session->userdata('logged_admin') != '') { ?>
                    <a href="<?php echo base_url()."dashboard/logout"; ?>" id="btn-mini-login" class="btn btn-primary btn-rounded btn-outline">Log Out</a>
                    <?php } ?>



                </div>
              </div>
            </div>
            <div class="navbar-collapse collapse">


                <div class="mobile-menu hidden-xs text-right" style="padding-top: 35px;">
					<?php if($this->session->userdata('id') == '') { ?>
                    <a href="<?php echo base_url()."webmanager"; ?>" id="btn-mini-login" class="hidden btn btn-primary btn-xs btn-rounded btn-outline" >Log In</a>
                    <?php } ?>


                </div>
				<?php if($this->session->userdata('logged_admin') == '') { ?>
                <div class="hidden-xs"><a href="#<?php // echo base_url()."signin"; ?>" class="pull-right btn btn-primary btn-rounded btn-outline login_btn" style="margin-top: -6px;" data-type="Agency">Log In</a></div>
				<?php } ?>

              <ul class="nav navbar-nav pull-right main_lg_nav main_lg_nav_admin">

              	<?php if($this->session->userdata('logged_admin') != '') { ?>

                    <li class="dropdown my-dropdown my-dropdown-merge my-dropdown-bottom-right">

                        <a id="dLabel" data-target="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="<?php if($this->uri->segment(3) != '') { echo 'on-page'; }?>">
                            Settings
                            <span class="caret"></span>
                          </a>

                        <ul class="dropdown-menu" aria-labelledby="dLabel">

                            <li>
                                <a href="<?php echo base_url('webmanager/settings/changePassword'); ?>" class="preloadThis"><i class="fa fa-key fa-fw"></i> Admin Settings</a>
                            </li>
                                
                            <li>
                                <a href="<?php echo base_url('webmanager/settings/timezone'); ?>" class="preloadThis"><i class="fa fa-globe fa-fw"></i> Timezone</a>
                            </li>
                            <!-- <li>
                                <a href="<?php echo base_url().'webmanager/settings/users'; ?>" class="preloadThis"><i class="fa fa-users fa-fw"></i> Manage Users</a>
                            </li> -->
                                
                            <li>
                                <a href="<?php echo base_url().'webmanager/settings/activitylog'; ?>" class="preloadThis"><i class="fa fa-file-o fa-fw"></i> Activity Log</a>
                            </li>
                            
                        
                            <li><a href="<?php echo base_url('webmanager/dashboard/logout')?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
                        </ul>
                    </li>

                <?php } ?>




              </ul>


            </div><!--/.nav-collapse -->
   	 	</div> <!-- /container -->
      </div>


    <div class="container-fluid section-simple section-simple-admin main_container main_container_admin">

        <div class="row">

            <div class="col-lg-2 col-md-3 col-sm-4 hidden-xs sidebar_nav">
                <div class="main_nav" <?php if($this->uri->segment(2) == 'settingsx' || $this->uri->segment(2) == 'contentsx') { echo 'style="display:none"'; }?>>
                    <!-- <a href="<?php echo base_url().'webmanager/dashboard' ?>" class="list-group-item <?php if($this->uri->segment(2) == 'dashboard' && $this->uri->segment(3) == '') { echo 'active'; }?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a> -->


                	<?php if(in_array('Referrals', $access_rights)) { ?>
                    <a href="<?php echo base_url().'webmanager/dashboard/referrals' ?>" class="list-group-item <?php if($this->uri->segment(3) == 'referrals') { echo 'active'; }?>"><i class="fa fa-pencil-square fa-fw"></i> Referrals <?php if($this->common->active_referral_count() > 0) { ?><span class="label label-danger label-circle pull-right"><?php echo $this->common->active_referral_count(); ?></span></a><?php } ?>
                    <?php } ?>


                	<?php if(in_array('Transactions', $access_rights)) { ?>
                    <a href="<?php echo base_url().'webmanager/dashboard/transactions' ?>" class="list-group-item <?php if($this->uri->segment(3) == 'transactions') { echo 'active'; }?>"><i class="fa fa-exchange fa-fw"></i> Bound Transactions</a>
                    <?php } ?>

                	<?php if(in_array('Manage Brokers', $access_rights)) { ?>
                    <a href="<?php echo base_url().'webmanager/agency/brokerage'; ?>" class="list-group-item preloadThis <?php if($this->uri->segment(2) == 'agency' && $this->uri->segment(3) == 'brokerage') { echo 'active'; }?>"><i class="fa fa-child fa-fw"></i> Agents</a>
                    <?php } ?>

                	<?php if(in_array('Manage Brokers', $access_rights)) { ?>
                    <a href="<?php echo base_url().'webmanager/agency/manage'; ?>" class="list-group-item preloadThis <?php if($this->uri->segment(2) == 'agency' && $this->uri->segment(3) == 'manage') { echo 'active'; }?>"><i class="fa fa-user fa-fw"></i> Agent Users</a>
                    <?php } ?>

                	<?php /* if(in_array('Manage Customers', $access_rights)) { ?>
                    <a href="<?php echo base_url().'webmanager/customers/manage'; ?>" class="list-group-item preloadThis <?php if($this->uri->segment(2) == 'customers') { echo 'active'; }?>"><i class="fa fa-users fa-fw"></i> Customers</a>
                    <?php } */ ?>

                	<?php if(in_array('Rating', $access_rights)) { ?>
                    <a href="<?php echo base_url().'webmanager/cargo/manage'; ?>" class="list-group-item preloadThis <?php if($this->uri->segment(2) == 'cargo') { echo 'active'; }?>"><i class="fa fa-line-chart fa-fw"></i> Rating</a>
                    <?php } ?>

                	<?php if(in_array('Manage Port Codes', $access_rights)) { ?>
                    <a href="<?php echo base_url().'webmanager/insurance/portcodes'; ?>" class="list-group-item preloadThis <?php if($this->uri->segment(3) == 'portcodes') { echo 'active'; }?>"><i class="fa fa-ship fa-fw"></i> Port Codes</a>
                    <?php } ?>

										<a href="<?php echo base_url().'webmanager/surveyors'; ?>" class="list-group-item preloadThis <?php if($this->uri->segment(2) == 'surveyors') { echo 'active'; }?>"><i class="fa fa-users fa-fw"></i> Surveyor Contacts</a>


                	<?php if(in_array('Manage Certifcate', $access_rights)) { ?>
                    <a href="<?php echo base_url().'webmanager/insurance/certificate'; ?>" class="list-group-item preloadThis <?php if($this->uri->segment(3) == 'certificate') { echo 'active'; }?>"><i class="fa fa-certificate fa-fw"></i> Certificate Template</a>
                    <?php } ?>

                	<?php if(in_array('Policy Document', $access_rights)) { ?>
                    <a href="<?php echo base_url().'webmanager/policydoc/manage'; ?>" class="list-group-item preloadThis <?php if($this->uri->segment(2) == 'policydoc') { echo 'active'; }?>"><i class="fa fa-file-pdf-o fa-fw"></i> Policy Document</a>
                    <?php } ?>

                	<?php if(in_array('Reporting', $access_rights)) { ?>
                    <a href="<?php echo base_url().'webmanager/reporting/manage'; ?>" class="list-group-item preloadThis <?php if($this->uri->segment(2) == 'reporting') { echo 'active'; }?>"><i class="fa fa-file-excel-o fa-fw"></i> Reporting</a>
                    <?php } ?>

                	<?php if(in_array('Outgoing Emails', $access_rights)) { ?>
                    <a href="<?php echo base_url().'webmanager/emails/manage'; ?>" class="list-group-item preloadThis <?php if($this->uri->segment(2) == 'emails') { echo 'active'; }?>"><i class="fa fa-envelope fa-fw"></i> Outgoing Emails</a>
                    <?php } ?>

										<a href="<?php echo base_url().'webmanager/contents/legal'; ?>" class="list-group-item preloadThis <?php if($this->uri->segment(3) == 'legal') { echo 'active'; }?>"><i class="fa fa-file-text-o fa-fw"></i> Manage Legal</a>


                </div>


            </div><!--end sidebar_nav-->
