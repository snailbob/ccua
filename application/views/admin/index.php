<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="twitter:widgets:csp" content="on">
      <title>Cargo &amp; Carriers Underwriting Agency</title>
      <!-- Custom CSS -->
      <?php /*?>
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/creative.css" type="text/css">
      <?php */?>
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" type="text/css">
      <link href="<?php echo base_url()?>assets/frontpage/corporate/css/bootstrap.css" rel="stylesheet">
      <!-- Plugin CSS -->
      <?php /*?>
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.min.css" type="text/css">
      <?php */?>
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/dataTables/css/dataTables.bootstrap.css" type="text/css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/dataTables/css/datatables.css" type="text/css">
      <!-- Plugin CSS -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/cropper/css/cropper.min.css" type="text/css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/cropper/css/crop-avatar.css" type="text/css">
      <!-- Custom CSS -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css" type="text/css">
      <!-- Custom CSS -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bs-switch/css/bootstrap3/bootstrap-switch.min.css" type="text/css">
      <link href="<?php echo base_url()?>assets/frontpage/corporate/css/main.css" rel="stylesheet">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" type="text/css">
      <link href="<?php echo base_url()?>assets/frontpage/corporate/css/animate.min.css" rel="stylesheet">
      <link rel="shortcut icon" href="<?php echo base_url()?>assets/frontpage/images/favicon.ico" type="image/x-icon" />
      <link type="text/css" rel="stylesheet" media="all" href="<?php echo base_url()?>assets/frontpage/corporate/css/jquery.mmenu.css" />
      <script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/js/libs/head.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/corporate/js/jquery.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/us/variablesCorporate"></script>
      <script>
         var base_url = '<?php echo base_url() ?>';
         var uri_1 = '<?php echo $this->uri->segment(1) ?>';
         var uri_2 = '<?php echo $this->uri->segment(2) ?>';
         var uri_3 = '<?php echo $this->uri->segment(3) ?>';
         var uri_4 = '<?php echo $this->uri->segment(4) ?>';
         var user_name = '<?php echo $this->session->userdata('name') ?>';
         var user_location = '<?php echo $this->session->userdata('location') ?>';
         var user_address = '<?php echo $this->session->userdata('address') ?>';
         var user_type = '<?php echo $this->session->userdata('type') ?>';
         var stripe_id = '<?php echo $this->session->userdata('stripe_id') ?>';
      </script>
      <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->
   </head>
   <body id="skrollr-body" class="root home landing" style="background: #efefef">
      <div class="container">
         <div class="row">
            <div class="col-md-4 col-md-offset-4">
               <div class="login-banner text-center" style="padding: 50px 0;">
                  <!--<h1><i class="fa fa-gears"></i> Crisis FLO</h1>-->
                  <a href="<?php echo base_url(); ?>">
                  <img src="<?php echo base_url('assets/frontpage/corporate/images/crisisflo-logo-medium.png') ?>" height="150" >
                  </a>
               </div>
               <div class="panel panel-default">
                  <div class="panel-heading login-heading">
                     <div class="panel-title">
                        <h3 style="color: #666;"><strong>Admin Login</strong>
                        </h3>
                     </div>
                     <div class="clearfix"></div>
                  </div>
                  <div class="panel-body">
                     <?php if($error !='') {
                        echo '<div class="alert alert-danger alert-dismissable" style="margin-bottom: 15px;">';

                        echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';


                            echo $error;

                        echo '</div>';
                        } ?>
                     <div id='login_form'>
                        <form action='' method='post' name="admin-login" id="admin-login">
                           <fieldset>
                              <div class="form-group">
                                 <input class="form-control" placeholder="Username" type='text' name='user_name' id='user_name' autofocus/>
                              </div>
                              <div class="form-group">
                                 <input class="form-control" placeholder="Password" type='password' name='pass_word' id='pass_word'/>
                              </div>
                              <div class="form-group">
                                 <input  class="btn btn-primary btn-block" type='submit' value='Login' name="btn_admin_login" id="btn_admin_login" />
                              </div>
                           </fieldset>
                           <p class="text-right">
                              <a href="<?php echo base_url().'webmanager/dashboard/forgotpassword' ?>" class="text-muted">Forgot your password?</a>
                           </p>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/js/browserDetection.js"></script>
      <!-- Plugin JavaScript -->
      <script src="<?php echo base_url() ?>assets/js/jquery.easing.min.js"></script>
      <script src="<?php echo base_url() ?>assets/js/jquery.fittext.js"></script>
      <script src="<?php echo base_url() ?>assets/js/wow.min.js"></script>
      <!-- Plugin JavaScript -->
      <script src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
      <script src="<?php echo base_url() ?>assets/plugins/geocomplete/jquery.geocomplete.js"></script>
      <script type="text/javascript" src="https://www.google.com/jsapi"></script>
      <!-- Plugin JavaScript -->
      <script src="<?php echo base_url() ?>assets/plugins/dataTables/js/jquery.dataTables.js"></script>
      <script src="<?php echo base_url() ?>assets/plugins/dataTables/js/dataTables.bootstrap.js"></script>
      <script src="<?php echo base_url() ?>assets/plugins/dataTables/js/datatables-bs3.js"></script>
      <!-- Plugin JavaScript -->
      <script src="<?php echo base_url() ?>assets/plugins/validate/jquery.validate.min.js"></script>
      <script src="<?php echo base_url() ?>assets/plugins/validate/additional-methods.min.js"></script>
      <!-- Plugin JavaScript -->
      <script src="<?php echo base_url() ?>assets/plugins/bootbox/bootbox.js"></script>
      <!-- Plugin JavaScript -->
      <script src="<?php echo base_url() ?>assets/plugins/jquery.form/jquery.form.js"></script>
      <!-- Plugin JavaScript -->
      <script src="<?php echo base_url() ?>assets/plugins/datetimepicker/dependencies/moment.js"></script>
      <script src="<?php echo base_url() ?>assets/plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
      <!-- Plugin JavaScript -->
      <script src="<?php echo base_url() ?>assets/plugins/bs-switch/js/bootstrap-switch.min.js"></script>
      <!-- Plugin JavaScript -->
      <script src="<?php echo base_url() ?>assets/plugins/cropper/js/cropper.min.js"></script>
      <script src="<?php echo base_url() ?>assets/plugins/cropper/js/crop-avatar.js"></script>
      <!-- Custom Theme JavaScript -->
      <script src="<?php echo base_url() ?>assets/admin/js/admin-script.js"></script>
      <script src="<?php echo base_url() ?>assets/admin/js/admin-validation.js"></script>
   </body>
</html>
