<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    	
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="twitter:widgets:csp" content="on">
    

		<title>Transit Insurance</title>

<!-- Custom CSS -->
<?php /*?><link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/creative.css" type="text/css"><?php */?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" type="text/css">


<link href="<?php echo base_url()?>assets/frontpage/corporate/css/bootstrap.css" rel="stylesheet">

<!-- Plugin CSS -->
<?php /*?><link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.min.css" type="text/css"><?php */?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/dataTables/css/dataTables.bootstrap.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/dataTables/css/datatables.css" type="text/css">

<!-- Plugin CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/cropper/css/cropper.min.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/cropper/css/crop-avatar.css" type="text/css">


<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css" type="text/css">

<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bs-switch/css/bootstrap3/bootstrap-switch.min.css" type="text/css">




<link href="<?php echo base_url()?>assets/frontpage/corporate/css/main.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" type="text/css">
<link href="<?php echo base_url()?>assets/frontpage/corporate/css/animate.min.css" rel="stylesheet">
<link rel="shortcut icon" href="<?php echo base_url()?>assets/frontpage/images/favicon.ico" type="image/x-icon" />
<link type="text/css" rel="stylesheet" media="all" href="<?php echo base_url()?>assets/frontpage/corporate/css/jquery.mmenu.css" />

    
</head>


    
 <body id="skrollr-body" class="root home landing" style="background: #efefef">
 


    <div class="container">

        <div class="row">

            <div class="col-md-4 col-md-offset-4">

                <div class="login-banner text-center" style="padding: 50px 0;">

                    <!--<h1><i class="fa fa-gears"></i> Crisis FLO</h1>-->
                    <a href="<?php echo base_url(); ?>">
						<img src="<?php echo base_url('assets/frontpage/corporate/images/crisisflo-logo-medium.png') ?>" >
                    </a>

                </div>


                <div class="panel panel-default">

                    <div class="panel-heading login-heading">

                        <div class="panel-title">

                            <h3><strong>Forgot Password</strong>

                            </h3>

                        </div>



                        <div class="clearfix"></div>

                    </div>

                    <div class="panel-body">

                    

                    

                        <div id='login_form'>

                            <form action='' method='post' name="admin-login" id="admin-login">

                                

                              <br />

                                 <?php if($success!="") { ?>

                            <div class="alert alert-success alert-dismissable" style="margin-bottom: 15px;">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <strong>Success!</strong> <?php echo $success;   ?>

                            </div>

                            <?php } 	 if($error!="")  { ?>

                            <div class="alert alert-danger alert-dismissable" style="margin-bottom: 15px;">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                            <?php echo $error;?></div>

                            <?php } ?>	

                              

                                <fieldset>

                                    <div class="form-group">
                                        <input class="form-control" placeholder="Email" type='text' name='user_name' id='user_name' />
                                    </div>

                                    <div class="form-group">
                                        <button class="btn btn-primary btn-block" type='submit' name="btn_forget" id="btn_forget">Submit</button>
                                    </div>

                                   


                                </fieldset>


                                <p class="">

                                    <a href="<?php echo base_url().'webmanager' ?>">Back to Login</a>

                                </p>

                            </form>

                        </div>

                        

                    </div>

                </div>

            </div>

        </div>

    </div>



</body>

</html>
