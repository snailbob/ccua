<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">
   <div class="row">
      <div class="col-lg-12">
         <div class="page-title">
            <h3>Manage <?php echo $title; ?></h3>
            <ol class="breadcrumb">
               <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>
               <li class="active">Manage <?php echo $title; ?></li>
            </ol>
         </div>
      </div>
      <!-- /.col-lg-12 -->
   </div>
   <!-- /.row -->
   <!-- end PAGE TITLE AREA -->
   <!-- Form AREA -->
   <div class="row">
      <div class="col-lg-12">
         <?php if($this->session->flashdata('success')!=""){ ?>
         <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>
         </div>
         <?php } if($this->session->flashdata('error')!=""){ ?>
         <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?>
         </div>
         <?php } ?>
      </div>
      <div class="col-lg-12">
         <div class="panel panel-default">
            <div class="panel-heading">
               <a href="#addAgencyModal" class="btn btn-primary pull-right" data-toggle="modal" data-controls-modal="#addAgencyModal">Add <?php echo $singular_title; ?></a>
               <div  class="panel-title">
                  <h4>Manage <?php echo $title; ?></h4>
               </div>
            </div>
            <?php if(count($users) > 0) { ?>
            <div class="table-responsivex">
               <table class="table table-striped table-hover table-datatable">
                  <thead>
                     <tr>
                        <th width="20%">Name</th>
                        <th width="60%">Brokerage address</th>
                        <!-- <th>Agent Code</th> -->
                        <?php /*?>
                        <th>White-labelling</th>
                        <th>Primary Contact</th>
                        <?php */?>
                        <th> </th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                        foreach($users as $u=>$user){
                        ?>
                     <tr>
                        <td><?php echo $user['company_name'] ?></td>
                        <td><?php echo $user['address'] ?></td>
                        <!-- <td><?php echo $this->common->agentcode($user['id']) ?></td> -->
                        <?php /*?>
                        <td><?php echo ($user['enable_whitelabel'] == 'Y') ? 'Enabled' : 'Disabled' ?></td>
                        <td>
                           <?php echo $user['first'].' '.$user['first'] ?>
                           <br />
                           <?php echo $user['email'] ?>
                           <br />
                           <?php echo $user['phone'] ?>
                        </td>
                        <?php */?>
                        <td>
                           <!-- Single button -->
                           <div class="btn-group pull-right">
                              <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                              Action <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu" role="menu">
                                 <li><a href="javascript:;" class="brokerage-update" data-id="<?php echo $user['id']?>" data-info='<?php echo json_encode($user) ?>'>Update</a></li>
                                 <li><a href="javascript:;" class="view_details_btn" data-id="<?php echo $user['id']?>" data-info='<?php echo json_encode($user) ?>'>View Details</a></li>
                                 <?php /*?>
                                 <li><a href="javascript:;" class="brokerage-whitelabel" data-id="<?php echo $user['id']?>" data-white="<?php echo $user['enable_whitelabel']?>" data><?php echo ($user['enable_whitelabel'] == 'Y') ? 'Disable' : 'Enable' ?> White-labelling</a></li>
                                 <?php */?>
                                 <li><a href="javascript:;" class="delete_btn_confirm" data-id="<?php echo $user['id']?>" data-table="brokerage">Delete</a></li>
                              </ul>
                           </div>
                        </td>
                     </tr>
                     <?php
                        }
                        ?>
                  </tbody>
               </table>
               <p class="lead" style="padding-bottom: 15px;">&nbsp;</p>
            </div>
            <!--end of table responsive-->
            <?php } else{
               echo '<div class="panel-body"><p class="text-center text-muted">No '.$title.'</p></div>';
               }?>
            <div class="panel-body hidden">
            </div>
         </div>
      </div>
   </div>
   <!--.row-->
</div>
<!-- Modal -->
<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"><?php echo $singular_title; ?> Information</h4>
         </div>
         <div class="modal-body">
            <div class="the-info">
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!-- Portfolio Modals -->
<div class="portfolio-modal modal fade" id="addAgencyModal" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-content">
      <div class="close-modal" data-dismiss="modal">
         <div class="lr">
            <div class="rl">
            </div>
         </div>
      </div>
      <div class="container">
         <div class="row">
            <div class="col-md-6 col-md-offset-3">
               <div class="modal-body">
                  <h1><?php echo $singular_title; ?></h1>
                  <hr />
                  <form id="brokerage_form" novalidate="novalidate" class="text-left">
                     <input type="hidden" name="id" />
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="form-group">
                              <label>Company Name</label>
                              <input type="text" class="form-control" name="company_name" required="required"/>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="form-group">
                              <label>Address</label>
                              <input type="text" class="form-control geolocation" name="address" required="required"/>
                              <input type="hidden" name="lat" />
                              <input type="hidden" name="lng" />
                           </div>
                        </div>
                        <div class="col-sm-4 hidden">
                           <div class="form-group">
                              <label>Company Domain</label>
                              <input type="text" class="form-control" name="domain"/>
                           </div>
                        </div>
                     </div>
                     <div class="row row-prime-contact hidden">
                        <div class="col-sm-12">
                           <h6 style="font-weight: bold; font-size: 16px">Primary Contact</h6>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label>First Name</label>
                              <input type="text" class="form-control" name="first"/>
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label>Last Name</label>
                              <input type="text" class="form-control" name="last"/>
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label>Email</label>
                              <input type="email" class="form-control" name="email"/>
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label>Phone</label>
                              <input type="text" class="form-control" name="phone"/>
                           </div>
                        </div>
                        <div class="col-sm-12">
                           <div class="form-group">
                              <label>Also first broker?</label><br />
                              <label class="radio-inline">
                              <input type="radio" name="first_broker" value="Y" required="required"> Yes
                              </label>
                              <label class="radio-inline">
                              <input type="radio" name="first_broker" value="N" checked required="required"> No
                              </label>
                           </div>
                        </div>
                     </div>
                     
                     <div class="form-group">
                        <button class="btn btn-primary" type="submit">Submit</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!--portfolio-modal-->
<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         </div>
         <div class="modal-body">
            Type 'DELETE' to confirm
            <form id="delete_form">
               <div class="form-group">
                  <input type="text" name="delete" class="form-control" />
                  <input type="hidden" name="delete2" id="delete2" class="form-control" value="DELETE"/>
                  <input type="hidden" name="id" class="form-control" value="DELETE"/>
               </div>
               <div class="form-group text-right">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Confirm</button>
            </form>
            </div>
         </div>
      </div>
   </div>
</div>
