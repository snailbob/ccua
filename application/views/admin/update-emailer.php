<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">
   <div class="row">
      <div class="col-lg-12">
         <div class="page-title">
            <h3>Update Outgoing Emails </h3>
            <ol class="breadcrumb">
               <li><i class="fa fa-dashboard"></i>
                  <a href="<?php echo base_url().'webmanager'?>">Dashboard</a>
               </li>
               <li class="active">Update Outgoing Emails</li>
            </ol>
         </div>
      </div>
      <!-- /.col-lg-12 -->
   </div>
   <!-- /.row -->
   <!-- end PAGE TITLE AREA -->
   <!-- Form AREA -->
   <div class="row">
      <div class="col-lg-12">
         <?php
            if($this->session->flashdata('success')!="")

            {

            ?>
         <div class="alert alert-success alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Success!</strong><?php echo $this->session->flashdata('success'); ?>
         </div>
         <?php
            }

            if($this->session->flashdata('error')!="")

            {

            ?>
         <div class="text-red">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong>Error :</strong><?php echo $this->session->flashdata('error'); ?>
         </div>
         <?php
            }

            ?>
         <div class="panel panel-default">
            <div class="panel-heading">
               <div class="panel-title">
                  <h4>Outgoing Emails</h4>
               </div>
               <div class="clearfix"></div>
            </div>
            <div class="panel-body">
               <div class="form-group">
                  <div class="col-sm-10 col-sm-offset-1">
                     <p>Emailer Name: </p>
                     <div>
                        <span class="text-muted price-text" data-id="<?php echo $mess_info[0]['id']; ?>" title="click to edit">
                        <?php echo $mess_info[0]['name']; ?>
                        </span>
                        <form class="text_form hidden">
                           <div class="form-group">
                              <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $mess_info[0]['name']; ?>" />
                              <input type="hidden" name="id" value="<?php echo $mess_info[0]['id']; ?>" />
                              <input type="hidden" name="field_name" value="name" />
                              <input type="hidden" name="db_name" value="outgoing_emails" />
                           </div>
                           <div class="form-group">
                              <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save fa-fw"></i></button>
                              <a href="#" class="btn btn-default btn-sm"><i class="fa fa-times fa-fw"></i></a>
                           </div>
                        </form>
                     </div>
                     <!-- <p class="lead">File Name: <br />
                        <span class="text-muted"><?php echo $mess_info[0]['name']; ?></span></p> -->
                  </div>
                  <div class="col-sm-10 col-sm-offset-1" style="padding-top: 25px;padding-bottom: 25px;">
                     <p>Subject: </p>
                     <div>
                        <span class="text-muted price-text" data-id="<?php echo $mess_info[0]['id']; ?>" title="click to edit">
                        <?php echo $mess_info[0]['subject']; ?>
                        </span>
                        <form class="text_form hidden">
                           <div class="form-group">
                              <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $mess_info[0]['subject']; ?>" />
                              <input type="hidden" name="id" value="<?php echo $mess_info[0]['id']; ?>" />
                              <input type="hidden" name="field_name" value="subject" />
                              <input type="hidden" name="db_name" value="outgoing_emails" />
                           </div>
                           <div class="form-group">
                              <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save fa-fw"></i></button>
                              <a href="#" class="btn btn-default btn-sm"><i class="fa fa-times fa-fw"></i></a>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
               <?php
                  $healthy = array(
                      "[case_id]",
                      "[click_here]",
                      "[clicking_here]",
                      "[password]",
                      "[scenario_name]",
                      "[incident_name]",
                      "[user_email]",
                      "[user_name]",
                      "[org_name]"
                  );

                  $yummy = array(
                      '<span class="text-info">*CASE_ID*</span>',
                      '<span class="text-info">click here</span>',
                      '<span class="text-info">clicking here</span>',
                      '<span class="text-info">*TEMP_PW*</span>',
                      '<span class="text-info">*SCENARIO_NAME*</span>',
                      '<span class="text-info">*INCIDENT_NAME*</span>',
                      '<span class="text-info">*USER_EMAIL*</span>',
                      '<span class="text-info">*USER_FULL_NAME*</span>',
                      '<span class="text-info">*ORG_NAME*</span>'
                  );

                  $newphrase = str_replace($healthy, $yummy, $mess_info[0]['content']);
                  ?>
               <form action='' name="frm-add-cc" id="frm-add-cc" method='post' class="form-horizontal" role="form" validate>
                  <div class="col-sm-10 col-sm-offset-1" style="padding: 30px;">
                     <img src="<?php echo base_url().'assets/frontpage/corporate/images/crisisflo-logo-medium.png' ?>" alt="logo" />
                  </div>
                  <div class="col-sm-10 col-sm-offset-1 thumbnail" style="padding: 30px; background-color: #f1f1f1;">
                     <p><span class="content_text" title="click to edit"><?php echo $newphrase; ?></span>
                     </p>
                     <textarea rows="3" class="form-control edit_content" id="content" name="content" placeholder="" value="" style="display: none;"><?php echo $mess_info[0]['content']; ?></textarea>
                     <button type="submit" class="submit_content btn btn-primary btn-xs pull-right" style="display: none; margin-top: 5px;" name="add_reseller" id="add_reseller">Submit</button>
                     <?php
                        if ($this->uri->segment(4) == '21'){
                          echo '<span class="text-info">*MESSAGE HERE*</span><br><br>';
                          echo '<span class="text-info">*LIST OF CRISIS DOCUMENT HERE*</span><br><br>';
                          echo 'Please <span class="text-info">click here</span> to log into your user panel.';
                        }
                        if ($this->uri->segment(4) == '22'){
                          echo 'Message from org: <span class="text-info">*MESSAGE HERE*</span><br><br>';
                        }
                        if ($this->uri->segment(4) == '29'){
                          echo '<span class="text-info">*STEP CATEGORY STATUS TABLE HERE*</span><br><br>';
                        }
                        ?>
                     <br>
               </form>
                <?php if($mess_info[0]['file_name'] != 'send-certifiicate-to-user') { ?>
                <div>
                  <button type="button" class="btn btn-primary btn-block price-text" data-id="<?php echo $mess_info[0]['id']; ?>" title="click to edit">
                    <?php echo (empty($mess_info[0]['button_text'])) ? '*Button Empty' : $mess_info[0]['button_text']; ?>
                  </button>
                  <form class="text_form hidden">
                    <div class="form-group">
                      <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $mess_info[0]['button_text']; ?>" />
                      <input type="hidden" name="id" value="<?php echo $mess_info[0]['id']; ?>" />
                      <input type="hidden" name="field_name" value="button_text" />
                      <input type="hidden" name="db_name" value="outgoing_emails" />
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary btn-sm">
                        <i class="fa fa-save fa-fw"></i>
                      </button>
                      <a href="#" class="btn btn-default btn-sm">
                        <i class="fa fa-times fa-fw"></i>
                      </a>
                    </div>
                  </form>
                  <!--/button-->
                </div>
                <?php } ?>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--.row-->
</div>
