<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">
   <div class="row">
      <div class="col-lg-12">
         <div class="page-title">
            <h3>Admin Email &amp; Password</h3>
            <ol class="breadcrumb">
               <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>
               <li class="active">Admin Email &amp; Password</li>
            </ol>
         </div>
      </div>
      <!-- /.col-lg-12 -->
   </div>
   <!-- /.row -->
   <!-- end PAGE TITLE AREA -->
   <!-- Form AREA -->
   <div class="row">
      <div class="col-lg-12">
         <?php if($this->session->flashdata('success')!=""){ ?>
         <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>
         </div>
         <?php } if($this->session->flashdata('error')!=""){ ?>
         <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?>
         </div>
         <?php } ?>
      </div>
      <div class="col-lg-12">
         <div class="panel panel-default">
            <div class="panel-heading">
               <div class="panel-title">
                  <h4>Admin Settings</h4>
               </div>
               <div class="clearfix"></div>
            </div>
            <div class="panel-body">
               <form action='' name="admin_change_password" id="admin_change_password" method='post' class="form-horizontal" role="form" novalidate>
                  <div class="form-group hidden">
                     <label for="lastname" class="col-md-2 control-label">Email Sender</label>
                     <div class="col-md-10">
                        <input type="email" class="form-control" id="email" name="email" value="<?php echo $admin_details[0]['email']?>" required="required">
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="recovery" class="col-md-2 control-label">Recovery Email</label>
                     <div class="col-md-10">
                        <input type="email" class="form-control" id="recovery_email" name="recovery_email" value="<?php echo $admin_details[0]['recovery_email']?>" required="required">
                     </div>
                  </div>

                  <?php
                  $admin_emails = (@unserialize($admin_details[0]['admin_emails'])) ? unserialize($admin_details[0]['admin_emails']) : array('');
                  foreach($admin_emails as $r=>$value){ ?>

                  <div class="form-group">
                     <label for="recovery" class="col-md-2 control-label">
                        Admin email 

                        <?php if($r == 0){ ?>
                            <a href="#" class="new_admin_email"><i class="fa fa-plus-circle" aria-hidden="true"></i></a> 
                            
                        <?php } ?>
                        <a href="#" class="delete_admin_email <?php echo ($r == 0) ? 'hidden' : '' ?>"><i class="fa fa-trash-o text-red" aria-hidden="true"></i></a>


                    </label>
                     <div class="col-md-10">
                        <input type="email" class="form-control" name="admin_email[]" value="<?php echo $value ?>">
                     </div>
                  </div>
                  <?php } ?>
                  
                  <div class="email-lists"></div>

                  <div class="form-group">
                     <label for="firstname" class="col-md-2 control-label">Password</label>
                     <div class="col-md-10">
                        <input type="password" class="form-control" id="password" name="password" required="required">
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="firstname" class="col-md-2 control-label">Confirm Password</label>
                     <div class="col-md-10">
                        <input type="password" class="form-control" id="password2" name="password2" required="required">
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-2 control-label"></label>
                     <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary" name="change_admin_pw" id="change_admin_pw">Submit</button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   <!--.row-->
</div>
