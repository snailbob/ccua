<?php
	if($this->session->userdata('name') == ''){
		redirect();
	}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Transit Insurance | Australia’s premier online dancing community hub | find and book dance classes</title>
 
    <link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png')?>" type="image/x-icon"/>
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" type="text/css">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url(); ?>assets/fonts/open-sans.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/fonts/merriweather.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/dataTables/css/dataTables.bootstrap.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/dataTables/css/datatables.css" type="text/css">
    
    <!-- Plugin CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/cropper/css/cropper.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/cropper/css/crop-avatar.css" type="text/css">
    
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css" type="text/css">
   
    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bs-switch/css/bootstrap3/bootstrap-switch.min.css" type="text/css">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/creative.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" type="text/css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<script>
		var base_url = '<?php echo base_url() ?>';
		var uri_1 = '<?php echo $this->uri->segment(1) ?>';
		var uri_2 = '<?php echo $this->uri->segment(2) ?>';
		var uri_3 = '<?php echo $this->uri->segment(3) ?>';
		var uri_4 = '<?php echo $this->uri->segment(4) ?>';
		var user_name = '<?php echo $this->session->userdata('name') ?>';
		var user_location = '<?php echo $this->session->userdata('location') ?>';
		var user_address = '<?php echo $this->session->userdata('address') ?>';
		var user_type = '<?php echo $this->session->userdata('type') ?>';
	</script>
</head>

<body id="page-top">
	
	<?php
		//vars
		$colors = $this->common->colors();
		$usertype = $this->session->userdata('type');
		$opposite_type = 'studio';
		if($usertype != 'instructor'){
			$opposite_type = 'instructor';
		}
	?>
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top <?php echo 'bg-choco'; //$colors[array_rand($colors)]; ?>">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="<?php echo base_url().'dashboard'; ?>">
                <?php /*?><img src="<?php echo base_url().'assets/img/logo.png'?>" alt="Dance Pass" title="" /><?php */?>Transit Insurance
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right main-nav-list">

                    <li class="<?php if($this->uri->segment(1) == 'dashboard') { echo 'active'; }?>">
                    	<a href="<?php echo base_url().'dashboard' ?>"><i class="fa fa-user"></i> <?php echo $this->session->userdata('name') ?></a>
                    </li>
                    
                    <li class="<?php if($this->uri->segment(2) == 'profile' && $this->uri->segment(3) == '') { echo 'active'; }?>"><a href="<?php echo base_url().'settings/profile' ?>"><?php echo ucfirst($usertype); ?> Profile</a></li>

                    <li class="<?php if($this->uri->segment(1) == 'instructors') { echo 'active'; }?> <?php if($this->session->userdata('type') != 'studio') { echo 'hidden';} ?>">
                        <a href="<?php echo base_url().'instructors' ?>">
                        	<?php if($this->session->userdata('type') == 'studio') { echo '<span class="label label-default label-circle">1</span>';} ?> Instructors
                        </a>
                    </li>
                    
                    
                    <li class="<?php if($this->uri->segment(1) == 'rating') { echo 'active'; }?> <?php if($this->session->userdata('type') != 'agency') { echo 'hidden';} ?>">
                        <a href="<?php echo base_url().'rating' ?>">
                            Rating
                        </a>
                    </li>
                
                    <li class="dropdown my-dropdown my-dropdown-merge <?php if($this->uri->segment(3) != '') { echo 'active'; }?>">
                    
                        <a id="dLabel" data-target="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            Settings
                            <span class="caret"></span>
                          </a>

                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <?php if($this->session->userdata('multiple_account') == 'yes'){ ?>
                            <li><a href="#" data-toggle="modal" data-target="#switchModal"><i class="fa fa-exchange fa-fw"></i> Switch to <?php echo ucfirst($opposite_type); ?></a></li>
                            <?php } ?>
                            <li class="hidden"><a href="<?php echo base_url().'settings/profile/connect' ?>"><i class="fa fa-facebook fa-fw"></i> Connect</a></li>
                            <?php if($this->session->userdata('type') == 'customer'){ ?>
                            <li><a href="<?php echo base_url().'settings/profile/payment' ?>"><i class="fa fa-credit-card fa-fw"></i> Payment Method</a></li>
                            <li class="<?php if($this->session->userdata('stripe_id') == '') { echo 'hidden'; }?>"><a href="<?php echo base_url().'buy' ?>"><i class="fa fa-cart-plus fa-fw"></i> Buy Insurance</a></li>
                            <?php } ?>
                            <li><a href="<?php echo base_url().'settings/profile/pw' ?>"><i class="fa fa-key fa-fw"></i> Password</a></li>
                            <li><a href="<?php echo base_url().'dashboard/logout' ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
                        </ul>                    
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
