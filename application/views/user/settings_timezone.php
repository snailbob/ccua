

<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">

                    <div class="panel-title">
                        <h4><?php echo $title; ?></h4>
                    </div>

                </div>

                <div class="panel-body">
                    <form id="tz_form">
                        <input type="hidden" name="id" value="<?php echo $customer[0]['id'] ?>">
                        <div class="form-group">
                            <label>Timezone</label>
                            <select name="timezone" id="" class="form-control" required>
                                <option value="">Select</option>
                                <?php
                                    foreach($time_zone as $r=>$value){
                                        echo '<option value="'.$value['id'].'"';
                                        echo ($value['id'] == $customer_tz) ? ' selected="selected"' : '';
                                        echo '>'.$value['name'].'</option>';
                                    }
                                ?>


                            </select>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>


                                

        </div>
    </div>
</div>