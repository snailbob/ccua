
<section class="section section-simple">
	<div class="container">
    	<div class="row">
        
        	<div class="col-md-12">
            	<?php if ($this->session->flashdata('ok') != ''){ ?>
                <div class="alert alert-success fade in">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>

                	<?php echo $this->session->flashdata('ok'); ?>
                </div>
                <?php } ?>
            
            	<?php if ($this->session->flashdata('error') != ''){ ?>
                <div class="alert alert-danger fade in">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>

                	<?php echo $this->session->flashdata('error'); ?>
                </div>
                <?php } ?>
            
            
            	<p class="text-right"><a href="#" class="btn btn-primary openmodal_add_btn" data-target="#instructorModal"><i class="fa fa-plus"></i> New Instructor</a></p>
            	<div class="panel panel-default <?php if(count($instructors) == 0) { echo ''; } ?>bg-shoes">
                
                    <?php
						if(count($instructors) > 0) {
					?>
                	<div class="panel-body">
                        <h2 class="my-panel-title"><?php echo $title; ?></h2>
                    </div>
                    <div class="bg-white"><!--bg-white-->
                    <table class="table table-hover mydataTb">
                    	<thead>
                        	<tr>
                            	<th width="33%">Name</th>
                            	<th width="33%">Email</th>
                            	<th width="34%">Status</th>
                            	<th></th>
                            </tr>
                        </thead>
                    	<tbody>
                        	<?php foreach($instructors as $r=>$value){ ?>
                        	<tr>
                            	<td><?php echo $this->common->instructor_name($value['id']) ?></td>
                            	<td><?php echo $value['email'] ?></td>
                            	<td><?php echo $this->common->instructor_status($value['status']) ?></td>
                            	<td>
                                
                                    <div class="dropdown pull-right">
                                      <button class="btn btn-default btn-xs" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        Action
                                        <span class="caret"></span>
                                      </button>
                                    
                                      <ul class="dropdown-menu" aria-labelledby="dLabel">
                                      	<?php /*?><?php
											//get attributes
											$data_attr = '';
											$data_attr_details = '';
											foreach($instructors_fields as $cf) {
												$data_val = $value[$cf];
												//$data_val_details = $value[$cf];
												
												//unserialize array to create object
												if($cf == 'dance_genres'){
													$data_val = json_encode(unserialize($data_val));
												}
												//status
												if($cf == 'status'){
													$data_val = $this->common->instructor_status($data_val);
												}
												
												$data_attr .= " data-".$cf."='".$data_val."'";
												//$data_attr_details .= " data-".$cf."='".$data_val_details."'";
											}
										?>
                                        <li><a href="#" class="view_inst_details_btn" <?php echo $data_attr; ?>>View Details</a></li>
                                        <li><a href="#" class="update_inst_btn" <?php echo $data_attr; ?>>Update</a></li><?php */?>
                                        
                                        <?php if($value['status'] == 'N') {?>
                                        <li><a href="#" class="resend_inst_btn" data-id="<?php echo $value['id'] ?>">Re-send invitation</a></li>
                                        <?php } ?>
                                        <li><a href="#" class="delete_btn" data-id="<?php echo $value['id'] ?>" data-table="instructors">Delete</a></li>
                                      </ul>
                                    </div>                                
                                
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    </div><!--bg-white-->
                    <?php } else { echo '<div class="panel-body"><h2 class="my-panel-title">'.$title.'</h2></div>';} ?>
                </div>
            </div>
        </div>
    </div>
</section>



    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="instructorModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">
                        
                        
                            <h2>Instructor</h2>
                            <hr class="star-primary">
                            
                            <form class="" id="instructor_form">
                            	<div class="row">
                                	<div class="col-sm-6">
                                        <div class="form-group">
                                            <label>First Name</label>
                                            <input type="text" class="form-control input-rounded" name="first" placeholder="First Name"/>
                                            <input type="hidden" name="id" value="" />
                                        </div>
                                    </div>
                                	<div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Last Name</label>
                                            <input type="text" class="form-control input-rounded" name="last" placeholder="Last Name"/>
                                            <input type="hidden" name="id" value="" />
                                        </div>
                                    </div>
                                </div>
                            
                            	<div class="form-group">
                                	<label>Email</label>
                                	<input type="text" class="form-control input-rounded" name="email" placeholder="Email"/>
                                    <input type="hidden" name="id" value="" />
                                </div>
                            
                                
                                                
                            	<div class="form-group text-right">
                                	<button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            
                            
                            </form>

                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->



    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="instructorDetailsModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">
                        
                            <h2>Class Details</h2>
                            <hr class="star-primary">
                            
                            <div class="well"></div>
	
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->
