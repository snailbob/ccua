<!-- progressbar -->
<!-- <div class="row">
    <ul id="progressbar" class="progressbar-clickable">
        <li class="active" data-target="#home">Transit</li>
        <li data-target="#quote">Quote</li>
        <li data-target="#profile">Consignor</li>
        <li data-target="#messages">Consignee</li>
        <li data-target="#decision" class="">Decision</li>
    </ul>   
</div> -->

<div>
  <!-- Nav tabs -->
  <ul class="nav nav-tabs progressbar-control hidden" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Transit</a></li>
    <li role="presentation"><a href="#quote" aria-controls="home" role="tab" data-toggle="tab">Quote</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Consignor</a></li>
    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Consignee</a></li>
    <li role="presentation"><a href="#decision" aria-controls="decision" role="tab" data-toggle="tab">Decision</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane fade in active" id="home">

<b>&nbsp;</b>
<?php
$insurance = $this->common->the_cert_data($id);

// var_dump($details);

if(count($buy) > 0){


    $buy_inputs = (isset($details['buy_inputs'])) ? $details['buy_inputs'] : array();
    $insurance_details = array();
    foreach($buy_inputs as $bi=>$bival){
      $insurance_details[$bival['name']] = $bival['value'];
    }

    $transitto = (isset($insurance_details['transitto'])) ? $this->common->db_field_id('country_t', 'short_name', $insurance_details['transitto'], 'country_id') : 'not specified';
    $transitfrom = (isset($insurance_details['transitfrom'])) ? $this->common->db_field_id('country_t', 'short_name', $insurance_details['transitfrom'], 'country_id') : 'not specified';
    

?>
    <!-- <div class="well well-sm">
        <div class="row gutter-md">
            <div class="col-sm-12">
            <ul class="list-group">
                <li class="list-group-item"><strong>Shipment Date: </strong><br><?php echo $insurance['shipmentdate'] ?></li>
                <li class="list-group-item"><strong>Insured Value: </strong><br><?php echo $insurance['insurance'] ?> <?php echo ($insurance['conv_insurance']) ? '(Converted base currency: '.$insurance['conv_insurance'].')' : '' ?>
                
                </li>
                <li class="list-group-item"><strong>Transit From: </strong><br><?php echo $transitfrom ?></li>
                <li class="list-group-item"><strong>Port of Loading: </strong><br><?php echo $insurance['portloading'] ?></li>
                <li class="list-group-item"><strong>Transit To: </strong><br><?php echo $transitto ?></li>
                <li class="list-group-item"><strong>Port of Discharge: </strong><br><?php echo $insurance['portdischarge'] ?></li>
                <li class="list-group-item"><strong>Cargo Cat: </strong><br><?php echo $insurance['cargocat'] ?></li>
                <li class="list-group-item"><strong>Description: </strong><br><?php echo $insurance['description'] ?></li>
                <li class="list-group-item"><strong>Deductible: </strong><br><?php echo $insurance['deductible'] ?></li>
            </ul>
            </div>
        
        </div>
    </div> -->

    <h3>Underwriting Details </h3>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="">Transit From *</label>
                <input type="text" readonly class="form-control" value="<?php echo strip_tags($transitfrom) ?>">
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="">Transit To *</label>
                <input type="text" readonly class="form-control" value="<?php echo strip_tags($transitto) ?>">
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="">Insured Value *</label>
                <?php $inv = strip_tags($insurance['insurance']); $inv = preg_replace("/[^0-9,.]/", "", $inv ); ?>
                <input type="text" readonly class="form-control" value="<?php echo $inv ?>">
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="">Currency * </label>
                <input type="text" readonly class="form-control" value="<?php echo strip_tags($insurance['currency']) ?>">
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label for="">Cargo * </label>
                <input type="text" readonly class="form-control" value="<?php echo strip_tags($insurance['cargocat']) ?>">
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label for="">Brief Description of Goods *</label>
                <input type="text" readonly class="form-control" value="<?php echo strip_tags($insurance['description']) ?>">
            </div>
        </div>

        <div class="col-sm-12">
            <div class="form-group">
                <label for="">Transportation *</label>

                <div class="row gutter-sm">
                    <div class="col-xs-6 col-md-3">
                        <a href="#" class="thumbnail text-center" style="padding: 25px 0">
                            <?php ?>
                            <span><?php echo strip_tags($insurance['transmethod']) ?> </span>
                        </a>
                    </div>
                    <!--col-xs-6-->
                </div>
            </div>
        </div>



        <div class="col-sm-12">
            <h3>Agent Notes</h3>

            <div class="form-group">
                <label for="">Notes from Agent/Broker </label>
                <input type="text" readonly class="form-control" value="<?php echo $notes_to_underwriter ?>">
            </div>
        </div>

        <div class="col-sm-12">
            <h3>Decision</h3>
            <form class="admin_review_form" action="<?php echo base_url().'webmanager/dashboard/accept_request/' ?>" method="post">

                <div class="form-group">
                    <label>Deductible </label>
                    <input type="text" class="form-control input-currency" name="deductible_dd" value="<?php echo strip_tags($insurance['deductible']) ?>" />

                    <input type="hidden" class="form-control" name="id" value="<?php echo $id ?>" />
                </div>
                <div class="form-group">
                    <label>Adjust Rated Premium </label>
                    <input type="text" class="form-control input-currency" name="premium" value="<?php echo number_format($details['premium'], 2, '.', ','); ?>" />
                    <input type="hidden" class="form-control" name="id" value="<?php echo $id ?>" />
                </div>
            
                <div class="form-group">
                    <label>Comments</label>
                    <textarea name="comment" class="form-control"><?php echo $comment; ?></textarea>
                </div>
            
                <div class="form-group">
                    <a href="#" class="btn btn-danger pull-left reject_btn">Reject</a>
                    <button type="submit" class="btn btn-primary pull-right admin_review_submit_btn">Accept</button>
                </div>
            </form>
        </div>
    	



    </div>


<?php } else { echo '<p class="text-muted">No transit details found.</p>'; } ?>


    </div>
    <div role="tabpanel" class="tab-pane fade" id="quote">
        <b>&nbsp;</b>
        <div class="well well-sm text-center">
            <h1 style="font-size: 50px"><?php echo '<small>'.$insurance['currency'].'</small>'.number_format($details['premium'], 2, '.', ',') ?></h1>
        </div>
    </div>
    <div role="tabpanel" class="tab-pane fade" id="profile">



<b>&nbsp;</b>
<?php if(count($single) > 0){ ?>
<div class="well well-sm">
	<div class="row gutter-md">
    	<?php /*?><div class="col-sm-4">
        	<div class="row gutter-sm">
                <div class="col-xs-8 col-xs-offset-2">
                    <img src="<?php echo $avatar ?>" class="img-circle img-responsive img-thumbnail" alt="" title="">
                </div>
            </div>
        </div><?php */?>
        
        <div class="col-sm-12">
          <!-- List group -->
          <ul class="list-group">
            <li class="list-group-item"><strong>Name: </strong><br><?php echo $single['first_name'].' '.$single['last_name'] ?></li>
            <li class="list-group-item"><strong>Business Name: </strong><br><?php echo $single['business_name'] ?></li>
            <li class="list-group-item"><strong>Address: </strong><br><?php echo $single['location'] ?></li>
            <li class="list-group-item"><strong>Email : </strong><br><?php echo $single['email'] ?></li>
            <li class="list-group-item"><strong>Mobile: </strong><br><?php echo $single['calling_code'].$single['calling_digits'] ?></li>
          </ul>
        </div>
    
    </div>
</div>
<?php } else { echo '<p class="text-muted">No consignor details found.</p>'; } ?>

    </div>
    <div role="tabpanel" class="tab-pane fade" id="messages">


<b>&nbsp;</b>
<?php if(count($consignee) > 0){ ?>
<div class="well well-sm">
	<div class="row gutter-md">
        <div class="col-sm-12">
          <!-- List group -->
          <ul class="list-group">
            <li class="list-group-item"><strong>Name: </strong><br><?php echo $consignee['first_name'].' '.$consignee['last_name'] ?></li>
            <li class="list-group-item"><strong>Business Name: </strong><br><?php echo $consignee['business_name'] ?></li>
            <li class="list-group-item"><strong>Address: </strong><br><?php echo $consignee['location'] ?></li>
            <li class="list-group-item"><strong>Email : </strong><br><?php echo $consignee['email'] ?></li>
            <li class="list-group-item"><strong>Mobile: </strong><br><?php echo $consignee['calling_code'].$consignee['calling_digits'] ?></li>
          </ul>
        </div>
    
    </div>
</div>
<?php } else { echo '<p class="text-muted">No consignee details found.</p>'; } ?>

    </div>
    <div role="tabpanel" class="tab-pane fade" id="decision">


    </div>
    
  </div>

</div>