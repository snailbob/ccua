

            <div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">

                <div class="row">


                  <div class="col-lg-12">


                    <?php if ($this->session->flashdata('ok') != ''){ ?>
                    <div class="alert alert-success fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>

                        <?php echo $this->session->flashdata('ok'); ?>
                    </div>
                    <?php } ?>

                    <?php if ($this->session->flashdata('error') != ''){ ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>

                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                    <?php } ?>



                    <div class="panel panel-default">

                        <div class="panel-heading">
                            <a href="<?php echo base_url().'buy' ?>" class="btn btn-primary pull-right">
                                <i class="fa fa-cart-plus fa-fw"></i> <?php echo ($this->session->userdata('customer_type') == 'N') ? 'Get Quote' : 'Buy Insurance' ?>
                            </a>

                            <div class="panel-title">
								<h4><?php echo $title; ?></h4>
                            </div>
                        </div>


                        <?php if(count($insurances) > 0){ ?>
                        <div class="bg-white">
                        <table class="table table-hover mydataTb">
                            <thead>
                                <tr>
                                    <th class="hidden">sort</th>
                                    <th>Date</th>
                                    <th>Quote ID</th>
                                    <th>Quote Expiry</th>
                                    <!-- <th>Consignor</th>
                                    <th>Consignee</th> -->
                                    <th>Transit from</th>
                                    <th>Transit to</th>
                                    <th>Premium</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
								$count = 1;
								foreach($insurances as $r=>$value){
                                    $insurance = $this->common->the_quote_data($value['id']);
                                    if(isset($value['saved'])){
                                        $insurance = $this->common->the_cert_data($value['id']);
                                    }

                                    $details = unserialize($value['details']);
                                    $premium = (isset($details['premium'])) ? $details['premium'] : 0;
									$premium = number_format($premium, 2, '.', ',');
                                    $buy_inputs = (isset($details['buy_inputs'])) ? $details['buy_inputs'] : array();
									$insurance_details = array();
									foreach($buy_inputs as $bi=>$bival){
										$insurance_details[$bival['name']] = $bival['value'];
									}

									$single_input = (isset($details['single_input'])) ? $details['single_input'] : array();
									$cust_name = 'Unknown';
									if(isset($single_input['first_name']) && isset($single_input['last_name']) && $value['status'] != 'S'){
										$cust_name = $single_input['first_name'].' '.$single_input['last_name'];
                                    }

									$consignee = (isset($details['consignee'])) ? $details['consignee'] : array();
                                    $consignee_name = 'Unknown';
									if(isset($consignee['first_name']) && isset($consignee['last_name']) && $value['status'] != 'S'){
										$consignee_name = $consignee['first_name'].' '.$consignee['last_name'];
                                    }
                                    

									$transitto = (isset($insurance_details['transitto'])) ? $this->common->db_field_id('country_t', 'short_name', $insurance_details['transitto'], 'country_id') : 'not specified';
									$transitfrom = (isset($insurance_details['transitfrom'])) ? $this->common->db_field_id('country_t', 'short_name', $insurance_details['transitfrom'], 'country_id') : 'not specified';


                                    $date_purch = date_format(date_create($value['date_added']), 'd/m/Y'); //F d, Y - l');
                                    if($value['date_added'] == '0000-00-00 00:00:00'){
                                        $date_purch = 'NA';
                                    }


                                    $startdate = $value['date_added'];
                                    $expire = strtotime($startdate. ' + 30 days + 0 hours');
                                    $today = strtotime("today");


                                    //Calculate difference
                                    // $diff=$expire-$today;//time returns current time in seconds

                                    // $days=floor($diff/(60*60*24));//seconds/minute*minutes/hour*hours/day)
                                    // $hours=round(($diff-$days*60*60*24)/(60*60));
                                    // $minutes=round(($diff-$days*60*60*24)/(60*60*60));
                                    // $seconds=round(($diff-$days*60*60*24)/(60*60*60*60));

                                    // $remaining = "$days days and $hours hours and $minutes minutes and $seconds seconds";

                                    $exp_date = date('Y-m-d h:i:s', strtotime($value['date_added']. ' +  30 days + 0 hours'));
                                    
                                    // $then = new DateTime("@" .$expire);
                                    // $now = new DateTime("@" .$today);

                                    $then = new DateTime($exp_date);
                                    $now = new DateTime();
                                    
                                    $sinceThen = $then->diff($now);
                                    
                                    //Combined
                                    // $remaining = $sinceThen->y.' y';
                                    // $remaining = ($sinceThen->m != '0') ? '30 d ' : '';
                                    $remaining = $sinceThen->format("%a").'d ';
                                    $remaining .= $sinceThen->h.'h ';
                                    $remaining .= $sinceThen->i.'m';

                                    if($sinceThen->format("%a") >= 30){
                                        $remaining = '29 d 23 h 59m';
                                    }
                                    
                                    // if($sinceThen->d > 30){
                                    //     $remaining = 'expired';
                                    // }
                                    
                                    // else if(isset($value['saved']) && $value['status'] != 'A'){
                                    //     $remaining = ' - ';
                                    // }


                                    if($today >= $expire){
                                        $remaining = 'expired';
                                    }
                                    
                                    else if(isset($value['saved']) && $value['status'] != 'A' && $value['status'] != 'D' ){
                                        $remaining = ' - ';
                                    }

                                ?>
                                <tr>
                                    <th class="hidden"><?php echo $count; ?></th>
                                    <td><?php echo $date_purch ?></td>
                                    <td><?php echo $this->common->policy_id_format($value['id'], 'Q'); ?></td>
                                    <td><?php echo $remaining ?></td>
                                    <!-- <td><?php echo $cust_name ?></td>
                                    <td><?php echo $consignee_name ?></td> -->
                                    <td><?php echo $transitfrom; ?></td>
                                    <td><?php echo $transitto; ?></td>
                                    <td><?php echo $insurance['currency'].$premium; ?></td>
                                    <td><?php echo $this->common->quote_status($value['status']); ?></td>
                                    <td>


                                        <div class="btn-group pull-right">
                                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                            Action <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <?php if($value['status'] == 'D' && $remaining != 'expired') {?>
                                                <li>
                                                    <a href="<?php echo base_url().'dashboard/proceed_buy/'.$value['id'].'/'.$value['status'] ?>" data-id="<?php echo $value['id'] ?>">
                                                        Revisit Quote
                                                    </a>
                                                </li>
                                                
                                                <?php } else { ?>
                                                <li class="<?php echo ($value['status'] != 'A') ? 'hidden' : ''?>">
                                                    <a href="<?php echo base_url().'dashboard/proceed_buy/'.$value['id'].'/'.$value['status'] ?>" data-id="<?php echo $value['id'] ?>">
                                                        Revisit Quote
                                                    </a>
                                                </li>

                                                <!-- <li class="<?php echo ($value['status'] != 'A') ? 'hidden' : ''?>">
                                                    <a class="bind_quote_btn" data-id="<?php echo $value['id'] ?>">
                                                        <?php echo ($value['status'] == 'P') ? 'Bound' : 'Bind'?>
                                                    </a>
                                                </li> -->

                                                <?php } ?>
                                                
                                                <li>
                                                    <a href="#" class="delete_btn" data-id="<?php echo $value['id'] ?>" data-table="<?php echo (!isset($value['saved'])) ? 'bought_quote' : 'bought_insurance'?>">
                                                        Delete
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>

                                    </td>
                                </tr>
                                <?php $count++; } ?>
                            </tbody>
                        </table>
                        </div>

						<?php
                          //loop sorted_schedule
						 }
						 else { echo '<p class="text-center text-muted" style="padding: 50px;">No records found.</p>'; }
						 ?>

                    </div><!--panel-->







                  </div><!-- /.col-lg-12 -->

                </div><!-- /.row -->
                <!-- end PAGE TITLE AREA -->

            </div><!--end of main_content-->



<!-- Modal for new task -->
<div class="portfolio-modal modal fade" id="infoModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-9 col-sm-offset-3">
                    <div class="modal-body">

                        <h1 class="buy_title hidden">Buy Single Marine Transit Policy</h1>
                        <hr class="star-primary hidden">


                        <div class="txt_details"></div>


                    </div>
                </div>
            </div>
        </div>


    </div>
</div><!--portfolio-modal-->
