<!-- progressbar -->
<?php /*?><div class="row">
    <ul id="progressbar" class="progressbar-clickable progressbar-four">
        <li class="active" data-target="#home">Transit</li>
        <li data-target="#quote">Quote</li>
        <li data-target="#profile">Consignor</li>
        <li data-target="#messages">Consignee</li>
    </ul>   
</div><?php */?>

<div>
  <!-- Nav tabs -->
  <ul class="nav nav-tabs progressbar-control" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Transit</a></li>
    <li role="presentation"><a href="#cargo" aria-controls="cargo" role="tab" data-toggle="tab">Cargo</a></li>
    <li role="presentation"><a href="#quote" aria-controls="home" role="tab" data-toggle="tab">Quote</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Policy</a></li>
    <li role="presentation" class="hidden"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Consignee</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane fade in active" id="home">

<b>&nbsp;</b>
<?php
//$insurance = $this->common->the_cert_data($id);
if(count($buy) > 0){
	$insurance = array();
	foreach($buy as $r=>$value){
		$insurance[$value['name']] = $value['value'];
	}
	
	$default_deductible = (isset($insurance['default_deductible'])) ? $insurance['default_deductible'] : 0;
	$dedd = $this->common->db_field_id('deductibles', 'deductible', $default_deductible, 'rate');
	
?>
<div class="well well-sm">
	<div class="row gutter-md">
    
        <div class="col-sm-12">
        	<div class="text-right">
            	<a href="#" class="btn btn-link confirmbuy_update_btn btn-sm" data-progress="0" data-target="tab-transit">Update</a>
                <p></p>
            </div>
        </div>
        <div class="col-sm-12">
          <!-- List group -->
          <ul class="list-group">
            <li class="list-group-item"><strong>Shipment Date: </strong><br><?php echo $insurance['shipment_date'] ?></li>
            <li class="list-group-item"><strong>Name of Vessel/Aircraft: </strong><br><?php echo $insurance['vessel_name'] ?></li>
            
            <li class="list-group-item"><strong>Insured Value: </strong><br><?php echo $insurance['invoice'] ?></li>
            <li class="list-group-item"><strong>Transit From: </strong><br><?php echo $this->common->db_field_id('country_t', 'short_name', $insurance['transitfrom'], 'country_id'); ?></li>
            <li class="list-group-item"><strong>Port of Loading: </strong><br><?php echo $this->common->selectedport($insurance['portloading']); ?></li>
            <li class="list-group-item"><strong>Transit To: </strong><br><?php echo $this->common->db_field_id('country_t', 'short_name', $insurance['transitto'], 'country_id'); ?></li>
            <li class="list-group-item"><strong>Port of Discharge: </strong><br><?php echo $this->common->selectedport($insurance['portdischarge']) ?></li>
            <li class="list-group-item hidden"><strong>Deductible: </strong><br><?php echo $dedd ?></li>
          </ul>
        </div>
    
    </div>
</div>
<?php } else { echo '<p class="text-muted">No transit details found.</p>'; } ?>


    </div>
    <div role="tabpanel" class="tab-pane fade" id="cargo">
<b>&nbsp;</b>
<?php
//$insurance = $this->common->the_cert_data($id);
if(count($buy) > 0){
	$insurance = array();
	foreach($buy as $r=>$value){
		$insurance[$value['name']] = $value['value'];
	}
	
	$default_deductible = (isset($insurance['default_deductible'])) ? $insurance['default_deductible'] : 0;
	$dedd = $this->common->db_field_id('deductibles', 'deductible', $default_deductible, 'rate');
	
?>
<div class="well well-sm">
	<div class="row gutter-md">
    
        <div class="col-sm-12">
        	<div class="text-right">
            	<a href="#" class="btn btn-link confirmbuy_update_btn btn-sm" data-progress="1" data-target="tab-cargo">Update</a>
                <p></p>
            </div>
        </div>
        <div class="col-sm-12">
          <!-- List group -->
          <ul class="list-group">
            <li class="list-group-item"><strong>Cargo Cat: </strong><br><?php echo $this->common->cargo_name($insurance['cargocat']); ?></li>
            <li class="list-group-item"><strong>Description: </strong><br><?php echo $insurance['goods_desc'] ?></li>
            <li class="list-group-item"><strong>Transportation: </strong><br><?php echo $insurance['transmethod'] ?></li>
            
          </ul>
        </div>
    
    </div>
</div>
<?php } else { echo '<p class="text-muted">No cargo details found.</p>'; } ?>

    </div>
    <div role="tabpanel" class="tab-pane fade" id="quote">
        <b>&nbsp;</b>
        <div class="well well-sm text-center">
        	<div class="text-right">
            	<a href="#" class="btn btn-link confirmbuy_update_btn btn-sm" data-progress="2" data-target="tab-quote">Update</a>
                <p></p>
            </div>
            <h1 style="font-size: 50px"><?php echo '<small>'.$insurance['currency'].'</small>'.number_format($premium, 2, '.', ',') ?></h1>
        </div>
    </div>
    <div role="tabpanel" class="tab-pane fade" id="profile">



<b>&nbsp;</b>
<div class="well well-sm">

<?php if(count($single) > 0){ ?>
<div class="wellx well-smx">
	<div class="row gutter-md">
    
        <div class="col-sm-12">
        	<div class="">
            	<a href="#" class="btn btn-link confirmbuy_update_btn btn-sm pull-right" data-progress="3" data-target="tab-details">Update</a>
                <b>Consignor</b>
                <p></p>
            </div>
        </div>
        
        <div class="col-sm-12">
          <!-- List group -->
          <ul class="list-group">
            <li class="list-group-item"><strong>Name: </strong><br><?php echo $single['first_name'].' '.$single['last_name'] ?></li>
            <li class="list-group-item"><strong>Business Name: </strong><br><?php echo $single['business_name'] ?></li>
            <li class="list-group-item"><strong>Address: </strong><br><?php echo $single['location'] ?></li>
            <li class="list-group-item"><strong>Email : </strong><br><?php echo $single['email'] ?></li>
            <li class="list-group-item"><strong>Mobile: </strong><br><?php echo $single['calling_code'].$single['calling_digits'] ?></li>
          </ul>
        </div>
    
    </div>
</div>
<?php } else { echo '<p class="text-muted">No consignor details found.</p>'; } ?>

    <?php /*?></div>
    <div role="tabpanel" class="tab-pane fade" id="messages"><?php */?>


<?php if(count($consignee) > 0){ ?>
<div class="wellx well-smx">
	<div class="row gutter-md">
    
        <div class="col-sm-12">
        	<div class="">
            	<a href="#" class="btn btn-link confirmbuy_update_btn btn-sm pull-right" data-progress="3" data-target="tab-details">Update</a>
                <b>Consignee</b>
                <p></p>
            </div>
        </div>
        <div class="col-sm-12">
          <!-- List group -->
          <ul class="list-group">
            <li class="list-group-item"><strong>Name: </strong><br><?php echo $consignee['first_name'].' '.$consignee['last_name'] ?></li>
            <li class="list-group-item"><strong>Business Name: </strong><br><?php echo $consignee['business_name'] ?></li>
            <li class="list-group-item"><strong>Address: </strong><br><?php echo $consignee['location'] ?></li>
            <li class="list-group-item"><strong>Email : </strong><br><?php echo $consignee['email'] ?></li>
            <li class="list-group-item"><strong>Mobile: </strong><br><?php echo $consignee['calling_code'].$consignee['calling_digits'] ?></li>
          </ul>
        </div>
    
    </div>
</div>
<?php } else { echo '<p class="text-muted">No consignee details found.</p>'; } ?>

    </div>
    
  </div>

</div><!--wellx-->

</div><!--well-->