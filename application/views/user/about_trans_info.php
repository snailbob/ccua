
<?php
    $policy_docs = $this->master->getRecords('policy_docs','','*',array('id'=>'DESC'));
    $policy_doc = (isset($policy_docs[0]['name'])) ? base_url().'uploads/policyd/'.$policy_docs[0]['name'] : '#';
?>

<div class="row">
    <div class="col-sm-5 col-md-3 col-lg-2 leftbar-still">

        <nav>
            <div class="lifecycle">
                <h5>
                    <?php echo 'Transaction Details' ?>
                </h5>
                <!-- <span class="help-online">Quotes in a snap. Free and easy. </span> -->
                <ul class="list life-list life-list-clickable">


                    <?php
                    foreach($themenu as $r=>$value){ ?>

                        <li class="item <?php echo ($r == 0) ? ' start complete' : ''; echo ($r == (count($themenu) - 1)) ? ' end' : ''; echo (!empty($value['data'])) ? ' complete' : '' ?>">
                            <?php echo '<a href="#" data-target="'.$value['target'].'" data-toggle="tab"> '; //echo (!empty($value['data']) || $thepage == ($r + 1)) ? '<a class="" href="?tab='.($r + 1).'">' : '<div>';  ?>
                            <div class="indicator">
                                <div class="semiline"></div>
                                <div class="semiline"></div>
                                <div class="circle wow animated zoomIn"></div>
                            </div>
                            <span>
                                <?php echo $value['name']?>
                            </span>
                            <?php echo '</a>'; // echo (!empty($value['data']) || $thepage == ($r + 1)) ? '</a>' : '</div>'; ?>
                        </li>

                        <?php
                    }
                ?>


                </ul>
            </div>

        </nav>
    </div>

    <div class="col-sm-7 col-md-9 col-lg-10">
    
    </div>

</div><!--.col-sm-5-->


<!-- progressbar -->
<!-- <div class="row">
    <ul id="progressbar" class="progressbar-clickable progressbar-four">
        <li class="active" data-target="#home">Transit</li>
        <li data-target="#quote">Quote</li>
        <li data-target="#profile">Consignor</li>
        <li data-target="#messages">Consignee</li>
    </ul>   
</div> -->

<div>
  <!-- Nav tabs -->
  <ul class="nav nav-tabs progressbar-control hidden" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Transit</a></li>
    <li role="presentation"><a href="#quote" aria-controls="home" role="tab" data-toggle="tab">Quote</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Consignor</a></li>
    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Consignee</a></li>
    <li role="presentation"><a href="#decision" aria-controls="decision" role="tab" data-toggle="tab">Decision</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane fade in active" id="home">

<b>&nbsp;</b>
<?php
$insurance = $this->common->the_cert_data($id);
if(count($buy) > 0){
    $insurance['deductible'] = (isset($details['new_deductible']) && !empty($details['new_deductible'])) ? $details['new_deductible'] : $insurance['deductible'];

    
    $buy_inputs = (isset($details['buy_inputs'])) ? $details['buy_inputs'] : array();
    $insurance_details = array();
    foreach($buy_inputs as $bi=>$bival){
      $insurance_details[$bival['name']] = $bival['value'];
    }

    $transitto = (isset($insurance_details['transitto'])) ? $this->common->db_field_id('country_t', 'short_name', $insurance_details['transitto'], 'country_id') : 'not specified';
    $transitfrom = (isset($insurance_details['transitfrom'])) ? $this->common->db_field_id('country_t', 'short_name', $insurance_details['transitfrom'], 'country_id') : 'not specified';
    
?>
<div class="wellx well-smx">
	<div class="row gutter-md">
          <!-- <ul class="list-group hidden">
            <li class="list-group-item"><strong>Shipment Date: </strong><br><?php echo $insurance['shipmentdate'] ?></li>
            <li class="list-group-item"><strong>Insured Value: </strong><br><?php echo $insurance['insurance'] ?></li>
            <li class="list-group-item"><strong>Transit From: </strong><br><?php echo $transitfrom ?></li>
            <li class="list-group-item"><strong>Port of Loading: </strong><br><?php echo $insurance['portloading'] ?></li>
            <li class="list-group-item"><strong>Transit To: </strong><br><?php echo $transitto ?></li>
            <li class="list-group-item"><strong>Port of Discharge: </strong><br><?php echo $insurance['portdischarge'] ?></li>
            <li class="list-group-item"><strong>Cargo Cat: </strong><br><?php echo $insurance['cargocat'] ?></li>
            <li class="list-group-item"><strong>Description: </strong><br><?php echo $insurance['description'] ?></li>
            <li class="list-group-item"><strong>Deductible: </strong><br><?php echo $insurance['deductible'] ?></li>
          </ul> -->
        <?php // var_dump($buy) ?>

        <div class="col-sm-6">
            <div class="form-group">
                <label for="">Transit From *</label>
                <input type="text" readonly class="form-control" value="<?php echo strip_tags($transitfrom) ?>">
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="">Transit To *</label>
                <input type="text" readonly class="form-control" value="<?php echo strip_tags($transitto) ?>">
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="">Insured Value *</label>
                <?php $inv = strip_tags($insurance['insurance']); $inv = preg_replace("/[^0-9,.]/", "", $inv ); ?>
                <input type="text" readonly class="form-control" value="<?php echo $inv ?>">
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="">Currency * </label>
                <input type="text" readonly class="form-control" value="<?php echo strip_tags($insurance['currency']) ?>">
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label for="">Cargo * </label>
                <input type="text" readonly class="form-control" value="<?php echo strip_tags($insurance['cargocat']) ?>">
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label for="">Brief Description of Goods *</label>
                <input type="text" readonly class="form-control" value="<?php echo strip_tags($insurance['description']) ?>">
            </div>
        </div>

        <div class="col-sm-12">
            <div class="form-group">
                <label for="">Transportation *</label>

                <div class="row gutter-sm">
                    <div class="col-xs-6 col-md-3">
                        <a href="#" class="thumbnail text-center" style="padding: 25px 0">
                            <span><?php echo strip_tags($insurance['transmethod']) ?> </span>
                        </a>
                    </div>
                    <!--col-xs-6-->
                </div>
            </div>
        </div>
    
    </div>
</div>
<?php } else { echo '<p class="text-muted">No transit details found.</p>'; } ?>


    </div>
    <div role="tabpanel" class="tab-pane fade" id="quote">
        <h3 class="text-center" style="font-size: 16px; color: #333">
            Your goods <strong class="cargo_description"><?php echo strip_tags($insurance['description']) ?></strong><br>
            Transit from <strong class="transit_from"><?php echo strip_tags($transitfrom) ?></strong> to <strong class="transit_to"><?php echo strip_tags($transitto) ?></strong> with a Sum Insured of <strong class="insured_value">$<?php echo $inv; ?></strong> and excess of <strong class="excess_value">$<?php echo strip_tags($insurance['deductible']) ?></strong>

        </h3>


        <div class="well well-sm text-center">
            <h1 style="font-size: 50px"><?php echo '<small>'.$insurance['currency'].'</small>'.number_format($details['premium'], 2, '.', ',') ?></h1>
        </div>


        <div class="form-group">
            <label for="">Excess *</label>
            <input type="text" readonly class="form-control" value="<?php echo strip_tags($insurance['deductible']) ?>">
        </div>

        <h3 class="text-left">The Cover</h3>
        <p style="color: #333">Your policy will cover physical loss or damage to your cargo from an external cause on an “All Risks” basis.  Cover is based on the latest version of Institute Cargo Clauses and includes:-</p>
        <div class="row">
        <div class="col-sm-6">
            <i class="fa fa-check"></i> Warehouse to warehouse cover <br>
            <i class="fa fa-check"></i> Theft and Pilferage <br>
            <i class="fa fa-check"></i> General Average <br>
        </div>
        <div class="col-sm-6">
            <i class="fa fa-check"></i> Non-delivery  <br>
            <i class="fa fa-check"></i> Breakage  <br>
            <i class="fa fa-check"></i> There are many other additional benefits. Refer to the full <a href="<?php echo $policy_doc; ?>" target="_blank">policy wording</a>

        </div>
        </div>

        <h3>Cover does not include </h3>
        <div class="row">
        <div class="col-sm-6">
            <i class="fa fa-close pull-left"></i> Wear and Tear  <br>
            <i class="fa fa-close pull-left"></i> Consequential Loss <br>
            <i class="fa fa-close pull-left"></i> Liabilities
        </div>
        <div class="col-sm-6">
            <i class="fa fa-close pull-left"></i> Damage caused by insufficient packing  <br>
            <i class="fa fa-close pull-left"></i> Inherent vice, delay, ordinary leakage, ordinary loss in weight
        </div>
        <div class="col-sm-12">
            There are other exclusions that you should know about. Please refer to the full <a href="<?php echo $policy_doc ?>" target="_blank">policy wording</a>, but if still in doubt <a href="#contactUsModal" data-toggle="modal" data-controls-modal="contactUsModal" >Get in touch</a>



        </div>
        </div>




    </div>
    <div role="tabpanel" class="tab-pane fade" id="profile">
        <b>&nbsp;</b>

        <div class="well">
            <h3>Cargo Details</h3>

            <div class="row gutter-md">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="">Date of Shipment *</label>
                        <input type="text" readonly class="form-control" value="<?php echo strip_tags($insurance['shipmentdate']) ?>">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="">Name of Vessel/Aircraft</label>
                        <input type="text" readonly class="form-control" value="<?php echo strip_tags($insurance_details['vessel_name']) ?>">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="">Port of Loading</label>
                        <input type="text" readonly class="form-control" value="<?php echo strip_tags($insurance['portloading']) ?>">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="">Port of Discharge</label>
                        <input type="text" readonly class="form-control" value="<?php echo strip_tags($insurance['portdischarge']) ?>">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="">Address where transit begins</label>
                        <input type="text" readonly class="form-control" value="<?php echo strip_tags($insurance_details['transit_begins']) ?>">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="">Address where transit ends</label>
                        <input type="text" readonly class="form-control" value="<?php echo strip_tags($insurance_details['transit_ends']) ?>">
                    </div>
                </div>
                
            
            </div>
        </div>

        <b>&nbsp;</b>
        <?php if(count($single) > 0){ ?>
        <div class="well">
            <h3>Consignor's Details</h3>

            <div class="row gutter-md">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="">First Name *</label>
                        <input type="text" readonly class="form-control" value="<?php echo $single['first_name'] ?>">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="">Last Name *</label>
                        <input type="text" readonly class="form-control" value="<?php echo $single['last_name'] ?>">
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="">Business Name *</label>
                        <input type="text" readonly class="form-control" value="<?php echo $single['business_name'] ?>">
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="">Address *</label>
                        <input type="text" readonly class="form-control" value="<?php echo $single['location'] ?>">
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="">Email *</label>
                        <input type="text" readonly class="form-control" value="<?php echo $single['email'] ?>">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="">Country Calling Code *</label>
                        <input type="text" readonly class="form-control" value="<?php echo $single['calling_code'] ?>">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="">Contact Number *</label>
                        <input type="text" readonly class="form-control" value="<?php echo $single['calling_digits'] ?>">
                    </div>
                </div>
                
                <!-- <div class="col-sm-12">
                <ul class="list-group">
                    <li class="list-group-item"><strong>Name: </strong><br><?php echo $single['first_name'].' '.$single['last_name'] ?></li>
                    <li class="list-group-item"><strong>Business Name: </strong><br><?php echo $single['business_name'] ?></li>
                    <li class="list-group-item"><strong>Address: </strong><br><?php echo $single['location'] ?></li>
                    <li class="list-group-item"><strong>Email : </strong><br><?php echo $single['email'] ?></li>
                    <li class="list-group-item"><strong>Mobile: </strong><br><?php echo $single['calling_code'].$single['calling_digits'] ?></li>
                </ul>
                </div> -->
            
            </div>
        </div>
        <?php } else { echo '<p class="text-muted">No consignor details found.</p>'; } ?>



        <b>&nbsp;</b>
        <?php if(count($consignee) > 0){ ?>
        <div class="well">
            <h3>Consignee's Details</h3>

            <div class="row gutter-md">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="">First Name *</label>
                        <input type="text" readonly class="form-control" value="<?php echo $consignee['first_name'] ?>">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="">Last Name *</label>
                        <input type="text" readonly class="form-control" value="<?php echo $consignee['last_name'] ?>">
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="">Business Name *</label>
                        <input type="text" readonly class="form-control" value="<?php echo $consignee['business_name'] ?>">
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="">Address *</label>
                        <input type="text" readonly class="form-control" value="<?php echo $consignee['location'] ?>">
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="">Email *</label>
                        <input type="text" readonly class="form-control" value="<?php echo $consignee['email'] ?>">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="">Country Calling Code *</label>
                        <input type="text" readonly class="form-control" value="<?php echo $consignee['calling_code'] ?>">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="">Contact Number *</label>
                        <input type="text" readonly class="form-control" value="<?php echo $consignee['calling_digits'] ?>">
                    </div>
                </div>
                
            
            </div>
        </div>
        <?php } else { echo '<p class="text-muted">No consignee details found.</p>'; } ?>


    </div>
    <div role="tabpanel" class="tab-pane fade" id="messages">


<b>&nbsp;</b>
<?php if(count($consignee) > 0){ ?>
<div class="well well-sm">
	<div class="row gutter-md">
        <div class="col-sm-12">
          <!-- List group -->
          <ul class="list-group">
            <li class="list-group-item"><strong>Name: </strong><br><?php echo $consignee['first_name'].' '.$consignee['last_name'] ?></li>
            <li class="list-group-item"><strong>Business Name: </strong><br><?php echo $consignee['business_name'] ?></li>
            <li class="list-group-item"><strong>Address: </strong><br><?php echo $consignee['location'] ?></li>
            <li class="list-group-item"><strong>Email : </strong><br><?php echo $consignee['email'] ?></li>
            <li class="list-group-item"><strong>Mobile: </strong><br><?php echo $consignee['calling_code'].$consignee['calling_digits'] ?></li>
          </ul>
        </div>
    
    </div>
</div>
<?php } else { echo '<p class="text-muted">No consignee details found.</p>'; } ?>

    </div>
    
  </div>

</div>