    <section class="section section-simple no-padding-bottom">
        <div class="container">

    

            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 col-lg-6 col-lg-offset-3" style="min-height: 100vh">
                    <!-- <h1 class="section-heading text-center"><?php echo $title; ?></h1> -->
                    <p class="text-center">Hi <?php echo $this->common->customer_name($agency[0]['id']) ?>, please assign a password and set your timezone to complete registration.</p>
                    <hr class="dark"/>

                    <form class="text-left" id="customer_password_form" novalidate>

                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="pw" id="pw" class="form-control" />
                            <input type="hidden" name="id" class="form-control" value="<?php echo $agency[0]['id'] ?>" />
                        </div>

                        <div class="form-group">
                        	<div class="row">
                            	<div class="col-sm-6">
                                    <div class="pwstrength_viewport_progress"></div>
                                </div>
                            	<div class="col-sm-6">
                                	<div class="well well-sm">
                                    	Use a mix of letters, numbers, and symbols in your password
                                    </div>
                                </div>
                            
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Repeat Password</label>
                            <input type="password" name="pw2" class="form-control" />
                        </div>
                        <div class="form-group hidden">
                          <div class="checkbox">
                            <label style="color: #333">
                              <input type="checkbox" name="terms" checked> I have read and understood <a href="#" class="terms_btn2">terms of use</a> &amp; <a href="#" class="privacy_btn2">conditions</a>
                            </label>
                          </div>
                        </div>


                        <div class="form-group">
                            <label>Timezone</label>
                            <select name="timezone" id="" class="form-control" required>
                                <option value="">Select</option>
                                <?php
                                    foreach($time_zone as $r=>$value){
                                        echo '<option value="'.$value['id'].'"';
                                        echo ($value['id'] == '32') ? ' selected="selected"' : '';
                                        echo '>'.$value['name'].'</option>';
                                    }
                                ?>


                            </select>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>

                                
                </div>
            </div>
      
      
    
        </div>

        
    </section>
