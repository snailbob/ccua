<div id="row-drivers" class="content-holder content-vert-padding">
   <div class="container clearfix">
      <div class="content-side-padding">
         <h3 class="title-content text-center">Contact Us</h3>
         <div class="row">
            <div class="col-md-8 col-md-offset-2">
               <form novalidate="novalidate" id="contact_form" class="contact_form">
                  <div class="row">
                     <div class="col-sm-6">
                        <div class="form-group">
                           <input type="text" class="form-control" name="name" placeholder="Name" required="required">
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="form-group">
                           <input type="text" class="form-control" name="phone" placeholder="Contact number" required="required">
                        </div>
                     </div>
                     <div class="col-sm-12">
                        <div class="form-group">
                           <textarea name="message" placeholder="How can we help you? " rows="8" class="form-control" required="required"></textarea>
                        </div>
                     </div>
                     <div class="col-sm-12 text-center">
                        <button class="btn btn-lg" type="submit">Submit</button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
