<div id="row-drivers" class="content-holder content-vert-padding">
   <div class="container clearfix">
      <div class="content-side-padding">
         <h3 class="title-content">Importers / Exporters</h3>
         <p class="subtitle-slide text-justify">Whether you’re a consumer shipping a small parcel, a trading company requiring single transit cover, or a large organisation shipping regularly, RedSky makes purchasing your insurance quick and easy.</p>
      </div>
   </div>
   <div class="container clearfix">
      <div class="row">
         <div class="col-sm-12 col-lg-8 col-lg-offset-2">
            <table class="table-bordered">
               <tr>
                  <td style="width: 50%; vertical-align: top;">
                     <div class="col-sm-12">
                        <h3><b>As an exporter…</b></h3>
                        <p>You value the relationship you have with your buyer.</p>
                        <p>You know when things go wrong it’s not just your business on the line, it’s your reputation.</p>
                        <p>That’s why you need a way to protect both your buyer, and your own business from risk.</p>
                        <p>
                           <i class="fa fa-arrow-right fa-fw"></i> Manage expectations <br>
                           <i class="fa fa-arrow-right fa-fw"></i> Control the buying experience<br>
                           <i class="fa fa-arrow-right fa-fw"></i> Safeguard your business
                        </p>
                        <p>Offer CIF/CIP with each transaction to ensure your customers are protected and return</p>
                     </div>
                  </td>
                  <td style="width: 50%; vertical-align: top;">
                     <div class="col-sm-12">
                        <h3><b>As an importer…</b></h3>
                        <p>You want your goods to arrive safely, and they usually do.</p>
                        <p>Until they don’t. Then what? If you’ve bought CIF/CIP your goods are already insured, right?</p>
                        <p>You might be covered, but things get tricky (and time consuming) when you need to make a claim and the insurer:</p>
                        <p>
                           <i class="fa fa-arrow-right fa-fw"></i> Is based in the seller’s country and does not have representation in the country you live in <br>
                           <i class="fa fa-arrow-right fa-fw"></i> Has no relationship with you
                        </p>
                        <p>Buy your goods FOB or C&F and buy your insurance locally from someone you trust and who has your interests at heart.</p>
                     </div>
                  </td>
               </tr>
               <tr>
                  <td style="width: 50%; vertical-align: top">
                     <p class="text-center">
                        <a href="javascript:;" class="btn btn-red buy_transit_btn">GET A QUOTE</a>
                     </p>
                  </td>
                  <td style="width: 50%; vertical-align: top">
                     <p class="text-center">
                        <a href="javascript:;" class="btn btn-red buy_transit_btn">GET A QUOTE</a>
                     </p>
                  </td>
               </tr>
            </table>
         </div>
      </div>
      <h2 class="text-center" style="margin-top: 50px;">
         Still not sure what you need?<br>
         <a href="<?php echo base_url().'contactus'?>" class="btn btn-red btn-sm">Contact Us</a>
      </h2>
   </div>
</div>
