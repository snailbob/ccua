
<!doctype html>
<html class="no-js" lang="">

<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="description" content="RedSky Cargo is the simpler, smarter, seamless delivery service putting the logic back into logistics.">
        <meta name="keywords" content="ecommerce,RedSky Cargo,Logistics,Mover,Deliverer,courier,delivery,delivering difficult items,premium delivery">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/frontpage/images/favicon.ico" type="image/x-icon">

        <title>Cargo &amp; Carriers Underwriting Agency </title>
        
        <link rel="dns-prefetch" href="//cdnjs.cloudflare.com">



        <link href="<?php echo base_url().'assets/meemeep/'?>static/feyTmWO0bMeD1aEaHxHMNNeKNRUGPPPfS1U6H5Eg00z.css" type="text/css" rel="stylesheet" media="screen, projection" />



         <link rel="stylesheet" href="//cdn.jsdelivr.net/jquery.slick/1.5.9/slick.css"/>
         <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
         <link rel="stylesheet" href="<?php echo base_url().'assets/meemeep/'?>lib/bootstrap/bootstrap.min.css">
         <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.7.6/jquery.fullPage.min.css">




          <!-- Plugin CSS -->
          <?php /*?><link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.min.css" type="text/css"><?php */?>
          <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/dataTables/css/dataTables.bootstrap.css" type="text/css">
          <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/dataTables/css/datatables.css" type="text/css">

          <!-- Plugin CSS -->
          <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/cropper/css/cropper.min.css" type="text/css">
          <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/cropper/css/crop-avatar.css" type="text/css">


          <!-- Custom CSS -->
          <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css" type="text/css">

          <!-- Custom CSS -->
          <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bs-switch/css/bootstrap3/bootstrap-switch.min.css" type="text/css">

          <!-- Custom CSS -->
          <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/silviomoreto-bootstrap-select/css/bootstrap-select.min.css" type="text/css">


          <link href="<?php echo base_url().'assets/meemeep/'?>static/eqAMgtx1ZdhVAZ7XHESdPFDzIRASWGjS7BiEOGnGhSb.css" type="text/css" rel="stylesheet" media="screen, projection" />
          <link href="<?php echo base_url().'assets/meemeep/'?>static/custom.css" type="text/css" rel="stylesheet" media="screen, projection" />


		<script>
			var base_url = '<?php echo base_url() ?>';
			var uri_1 = '<?php echo $this->uri->segment(1) ?>';
			var uri_2 = '<?php echo $this->uri->segment(2) ?>';
			var uri_3 = '<?php echo $this->uri->segment(3) ?>';
			var uri_4 = '<?php echo $this->uri->segment(4) ?>';
			var user_id = '<?php echo $this->session->userdata('id') ?>';
			var user_name = '<?php echo $this->session->userdata('name') ?>';
			var user_location = '<?php echo $this->session->userdata('location') ?>';
			var user_address = '<?php echo $this->session->userdata('address') ?>';
			var user_type = '<?php echo $this->session->userdata('type') ?>';
			var stripe_id = '<?php echo $this->session->userdata('stripe_id') ?>';
			var customer_type = '<?php echo $this->session->userdata('customer_type') ?>';

      <?php
        $default_cargo = $this->common->default_cargo();
        $get_loc = $this->common->db_field_id('agency', 'country_currency', $default_cargo);
      ?>
      var get_location = '<?php echo $get_loc ?>';


		</script>
        <script src="<?php echo base_url().'assets/meemeep/'?>static/knffveD2OzCF16hdyrEAfPWa4B3ZjiSrq7fkcGVU1VX.js"></script>

	    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
	    <!-------------------------------->
	    <!-- link to liveAgentStyle.css -->
	    <!-------------------------------->
	    <link rel="stylesheet" href="<?php echo base_url().'assets/meemeep/'?>static/O6H3OVWClXGKVT6552GXx18epTs9syatcA3WgDWEnuS.css" type="text/css">
</head>
<body style="background: #fff">

    <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="modal-body center-block page-login">

                            <h2 class="text-center">
                            	<img src="<?php echo base_url().'assets/meemeep/'?>static/qPqvynPHJHWaFgvoN9BYekMooIPlMUooSeTLkzowpUd.png" height="150" alt="Grails">
                            </h2>

                            <div class="omb_login">
                                <div class="row" style="margin-bottom: 15px;">
                                    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                                        <hr class="star-primary">
                                        <p class="text-center">Don't have an account? <a href="#" class="signup_btn">Request Access</a></p>
                                    	<div class="alert <?php if($this->session->flashdata('info')) { echo 'alert-success'; } else { echo 'hidden'; }?>">
                                        	<button type="button" class="close" onclick="$(this).closest('.alert').addClass('hidden');" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            <span class="my-text">
                                            	<?php echo $this->session->flashdata('info')?>
                                            </span>
                                        </div>

                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                                        <form class="omb_loginForm" id="login_form">
                                            <input type="hidden" class="form-control" name="google_id">
                                            <input type="hidden" class="form-control" name="facebook_id">
                                            <input type="hidden" class="form-control refer_from" name="refer_from" value="<?php if(isset($_GET['refer_from'])) { echo $_GET['refer_from']; } else { echo $this->session->userdata('id'); }?>">
                                            <input type="hidden" class="form-control" name="from_buyform" value="<?php if(isset($_GET['refer_from'])) { echo $_GET['refer_from']; }?>">

                                            <div class="form-group hidden">
                                                <div class="dropdown dropdown-form dropdown-login">
                                                  <button class="btn btn-primary btn-block btn-outline"type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-chevron-down fa-fw pull-right" style="margin-right: 10px;"></i>
                                                    Sign in as <span class="u_type"> - Agency</span>
                                                  </button>
                                                  <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                    <li class="active"><a href="#" data-type="Agency">Agency</a></li>

                                                    <li><a href="#" data-type="Customer">Customer</a></li>
                                                  </ul>
                                                </div>

                                                <input type="hidden" name="utype" value="Customer" />
                                            </div>

                                            <div class="form-group">


                                                <input type="email" class="form-control" name="email" placeholder="Email Address">

                                            </div>

                                            <div class="form-group">
                                                <input  type="password" class="form-control" name="password" placeholder="Password">

                                            </div>

                                            <div class="form-group hidden">
                                            	<label>Sign in as</label>
                                                <input type="checkbox" name="usertype" checked>
                                            </div>

                                            <div class="form-group">
                                                <button class="btn btn-red btn-block" type="submit">Sign in</button>
                                            </div>


                                        </form>

                                        <p class="omb_forgotPwd text-center">
                                            <a href="#" class="forgot_btn">Forgot password?</a>
                                        </p>
                                        <p class="text-muted">
                                            <small>
                                            By logging in, you agree to our <a href="#" class="terms_btn">Terms of Use</a> and <a href="#" class="privacy_btn">Privacy Policy</a>
                                            </small>
                                        </p>
                                    </div>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
    </div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>




    <!-- termsModal -->
    <div class="portfolio-modal modal fade" id="termsModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="modal-body">

                            <h2>Terms of Use</h2>
                            <hr class="star-primary">

                            <div class="text_content text-left"></div>

                        </div><!--modal-body-->
                        <div class="modal-footer hidden">
                        	<button class="btn btn-primary btn-rounded" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div><!--portfolio-modal-->


    <!-- privacyModal -->
    <div class="portfolio-modal modal fade" id="privacyModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
                        <div class="modal-body">

                            <h2>Privacy Policy</h2>
                            <hr class="star-primary">


                            <div class="text_content text-left"></div>



                        </div><!--modal-body-->
                        <div class="modal-footer hidden">
                        	<button class="btn btn-primary btn-rounded" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div><!--portfolio-modal-->



    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="forgotModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="modal-body">

                            <h2>Forgot Password</h2>



                            <div class="omb_login">

                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                        <hr class="star-primary">
                                        <p class="text-center">Enter your email below and we will send you instructions on how to reset your password</p>

                                        <form class="omb_loginForm" id="forgot_form">
                                            <div class="form-group">
                                                <input type="email" class="form-control" name="email" placeholder="Email Address">

                                            </div>

                                            <div class="form-group">
                                                <button class="btn btn-red btn-block" type="submit">Send</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!--portfolio-modal-->


    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="signupModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="modal-body">
                            <h2>
                            	<img src="<?php echo base_url().'assets/meemeep/'?>static/qPqvynPHJHWaFgvoN9BYekMooIPlMUooSeTLkzowpUd.png" height="150" alt="Grails">
                            </h2>
                            <div class="omb_login">

                                <div class="row">
                                    <div class="col-sm-6 col-sm-offset-3">
                                        <h3 class="text-center text-red" style="font-size: 26px">
                                            Request Access
                                        </h3>
                                        <hr class="star-primary">
                                        <h3 class="text-center" style="font-size: 20px">
                                            Enter your details below and we'll be in touch to discuss your requirements.
                                        </h3>
                                        <p class="text-center">Already have an account? <a href="#" class="login_btn">Sign in</a></p>

                                    	<div class="alert alert-success" style="display: none;">
                                        	<button type="button" class="close" onclick="$(this).closest('.alert').hide();" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            <span class="my-text"></span>
                                        </div>

                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                        <form id="request_access_form" novalidate>



                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="name" required placeholder="Name">

                                                    </div>

                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="company" required placeholder="Company Name">

                                                    </div>

                                                </div>

                                            </div>


                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="email" class="form-control" name="email" required placeholder="Work Email">

                                                    </div>

                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="phone" required placeholder="Phone">

                                                    </div>

                                                </div>

                                            </div>

                                            <div class="row">
                                                <div class="col-md-6 col-md-offset-3">
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-red btn-block">Request Access</button>
                                                    </div>
                                                </div>
                                            </div>



                                        </form>
                                        <?php /* ?> 
                                        <form class="omb_loginForm" id="signup_form" autocomplete="off">
                                            <input type="email" class="hidden" name="fakeee">
                                            <input type="password" class="hidden" name="fakeee">


                                            <input type="hidden" class="form-control" name="google_id">
                                            <input type="hidden" class="form-control" name="facebook_id">
                                            <input type="hidden" class="form-control refer_from" name="refer_from" value="<?php if(isset($_GET['refer_from'])) { echo $_GET['refer_from']; } else { echo $this->session->userdata('id'); }?>">

                                            <input type="hidden" class="form-control" name="google_img">
                                            <input type="hidden" class="form-control" name="fb_img">
                                            <input type="hidden" class="form-control" id="social_type" name="social_type">
                                            <input type="hidden" class="form-control" id="social_site" name="social_site">


                                            <div class="form-group hidden">
                                                <div class="dropdown dropdown-form dropdown-signup">
                                                  <button class="btn btn-primary btn-block btn-outline"type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-chevron-down fa-fw pull-right"></i>
                                                    Sign up as <span class="u_type"> - Agency</span>
                                                  </button>
                                                  <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                    <li class="active"><a href="#" data-type="Agency">Agency</a></li>

                                                    <li><a href="#" data-type="Customer">Customer</a></li>
                                                  </ul>
                                                </div>

                                                <input type="hidden" name="utype" value="Customer" />
                                            </div>

                                            <div class="form-group studio_only hidden">


                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                                    <input type="text" class="form-control" name="name" placeholder="Agency Name">
                                                </div>
                                            </div>


                                            <div class="row notfor_studio">
                                            	<div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="first_name" placeholder="First Name">

                                                    </div>

                                                </div>

                                            	<div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="last_name" placeholder="Last Name">

                                                    </div>

                                                </div>

                                            </div>



                                            <div class="form-group">
                                                <input type="email" class="form-control" name="email" id="signup_email" placeholder="Email Address" autocomplete="nope" value="">

                                            </div>

                                            <div class="form-group">
                                                <input type="email" class="form-control" name="email2" placeholder="Confirm Email Address" autocomplete="nope" value="">

                                            </div>

                                            <div class="form-group">
                                                <input type="text" class="form-control" name="business_name" placeholder="Enter your name" required>
                                            </div>

                                            <div class="form-group">
                                                <input type="text" class="form-control user_geolocation" name="location" placeholder="Enter your address" required/>
                                                <input type="hidden" name="lat" value=""/>
                                                <input type="hidden" name="lng" value=""/>

                                            </div>

                                            <div class="row gutter-md">

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Country Calling Code</label><br>
                                                        <select class="country_code_dd form-control" name="country_code" data-live-search="true" required="required">
                                                            <option value="" data-code="">Select..</option>
                                                            <?php
                                                                foreach($countries as $r=>$mc){
                                                                    echo '<option value="'.$mc['country_id'].'" data-code="'.$mc['calling_code'].'"';
                                                                    if($this->session->userdata('country_id') == $mc['country_id'] && ($this->session->userdata('customer_type') == 'Y')){
                                                                        echo ' selected="selected"';
                                                                    }


                                                                    echo '>'.$mc['short_name'].' (+'.$mc['calling_code'].')</option>';
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group mobile_input">
                                                        <label class="control-label">&nbsp;</label>
                                                          <input type="number" class="form-control" placeholder="Contact Number" name="mobile" autocomplete="nope" value="" required>

                                                        <input type="hidden" name="country_short" value=""/>
                                                    </div>
                                                </div>

                                            </div><!--phone number-->




                                            <div class="form-group">
                                                <input  type="password" class="form-control" id="signup_password" name="password" placeholder="Password" autocomplete="new-password" value="">

                                            </div>
                                            <div class="form-group">
                                                <input  type="password" class="form-control" name="password2" placeholder="Confirm Password" autocomplete="new-password" value="">

                                            </div>

                                            <div class="form-group">
                                                <div class="pwstrength_viewport_progress"></div>
                                            </div>


                                            <div class="form-group">
                                              <div class="checkbox">
                                                <label style="color: #333">
                                                  <input type="checkbox" name="terms"> I have read and understood <a href="#" class="terms_btn2">terms of use</a> &amp; <a href="#" class="privacy_btn2">conditions</a>
                                                </label>
                                              </div>
                                            </div>

                                            <div class="form-group">
                                                <button class="btn btn-red btn-block" type="submit">Sign Up</button>
                                            </div>

                                        </form>
                                        <?php */?>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!--portfolio-modal-->



    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="loginModalx" tabindex="-1" role="dialog" data-keyboard="false" aria-hidden="true">
        <div class="modal-content">
            <!-- <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div> -->

            <div class="container">

            </div>


        </div>
    </div><!--portfolio-modal-->














    <!--buy modal -landing-page --->

    <!-- Modal for new task -->
    <div class="portfolio-modal modal fade" id="buyNowModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>


            <!--.new transit menu-->
            <div class="row hidden-sm hidden-xs">
                <div class="col-sm-12 col-md-3 col-lg-2 rightbar-still">
                    <div class="numpad" style="display: none;">

                        <table class="keyboard">
                            <tbody>
                                <tr class="keyboard__row">
                                    <td class="keyboard__number">7</td>
                                    <td class="keyboard__number">8</td>
                                    <td class="keyboard__number">9</td>
                                </tr>
                                <tr class="keyboard__row">
                                    <td class="keyboard__number">4</td>
                                    <td class="keyboard__number">5</td>
                                    <td class="keyboard__number">6</td>
                                </tr>
                                <tr class="keyboard__row">
                                    <td class="keyboard__number">1</td>
                                    <td class="keyboard__number">2</td>
                                    <td class="keyboard__number">3</td>
                                </tr>
                                <tr class="keyboard__row">
                                    <td class="keyboard__number keyboard__number--0" colspan="2">0</td>
                                    <td class="keyboard__delete keyboard__backspace">&larr;</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="the-homebiz-info">

                    </div>
                </div><!--.col-sm-7-->
            </div>

            <div class="row">
                <div class="col-sm-5 col-md-3 col-lg-2 leftbar-still hidden-sm hidden-xs">

                    <nav>
                    <div class="lifecycle">
                        <h5><?php echo (!empty($user_sess['logged_admin_email'])) ? ' Logged in as Admin' : 'Your Online Application' ?></h5><span class="help-online">Quotes in a snap. Free and easy. </span>
                        <ul class="list life-list">


                            <?php
                                foreach($themenu as $r=>$value){ ?>

                            <li class="item <?php echo ($thepage == ($r + 1)) ? ' current' : ''; echo ($r == 0) ? ' start' : ''; echo ($r == (count($themenu) - 1)) ? ' end' : ''; echo (!empty($value['data'])) ? ' complete' : '' ?>">
                                <?php echo '<a href="#" data-target="'.$value['target'].'" data-toggle="tab"> '; //echo (!empty($value['data']) || $thepage == ($r + 1)) ? '<a class="" href="?tab='.($r + 1).'">' : '<div>';  ?>
                                    <div class="indicator">
                                        <div class="semiline"></div>
                                        <div class="semiline"></div>
                                        <div class="circle wow animated zoomIn"></div>
                                    </div><span><?php echo $value['name']?></span>
                                <?php echo '</a>'; // echo (!empty($value['data']) || $thepage == ($r + 1)) ? '</a>' : '</div>'; ?>
                            </li>

                                <?php
                                }
                            ?>


                        </ul>
                    </div>

                    </nav>
                </div><!--.col-sm-5-->

           <?php /*?> </div>
            <!--.new transit menu-->


            <div class="container-fluid">
                <div class="row"><?php */?>


                    <div class="col-lg-10 col-md-9">
                      <div class="container-fluid">

                        <div class="modal-bodyx">

                            <h1 class="buy_title hidden">Buy Single Marine Transit Policy</h1>
                            <hr class="star-primary hidden">

                            <?php /*?><!-- progressbar -->
                            <ul id="progressbar" class="progressbar-main">
                                <li class="active" data-target="tab-transit">Transit</li>
                                <li data-target="tab-cargo">Cargo</li>
                                <li data-target="tab-quote">Quote</li>
                                <li data-target="tab-details">Policy</li>
                                <li data-target="tab-confirm">Confirm</li>
                                <li data-target="tab-payment">Payment</li>
                            </ul> <?php */?>
                            <div class="clearfix"></div>
                                <!-- Tab panes -->
                                <div class="tab-content tab-content-buy">


                                    <div role="tabpanel" class="tab-pane active" id="tab-transit">
                                        <form id="cargotab_form"> <!--#buy_form-->
                                    	<h4 class="">&nbsp;</h4>


                                         <div class="row">

                                            <div class="col-sm-6 form-group">
                                            	<label for="transitfrom">Transit From *</label>
                                                <select class="form-control" name="transitfrom" id="transitfrom" data-live-search="true">
                                                  <option value="">Select Country</option>
                                                  <?php
                                                  foreach($countries as $r=>$value){
                                                      $price = 0;
													  $referral = '';
                                                      if(isset($country_prices[$value['country_id']])){
                                                          $zone = $country_prices[$value['country_id']] - 1;

														  //zone multiplier
														  if(isset($zone_multiplier[$zone])){
															  $price = $zone_multiplier[$zone];
														  }
                                                      }

													  if(isset($country_referral[$value['country_id']])){
														  $referral = 'yes';
													  }

                                                      echo '<option value="'.$value['country_id'].'" data-price="'.$price.'" data-referral="'.$referral.'" data-iso="'.$value['iso2'].'" data-text="'.$value['short_name'].' ('.$value['iso2'].')"';
                                                      if($transitfrom == $value['country_id']){
                                                          echo ' selected="selected"';
                                                      }
                                                      echo '>'.$value['short_name'].' ('.$value['iso2'].')</option>';

                                                  }?>


                                                </select>

                                            </div>


                                            <div class="col-sm-6 form-group">
                                            	<label for="transitto">Transit To *</label>

                                                <select class="form-control" name="transitto" id="transitto" data-live-search="true">
                                                  <option value="">Select Country</option>
                                                  <?php
                                                  foreach($countries as $r=>$value){
                                                      $price = 0;
													  $referral = '';
                                                      if(isset($country_prices[$value['country_id']])){
                                                          $zone = $country_prices[$value['country_id']] - 1;

														  //zone multiplier
														  if(isset($zone_multiplier[$zone])){
															  $price = $zone_multiplier[$zone];
														  }
                                                      }

													  if(isset($country_referral[$value['country_id']])){
														  $referral = 'yes';
													  }

                                                      echo '<option value="'.$value['country_id'].'" data-price="'.$price.'" data-referral="'.$referral.'" data-iso="'.$value['iso2'].'" data-text="'.$value['short_name'].' ('.$value['iso2'].')"';
                                                      if($transitto == $value['country_id']){
                                                          echo ' selected="selected"';
                                                      }
                                                      echo '>'.$value['short_name'].' ('.$value['iso2'].')</option>';

                                                  }?>

                                                </select>
                                            </div>

                                          </div><!--row-->


                                          <div class="row">

                                              <div class="col-sm-6 form-group">
                                                    <label for="invoice">Insured Value *</label>
                                                    <input type="text" class="form-control" name="invoice" id="invoice" placeholder="Invoice Value" value="<?php echo $invoice; ?>" data-max="<?php echo $max_insured; ?>">
                                              </div>

                                              <div class="col-sm-6 form-group">
                                                <label for="currency">Currency * <i class="fa fa-info-circle text-info" data-toggle="tooltip" data-placement="top" data-title="(*) Not supported on American Express."></i></label>
                                                <select class="form-control" name="currency" id="currency" data-live-search="true" data-country="<?php echo $country_currency?>">
                                                  <option value="">Select Currency</option>
                                                  <?php
                                                  foreach($currencies as $r=>$value){

                                                      echo '<option value="'.$value['currency'].'"';
                                                      if($currency == $value['currency']){
                                                          echo ' selected="selected"';                                                      }
                                                      echo ' data-country="'.substr($value['currency'], 0 , 2).'">'.$value['name'].' ('.$value['currency'].')</option>';

                                                  }?>

                                                </select>
                                              </div>
                                          </div>


                                          <div class="form-group fg-cargo-cat">
                                          	<div class="row">
                                            	<div class="col-sm-12">
                                                    <label>Cargo *</label><br />
                                                    <p><a href="#cargoCatModal" data-toggle="modal" data-controls-modal="cargoCatModal" class="btn btn-primary btn-block">Cargo Description</a></p>

                                                    <select class="form-control hidden" name="cargocat" id="cargocat" data-live-search="true">
                                                      <option value="">Select Cargo</option>
                                                      <?php
                                                      foreach($cargos as $r=>$value){
														$acargo_price = $value['multiplier'];
														$acargo_ref = $value['referral'];
														if(count($the_agency) > 0) {
															if(isset($cargo_prices[$value['id']])) {
																$acargo_price = str_replace(',','', $cargo_prices[$value['id']]);
															}
														}

														  echo '<option value="'.$value['id'].'"';
														  echo ' data-info="'.$acargo_price.','.$acargo_ref.','.$value['uninsurable'].'"';
                                                          if($cargocat == $value['id']){
                                                              echo ' selected="selected"';                                                          }
                                                          echo '>'.$value['hs_code'].' &middot; '.$value['description'].'</option>';

                                                      }?>

                                                    </select>


                                                </div>
                                                <div class="col-sm-12">


                                                    <?php /*?><label>&nbsp;</label><br /><?php */?>
                                                    <div class="genre_inputs" style="display: none;">
                                                        <?php /*?><input type="hidden" name="cargocat" data-info="<?php echo $cargo_price ?>,<?php echo $cargo_max_val?>,<?php echo $bindoption?>" value="<?php echo $cargocat; ?>"/><?php */?>
                                                    </div>

                                                    <div class="well well-sm genre_well" <?php if($cargocat == '') { echo 'style="display: none;"'; }?>>
                                                        <?php /*?><span class="label label-info"><?php echo $this->common->cargo_name($cargocat); ?> <i class="fa fa-times close-gnre"></i></span><?php */?>
                                                        <div class="thumbnail thumbnail-cargo"><button type="button" aria-label="Close" class="close close-cargo"><span aria-hidden="true">&times;</span></button><?php echo $this->common->cargo_name($cargocat); ?></div>
                                                    </div>
                                                </div>
                                            </div>

                                          </div>

                                          <div class="form-group">
                                                <label>Brief Description of Goods *</label><br />
                                                <input type="text" class="form-control input-count-char" name="goods_desc" id="goods_desc" value="<?php echo $goods_desc; ?>" />
                                                <span class="small text-muted pull-right"><?php echo $goods_count; ?> characters remaining.</span>
                                          </div>

                                          <div class="form-group">
                                                <label>Transportation *</label><br />


                                                <div class="the_genres">
                                                    <div class="genre_list gutter-sm">
													  <?php
													  $trans_iall = '';
												      $count = 0;
                                                      foreach($transportations as $r){
														  $price = 0;
														  if(isset($transportation_prices[$count])){
															$price = $transportation_prices[$count];
														  }
                                                    ?>
                                                    <div class="col-xs-6 col-md-3">
                                                    <a href="#" class="thumbnail text-center genre_thumbs <?php if($transmethod == $r) { echo 'active" '; } ?>">


                                                        <span>
															<?php
															//hide icon for last item
															if(($count + 1) != count($transportations)){

																$trans_i = '<i class="'.$trans_icon[$count].' fa-fw"></i>';
																echo $trans_i;
																$trans_iall .= $trans_i;
															}
															else{
																echo $trans_iall;
															}

															 echo '<br>'.$r?>
                                                        </span>

                                                    <div class="checkbox hidden">
                                                      <label>
                                                        <input type="radio" name="transmethod" <?php echo ' data-price="'.$price.' "value="'.$r.'"';
														  if($transmethod == $r) {
																echo 'checked="checked" ';
														  }

														?>

                                                             /><span><?php echo $r?></span>

                                                      </label>
                                                    </div> <!--checkbox-->
                                                    </a>
                                                    </div><!--col-xs-6-->
                                                    <?php $count++; } ?>

                                                    </div>
                                                </div>

                                          </div>
                                          <div class="form-group">
                                                <input type="hidden" name="base_rate" value="<?php echo $the_agency[0]['base_rate']; ?>">
                                                <input type="hidden" name="minimum_premium" value="<?php echo $the_agency[0]['minimum_premium']; ?>">
                                                <input type="hidden" name="cargo_price" value="<?php echo $cargo_price ?>" />
                                                <input type="hidden" name="cargo_max_val" value="<?php echo $cargo_max_val ?>" />
                                                <input type="hidden" name="premium" value="<?php echo $premium ?>" />
                                                <input type="hidden" name="default_deductible" value="<?php echo $selected_deductible ?>" />


                                            	<button class="btn btn-warning pull-right" type="submit">Continue</button>
                                          </div>

                                        </form>

                                    </div><!--tab-transit-->
                                   <?php /*?>
                                    <div role="tabpanel" class="tab-pane" id="tab-cargo">
                                        <form id="cargotab_form">
                                            <h4 class="text-left">&nbsp;</h4>


                                                <a href="#" class="btn btn-default buy_back_btn pull-left">Back</a>
                                            	<button class="btn btn-warning pull-right" type="submit">Continue</button>




                                        </form>
                                    </div><!--tab-cargo--><?php */?>
                                    <div role="tabpanel" class="tab-pane" id="tab-quote">


                                      	<h4 class="text-left">&nbsp;</h4>
                                        <p class="text-center" style="font-size: 16px; color: #333">
                                            Your goods <strong class="cargo_description"></strong><br>
                                            Transit from <strong class="transit_from"></strong> to <strong class="transit_to"></strong> with a Sum Insured of <strong class="insured_value"></strong> and excess of <strong class="excess_value"></strong>

                                        </p>
                                        <div class="row">
                                          <div class="col-lg-8 col-md-7">
                                            <div class="well well-premium text-center">
                                                <h1 style="font-size:60px !important; margin: 30px 0;">23</h1>
                                            </div>

                                            <?php if(count($deductibles) > 1){ ?>
                                            <div class="row">
                                              <div class="col-sm-4 col-lg-3">
                                                <label class="form-control-static">Change Excess</label>

                                              </div>
                                              <div class="col-sm-8 col-lg-9">
                                                <div class="form-group">

                                                    <select name="deductible_dd" class="form-control">
                                                    	<?php
                              													foreach($deductibles as $r=>$value){
                              														echo '<option value="'.$value['rate'].'"';
                              														if($value['rate'] == $selected_deductible){
                              															echo ' selected="selected"';
                              														}
                              														echo '>$'.$value['deductible'].'</option>';
                              													}
                              												?>
                                                    </select>
                                                </div>
                                              </div>
                                            </div>
                                            <?php } ?>



                                          </div>
                                          <div class="col-lg-4 col-md-5">
                                            <a href="#" class="btn btn-link btn-block quote_continue_btn" style="padding: 27px 15px" data-id="<?php echo $this->session->userdata('id')?>">
                                              <span style="font-size: 200%"><strong>BUY NOW</strong></span> <br>
                                              <small>Your Insurance Certificate will be generated immediately</small>
                                            </a>
                                            <div class="row sm-gutter">
                                              <div class="col-sm-6">

                                                <a href="#quoteEmail" data-toggle="modal" data-controls-modal="quoteEmail" class="btn btn-primary btn-sm btn-block quoteaction-button">
                                                  <span>Would you like to email this quote?</span>
                                                </a>
                                              </div>
                                              <div class="col-sm-6">

                                                <a href="#" data-id="<?php echo $this->session->userdata('id')?>" class="save-quote-btn btn btn-primary btn-sm btn-block quoteaction-button">
                                                  <span>Would you like to save this quote and come back to it later?</span>
                                                </a>
                                                <div class="clearfix"></div>
                                              </div>

                                            </div>

                                          </div>
                                        </div>

                                        <?php $policy_doc = (isset($policy_docs[0]['name'])) ? base_url().'uploads/policyd/'.$policy_docs[0]['name'] : '#' ?>

                                        <h3 class="text-left">The Cover</h3>
                                        <p style="color: #333">Your policy will cover physical loss or damage to your cargo from an external cause on an “All Risks” basis.  Cover is based on the latest version of Institute Cargo Clauses and includes:-</p>
                                        <div class="row">
                                          <div class="col-sm-6">
                                              <i class="fa fa-check"></i> Warehouse to warehouse cover <br>
                                              <i class="fa fa-check"></i> Theft and Pilferage <br>
                                              <i class="fa fa-check"></i> General Average <br>
                                          </div>
                                          <div class="col-sm-6">
                                              <i class="fa fa-check"></i> Non-delivery  <br>
                                              <i class="fa fa-check"></i> Breakage  <br>
                                              <i class="fa fa-check"></i> There are many other additional benefits. Refer to the full <a href="<?php echo $policy_doc; ?>" target="_blank">policy wording</a>

                                          </div>
                                        </div>

                                        <h3>Cover does not include </h3>
                                        <div class="row">
                                          <div class="col-sm-6">
                                              <i class="fa fa-close pull-left"></i> Wear and Tear  <br>
                                              <i class="fa fa-close pull-left"></i> Consequential Loss <br>
                                              <i class="fa fa-close pull-left"></i> Liabilities
                                          </div>
                                          <div class="col-sm-6">
                                              <i class="fa fa-close pull-left"></i> Damage caused by insufficient packing  <br>
                                              <i class="fa fa-close pull-left"></i> Inherent vice, delay, ordinary leakage, ordinary loss in weight
                                          </div>
                                          <div class="col-sm-12">
                                              There are other exclusions that you should know about. Please refer to the full <a href="<?php echo $policy_doc ?>" target="_blank">policy wording</a>, but if still in doubt <a href="<?php echo base_url().'contactus'; ?>">Get in touch</a>



                                          </div>
                                        </div>





                                        <?php
                    										$referral_comment = $this->session->userdata('referral_comment');
                    										if($referral_comment != ''){
                    											echo '<blockquote><p>'.$referral_comment.'</p><footer>- comment from <cite title="Source Title">Admin</cite></footer></blockquote>';
                    										}

                                        // if(count($policy_docs) > 0) {
                                        //     echo '<p class="lead text-right policydoc"><a href="'.base_url().'uploads/policyd/'.$policy_docs[0]['name'].'" target="_blank" class="btn btn-link"><i class="fa fa-file-pdf-o"></i> View Policy Document</a> <a href="'.base_url().'uploads/policyd/'.$policy_docs[0]['name'].'" target="_blank" class="btn btn-link"><i class="fa fa-download"></i> Download Policy Document</a></p>';
                                        //
                                        // }
                                        ?>

                                        <div class="form-group" style="margin-top: 15px;">
                                            <a href="#" class="btn btn-default buy_back_btn pull-left">Back</a>
                                            <!-- <button class="btn btn-warning quote_continue_btn pull-right" data-id="<?php echo $this->session->userdata('id')?>">Continue</button> -->
                                        </div>

                                    </div><!--tab-quote-->


                                    <div role="tabpanel" class="tab-pane" id="tab-details">
                                    	<h4>&nbsp;</h4>

                                        <div class="well">
                                        	<form id="cargo_details_form">

                                                <h4>Cargo Details</h4>

                                                <div class="row">

                                                    <div class="col-sm-6 form-group">
                                                        <label for="shipment_date">Date of Shipment *</label>
                                                        <input type="text" class="form-control input-date" id="shipment_date" name="shipment_date" value="<?php echo $shipment_date;?>" />
                                                    </div>

                                                    <div class="col-sm-6 form-group">
                                                        <label for="vessel_name">Name of Vessel/Aircraft</label>
                                                        <input type="text" class="form-control" id="vessel_name" name="vessel_name" value="<?php echo $vessel_name;?>" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6 form-group">
                                                        <label for="portloading">Port of Loading</label><br />
                                                        <a href="#" class="btn btn-default btn-block select_port_btn <?php if($transitfrom == '') { echo 'disabled'; } ?>" <?php if($portloading != '') { echo 'style="display: none;"'; } ?> data-type="portloading">Select Port of Loading</a>

                                                        <input type="hidden" name="portloading" id="portloading" value="<?php echo $portloading; ?>">

                                                        <div class="btn-group" <?php if($portloading == '') { echo 'style="display: none;"'; } ?>>
                                                          <a href="#" class="btn btn-default select_port_btn" data-type="portloading" ><?php if(is_numeric($portloading)) { echo $this->common->selectedport($portloading); } else { echo $portloading; }?></a>
                                                          <a href="#" class="btn btn-default remove_selected_btn"><i class="fa fa-times-circle"></i></a>
                                                        </div>

                                                    </div>

                                                    <div class="col-sm-6 form-group">
                                                        <label for="portdischarge">Port of Discharge</label><br />
                                                        <a href="#" class="btn btn-default btn-block select_port_btn <?php if($transitto == '') { echo 'disabled'; } ?>" <?php if($portdischarge != '') { echo 'style="display: none;"'; } ?> data-type="portdischarge">Select Port of Discharge</a>

                                                        <input type="hidden" name="portdischarge" id="portdischarge" value="<?php echo $portdischarge; ?>">

                                                        <div class="btn-group" <?php if($portdischarge == '') { echo 'style="display: none;"'; } ?>>
                                                          <a href="#" class="btn btn-default select_port_btn" data-type="portdischarge"><?php if(is_numeric($portdischarge)) { echo $this->common->selectedport($portdischarge); } else { echo $portdischarge; }?></a>
                                                          <a href="#" class="btn btn-default remove_selected_btn"><i class="fa fa-times-circle"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">

                                                    <div class="col-sm-6 form-group">
                                                        <label for="transit_begins">Address where transit begins</label>
                                                        <input type="text" class="form-control input_location" id="transit_begins" name="transit_begins" placeholder="Enter address where transit begins" value="<?php echo $transit_begins;?>" required="required"/>
                                                        <input type="hidden" name="lat" value="<?php echo $this->session->userdata('lat') ?>"/>
                                                        <input type="hidden" name="lng" value="<?php echo $this->session->userdata('lng') ?>"/>

                                                    </div>

                                                    <div class="col-sm-6 form-group">
                                                        <label for="transit_ends">Address where transit ends</label>
                                                        <input type="text" class="form-control input_location" id="transit_ends" name="transit_ends" placeholder="Enter address where transit ends" value="<?php echo $transit_ends;?>" required="required"/>
                                                        <input type="hidden" name="lat" value="<?php echo $this->session->userdata('lat') ?>"/>
                                                        <input type="hidden" name="lng" value="<?php echo $this->session->userdata('lng') ?>"/>

                                                    </div>
                                                </div>


                                            </form>
                                        </div>


                                        <div class="select_consignee <?php if(count($mycustomers) == 0) { echo 'hidden'; } ?>">
                                        	<form class="mycustomer_form">
                                                <div class="well form-group">
                                                    <label>Select Existing Consignor</label><br />

                                                    <select name="mycustomers" class="form-control">
                                                        <option value="">Select Consignor</option>
                                                        <?php
                                                        foreach($mycustomers as $r=>$value){
                                                            $details = unserialize($value['details']);
                                                            echo '<option value="'.$value['id'].'">'.$value['name'].'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>

                                                <div class="form-group hidden">
                                                    <a href="#" class="btn btn-info" onclick="$('.new_consignee').removeClass('hidden'); $(this).closest('.select_consignee').addClass('hidden');">New Consignee</a>
                                                    <button type="submit" class="btn btn-warning pull-right">Continue</button>

                                                </div>


                                            </form>
                                        </div><!--agent mycustomers-->


                                        <div class="well">
                                    	<h4>Consignor's Details</h4>
                                        <form id="single_purchase_form" class="single_purchase_form">

                                         <div class="row">

                                            <div class="col-sm-12 form-group">
                                                <div class="wellx">
                                                    <h3 class="hidden">Single Purchase</h3>
                                                    <div class="row gutter-md">
                                                        <div class="col-md-6 form-group">
                                                            <label>First Name</label>
                                                            <input type="text" class="form-control" name="first" placeholder="First Name" value="<?php echo ($this->session->userdata('customer_type') == 'Y') ? $this->session->userdata('first_name') : ''?>"/>
                                                        </div>
                                                        <div class="col-md-6 form-group">
                                                            <label>Last Name</label>
                                                            <input type="text" class="form-control" name="last" placeholder="Last Name" value="<?php echo ($this->session->userdata('customer_type') == 'Y') ? $this->session->userdata('last_name') : '' ?>"/>
                                                        </div>

                                                    </div><!--name row-->

                                                    <div class="row gutter-md">
                                                        <div class="col-sm-12 form-group">
                                                            <label>Business Name</label>
                                                            <input type="text" class="form-control" name="bname" placeholder="Business Name" value="<?php echo ($this->session->userdata('customer_type') == 'Y') ? $this->session->userdata('business_name') : '' ?>"/>
                                                        </div>
                                                    </div><!--bname row-->

                                                    <div class="form-group">
                                                        <label>Is address the same address as where transit begins?</label><br>
                                                        <label class="radio-inline">
                                                          <input type="radio" name="same_transit_begins" value="y"> Yes
                                                        </label>
                                                        <label class="radio-inline">
                                                          <input type="radio" name="same_transit_begins" value="n"> No
                                                        </label>
                                                    </div>



                                                    <div class="row gutter-md">
                                                        <div class="col-sm-12 form-group">
                                                            <label>Address</label>
                                                            <input type="text" class="form-control input_location" name="address" placeholder="Enter address" value="<?php echo ($this->session->userdata('customer_type') == 'Y') ? $this->session->userdata('address') : '' ?>"/>
                                                            <input type="hidden" name="lat" value="<?php echo $this->session->userdata('lat') ?>"/>
                                                            <input type="hidden" name="lng" value="<?php echo $this->session->userdata('lng') ?>"/>

                                                        </div>
                                                    </div><!--bname row-->

                                                    <div class="row gutter-md">
                                                        <div class="col-sm-12 form-group">
                                                            <label>Email</label>
                                                            <input type="text" class="form-control" name="email" placeholder="Email" value="<?php echo ($this->session->userdata('customer_type') == 'Y') ? $this->session->userdata('email') : '' ?>"/>
                                                        </div>
                                                    </div><!--bname row-->

                                                    <div class="row gutter-md">

                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Country Calling Code</label><br>
                                                                <select class="country_code_dd form-control" name="country_code" data-live-search="true">
                                                                    <option value="" data-code="">Select..</option>
                                                                    <?php
                                                                        foreach($countries as $r=>$mc){
                                                                            echo '<option value="'.$mc['country_id'].'" data-code="'.$mc['calling_code'].'"';
																			if($this->session->userdata('country_id') == $mc['country_id'] && ($this->session->userdata('customer_type') == 'Y')){
																				echo ' selected="selected"';
																			}


                                                                            echo '>'.$mc['short_name'].' (+'.$mc['calling_code'].')</option>';
                                                                        }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <div class="form-group mobile_input">
                                                                <label class="control-label">Contact Number</label>
                                                                  <input type="text" class="form-control" placeholder="Contact Number" name="mobile" value="<?php echo ($this->session->userdata('customer_type') == 'Y') ? $this->session->userdata('calling_digits') : '' ?>">


                                                                <?php /*?><div class="input-group">
                                                                  <span class="input-group-addon addon-shortcode">+<?php echo ($this->session->userdata('customer_type') == 'Y') ? $this->session->userdata('calling_code') : '' ?></span>
                                                                </div><?php */?>
                                                                <input type="hidden" name="country_short" value=""/>
                                                            </div>
                                                        </div>

                                                    </div><!--phone number-->


                                                </div>

                                            </div>


                                            <div class="col-sm-12 form-group hidden">

                                                <a href="#" class="btn btn-default pull-left buy_back_btn">Back</a>
                                                <button class="btn btn-warning pull-right" type="submit">Continue</button>

                                            </div>

                                        </div>

                                        </form><!--hide form if logged in-->

                                        <hr style="margin-top: 0px;" />
                                    	<h4>Consignee's Details</h4>
                                        <form id="consignee_form" class="single_purchase_form">
                                         <input type="hidden" name="type" value="consignee" />

                                         <div class="row">

                                            <div class="col-sm-12 form-group">
                                                <div class="wellx">
                                                    <h3 class="hidden">Single Purchase</h3>
                                                    <div class="row gutter-md">
                                                        <div class="col-md-6 form-group">
                                                            <label>First Name</label>
                                                            <input type="text" class="form-control" name="first" placeholder="First Name" value=""/>
                                                        </div>
                                                        <div class="col-md-6 form-group">
                                                            <label>Last Name</label>
                                                            <input type="text" class="form-control" name="last" placeholder="Last Name" value=""/>
                                                        </div>

                                                    </div><!--name row-->

                                                    <div class="row gutter-md">
                                                        <div class="col-sm-12 form-group">
                                                            <label>Business Name</label>
                                                            <input type="text" class="form-control" name="bname" placeholder="Business Name" value=""/>
                                                        </div>
                                                    </div><!--bname row-->

                                                    <div class="form-group">
                                                        <label>Is address the same address as where transit ends?</label><br>
                                                        <label class="radio-inline">
                                                          <input type="radio" name="same_transit_ends" value="y"> Yes
                                                        </label>
                                                        <label class="radio-inline">
                                                          <input type="radio" name="same_transit_ends" value="n"> No
                                                        </label>
                                                    </div>


                                                    <div class="row gutter-md">
                                                        <div class="col-sm-12 form-group">
                                                            <label>Address</label>
                                                            <input type="text" class="form-control input_location" name="address" placeholder="Enter address" value=""/>
                                                            <input type="hidden" name="lat" value=""/>
                                                            <input type="hidden" name="lng" value=""/>

                                                        </div>
                                                    </div><!--bname row-->

                                                    <div class="row gutter-md">
                                                        <div class="col-sm-12 form-group">
                                                            <label>Email</label>
                                                            <input type="text" class="form-control" name="email" placeholder="Email" value=""/>
                                                        </div>
                                                    </div><!--bname row-->

                                                    <div class="row gutter-md">

                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Country Calling Code</label><br>
                                                                <select class="country_code_dd form-control" name="country_code" data-live-search="true">
                                                                    <option value="" data-code="">Select..</option>
                                                                    <?php
                                                                        foreach($countries as $r=>$mc){
                                                                            echo '<option value="'.$mc['country_id'].'" data-code="'.$mc['calling_code'].'">'.$mc['short_name'].' (+'.$mc['calling_code'].')</option>';
                                                                        }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <div class="form-group mobile_input">
                                                                <label class="control-label">Contact Number</label>
                                                                  <input type="text" class="form-control" placeholder="Contact Number" name="mobile" value="">


<?php /*?>                                                                <div class="input-group">
                                                                  <span class="input-group-addon addon-shortcode">+</span>
                                                                </div>
<?php */?>                                                                <input type="hidden" name="country_short" value=""/>
                                                            </div>
                                                        </div>

                                                    </div><!--phone number-->

                                                </div>



                                            </div>

                                            <?php /*?><div class="col-sm-12 form-group hidden">

                                                <a href="#" class="btn btn-default pull-left buy_back_btn">Back</a>
                                                <button class="btn btn-warning pull-right">Continue</button>

                                            </div><?php */?>


                                        </div>
                                        </form><!--hide form if logged in-->
                                        </div>


                                        <div class="form-group">
                                            <a href="#" class="btn btn-default pull-left buy_back_btn">Back</a>
                                            <button class="btn btn-warning pull-right policy_btn">Continue</button>
                                        </div>

                                    <?php /*?></div><!--tab-details-->
                                    <div role="tabpanel" class="tab-pane" id="tab-consignee"><?php */?>




                                        <?php /*?><div class="alert alert-info new_consignee <?php if(count($mycustomers) > 0) { echo 'hidden'; } ?>">
                                        	<p>Same address as the Consignor?</p><hr />
                                        	<a href="#" class="btn btn-info a_consignee" data-type="yes">Yes</a>
                                        	<a href="#" class="btn btn-default a_consignee" data-type="no">No</a>
                                        </div><?php */?>

                                    </div><!--tab-consignee-->


                                    <?php /*?><div role="tabpanel" class="tab-pane" id="tab-confirm">
                                    	<h4 class="text-left">Confirm</h4>

                                        <div class="confirm-holder">

                                        </div>


                                        <div class="form-group">
                                            <a href="#" class="btn btn-default buy_back_btn pull-left">Back</a>

                                            <button class="btn btn-warning quote_continue_btn pull-right" data-id="<?php echo $this->session->userdata('id')?>" style="margin-left: 5px;">Continue</button>

                                            <button class="btn btn-warning pull-right save_quote_transaction_btn <?php echo ($this->session->userdata('id') == '') ? 'hidden' : '' ?>" type="button">Save</button>

                                        </div>

                                    </div><!--tab-confirm-->
                                      <?php */?>

                                    <div role="tabpanel" class="tab-pane" id="tab-payment">
                                    	<h4 class="text-left">&nbsp;<?php  //echo ($this->session->userdata('customer_type') == 'N') ? 'Send' : 'Payment' ?></h4>

                                        <div class="row <?php echo ($this->session->userdata('customer_type') == 'N') ? 'hidden' : '' ?>">
                                            <div class="col-md-10">
                                                <div class="well text-left">

                                                    <div class="form-group">
                                                        <label>Agent Code (Optional)</label>
                                                        <input
                                                            type="tel"
                                                            class="form-control"
                                                            name="agent_code"
                                                            placeholder=""
                                                        />
                                                    </div>
                                                    <div class="form-group">
                                                        <button class="btn btn-default validate-agentcode-btn">Validate</button>
                                                        <a class="btn-link validate-result-text"></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <form role="form" id="payment-form" class="<?php if($this->session->userdata('stripe_id') != '' || $this->session->userdata('customer_type') == 'N') { echo 'hidden'; }?>">


                                <div class="row">
                                    <!-- You can make it whatever width you want. I'm making it full width
                                         on <= small devices and 4/12 page width on >= medium devices -->
                                    <div class="col-md-10">
                                        <div class="well">
                                            <!-- CREDIT CARD FORM STARTS HERE -->
                                            <div class="panelx credit-card-box">

                                                <div class="panel-bodyx">
                                                    <p class="text-muted text-left">
                                                        <img class="img-responsive pull-right" src="<?php echo base_url().'assets/img/'?>accepted_c22e0.png">
                                                    </p>


                                                    <div class="row hidden">
                                                        <div class="col-xs-12">
                                                            <div class="form-group">
                                                                <label for="couponCode">Card Holder's Name</label>
                                                                <input type="text" class="form-control" name="name" placeholder="Card Holder's Name" autofocus/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="cardNumber">Card number</label>
                                                                <input
                                                                    type="tel"
                                                                    class="form-control"
                                                                    name="cardNumber"
                                                                    placeholder="Valid Card Number"
                                                                    autocomplete="cc-number"
                                                                    required autofocus
                                                                />
                                                                <?php /*?><div class="input-group">
                                                                    <input
                                                                        type="tel"
                                                                        class="form-control"
                                                                        name="cardNumber"
                                                                        placeholder="Valid Card Number"
                                                                        autocomplete="cc-number"
                                                                        required autofocus
                                                                    />
                                                                    <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                                                </div><?php */?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-7">
                                                            <div class="form-group">
                                                                <label for="cardExpiry">Expiration Date</label>
                                                                <input
                                                                    type="tel"
                                                                    class="form-control"
                                                                    name="cardExpiry"
                                                                    placeholder="MM / YY"
                                                                    autocomplete="cc-exp"
                                                                    required
                                                                />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="form-group">
                                                                <label for="cardCVC">CV Code</label>
                                                                <input
                                                                    type="tel"
                                                                    class="form-control"
                                                                    name="cardCVC"
                                                                    placeholder="CVC"
                                                                    autocomplete="cc-csc"
                                                                    required
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                        <!-- CREDIT CARD FORM ENDS HERE -->

                                       	<div class="payment_completed hidden">
                                            <div style="margin-top: 50px;" class="alert alert-success text-center">Payment method added. You are completely ready to buy transit insurance. </div>

                                        </div>

                                    </div>
                                </div><!--payment form-->

                                        </div><!--well-->

                                        <div class="row">
                                            <div class="col-xs-12">
                                            	<input type="hidden" name="from_buy" value="yes" />
                                                <a href="#" class="btn btn-default buy_back_btn pull-left">Back</a>
                                                <button class="btn btn-warning pull-right" type="submit">Submit</button>
                                            </div>
                                        </div>
                                        <div class="row" style="display:none;">
                                            <div class="col-xs-12">
                                                <p class="payment-errors text-danger"></p>
                                            </div>
                                        </div>

                                        </form>


                                        <div class="payment_form_buy <?php if($this->session->userdata('stripe_id') == '' || $this->session->userdata('customer_type') == 'N') { echo 'hidden';}?>">

                                            <div class="row">
                                                <div class="col-xs-12">
                                                	<div class="alert alert-success">
                                                    	<i class="fa fa-info-circle text-success"></i> You have already added your payment method. You are ready to Buy Single Marine Transit Policy.
                                                    </div>

                                                    <a href="#" class="btn btn-default buy_back_btn pull-left">Back</a>
                                                    <button class="btn btn-warning payment_logged_btn pull-right" type="submit">Submit</button>
                                                </div>
                                            </div>
                                        </div>


                                    	<?php if($this->session->userdata('customer_type') == 'N') { ?>

                                        	<div class="alert alert-info">
                                            	<a href="<?php echo base_url().'landing/quote_preview' ?>" target="_blank">Preview</a> quote in PDF.
                                            </div>

                                            <object data="<?php echo base_url().'landing/quote_preview' ?>" type="application/pdf" style="width:100%; height: 350px;">
                                                <embed src="<?php echo base_url().'landing/quote_preview' ?>" type="application/pdf" />
                                            </object>


                                            <div class="form-group">
                                                <a href="<?php echo base_url().'landing/quote_preview' ?>" class="btn btn-default buy_back_btn pull-left">Back</a>
                                                <button class="btn btn-warning pull-right agent_save_btn" data-id="<?php echo $this->session->userdata('id')?>">Email Quote to Consignor</button>
                                            </div>

                                        <?php } ?>


                                    </div><!--tab-payment-->


                                    <div role="tabpanel" class="tab-pane" id="tab-done">
                                    	<h4>&nbsp;</h4>

                                      <div class="done_message">


                                      </div>
                                    </div><!--tab-done-->


                                </div><!--tabs-->

                        </div><!--modal-body-->
                    </div>
                </div>
            </div>

        </div>
    </div><!--portfolio-modal-->


    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="portModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="modal-body">

                            <h1>Port Codes</h1>
                            <hr class="star-primary">


                            	<div class="row gutter-md">
                                	<div class="col-sm-6">
                                    	<div class="well">
                                            <label>Search Port Codes</label>
                                            <input type="search" class="form-control input-search" id="input-search" placeholder="Search port codes.." >
                                        </div>
                                    </div>
                                	<div class="col-sm-6">
                                    	<div class="well">
                                            <label>Not in the list?</label><br />
                                            <div class="row gutter-sm">
                                            	<div class="col-sm-8">
                                                  <input type="text" class="form-control" placeholder="Enter port code or name">
                                                </div>
                                            	<div class="col-sm-4">
                                                    <button class="btn btn-default custom_port_btn" type="button">Submit</button>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            <form class="radio_list_form">
                            	<input type="hidden" name="type" value="" />
                            	<input type="hidden" name="field" value="" />
                                <div class="form-group">
                                    <div class="the_genres">
                                    	<div class="row genre_list">

                                            <div class="col-xs-6 col-md-4">
                                                <a href="#" class="thumbnail text-center genre_thumbs">
                                                <span></span>
                                                <div class="checkbox hidden">
                                                  <label>
                                                    <input type="radio" name="cargocat" /><span></span>
                                                  </label>
                                                </div> <!--checkbox-->
                                                </a>
                                            </div><!--col-xs-6-->

                                        </div>
                                    </div>

                                </div>

                            </form>


                        </div><!--modal-body-->
                    </div>
                </div>
            </div>

        </div>
    </div><!--portfolio-modal-->

    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="cargoCatModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="modal-body">

                            <h1>Cargo</h1>
                            <hr class="star-primary">

                            <div class="well">
                                <input type="search" class="form-control input-search-table" placeholder="Enter HS Code or cargo description.." >
                            </div>


                            <table class="table table-hover mydataTbx clickable-cargo-table">
                                <thead>
                                    <tr>
                                        <th width="10%">HS Code</th>
                                        <th width="90%">Description</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php
								foreach($cargos as $r=>$value){
                                    $acargo_price = $value['multiplier'];
                                    $acargo_ref = $value['referral'];
                                    if(count($the_agency) > 0) {
                                        if(isset($cargo_prices[$value['id']])) {
                                            $acargo_price = str_replace(',','', $cargo_prices[$value['id']]);
                                        }
									}
								?>
                                    <tr <?php echo 'data-value="'.$value['id'].'" data-price="'.$acargo_price.'" data-max="'.$acargo_ref.'" data-bindoption="'.$value['uninsurable'].'"'; if($cargocat == $value['id']){ echo ' class="active"';  } ?> data-text="<?php echo $value['hs_code'].' '.$value['description']?>" title="click to select">
                                        <td><?php echo $value['hs_code'] ?></td>
                                        <td><?php echo $value['description'] ?></td>
                                    </tr>

                                <?php } ?>


                                </tbody>
                            </table>
                            <?php /*?><form id="filter_genre_form">
                            	<input type="hidden" name="id" value="<?php echo $this->session->userdata('id')?>" />
                                <div class="form-group">
                                    <div class="the_genres">
                                    	<div class="row genre_list">
                                        <?php foreach($cargos as $r=>$value){
											$acargo_price = $value['multiplier'];
											$acargo_ref = $value['referral'];
											if(count($the_agency) > 0) {
												if(isset($cargo_prices[$value['id']])) {
													$acargo_price = str_replace(',','', $cargo_prices[$value['id']]);
												}
//												$acargo_max = 0;
//												if(isset($cargo_max[$value['id']])) {
//													$acargo_max = str_replace(',','',$cargo_max[$value['id']]);
//												}
											}

										?>
                                        <div class="col-xs-6 col-md-4">
                                        <a href="#" class="thumbnail text-center genre_thumbs <?php if($cargocat == $value['id']){ echo 'active'; } ?>" title="<?php echo $value['description']?>">
                                        <span><?php echo (strlen($value['description']) > 43) ? $value['hs_code'].' &middot; '.substr($value['description'],0 ,40).'...' : $value['hs_code'].' &middot; '.$value['description']; ?></span>
                                        <span class="hidden"><?php echo $value['description']; ?></span>
                                        <div class="checkbox hidden">
                                          <label>
                                            <input type="radio" name="cargocat" <?php echo 'value="'.$value['id'].'" data-price="'.$acargo_price.'" data-max="'.$acargo_ref.'" data-bindoption="'.$value['uninsurable'].'" ';
												if($cargocat == $value['id']){
													echo 'checked="checked"';
												} ?>

												 /><span><?php echo (strlen($value['description']) > 43) ? $value['hs_code'].' &middot; '.substr($value['description'],0 ,40).'...' : $value['hs_code'].' &middot; '.$value['description']; ?></span>

                                          </label>
                                        </div> <!--checkbox-->
                                        </a>
                                        </div><!--col-xs-6-->
                                        <?php } ?>

                                        </div>
                                    </div>

                                </div>

                                <div class="form-group hidden">
                                	<button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="#" class="btn btn-default" onclick="$(this).closest('form').find('input').prop('checked',false);$(this).closest('form').find('input').closest('a').removeClass('active');">Reset</a>
                                </div>
                            </form><?php */?>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!--portfolio-modal-->




    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="referCountryModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="modal-body">

                            <h1>Referral Required</h1>
                            <hr class="star-primary">


                            <form class="single_purchase_form">
                             <input type="hidden" name="type" value="consignor" />
                             <div class="row">

                                <div class="col-sm-12 form-group">
                                    <div class="well">
                                        <h3 class="hidden">Single Purchase</h3>
                                        <div class="row gutter-md">
                                            <div class="col-md-6 form-group">
                                                <label>First Name</label>
                                                <input type="text" class="form-control" name="first" placeholder="First Name" value="<?php echo $this->session->userdata('first_name') ?>"/>
                                                <input type="hidden" name="refer_country" value="yes" />
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <label>Last Name</label>
                                                <input type="text" class="form-control" name="last" placeholder="Last Name" value="<?php echo $this->session->userdata('last_name') ?>"/>
                                            </div>

                                        </div><!--name row-->

                                        <div class="row gutter-md">
                                            <div class="col-sm-12 form-group">
                                                <label>Business Name</label>
                                                <input type="text" class="form-control" name="bname" placeholder="Business Name" value="<?php echo $this->session->userdata('business_name') ?>"/>
                                            </div>
                                        </div><!--bname row-->

                                        <div class="row gutter-md">
                                            <div class="col-sm-12 form-group">
                                                <label>Address</label>
                                                <input type="text" class="form-control input_location" name="address" placeholder="Enter address" value="<?php echo $this->session->userdata('address') ?>"/>
                                                <input type="hidden" name="lat" value="<?php echo $this->session->userdata('lat') ?>"/>
                                                <input type="hidden" name="lng" value="<?php echo $this->session->userdata('lng') ?>"/>

                                            </div>
                                        </div><!--bname row-->

                                        <div class="row gutter-md">
                                            <div class="col-sm-12 form-group">
                                                <label>Email</label>
                                                <input type="text" class="form-control" name="email" placeholder="Email" value="<?php echo $this->session->userdata('email') ?>"/>
                                            </div>
                                        </div><!--bname row-->

                                        <div class="row gutter-md">

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Country Calling Code</label><br>
                                                    <select class="country_code_dd form-control" name="country_code" data-live-search="true">
                                                        <option value="" data-code="">Select..</option>
                                                        <?php
                                                            foreach($countries as $r=>$mc){
                                                                echo '<option value="'.$mc['country_id'].'" data-code="'.$mc['calling_code'].'"';
                                                                if($this->session->userdata('country_id') == $mc['country_id']){
                                                                    echo ' selected="selected"';
                                                                }


                                                                echo '>'.$mc['short_name'].' (+'.$mc['calling_code'].')</option>';
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group mobile_input">
                                                    <label class="control-label">Contact Number</label>
                                                    <input type="text" class="form-control" placeholder="Contact Number" name="mobile" value="<?php echo $this->session->userdata('calling_digits') ?>">
                                                    <?php /*?><div class="input-group">
                                                      <span class="input-group-addon addon-shortcode">+<?php echo $this->session->userdata('calling_code') ?></span>
                                                      <input type="text" class="form-control" placeholder="Contact Number" name="mobile" value="<?php echo $this->session->userdata('calling_digits') ?>">
                                                    </div><?php */?>
                                                    <input type="hidden" name="country_short" value=""/>
                                                </div>
                                            </div>

                                        </div><!--phone number-->


                                    </div>

                                </div>


                                <div class="col-sm-12 form-group">
                                    <button class="btn btn-warning pull-right" type="submit">Continue</button>
                                </div>

                            </div>
                            </form><!--hide form if logged in-->



                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!--portfolio-modal-->



    <!-- Modal -->
    <div class="modal fade" id="checkSingleModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Would you like to register to save your details and purchase history?</h4>
          </div>
          <div class="modal-body">
          	<div class="row gutter-md">

            	<div class="col-sm-6" style="margin-bottom: 15px;">
                	<a href="#" class="btn btn-primary btn-lg btn-block buy_thanks_btn" style="padding: 50px 0 50px 0">
                    	<span >No thanks, continue with purchase</span>
                    </a>
                </div>

            	<div class="col-sm-6" style="margin-bottom: 15px;">
                	<a href="#" class="btn btn-info btn-lg btn-block buy_regnow_btn" style="padding: 50px 0 50px 0">
                    	<span>Yes please! Register me now</span>

                    </a>
                </div>
            </div>

          </div>
        </div>
      </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="quoteEmail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Send Quote</h4>
          </div>
          <form id="sendquote_form">
              <div class="modal-body">
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" value="" placeholder="Email" />
                    </div>
              </div>
              <div class="modal-footer">
                <a class="btn btn-default" data-dismiss="modal">Cancel</a>
                <button type="submit" class="btn btn-primary">Send</button>
              </div>
          </form>

        </div>
      </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="quoteSave" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

          <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4>Please sign in to save quote.</h4>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-primary login_btn">Sign In</button>
          </div>
        </div>
      </div>
    </div>







  <ul class="map-details hidden">
    <li>Location: <span data-geo="location"></span></li>
    <li>Route: <span data-geo="route"></span></li>
    <li>Street Number: <span data-geo="street_number"></span></li>
    <li>Postal Code: <span data-geo="postal_code"></span></li>
    <li>Locality: <span data-geo="locality"></span></li>
    <li>Country Code: <span data-geo="country_short"></span></li>
    <li>State: <span data-geo="administrative_area_level_1"></span></li>
  </ul>





    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>


    <script src="<?php echo base_url().'assets/meemeep/'?>static/Bol81ivSMjD2NR3Vu77yR2L7z383yh2y6v11UMz3FFj.js"></script>
    <script src="//cdn.jsdelivr.net/jquery.slick/1.5.9/slick.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.7.6/jquery.fullPage.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/TweenMax.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/plugins/ScrollToPlugin.min.js"></script>
    <script src="<?php echo base_url().'assets/meemeep/'?>static/DaFWFciF2bzJSz2xswXpCo4PCGA9oe0e5PbuORmHOSr.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>


    <script src="<?php echo base_url().'assets/meemeep/'?>lib/bootstrap/bootstrap.js"></script>



    <!-- Plugin JavaScript -->
	<script src="https://maps.googleapis.com/maps/api/js?libraries=places&amp;key=AIzaSyDVL8WaKGrSPhJ7ZY8XHrJeWascHtNA0qc"></script>
    <script src="<?php echo base_url() ?>assets/plugins/geocomplete/jquery.geocomplete.js"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/dataTables/js/jquery.dataTables.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/dataTables/js/dataTables.bootstrap.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/dataTables/js/datatables-bs3.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/validate/jquery.validate.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/validate/additional-methods.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/bootbox/bootbox.js"></script>


    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/jquery.form/jquery.form.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/datetimepicker/dependencies/moment.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/bs-switch/js/bootstrap-switch.min.js"></script>


    <!-- Plugin JavaScript -->
	<script src="<?php echo base_url() ?>assets/plugins/pwstrength/pwstrength-bootstrap.js"></script>


    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/silviomoreto-bootstrap-select/js/bootstrap-select.min.js"></script>

	<?php if((($this->uri->segment(2) == 'profile' && $this->session->userdata('stripe_id') == '') || ($this->uri->segment(1) == 'buy' || $this->uri->segment(1) == '')) && $this->session->userdata('customer_type') != 'N') { ?>
    <!-- Plugin JavaScript -->
	<script type="text/javascript" src="<?php echo base_url()?>assets/plugins/validate/jquery.payment.min.js"></script>
    <!-- If you're using Stripe for payments -->
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <?php } ?>

	<?php if($this->uri->segment(1) == 'settings' && $this->uri->segment(2) == 'profile') { ?>
    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/cropper/js/cropper.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/cropper/js/crop-avatar.js"></script>
	<?php } ?>

    <script src="<?php echo base_url() ?>assets/js/script.js"></script>

</body>

</html>
