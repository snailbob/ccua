<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    /***************************
    landing_view
    ***************************/
    public function index()
    {

        $data = array(
            'view' => 'landing_view'
        );

        $this->load->view('includes/view', $data);
    }


    /***************************
    verify_user
    ***************************/
    public function verify_user()
    {
        $id  = $this->uri->segment(3);
        $sid = $this->uri->segment(4);
        $db  = $this->uri->segment(5);

        $message    = '<i class="fa fa-check-circle"></i> Email successfully verified. You can now login to your account.';
        $refer_from = '';
        if (isset($_GET['refer_from'])) {
            $refer_from = '?refer_from=buy_form';
            $message    = '<i class="fa fa-check-circle"></i> Email successfully verified. Please log into your account to complete your purchase.';
        }
        if (md5($id) != $sid) {
            echo '<p style="text-align: center; padding: 75px; font-size: 18px; color: red;">Link invalid.</p>';
            return false;
        }

        $this->master->updateRecord($db, array(
            'enabled' => 'Y',
            'status' => 'Y'
        ), array(
            'id' => $id
        ));
        $this->session->set_flashdata('info', $message);
        redirect(base_url() . $refer_from . '#login');
    }




    /***************************
    confirm_customer
    ***************************/
    public function confirm_customer()
    {
        $id         = $this->uri->segment(3);
        $md5email   = $this->uri->segment(4);
        $time_zone     = $this->master->getRecords('time_zone');

        $agency     = $this->master->getRecords('customers', array(
            'id' => $id
        ));
        //get fields
        $the_fields = array();
        if (count($agency) > 0) {
            foreach ($agency[0] as $r => $value) {
                if ($r == 'name' || $r == 'email') {
                    $the_fields[] = $r;
                }
            }
        }


        $data = array(
            'view' => 'customer_verify_view',
            'title' => '&nbsp;',
            'agency' => $agency,
            'time_zone' => $time_zone,
            'the_fields' => $the_fields
        );
        $this->load->view('includes/view', $data);


    }

    /***************************
    confirm_quote
    ***************************/
    public function confirm_quote()
    {
        $user_id  = $this->uri->segment(3);
        $id       = $this->uri->segment(4);
        $md5email = $this->uri->segment(5);

        $booking = $this->master->getRecords('bought_quote', array(
            'id' => $id
        ));
        if (md5($user_id) != $md5email && count($booking) > 0) {
            echo '<p style="text-align: center; color: red;">Invalid link.</p>';
            return false;
        }

        $data = array(
            'view' => 'customer_verify_quote_view',
            'title' => 'Confirm Quote',
            'id' => $id,
            'booking' => $booking,
            'user_id' => $user_id,
            'quote_url' => base_url() . 'landing/quote_preview/' . $id . '/' . md5($id)
        );

        $this->load->view('includes/view', $data);
    }


    /***************************
    confirm_quote_status
    ***************************/
    public function confirm_quote_status()
    {
        $id  = $_POST['id'];
        $uid = $_POST['uid'];

        $arr = array(
            'status' => 'A'
        );

        $this->master->updateRecord('bought_quote', $arr, array(
            'id' => $id
        ));

        //admin emailer info
        $adminemail = $this->common->admin_email();

        $agent_name  = $this->common->customer_name($uid);
        $agent_email = $this->common->db_field_id('customers', 'email', $uid);
        $email_temp  = 'quote-confirm-to-agent';
        //email settings for customer
        $info_arr    = array(
            'to' => $agent_email,
            'from' => $adminemail,
            'subject' => 'Welcome to Transit Insurance',
            'view' => $email_temp,
            'emailer_file_name' => $email_temp
        );

        $other_info         = array(
            'password' => '',
            'view' => $email_temp,
            'emailer_file_name' => $email_temp,
            'name' => $agent_name,
            'agency_name' => '', //$agency_info[0]['name'],
            'user_name' => $agent_email,
            'user_email' => $agent_email,
            'link' => 'dashboard'
        );
        $arr['info_arr2']   = $info_arr;
        $arr['other_info2'] = $other_info;
        $this->emailer->sendmail($info_arr, $other_info);

        echo json_encode($arr);
    }

    public function confirm_quote_bind()
    {
        $id  = $_POST['id'];
        $uid = $this->session->userdata('id');

        $arr = array(
            'status' => 'P'
        );

        $this->master->updateRecord('bought_quote', $arr, array(
            'id' => $id
        ));

        //admin emailer info
        $adminemail = $this->common->admin_email();

        $recepient_name  = '';
        $recepient_email = $this->common->db_field_id('bought_quote', 'email', $uid);
        $email_temp      = 'quote-bind-to-consignor';
        //email settings for consignor
        $info_arr        = array(
            'to' => $recepient_email,
            'from' => $adminemail,
            'subject' => 'Welcome to Transit Insurance',
            'view' => $email_temp,
            'emailer_file_name' => $email_temp
        );

        $other_info        = array(
            'password' => '',
            'view' => $email_temp,
            'emailer_file_name' => $email_temp,
            'name' => $recepient_name,
            'agency_name' => '', //$agency_info[0]['name'],
            'user_name' => $recepient_email,
            'user_email' => $recepient_email,
            'link' => 'landing/download_quote/' . $id
        );
        $arr['info_arr']   = $info_arr;
        $arr['other_info'] = $other_info;
        $this->emailer->sendmail($info_arr, $other_info);

        $recepient_name  = 'Admin';
        $recepient_email = $this->common->db_field_id('admin', 'recovery_email', '2');
        $email_temp      = 'quote-bind-to-admin';
        //email settings for admin
        $info_arr        = array(
            'to' => $recepient_email,
            'from' => $adminemail,
            'subject' => 'Welcome to Transit Insurance',
            'view' => $email_temp,
            'emailer_file_name' => $email_temp
        );

        $other_info         = array(
            'password' => '',
            'view' => $email_temp,
            'emailer_file_name' => $email_temp,
            'name' => $recepient_name,
            'agency_name' => '', //$agency_info[0]['name'],
            'user_name' => $recepient_email,
            'user_email' => $recepient_email,
            'link' => 'webmanager'
        );
        $arr['info_arr2']   = $info_arr;
        $arr['other_info2'] = $other_info;

        $this->emailer->sendmail($info_arr, $other_info);
        echo json_encode($arr);

    }

    public function reqdetails()
    {

    }

    public function purchasepolicy()
    {
        $user_id    = $this->session->userdata('id');
        $id         = $this->uri->segment(3);
        $emailcrypt = $this->uri->segment(4);
        $insurances = $this->master->getRecords('bought_insurance', array(
            'id' => $id
        ));

        if ($emailcrypt == md5($insurances[0]['email'])) {
            //set data to session
            $details           = unserialize($insurances[0]['details']);
            $details['single'] = $details['single_input'];

            //set userdata
            $single            = $details['single'];
            $single['address'] = $single['location'];
            unset($single['lat']);
            unset($single['lng']);
            unset($single['country_id']);
            unset($single['calling_code']);
            unset($single['calling_digits']);
            while ($value = current($single)) {
                $key           = key($single);
                $details[$key] = $value;
                next($single);
            }


            unset($details['single_input']);
            $details['referral_id']      = $id;
            $details['referral_comment'] = $insurances[0]['comment'];
            $this->session->set_userdata($details);
            $refer_from = '?refer_from=req_buy_form#login';

            if ($insurances[0]['customer_id'] == '0' || $this->session->userdata('id') != '') {
                $refer_from = '?refer_from=req_buy_form#buyform';
            }
            redirect(base_url() . $refer_from);

        } else {
            echo '<p style="margin: 150px; text-align: center;">Invalid Link</p>';
        }

    }



    /***************************
    buy
    ***************************/
    public function buy()
    {
        $cargos          = $this->master->getRecords('cargo_category');
        $transportations = $this->common->transportations();
        $countries       = $this->master->getRecords('country_t');

        $admin_info      = $this->master->getRecords('admin');
        $the_agency      = $this->master->getRecords('agency', array(
            'id' => $admin_info[0]['default_cargo']
        ));
        $minimum_premium = $this->common->agency_minimum_premium($admin_info[0]['default_cargo']);
        $agency_id       = '0';
        if (count($the_agency) > 0) {
            $agency_id = $the_agency[0]['id'];
        }

        $cargo_prices          = $this->common->agency_prices($agency_id, 'cargo_prices');
        $transportation_prices = $this->common->agency_prices($agency_id, 'transportation_prices');
        $country_prices        = $this->common->agency_prices($agency_id, 'country_prices');

        $data = array(
            'cargos' => $cargos,
            'transportations' => $transportations,
            'countries' => $countries,
            'cargo_prices' => $cargo_prices,
            'transportation_prices' => $transportation_prices,
            'country_prices' => $country_prices,
            'the_agency' => $the_agency,
            'minimum_premium' => $minimum_premium,
            'title' => 'Buy Insurance',
            'view' => 'buy_view'
        );

        $this->load->view('includes/view', $data);

    }

    public function store_cargo()
    {
        $cargos = $_POST['cargos'];
        $aw     = array();
        foreach ($cargos as $r => $value) {
            $aw[] = $value;
            //$this->master->insertRecord('cargo_category', $value);
        }
        echo json_encode($aw);
    }
    /***************************
    search_view
    ***************************/
    public function search()
    {
        $genres       = $this->master->getRecords('genres');
        $days_of_week = $this->common->days_of_week();

        $data = array(
            'genres' => $genres,
            'days_of_week' => $days_of_week,
            'view' => 'search_view'
        );

        $this->load->view('includes/view', $data);

    }

    /***************************
    search_list
    ***************************/
    public function search_list()
    {
        $home_location = $_POST['location'];
        $location_lat  = $_POST['location_lat'];
        $location_long = $_POST['location_long'];
        $genres        = array();
        $dayweek       = array();
        $distance      = 20;

        $level      = '';
        $searchtype = 'class';


        if (isset($_POST['searchtype'])) {
            $searchtype = 'agency';
        }

        if (isset($_POST['level'])) {
            $level = $_POST['level'];
        }

        if (isset($_POST['genres'])) {
            $genres = $_POST['genres'];
        }

        if (isset($_POST['dayweek'])) {
            $dayweek = $_POST['dayweek'];
        }
        if (isset($_POST['distance'])) {
            $distance = $_POST['distance'];
        }

        $the_loc      = explode(' ', $home_location);
        $string_count = count($the_loc) - 1;
        $the_loc      = $the_loc[$string_count];

        $whr_space = array();

        //select table to search
        if ($searchtype == 'class') {
            $table = 'classes';
            $this->db->like('location', $the_loc);
            if ($level != '') {
                $whr_space['level'] = $level;
            }
        } else {
            $table = 'agency';
            $this->db->like('address', $the_loc);
        }

        $spaces = $this->master->getRecords($table, $whr_space, '*', array(
            'id' => 'DESC'
        ));

        $places = array();


        if (count($spaces) > 0) {
            foreach ($spaces as $t => $space) {

                $the_distance = $this->common->distanceCalculation($location_lat, $location_long, $space['lat'], $space['lng']);


                //default for agency and empty values
                $instrr      = array();
                $inst_avatar = $this->common->avatar('0', 'customer');

                $schedule       = array();
                $sched_start    = array();
                $sched_end      = array();
                $class_genre    = array();
                $cost           = 0;
                $max_student    = 0;
                $customers_id   = array();
                $customers_name = array();
                $genres_name    = array();
                $class_level    = '';
                $background     = '';
                $facilities     = '';

                //use data according to searchtype
                if ($searchtype == 'class') {

                    //get one avatar from customers
                    if ($space['customers'] != '') {
                        $instrr = unserialize($space['customers']);
                    }

                    if (count($instrr) > 0) {
                        $inst_avatar = $this->common->avatar($instrr[0], 'customer');
                    }

                    if ($space['schedule'] != '') {
                        $schedule    = unserialize($space['schedule']);
                        $sched_start = unserialize($space['sched_start']);
                        $sched_end   = unserialize($space['sched_end']);
                    }

                    if ($space['genres'] != '') {
                        $class_genre = unserialize($space['genres']);
                    }

                    $geo_name       = $space['location'];
                    $agency_image   = $this->common->avatar($space['user_id']);
                    $cost           = $space['cost'];
                    $max_student    = $space['max_students'];
                    $customers_id   = unserialize($space['customers']);
                    $customers_name = $this->common->customer_array($space['customers']);
                    $genres_name    = $this->common->genre_array($space['genres']);
                    $agency_name    = $this->common->agency_name($space['user_id']);
                    $agency_id      = $space['user_id'];
                    $class_level    = $space['level'];
                }
                //data for agency
                else {
                    $geo_name      = $space['address'];
                    $agency_image  = $this->common->avatar($space['id'], 'agency');
                    $agency_name   = $space['name'];
                    $agency_id     = $space['id'];
                    $background    = $space['background'];
                    $facilities    = $space['facilities'];
                    $agency_genres = $this->common->agency_genres($space['id']);
                    $genres_name   = $this->common->genre_array(serialize($agency_genres));

                }



                $the_class = array(
                    "name" => $space['name'],
                    "geo_name" => $geo_name,
                    "seeker_img" => $inst_avatar,
                    "id" => $space['id'],
                    "distance" => $the_distance,
                    "image" => $agency_image,
                    "schedule" => $schedule,
                    "sched_start" => $sched_start,
                    "sched_end" => $sched_end,
                    "cost" => $cost,
                    "max_students" => $max_student,
                    "customers_id" => $customers_id,
                    "customers_name" => $customers_name,
                    "genres_id" => $class_genre,
                    "genres_name" => $genres_name,
                    "agency_id" => $agency_id,
                    "agency_name" => $agency_name,
                    "level" => $class_level,
                    "background" => $background,
                    "facilities" => $facilities,
                    "geo" => array(
                        $space['lat'],
                        $space['lng']
                    )
                );

                //check if days of week filter
                $daysmatches       = 0;
                $display_this_days = 'no';
                if (count($dayweek) > 0) {
                    if (count($schedule) > 0) {
                        foreach ($dayweek as $dw) {
                            if (in_array($dw, $schedule)) {
                                $daysmatches++;
                            }
                        }
                        //schedule filter match
                        if ($daysmatches > 0) { // == count($dayweek)){
                            $display_this = 'yes';
                            $places[]     = $the_class;
                        }
                    }

                }


                else {
                    $places[] = $the_class;
                }





            }
        }



        //filter location within $distance
        $filtered_places = array();

        if (count($places) > 0) {
            foreach ($places as $r => $place) {
                if ($place['distance'] < $distance) {
                    $the_filtered_class = $place;

                    $class_genre = $place['genres_id'];


                    //check if days of week filter
                    $genrematches       = 0;
                    $display_this_genre = 'no';
                    if (count($genres) > 0) {
                        if (count($class_genre) > 0) {
                            foreach ($genres as $dw) {
                                if (in_array($dw, $class_genre)) {
                                    $genrematches++;
                                }
                            }
                            //schedule filter match
                            if ($genrematches == count($genres)) {
                                $display_this_genre = 'yes';
                                $filtered_places[]  = $the_filtered_class;
                            }
                        }

                    } else {
                        $filtered_places[] = $the_filtered_class;
                    }



                }
            }
        }

        $data               = array(
            'status' => 'OK',
            'genres' => $genres,
            'dayweek' => $dayweek,
            'level' => $level,
            'places' => $filtered_places
        );
        //		$data['access'] = $access;
        //		$data['loc_search'] = $the_loc;
        $data['searchtype'] = $searchtype;

        echo json_encode($data);

    }



    /***************************
    legal_text
    ***************************/
    public function legal_text()
    {
        $admin_info = $this->master->getRecords('admin');
        $text_type  = $this->uri->segment(3);

        $data = array(
            'admin_info' => $admin_info,
            'text_type' => $text_type
        );

        $this->load->view('user/terms_text', $data);
    }




    public function refferal_info()
    {
        $id   = $this->uri->segment(3);
        $type = $this->uri->segment(4);
        $bought_insurance = $this->master->getRecords('bought_insurance', array(
            'id' => $id
        ));
        $data = array();

        echo $type;

        if (count($bought_insurance) == 0) {
            echo '<p class="text-muted text-center" style="padding-top: 50px;"><i class="fa fa-image fa-3x"></i><br>No information to display</p>';
            return false;
        }

        $details = array();

        foreach ($bought_insurance as $r => $value) {


            $value['avatar']  = $this->common->avatar($value['customer_id'], 'customer');
            $value['details'] = unserialize($value['details']);

            $value['buy']       = (isset($value['details']['buy_inputs'])) ? $value['details']['buy_inputs'] : array();
            $value['single']    = (isset($value['details']['single_input'])) ? $value['details']['single_input'] : array();
            $value['consignee'] = (isset($value['details']['consignee'])) ? $value['details']['consignee'] : array();
            $value['premium']   = (isset($value['details']['premium'])) ? $value['details']['premium'] : 0;

            $data = $value;
        }

        $this->load->view('user/about_referral_info', $data);
    }

    public function trans_info()
    {
        $id   = $this->uri->segment(3);
        $user = $this->master->getRecords('bought_insurance', array(
            'id' => $id
        ));
        $data = array();

        if (count($user) == 0) {
            echo '<p class="text-muted text-center" style="padding-top: 50px;"><i class="fa fa-image fa-3x"></i><br>No information to display</p>';
            return false;
        }

        $details = array();

        foreach ($user as $r => $value) {

            $themenu         = array(
                array(
                    'name' => 'Transit',
                    'data' => '', //$data['homebiz1'],
                    'target' => 'home'
                ),
                array(
                    'name' => 'Quote',
                    'data' => '', //$data['homebiz2'],
                    'target' => 'quote'
                ),
                array(
                    'name' => 'Bind',
                    'data' => '', //$data['homebiz3'],
                    'target' => 'profile'
                ),
                // array(
                //     'name' => 'Consignee',
                //     'data' => '', //$data['homebiz5'],
                //     'target' => 'messages'
                // )
            );

            $value['themenu'] = $themenu;

            $value['avatar']  = $this->common->avatar($value['customer_id'], 'customer');
            $value['details'] = unserialize($value['details']);

            $value['buy']       = (isset($value['details']['buy_inputs'])) ? $value['details']['buy_inputs'] : array();
            $value['single']    = (isset($value['details']['single_input'])) ? $value['details']['single_input'] : array();
            $value['consignee'] = (isset($value['details']['consignee'])) ? $value['details']['consignee'] : array();
            $value['premium']   = (isset($value['details']['premium'])) ? $value['details']['premium'] : 0;

            $data = $value;
        }

        $this->load->view('user/about_trans_info', $data);
    }


    public function confirmbuy_info()
    {

        $data['buy']       = $this->session->userdata('buy_inputs');
        $data['single']    = $this->session->userdata('single');
        $data['consignee'] = $this->session->userdata('consignee');
        $data['premium']   = $this->session->userdata('premium');

        $this->load->view('user/about_confirmbuy', $data);
    }


    public function agentcust_info()
    {
        $id   = $this->uri->segment(3);
        $user = $this->master->getRecords('agent_customers', array(
            'id' => $id
        ));
        $data = array();

        if (count($user) == 0) {
            echo '<p class="text-muted text-center" style="padding-top: 50px;"><i class="fa fa-image fa-3x"></i><br>No information to display</p>';
            return false;
        }

        foreach ($user as $r => $value) {
            $value['avatar']  = $this->common->avatar('0', 'customer');
            $value['details'] = unserialize($value['details']);
            $data             = $value;
        }

        $this->load->view('user/about_agent_customer', $data);
    }

    /***************************
    customer_info
    ***************************/
    public function customer_info()
    {
        $id   = $this->uri->segment(3);
        $user = $this->master->getRecords('customers', array(
            'id' => $id
        ));
        $data = array();

        if (count($user) == 0) {
            echo '<p class="text-muted text-center" style="padding-top: 50px;"><i class="fa fa-image fa-3x"></i><br>No information to display</p>';
            return false;
        }

        foreach ($user as $r => $value) {
            $value['avatar'] = $this->common->avatar($id, 'customer');
            $data            = $value;
        }

        $this->load->view('user/about_customer', $data);
    }


    /***************************
    agency_info
    ***************************/
    public function agency_info()
    {
        $id   = $this->uri->segment(3);
        $user = $this->master->getRecords('agency', array(
            'id' => $id
        ));
        $data = array();

        if (count($user) == 0) {
            echo '<p class="text-muted text-center" style="padding-top: 50px;"><i class="fa fa-image fa-3x"></i><br>No information to display</p>';
            return false;
        }

        foreach ($user as $r => $value) {
            $value['avatar'] = $this->common->avatar($id, 'agency');
            $data            = $value;
        }

        //$this->load->view('welcome_message');
        $this->load->view('user/about_agency', $data);
    }

    /***************************
    welcome
    ***************************/
    public function the_genres()
    {
        $genres = $this->master->getRecords('genres');

        $the_genres = array();
        foreach ($genres as $r => $val) {
            unset($val['date_updated']);
            $the_genres[] = $val;
        }
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        echo json_encode($the_genres, JSON_PRETTY_PRINT);

    }

    /***************************
    welcome
    ***************************/
    public function welcome()
    {
        $buy_inputs = $this->session->userdata('buy_inputs');
        $b          = ''; //$this->common->buy_inputs($buy_inputs);
        $other_info = array(
            'password' => '',
            //'details'=>$b,
            'name' => '', //$first.' '.$last,
            'agency_name' => 'asdf@asdf.com', //$name,
            'user_name' => 'asdf@asdf.com', //,
            'user_email' => 'asdf@asdf.com', //,
            'link' => '',
            'emailer_file_name' => 'send-certifiicate-to-user',
            'view' => 'send-certifiicate-to-user'
        );

        //$this->load->view('welcome_message');
        $this->load->view('emails/standard-email-template', $other_info);
    }


    public function test_email()
    {

        //admin emailer info
        $adminemail = $this->common->admin_email();

        //email settings for agency
        $info_arr = array(
            'to' => 'snailbob01@gmail.com',
            'from' => $adminemail,
            'subject' => 'Welcome to Transit Insurance',
            'view' => 'new-agency-confirm-email',
            'emailer_file_name' => 'new-agency-confirm-email'
        );


        $other_info          = array(
            'password' => '',
            'view' => 'new-agency-confirm-email',
            'emailer_file_name' => 'new-agency-confirm-email',
            'name' => '', //$first.' '.$last,
            'agency_name' => 'Admin',
            'user_name' => $adminemail,
            'user_email' => $adminemail,
            'link' => 'webmanager/insurance'
        );
        $data['info_arr1']   = $info_arr;
        $data['other_info1'] = $other_info;
        $this->emailer->sendmail($info_arr, $other_info);
        $this->load->view('emails/standard-email-template', $other_info);


    }
    /***************************
    signin_view
    ***************************/
    public function mysession()
    {
        $data = $this->session->all_userdata();
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        echo json_encode($data, JSON_PRETTY_PRINT);
    }

    /***************************
    signin_view
    ***************************/
    public function login()
    {

        $data = array(
            'view' => 'signin_view'
        );

        $this->load->view('includes/view', $data);
    }


    /***************************
    signup
    ***************************/
    public function signup()
    {

        $data = array(
            'view' => 'signup_view'
        );

        $this->load->view('includes/view', $data);
    }


    /***************************
    forgot password
    ***************************/
    public function forgot()
    {

        $data = array(
            'view' => 'forgot_view'
        );

        $this->load->view('includes/view', $data);
    }



    /***************************
    agency_profile
    ***************************/
    public function agency_profile()
    {
        $agency_id = $_GET['agency_id'];
        $type      = $_GET['type'];
        $id        = $_GET['id'];

        $class_id = NULL;
        if ($type == 'class') {
            $class_id = $id;
        }

        $sorted_schedule = $this->common->classes_schedules($agency_id, $class_id, 'future'); //agency_id, class_id, type
        $agency_info     = $this->master->getRecords('agency', array(
            'id' => $agency_id
        ));


        $agency_genres    = $this->common->agency_genres($agency_id);
        $agency_customers = $this->common->agency_customers($agency_id);



        $data = array(
            'view' => 'agency_profile_view',
            'title' => $this->common->agency_name($agency_id),
            'sorted_schedule' => $sorted_schedule,
            'agency_info' => $agency_info,
            'agency_genres' => $agency_genres,
            'agency_customers' => $agency_customers
        );

        $this->load->view('includes/view', $data);
    }


    /***************************
    forgot password
    ***************************/
    public function forohfor()
    {

        $data = array(
            'view' => 'forohfor_view'
        );

        $this->load->view('includes/view', $data);
    }

    /***************************
    teststripe
    ***************************/
    public function teststripe()
    {

        Stripe::setApiKey("sk_test_21OnMrInOQSqxd6aSeAO3Nu0");
        $all_plans = Stripe_Plan::all();
        echo $all_plans;
        $this->load->view('user/test-stripe');
    }





    /***************************
    stripe_paynow
    ***************************/
    public function stripe_paynow()
    {

        //get session values
        $buy_inputs = $_POST['buy_inputs'];
        $premium    = $_POST['premium'];
        $useremail  = $this->session->userdata('email');

        $details = array(
            'buy_inputs' => $buy_inputs,
            'premium' => $premium
        );

        $user_id = $this->session->userdata('id');

        $nowtime       = date('Y-m-d H:i:s');
        $customer_info = $this->master->getRecords('customers', array(
            'id' => $user_id
        ));

        $stripe_email = $this->session->userdata('email'); //$_POST['stripe_email'];
        //$tokenid = $_POST['tokenid'];


        Stripe::setApiKey("sk_test_21OnMrInOQSqxd6aSeAO3Nu0"); //sk_test_UTHtN8EB5Y5xwTB1OkeS6lJ8"); //sk_test_21OnMrInOQSqxd6aSeAO3Nu0");

        //get validation message from admin
        $action_message = $this->common->get_message('buy_insurance');

        // Create the charge on Stripe's servers - this will charge the user's card
        try {
            $charge = Stripe_Charge::create(array(
                "amount" => $premium * 100, // amount in cents, again
                "currency" => "usd",
                //"card" => $tokenid,
                "customer" => $customer_info[0]['stripe_id'],
                "description" => 'bought insurance in transit insurance'
            ));


            $data = array(
                'customer_id' => $user_id,
                'details' => serialize($details),
                'date_added' => $nowtime
            );

            $uuid         = $this->common->uuid();
            $data['uuid'] = $uuid;

            $booking_id = $this->master->insertRecord('bought_insurance', $data, true);


            //admin emailer info
            $adminemail = $this->common->admin_email();

            //email settings for agency
            $info_arr = array(
                'to' => $adminemail,
                'from' => $adminemail,
                'subject' => 'Welcome to Transit Insurance',
                'view' => 'payment-notification-to-admin',
                'emailer_file_name' => 'payment-notification-to-admin'
            );


            $other_info          = array(
                'password' => '',
                'view' => 'payment-notification-to-admin',
                'emailer_file_name' => 'payment-notification-to-admin',
                'name' => '', //$first.' '.$last,
                'agency_name' => 'Admin',
                'user_name' => $adminemail,
                'user_email' => $adminemail,
                'link' => 'webmanager/insurance'
            );
            $data['info_arr1']   = $info_arr;
            $data['other_info1'] = $other_info;
            $this->emailer->sendmail($info_arr, $other_info);

            //email settings for student
            $info_arr = array(
                'to' => $stripe_email,
                'from' => $adminemail,
                'subject' => 'Welcome to Transit Insurance',
                'view' => 'payment-notification-to-customer',
                'emailer_file_name' => 'payment-notification-to-customer'
            );


            $other_info          = array(
                'password' => '',
                'view' => 'payment-notification-to-customer',
                'emailer_file_name' => 'payment-notification-to-customer',
                'name' => '', //$first.' '.$last,
                'agency_name' => '', //$agency_info[0]['name'],
                'user_name' => $stripe_email,
                'user_email' => $stripe_email,
                'link' => 'dashboard' //landing/download/'.$user_id.'/'.$booking_id.'/'.md5($user_id)
            );
            $data['info_arr2']   = $info_arr;
            $data['other_info2'] = $other_info;
            $this->emailer->sendmail($info_arr, $other_info);


            $data['result']  = 'ok';
            $data['message'] = $action_message['success'];
            //$this->session->set_flashdata('ok', $action_message['success']);
            echo json_encode($data);

        }
        catch (Stripe_CardError $e) {
            // The card has been declined
            $data = array(
                'result' => 'error',
                'error' => $e
            );

            $body            = $e->getJsonBody();
            $err             = $body['error'];
            $data['message'] = $err['message'];

            echo json_encode($data);
        }
    }


    public function certificate()
    {
        $this->load->helper('pdf_helper');
        $request_id        = $this->uri->segment(3);
        $admin_info        = $this->master->getRecords('admin');
        $insurance_details = $this->common->the_cert_data($request_id);
        $template_details  = $this->common->the_cert_editor();

        $arr['nowtime']           = date('Y-m-d H:i:s');
        $arr['title']             = 'Quote for ';
        $arr['schedule']          = '06202016';
        $arr['cert_template']     = $admin_info[0]['certificate_template'];
        $arr['insurance_details'] = $insurance_details;
        $arr['template_details']  = $template_details;

        $arr['data'] = $arr;

        // var_dump($insurance_details); return false;

        $this->load->view('includes/pdfreport', $arr);
    }

    // Download document
    public function download()
    {
        $this->load->helper('download');
        //
        //
        $req_id  = $this->uri->segment(3);
        $nowtime = date('Y-m-d_H_i_s');

        $name = 'RedSkyCertificate-' . $nowtime . '.pdf';


        $file = base_url('landing/certificate/' . $req_id);
        $ch   = curl_init($file);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_REFERER, $file);

        $data = curl_exec($ch);

        curl_close($ch);

        force_download($name, $data);

    }

    function downloadFile($file)
    {
        $nowtime = date('Y-m-d_H_i_s');
        $name    = 'RedSkyCertificate-' . $nowtime . '.pdf';

        $file_name = $name;
        $mime      = 'application/force-download';
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false);
        header('Content-Type: ' . $mime);
        header('Content-Disposition: attachment; filename="' . basename($file_name) . '"');
        header('Content-Transfer-Encoding: binary');
        header('Connection: close');
        readfile($file_name);
        exit();
    }

    public function quote_preview()
    {
        $this->load->helper('pdf_helper');
        $request_id        = $this->uri->segment(3);
        $admin_info        = $this->master->getRecords('admin');

        //use cert for preview
        $bought_quote = $this->master->getRecords('bought_quote', array('id'=>$request_id));

        if(!empty($bought_quote) && !empty($request_id)){
            $insurance_details = $this->common->the_quote_data($request_id);
            $template_details  = $this->common->the_cert_editor();
        }
        else{
            $insurance_details = $this->common->the_cert_data($request_id);
            $template_details  = $this->common->the_cert_editor();
        }

        $arr['nowtime']           = date('Y-m-d H:i:s');
        $arr['title']             = 'Quote for ';
        $arr['schedule']          = '06202016';
        $arr['cert_template']     = $admin_info[0]['quote_template'];
        $arr['insurance_details'] = $insurance_details;
        $arr['template_details']  = $template_details;

        $arr['data'] = $arr;

        //		echo json_encode($arr);
        //		return false;
        $this->load->view('includes/pdfreport', $arr);
    }

    public function download_quote()
    {
        $this->load->helper('download');
        $req_id  = $this->uri->segment(3);
        $nowtime = date('Y-m-d_H_i_s');
        $data    = file_get_contents(base_url() . 'landing/quote_preview/' . $req_id);

        $name = 'QuoteDetails-' . $nowtime . '.pdf';

        force_download($name, $data);
    }


    public function getports()
    {
        $iso        = $_POST['iso'];
        $selected   = $_POST['selected'];
        $port_codes = $this->master->getRecords('port_codes', array(
            'country_code' => $iso
        ));

        $opts = '<p class="text-muted text-center" style="margin-top: 50px;">No ports found.</p>';
        if (count($port_codes) > 0) {
            $opts = '';
            foreach ($port_codes as $r => $value) {
                //$opts .='<option value="'.$value['id'].'">'.$value['country_code'].' - '.$value['port_name'].'('.$value['port_code'].')</option>';

                $opts .= '<div class="col-xs-6 col-md-4">';
                $opts .= '	<a href="#" class="thumbnail text-center genre_thumbs';

                if ($selected == $value['id']) {
                    $opts .= ' active';
                }

                $opts .= '">';
                $opts .= '	<span>' . $value['country_code'] . ' - ' . $value['port_name'] . '(' . $value['port_code'] . ')</span>';
                $opts .= '	<div class="checkbox hidden">     ';
                $opts .= '	  <label>';
                $opts .= '		<input type="radio" name="cargocat" value="' . $value['id'] . '"';

                if ($selected == $value['id']) {
                    $opts .= ' checked="checked"';
                }

                $opts .= '/><span>' . $value['country_code'] . ' - ' . $value['port_name'] . '(' . $value['port_code'] . ')</span>';
                $opts .= '	  </label>';
                $opts .= '	</div>';
                $opts .= '	</a>';
                $opts .= '</div>';


            }
        }

        echo $opts;


    }


    public function sent_quote()
    {
        $query      = $_GET;
        $date1      = $query['date_sent'];
        $date2      = date('Y-m-d H:i:s');
        $dayspassed = $this->common->differ_days($date1, $date2);
        $arr        = array();

        if ($dayspassed > 5) {
            $arr['heading'] = 'Opps!';
            $arr['message'] = '<p>All quotes are valid for 20 days. Looks like this quote has already expired</p>';
            $this->load->view('errors/html/error_general', $arr);
            // echo '<h2>All quotes are valid for 20 days. Looks like this quote has already expired.</h2>';
        } else {
            foreach ($query as $r => $value) {
                $data  = array(
                    'name' => $r,
                    'value' => $value
                );
                $arr[] = $data;

            }
            $this->session->set_userdata('buy_inputs', $arr);
            // echo json_encode($arr);


            redirect('?reset=no#buyform');
        }


    }
}
