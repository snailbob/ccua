<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('id') == '') {
            redirect(base_url());
        }
    }
    
    /***************************
    index
    ***************************/
    public function index()
    {


        if ($this->session->userdata('id') == '') {
            redirect(base_url());
        }

        redirect('dashboard/quotes');

        $user_id   = $this->session->userdata('id');
        $type      = $this->session->userdata('type');
        $address   = $this->session->userdata('address');
        $stripe_id = $this->session->userdata('stripe_id');

        $policy_docs = $this->master->getRecords('policy_docs', '', '*', array(
            'id' => 'DESC'
        ));
        $insurances  = array();

        if ($type == 'customer') {
            $insurances = $this->master->getRecords('bought_insurance', array(
                'customer_id' => $user_id
            ));
        }

        $data = array(
            'view' => 'dashboard_view',
            'title' => 'Dashboard',
            'insurances' => $insurances,
            'address' => $address,
            'policy_docs' => $policy_docs,
            'stripe_id' => $stripe_id
        );

        $this->load->view('includes/user_view', $data);

    }


    /*-------------------------------------
    referrals
    -------------------------------------*/
    public function referrals()
    {
        if ($this->session->userdata('id') == '') {
            redirect(base_url());
        }

        $user_id = $this->session->userdata('id');
        $type    = $this->session->userdata('type');

        //seekers
        $insurances = $this->master->getRecords('bought_insurance', array(
            'customer_id' => $user_id,
            'status !=' => 'P'
        ), '', array(
            'status' => 'DESC'
        ));

        $data = array(
            'title' => 'Policy Referrals',
            'view' => 'manage-referrals',
            'insurances' => $insurances

        );

        $this->load->view('includes/user_view', $data);

    }


    public function proceed_buy()
    {
        $id      = $this->uri->segment(3);
        $status      = $this->uri->segment(4);
        $editable = ($status == 'A') ? '&editable=no' : '';
        
        $booking = $this->master->getRecords('bought_insurance', array(
            'id' => $id,
            'status'=>'D'
        ));

        if(empty($booking) && !empty($id)){
            $booking = $this->master->getRecords('bought_insurance', array(
                'id' => $id,
                'status' => 'A'
            ));
        }

        if (count($booking) > 0) {
            $details           = unserialize($booking[0]['details']);
            $arr['buy_inputs'] = $details['buy_inputs'];
            $arr['single']     = $details['single_input'];
            $arr['consignee']  = (isset($details['consignee'])) ? $details['consignee']  : null;
            $arr['premium']    = $details['premium'];
            $arr['insurance_id']    = $id;
            $arr['is_approved']    = ($status == 'A') ? 'yes' : 'no';
            $arr['new_deductible']  = (isset($details['new_deductible'])) ? $details['new_deductible']  : null;
            $this->session->set_userdata($arr);
            redirect('buy?reset=no'.$editable);
            return false;
        }
        // echo json_encode(count($booking));
        redirect('buy');

    }

    public function continue_transaction()
    {
        $id      = $this->uri->segment(3);
        $booking = $this->master->getRecords('bought_insurance', array(
            'id' => $id
        ));

        if (count($booking) > 0) {
            $details           = unserialize($booking[0]['details']);
            $arr['buy_inputs'] = $details['buy_inputs'];
            $arr['single']     = $details['single_input'];
            $arr['consignee']  = $details['consignee'];
            $arr['premium']    = $details['premium'];
            $this->session->set_userdata($arr);
        }

        redirect('buy');

    }
    /***************************
    buy
    ***************************/
    public function buy()
    {
        if ($this->session->userdata('id') == '') {
            redirect(base_url());
        }

        $reset = (isset($_GET['reset'])) ? $_GET['reset'] : 'yes';
        $data  = $this->common->home_data($reset);

        $data['view']  = 'buy_insurance';
        $data['title'] = ($this->session->userdata('customer_type') == 'N') ? 'Get Quote' : 'Buy Insurance';

        $this->load->view('includes/user_view', $data);
    }


    /*-------------------------------------
    transactions
    -------------------------------------*/
    public function transactions()
    {
        if ($this->session->userdata('id') == '') {
            redirect(base_url());
        }

        $user_id = $this->session->userdata('id');
        $type    = $this->session->userdata('type');

        //seekers
        $insurances = $this->master->getRecords('bought_insurance', array(
            'customer_id' => $user_id,
            'status' => 'P' //exclude needs referral
        ), '', array(
            'date_updated' => 'DESC'
        ));

        $data = array(
            'title' => 'Bound Transactions',
            'view' => 'manage-transactions',
            'insurances' => $insurances

        );

        $this->load->view('includes/user_view', $data);

    }


    /***************************
    quotes
    ***************************/
    public function quotes()
    {
        $user_id    = $this->session->userdata('id');
        $type       = $this->session->userdata('type');
        // $insurances = $this->master->getRecords('bought_quote', array(
        //     'customer_id' => $user_id
        // ), '*', array(
        //     'id' => 'DESC'
        // ));

        //seekers
        $insurances2 = $this->master->getRecords('bought_insurance', array(
            'customer_id' => $user_id,
            'status !=' => 'P' //exclude needs referral
        ), '', array(
            'id' => 'DESC'
        ));

        $all_insurances = $insurances2; // array_merge($insurances, $insurances2);

        $data = array(
            'view' => 'manage-quotes',
            'title' => 'Quotes',
            'insurances' => $all_insurances
        );

        $this->load->view('includes/user_view', $data);

    }


    /***************************
    rating
    ***************************/
    public function rating()
    {
        $user_id = $this->session->userdata('id');
        $type    = $this->session->userdata('type');

        $agency_info     = $this->master->getRecords('agency', array(
            'id' => $user_id
        ));
        $cargo           = $this->master->getRecords('cargo_category');
        $transportations = $this->common->transportations();
        $countries       = $this->master->getRecords('country_t');

        $cargo_prices          = $this->common->agency_prices($user_id, 'cargo_prices');
        $transportation_prices = $this->common->agency_prices($user_id, 'transportation_prices');
        $country_prices        = $this->common->agency_prices($user_id, 'country_prices');


        $data = array(
            'view' => 'rating_view',
            'title' => 'Rating',
            'cargo' => $cargo,
            'transportations' => $transportations,
            'countries' => $countries,
            'cargo_prices' => $cargo_prices,
            'transportation_prices' => $transportation_prices,
            'country_prices' => $country_prices,
            'agency_info' => $agency_info
        );

        $this->load->view('includes/user_view', $data);

    }

    public function get_scheds()
    {
        $user_id         = $this->session->userdata('id');
        $type            = $this->session->userdata('type');
        $sorted_schedule = $this->common->classes_schedules($user_id, NULL); //studio_id, class_id, type

        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        echo json_encode($sorted_schedule, JSON_PRETTY_PRINT);
        return false;
    }


    /***************************
    mycustomers for agents
    ***************************/
    public function mycustomers()
    {
        $user_id = $this->session->userdata('id');
        $type    = $this->session->userdata('type');

        $countries = $this->master->getRecords('country_t');


        $arr = array(
            'user_id' => $user_id
        );

        $customers = $this->master->getRecords('agent_customers', array(
            'agent_id' => $user_id
        ));


        $data = array(
            'view' => 'manage_customers_view',
            'title' => 'Manage Customers',
            'customers' => $customers,
            'countries' => $countries
        );

        $this->load->view('includes/user_view', $data);

    }



    /***************************
    classes
    ***************************/
    public function settings()
    {

        $user_id = $this->session->userdata('id');
        $type    = $this->session->userdata('type');

        $studio = $this->master->getRecords('agency', array(
            'id' => $user_id
        ));

        $data = array(
            'view' => 'settings_view',
            'title' => 'Settings',
            'studio' => $studio
        );

        $this->load->view('includes/user_view', $data);


    }


    /***************************
    logout
    ***************************/
    public function logout()
    {
        $user_data = $this->session->all_userdata();

        $log_activity = array(
            'name' => $user_data['name'] . ' user logout',
            'type' => 'logout',
            'details' => serialize($user_data)
        );
        $this->master->insertRecord('activity_log', $log_activity);

        foreach ($user_data as $key => $value) {
            //do not remove admin session
            if ($key != 'logged_admin' && $key != 'logged_admin_email') {
                $this->session->unset_userdata($key);
            }
        }

        redirect(base_url());

    }

    /***************************
    reset_inputs
    ***************************/
    public function reset_inputs()
    {

        $details = array(
            'buy_inputs' => '',
            'single_input' => '',
            'consignee' => '',
            'premium' => '',
            'premium_format' => '',
            'selected_deductible' => '',
            'cargo_price' => '',
            'currency' => ''
        );

        //unset from session
        foreach ($details as $key => $value) {
            $this->session->unset_userdata($key);
        }

    }


}
