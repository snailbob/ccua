<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Surveyors extends CI_Controller

{

	public function __construct(){

		parent::__construct();}


	#--------------------------------------------->>index view loading<<-------------------------------------
	public function indexxx() {
			// $csv_file = 'iso_3166_2_countries.csv';
			//
			// $f = fopen(base_url()."uploads/policyd/".$csv_file, "r");
			//
			// $col = array(
			// 	'iso_code',
			// 	'common_name',
			// 	'agent_name',
			// 	'main_address_line',
			// 	'main_address_line_2',
			// 	'main_address_line_3',
			// 	'main_city',
			// 	'state',
			// 	'postcode',
			// 	'main_country',
			// 	'full_main_address',
			// 	'main_email',
			// 	'after_hours',
			// 	'main_phone_no',
			// 	'main_fax_no',
			// 	'contact_mobile_no'
			// );
			// $arr = array();
			// while (($data = fgetcsv($f, 1000, ",")) !== FALSE) {
			// 	for($i = 0; $i < 16; $i++){
			// 		if(array_key_exists($i, $data)){
			// 			// if($data[$i] != ''){
			// 				$arr[$col[$i]][] = $data[$i];
			// 			// }
			// 		}
			//
			// 	}
			// }
			//
			// $arr['count'] = array(
			// 	'iso'=>count($arr['iso_code']),
			// 	'contact'=>count($arr['contact_mobile_no'])
			// );
			//
			// $nowtime = date('Y-m-d H:i:s');
			//
			// $groups = array();
			// for($i = 1; $i < count($arr['iso_code']); $i++){
			// 	if($arr['iso_code'][$i] != ''){
			// 		$surv = array();
			//
			// 		foreach($col as $r=>$value){
			// 			$surv[$value] = $arr[$value][$i];
			// 		}
			// 		$surv['date_added'] = $nowtime;
			//
			// 		$groups[] = $surv;
			// 		// $this->master->insertRecord('surveyors', $surv);
			//
			// 	}
			// }
			//
			// echo json_encode($groups);

	}


	public function index() {
		$users = $this->master->getRecords('surveyors');
		$surveyors_fields = $this->common->surveyors_fields();
		//echo json_encode($brokerage); return false;
		$data = array(
			'middle_content'=>'manage-surveyors',
			'title'=>'Surveyor Contacts',
			'singular_title'=>'Surveyor Contact',
			'users'=>$users,
			'surveyors_fields'=>$surveyors_fields
		);
		$this->load->view('admin/admin-view',$data);


	}

	public function add(){
		$email = $_POST['email'];
		$first = $_POST['first'];
		$last = $_POST['last'];
		$bname = $_POST['bname'];
		$ctype = 'N';
		if(isset($_POST['ctype'])){
			$ctype = 'Y';
		}
		$nowtime = date('Y-m-d H:i:s');
		$data = array(
			'first_name'=>$first,
			'last_name'=>$last,
			'business_name'=>$bname,
			'customer_type'=>$ctype,
			'email'=>$email,
			'status'=>'N',
			'date_added'=>$nowtime
		);
		$id = $this->master->insertRecord('customers', $data, TRUE);

		//admin emailer info
		$adminemail = $this->common->admin_email();

		//email settings for studio
		$info_arr = array(
			'to'=>$email,
			'from'=>$adminemail,
			'subject'=>'Welcome to Transit Insurance',
			'view'=>'new-agency-confirm-email',
		);

		$other_info = array(
			'password'=>'',
			'emailer_file_name'=>'new-agency-confirm-email',
			'name'=>$first.' '.$last,
			'studio_name'=>$first.' '.$last,
			'user_name'=>$email,
			'user_email'=>$email,
			'link'=>'landing/confirm_customer/'.$id.'/'.md5($email)
		);

		$data['info_arr'] = $info_arr;
		$data['other_info'] = $other_info;
		$this->emailer->sendmail($info_arr,$other_info);
		$this->session->set_flashdata('success',' Customer successfully added.');
		echo json_encode($data);

	}


	#--------------------------------------------->>activate view loading<<-------------------------------------
	public function activate() {
		$inc = $this->uri->segment(4);
		$user_id = $this->uri->segment(5);

		if($this->master->updateRecord('customers',array('enabled'=>$inc),array('id'=>$user_id))){
			if($inc == 'Y'){
				$success_mess = 'Customer successfully activated';
			}
			else{
				$success_mess = 'Customer successfully deactivated';
			}

			$this->session->set_flashdata('success',$success_mess);

			redirect('webmanager/customers/manage');
		}else{
			$this->session->set_flashdata('error','Something went wrong. Please try again.');
			redirect('webmanager/customers/manage');
		}
	}


	#--------------------------------------------->>get_info view loading<<-------------------------------------
	public function get_info() {
		$id = $_POST['id'];

		$user = $this->master->getRecords('customers',array('id'=>$id));


		$info = array(
			'email'=>$user[0]['email'],
			'first_name'=>$user[0]['first_name'],
			'last_name'=>$user[0]['last_name'],
			'address'=>$user[0]['address'],
			'require_space'=>$user[0]['require_space'],
			'trade_space'=>$user[0]['trade_space'],
		);

		echo json_encode($info);

	}
	#--------------------------------------------->>get_info view loading<<-------------------------------------
	public function update_info() {
		$first = $_POST['firstname'];
		$last = $_POST['lastname'];
		$address = $_POST['address'];
		$trade_space = $_POST['trade_space'];
		$require_space = $_POST['require_space'];
		$id = $_POST['id'];


		$arr = array(
			'first_name'=>$first,
			'last_name'=>$last,
			'address'=>$address,
			'require_space'=>$require_space,
			'trade_space'=>$trade_space
		);

		if($this->master->updateRecord('customers',$arr,array('id'=>$id))){
			$this->session->set_flashdata('success',' Customer info updated successfully');
			echo 'success';
		}else{
			echo 'error';
		}
	}


	#--------------------------------------------->>delete view loading<<-------------------------------------
	public function delete() {
		$user_id = $this->uri->segment(4);

		if($this->master->deleteRecord('customers','id',$user_id)) {

			$success_mess = 'Customer successfully deleted';
			$this->session->set_flashdata('success',$success_mess);
			redirect('webmanager/customers/manage');
		}else{
			$this->session->set_flashdata('error','Something went wrong. Please try again.');
			redirect('webmanager/customers/manage');
		}
	}


	#--------------------------------------------->>manage view loading<<-------------------------------------
	public function brokerage() {
		$agency_id = $this->common->default_cargo();
		$users = $this->master->getRecords('brokerage');
//		if($agency_id == '6'){
//			$users = $this->master->getRecords('brokerage');
//		}
//		else{
//			$users = $this->master->getRecords('brokerage', array('agency_id'=>$agency_id));
//		}

		$the_brokers = $this->master->getRecords('customers', array('customer_type'=>'N', 'super_admin'=>'N'));

		$single = 'Agent';
		$title = $single.'s';

		$data = array(
			'middle_content'=>'manage-brokerage',
			'title'=>$title,
			'singular_title'=>$single,
			'the_brokers'=>$the_brokers,
			'users'=>$users
		);
		$this->load->view('admin/admin-view',$data);


	}


	public function save_brokerage(){
		$data = $_POST['data'];
		$id = '';
		$input = $data;

		$nowtime = date('Y-m-d H:i:s');

		$data = array();
		foreach($input as $r=>$value){
			if($value['name'] != 'id'){
				$data[$value['name']] = $value['value'];
			}
			else{
				$id = $value['value'];
			}
		}

		if($id == ''){

			$email = $data['email'];
			$first = $data['first'];
			$last = $data['last'];
			$first_broker = $data['first_broker'];


			$success_mess = 'Brokerage successfully added.';
			$this->session->set_flashdata('success',$success_mess);
			$data['agency_id'] = $this->common->default_cargo();
			$data['date_added'] = $nowtime;
			unset($data['first_broker']);
			$id = $this->master->insertRecord('brokerage', $data, true);



			if(!empty($email)){


				$broker = array(
					'first_name'=>$data['first'],
					'last_name'=>$data['last'],
					'email'=>$data['email'],
					'customer_type'=>'N',
					'super_admin'=>'N',
					'access_rights'=>'a:10:{i:0;s:9:"Referrals";i:1;s:14:"Manage Brokers";i:2;s:6:"Rating";i:3;s:17:"Manage Port Codes";i:4;s:17:"Manage Certifcate";i:5;s:15:"Policy Document";i:6;s:9:"Reporting";i:7;s:15:"Outgoing Emails";i:8;s:14:"Manage Content";i:9;s:8:"Settings";}',
					'brokerage_id'=>$id,
					'status'=>'N',
					'first_broker'=>$first_broker
				);

				$access_rights = (isset($_POST['access_rights'])) ? $_POST['access_rights'] : '';
				if($access_rights != ''){
					$broker['access_rights'] = serialize($access_rights);
				}


				$id = $this->master->insertRecord('customers', $broker, TRUE);


				$log_activity = array(
					'name'=>'New broker was created.',
					'type'=>'add_user',
					'details'=>serialize($data)
				);
				$this->master->insertRecord('activity_log', $log_activity);


				//admin emailer info
				$adminemail = $this->common->admin_email();

				//email settings for studio
				$info_arr = array(
					'to'=>$data['email'],
					'from'=>$adminemail,
					'subject'=>'Welcome to Transit Insurance',
					'view'=>'new-agency-confirm-email',
				);

				$other_info = array(
					'password'=>'',
					'emailer_file_name'=>'new-agency-confirm-email',
					'name'=>$data['first'].' '.$data['last'],
					'studio_name'=>$data['first'].' '.$data['last'],
					'user_name'=>$data['email'],
					'user_email'=>$data['email'],
					'link'=>'landing/confirm_customer/'.$id.'/'.md5($data['email'])
				);

				$data['info_arr'] = $info_arr;
				$data['other_info'] = $other_info;
				$this->emailer->sendmail($info_arr,$other_info);


			}
		}
		else{
			$success_mess = 'Brokerage successfully updated.';
			$this->session->set_flashdata('success',$success_mess);

			$this->master->updateRecord('brokerage', $data, array('id'=>$id));

		}

		echo json_encode($data);

	}



}
