<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customers extends CI_Controller 

{

	public function __construct(){

		parent::__construct();}


	#--------------------------------------------->>index view loading<<-------------------------------------
	public function index() {

		redirect(base_url().'webmanager/customers/manage');

	}


	#--------------------------------------------->>manage view loading<<-------------------------------------
	public function manage() {
		$users = $this->master->getRecords('customers', array('customer_type'=>'Y', 'super_admin'=>'N'));
		$data = array(
			'middle_content'=>'manage-customers',
			'title'=>'Customers',
			'singular_title'=>'Customer',
			'users'=>$users,
		);	
		$this->load->view('admin/admin-view',$data);

	}
	
	public function add(){
		$user_session = $this->session->all_userdata();
		$user_id = (empty($user_session['id'])) ? '0' : $user_session['id'];
		$logged_admin_id = (!isset($user_session['logged_admin_id'])) ? '0' : $user_session['logged_admin_id'];
		
		
		$email = $_POST['email'];
		$first = $_POST['first'];
		$last = $_POST['last'];
		$bname = $_POST['bname'];
		$brokerage_id = $_POST['brokerage_id'];
		

		$id = $_POST['id'];
		$super_admin = (isset($_POST['super_admin'])) ? $_POST['super_admin'] : 'N';
		$access_rights = (isset($_POST['access_rights'])) ? $_POST['access_rights'] : '';

		$ctype = $_POST['ctype'];
		$mess = ($ctype == 'N') ? 'Agent successfully added.' : 'Customer successfully added.';

		$nowtime = date('Y-m-d H:i:s');
		
		//for user type	
		$usertype = ($super_admin == 'Y') ? 'Super Admin' : 'Regular User';
		if($user_id == '0'){
			//$ctype = ($super_admin == 'Y') ? 'N' : 'Y';
		}
		$usertype = ($ctype == 'Y') ? 'Customer' : $usertype;
		$admintype = ($logged_admin_id == '') ? 'Webmanager' : 'Super Admin';
		
		

	
		$data = array(
			'first_name'=>$first,
			'last_name'=>$last,
			'brokerage_id'=>$brokerage_id,
			'business_name'=>$bname,
			'customer_type'=>$ctype,
			'super_admin'=>$super_admin,
			'email'=>$email,
			'status'=>'N',
			'date_added'=>$nowtime
		);
		
		//check duplicate
		$dups = $this->master->getRecordCount('customers', array('email'=>$email));
		
		if($dups > 0 && empty($id)){
			$mess = 'User already exist.';
			$result = 'duplicate';
			$this->session->set_flashdata('error',$mess);
			echo json_encode($data);
			return false;
		}
		
		
		
		if(empty($id)){
			if($access_rights != ''){
				$data['access_rights'] = serialize($access_rights);
			}
			
			
			$id = $this->master->insertRecord('customers', $data, TRUE);
			

			$log_activity = array(
				'name'=>$admintype.' created new '.$usertype,
				'type'=>'add_user',
				'details'=>serialize($data)
			);
			$this->master->insertRecord('activity_log', $log_activity);
			
			
			//admin emailer info
			$adminemail = $this->common->admin_email();
	
			//email settings for studio
			$info_arr = array(
				'to'=>$email,
				'from'=>$adminemail,
				'subject'=>'Welcome to Transit Insurance',
				'view'=>'new-agency-confirm-email',
			);
			
			$other_info = array(
				'password'=>'',
				'emailer_file_name'=>'new-agency-confirm-email',
				'name'=>$first.' '.$last,
				'studio_name'=>$first.' '.$last,
				'user_name'=>$email,
				'user_email'=>$email,
				'link'=>'landing/confirm_customer/'.$id.'/'.md5($email)
			);
			
			$data['info_arr'] = $info_arr;
			$data['other_info'] = $other_info;
			$this->emailer->sendmail($info_arr,$other_info);
			
		}
		else{
			
			$mess = ' User successfully updated.';
			$data = array(
				'first_name'=>$first,
				'last_name'=>$last,
				'business_name'=>$bname,
			);
			
			if($access_rights != ''){
				$data['access_rights'] = serialize($access_rights);

				if($user_id == $id){
					$this->session->set_userdata('access_rights', $access_rights);
				}
			
			}
			$this->master->updateRecord('customers', $data, array('id'=>$id));
		
		
			$log_activity = array(
				'name'=>$admintype.' updated '.$usertype,
				'type'=>'update_user',
				'details'=>serialize($data)
			);
			$this->master->insertRecord('activity_log', $log_activity);
		
		}

		$this->session->set_flashdata('success',$mess);
		echo json_encode($data);
		
	}
	
	
	#--------------------------------------------->>activate view loading<<-------------------------------------
	public function activate() {
		$inc = $this->uri->segment(4);
		$user_id = $this->uri->segment(5);
		
		if($this->master->updateRecord('customers',array('enabled'=>$inc),array('id'=>$user_id))){
			if($inc == 'Y'){
				$success_mess = 'User successfully activated';
			}
			else{
				$success_mess = 'User successfully deactivated';
			}
			
			$this->session->set_flashdata('success',$success_mess);
			
			redirect('webmanager/agency/manage');
		}else{
			$this->session->set_flashdata('error','Something went wrong. Please try again.');
			redirect('webmanager/agency/manage');
		}
	}
	
	
	#--------------------------------------------->>get_info view loading<<-------------------------------------
	public function get_info() {
		$id = $_POST['id'];
		
		$user = $this->master->getRecords('customers',array('id'=>$id));
		
		
		$info = array(
			'email'=>$user[0]['email'],
			'first_name'=>$user[0]['first_name'],
			'last_name'=>$user[0]['last_name'],
			'address'=>$user[0]['address'],
			'require_space'=>$user[0]['require_space'],
			'trade_space'=>$user[0]['trade_space'],
		);

		echo json_encode($info);

	}
	#--------------------------------------------->>get_info view loading<<-------------------------------------
	public function update_info() {
		$first = $_POST['firstname'];
		$last = $_POST['lastname'];
		$address = $_POST['address'];
		$trade_space = $_POST['trade_space'];
		$require_space = $_POST['require_space'];
		$id = $_POST['id'];


		$arr = array(
			'first_name'=>$first,
			'last_name'=>$last,
			'address'=>$address,
			'require_space'=>$require_space,
			'trade_space'=>$trade_space
		);
		
		if($this->master->updateRecord('customers',$arr,array('id'=>$id))){
			$this->session->set_flashdata('success',' Customer info updated successfully');
			echo 'success';
		}else{
			echo 'error';
		}
	}
	

	#--------------------------------------------->>delete view loading<<-------------------------------------
	public function delete() {
		$user_id = $this->uri->segment(4);
		$type = $this->uri->segment(5);

		if($this->master->deleteRecord('customers','id',$user_id)) {	
			
			$success_mess = ($type == 'Y') ? 'Customer successfully deleted' : 'Broker successfully deleted';
			$this->session->set_flashdata('success',$success_mess);
		}else{
			$this->session->set_flashdata('error','Something went wrong. Please try again.');
		}
		
		
		if($type == 'Y'){
			redirect('webmanager/customers/manage');
		}
		else{
			redirect('webmanager/agency/manage');
		}
		
		
	}
	

}