<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cargo extends CI_Controller 

{

	public function __construct(){

		parent::__construct();}


	#--------------------------------------------->>index view loading<<-------------------------------------
	public function index() {

		redirect(base_url().'webmanager/jobs/manage');

	}


	#--------------------------------------------->>manage view loading<<-------------------------------------
	public function manage() {
		$admin_info = $this->master->getRecords('admin');
		$user_id = $admin_info[0]['default_cargo'];
		$agency_info = $this->master->getRecords('agency', array('id'=>$user_id));
		$deductibles = $this->master->getRecords('deductibles', array('agency_id'=>$user_id));
		$cargo = $this->master->getRecords('cargo_category');
		$transportations = $this->common->transportations();
		$countries = $this->master->getRecords('country_t');
		$currencies = $this->master->getRecords('api_currencies', array('in_stripe'=>'Y'));


		$cargo_prices = $this->common->agency_prices($user_id,'cargo_prices');
		$cargo_max = $this->common->agency_prices($user_id,'cargo_max');
		$transportation_prices = $this->common->agency_prices($user_id,'transportation_prices');
		$country_prices = $this->common->agency_prices($user_id,'country_prices');
		$country_referral = $this->common->agency_prices($user_id,'country_referral');
		$zone_multiplier = $this->common->agency_prices($user_id,'zone_multiplier');
		$base_currency = ($agency_info[0]['base_currency']) ? $agency_info[0]['base_currency'] : 'AUD';
		$country_currency = ($agency_info[0]['country_currency']) ? $agency_info[0]['country_currency'] : 'Y';
		$data = array(
			'middle_content'=>'manage-cargo',
			'title'=>'Rating',
			'cargo'=>$cargo,
			'deductibles'=>$deductibles,
			'transportations'=>$transportations,
			'countries'=>$countries,
			'cargo_prices'=>$cargo_prices,
			'cargo_max'=>$cargo_max,
			'transportation_prices'=>$transportation_prices,
			'country_prices'=>$country_prices,
			'agency_info'=>$agency_info,
			'country_referral'=>$country_referral,
			'zone_multiplier'=>$zone_multiplier,
			'user_id'=>$user_id,
			'currencies'=>$currencies,
			'base_currency'=>$base_currency,
			'country_currency'=>$country_currency
		);

		$this->load->view('admin/admin-view',$data);

	}
	
	public function save_country_currency(){
		$checked = $_POST['checked'];
		$default_cargo = $this->common->default_cargo();
		
		$arr = array(
			'country_currency'=>$checked
		);
		
		$this->master->updateRecord('agency', $arr, array('id'=>$default_cargo));
		
		echo json_encode($arr);
	}
	
	public function save_currency(){
		$id = $_POST['id'];
		$currency = $_POST['currency'];
		$this->master->updateRecord('agency', array('base_currency'=>$currency), array('id'=>$id));
		echo json_encode(array('base_currency'=>$currency));
	}
	
	
	public function save_zone(){
		$zone = $_POST['zone'];
		$id = $_POST['id'];
		$this->master->updateRecord('agency', array('zone_multiplier'=>serialize($zone)), array('id'=>$id));
		echo json_encode(array('id'=>$id));
	}
	
	public function deductible_default(){
		$agency_id = $_POST['agency_id'];
		$id = $_POST['id'];
		
		$this->master->updateRecord('deductibles', array('not_default'=>'N'), array('id'=>$id));
		
		$not_def = $this->master->getRecords('deductibles', array('agency_id'=>$agency_id, 'id !='=>$id));
		if(count($not_def) > 0){
			foreach($not_def as $r=>$value){
				$this->master->updateRecord('deductibles', array('not_default'=>'Y'), array('id'=>$value['id']));
			}
		}
		echo json_encode(array('id'=>$id));
	}
	
	public function country_referral(){
		$admin_info = $this->master->getRecords('admin');
		$user_id = $admin_info[0]['default_cargo'];
	
		$type = $_POST['type'];
		$country_id = $_POST['country_id'];
		$country_referral = $this->common->agency_prices($user_id,'country_referral');
		
		if($type == 'set'){
			$country_referral[$country_id] = $country_id;
		}
		else{
			unset($country_referral[$country_id]);
		}
		
		$data = array(
			'country_referral'=>serialize($country_referral)
		);
		$this->master->updateRecord('agency', $data , array('id'=>$user_id));
		
		echo json_encode($data);
	}
	
	public function cargo_referral(){
		$admin_info = $this->master->getRecords('admin');
		$user_id = $admin_info[0]['default_cargo'];
	
		$type = $_POST['type'];
		$id = $_POST['id'];

		$data = array(
			'referral'=>$type
		);
		$this->master->updateRecord('cargo_category', $data , array('id'=>$id));
		
		echo json_encode($data);
	}

	#--------------------------------------------->>manage view loading<<-------------------------------------
	public function view() {
		$agency_id = $this->uri->segment(4);
		$cargo = $this->master->getRecords('cargo_category');
		$agencies = $this->master->getRecords('agency');
		$the_agency = $this->master->getRecords('agency', array('id'=>$agency_id));
		$admin_info = $this->master->getRecords('admin');
	
		$agency_prices = $this->common->agency_prices($agency_id, 'cargo_prices');

		$data = array(
			'middle_content'=>'view-cargo',
			'cargo'=>$cargo,
			'agencies'=>$agencies,
			'the_agency'=>$the_agency,
			'admin_info'=>$admin_info,
			'agency_prices'=>$agency_prices
		);	
		$this->load->view('admin/admin-view',$data);


	}
	
	
	public function makedefault(){
		$id = $this->uri->segment(4);
		$this->master->updateRecord('admin', array('default_cargo'=>$id), array('id'=>'2'));
		$this->session->set_flashdata('success', 'Default cargo category successfully updated.');
		redirect('webmanager/cargo/manage');
	}
	

}