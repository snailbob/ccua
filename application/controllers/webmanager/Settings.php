<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Settings extends CI_Controller {

	public function __construct(){

		parent::__construct();}
	
	public function commss(){
		$wow = array();
		for($i = 1; $i < 11; $i++){
			$wow[] = $i;
		}
		$this->master->updateRecord('admin_login', array('commission_rate'=>serialize($wow),'commission_rate_manual'=>serialize($wow)), array('id'=>'1'));
	}


    public function timezone(){
        $user_id = '2';

        $time_zone     = $this->master->getRecords('time_zone');

        $customer = $this->master->getRecords('admin', array(
            'id' => $user_id
        ));
        $customer_tz = (!empty($customer)) ? $customer[0]['timezone'] : '32';
        $customer_tz = (empty($customer_tz)) ? '32' : $customer_tz;

        $data = array(
            'middle_content' => 'settings_timezone',
            'title' => 'Timezone',
            'time_zone' => $time_zone,
            'customer' => $customer,
            'customer_tz' => $customer_tz
        );
	
		$this->load->view('admin/admin-view',$data);
	

    }


	/*---------------------------------------------
				resetdb
	----------------------------------------------*/
	public function resetdb() {
		
		if($this->uri->segment(4) == 'delete'){ //isset($_POST['action'])){

			$res = array();
			//custom delete organization_master
			$dbname = 'agency';
			$this->master->deleteRecord($dbname,'id >','6');
			$reset_query = 'ALTER TABLE '.$dbname.' AUTO_INCREMENT = 7';
			$this->db->query($reset_query);
			$res[] = array(
				'dbname'=>$dbname,
				'result'=>'reset - empty success'
			);


			$dbs = array(
				'activity_log',
				'agent_customers',
				'bought_insurance',
				'bought_quote',
				// 'brokerage',
				// 'customers',
				//'deductibles',
				// 'policy_docs',
				// 'report_template'
			);
			
			
			foreach($dbs as $dbname){
				
				$query = 'DELETE FROM '.$dbname;
				$reset_query = 'ALTER TABLE `'.$dbname.'` AUTO_INCREMENT = 1';
				$this->db->query($query);
				$this->db->query($reset_query);
				$res[] = array(
					'dbname'=>$dbname,
					'result'=>'reset - empty success'
				);
			}	
			
			//$this->session->set_flashdata('success','Database reset successful.');	
		
			echo json_encode($res);
			return false;
		
		}//reset end
		else{
			echo 'err';//uri-4 must be "delete"';
		}
		
		
//		$admin_details = $this->master->getRecords('admin_login');
//		$data = array(
//			'middle_content'=>'reset-db-view',
//			'page_title'=>'Reset Database',
//		);	
//		
//		$this->load->view('admin/admin-view',$data);
	
	
	}
	


	public function changePassword(){

		$admin_details = $this->master->getRecords('admin');
		

		if(isset($_POST['change_admin_pw'])){


			$pass = $this->input->post('password');
			$pass2 = $this->input->post('password2');
			$admin_email = $this->input->post('email');
			$recovery_email = $this->input->post('recovery_email');
			$admin_emails = $this->input->post('admin_email');
			
			
			if($pass != $pass2){
				$this->session->set_flashdata('error','Password didn\'t match.');
				redirect(base_url().'webmanager/settings/changePassword');
				return false;
			}

			$data_array = array(
				'password'=>md5($pass),
				'recovery_email'=>$recovery_email,
				'admin_emails'=>serialize($admin_emails),
				'email'=>$admin_email
			);


			if($this->master->updateRecord('admin',$data_array,array('id'=>'2'))){
				$arr = array();
				//send email to new admin email
				$admin_emails_stack = (@unserialize($admin_details[0]['admin_emails'])) ? unserialize($admin_details[0]['admin_emails']) : array();

				foreach($admin_emails as $r=>$value){
					if(!in_array($value, $admin_emails_stack)){
						//send email
						$emailer_file_name = 'new_admin_email_to_admin';
						$adminemail        = $this->common->recovery_admin();
						
						$to_email = $value;
				
						//email settings
						$info_arr = array(
							'to' => $to_email, //$arr['email'],
							'from' => $adminemail,
							'subject' => 'Greetings from Ebinder',
							'view' => $emailer_file_name
						);
				
				
						$other_info = array(
							'password' => '',
							'emailer_file_name' => $emailer_file_name,
							'name' => 'Admin', // $arr['name'],
							'agency_name' => '',
							'user_name' => '',
							'user_email' => '',
							'details' => '',
							'link' => 'webmanager'
						);

						$arr[]['other_info'] = $other_info;
						$arr[]['info_arr']   = $info_arr;
						$this->emailer->sendmail($info_arr, $other_info);
					}
				}
			
				$this->session->set_flashdata('success','Infomation updated successfully.');

				// echo json_encode($arr);
				// return false;
				redirect(base_url().'webmanager/settings/changePassword');

			}

			else{

				$this->session->set_flashdata('error','Error while updating information');

			}

			
		}


		$data = array(
			'middle_content'=>'change-password',
			'page_title'=>'Change Password',
			'admin_details'=>$admin_details
		);	

		$this->load->view('admin/admin-view',$data);


	}



	



	



	/*--------------------------------------
		Function for updating emails
	-----------------------------------*/

	public function manageEmail(){

		if(isset($_POST['admin_update_email'])){
	
			$this->form_validation->set_rules('contact_email','Contact Email','required|xss_clean|valid_email');
			$this->form_validation->set_rules('info_email','info email','required|xss_clean|valid_email');
			$this->form_validation->set_rules('support_email','Support Email','required|xss_clean|valid_email');	
	
	
	
			if($this->form_validation->run()){
	
				$contact_email=$this->input->post('contact_email','',true);
				$info_email=$this->input->post('info_email','',true);
				$support_email=$this->input->post('support_email','',true);
	
				$data_array = array(
					'contact_email '=>$contact_email,
					'info_email'=>$info_email,
					'support_email'=>$support_email
				);
	
	
				
				if($this->master->updateRecord('email_id_master',$data_array,array('id'=>'1'))){
	
					$this->session->set_flashdata('success','Admin Email updated successfully.');
					redirect(base_url().'webmanager/settings/manageEmail/');
	
				}
	
				else{
	
					$this->session->set_flashdata('error','Error while updating Admin Email');
	
				}
	
			}
	
		}	

		$admin_email = $this->master->getRecords('email_id_master');
	
		$data = array(
			'middle_content'=>'update-admin-email',
			'page_title'=>'Update Admin Email',
			'admin_email'=>$admin_email
		);	
	
		$this->load->view('admin/admin-view',$data);
	
	}


	

	#--------------------------------------------->>users<<-------------------------------------
	public function users() {
		$users = $this->master->getRecords('customers', array('customer_type'=>'Y', 'super_admin'=>'Y'));
		$access_rights = $this->common->access_rights();
		$data = array(
			'middle_content'=>'manage-super-users',
			'title'=>'Users',
			'singular_title'=>'User',
			'access_rights'=>$access_rights,
			'users'=>$users
		);	
		$this->load->view('admin/admin-view',$data);

	}
	

	
	public function activitylog(){
		$admin_info = $this->master->getRecords('admin');
		
		$activity_logged = $this->session->userdata('activity_logged');
		$activity_log = ($activity_logged == 'Y') ? $this->master->getRecords('activity_log', '', '*', array('id'=>'DESC')) : array();
		$data = array(
			'middle_content'=>'manage-activitylog',
			'title'=>'Acitivity Log',
			'admin_info'=>$admin_info,
			'activity_log'=>$activity_log
		);	
		
		$this->load->view('admin/admin-view',$data);
		
	}


	public function logo(){
		$data = array(
			'middle_content'=>'logo-settings-view',
			'title'=>'Update Logo'
		);	
	
		$this->load->view('admin/admin-view',$data);
	}

}