<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reporting extends CI_Controller 

{

	public function __construct(){

		parent::__construct();}


	#--------------------------------------------->>index view loading<<-------------------------------------
	public function index() {

		redirect(base_url().'webmanager/reporting/manage');

	}


	/*--------------------------------
		manage view loading
	--------------------------------*/
	public function manage() {
		redirect('webmanager/reporting/generate_formpage');
		
		$report_header = $this->common->report_header();
		$report_header_name = $this->common->report_header_name();
		$report_icons = $this->common->report_icons();
		$report_templates = $this->master->getRecords('report_template');

		$insurances = $this->master->getRecords('bought_quote', array('status'=>'P'));

		$data = array(
			'middle_content'=>'manage-reporting',
			'title'=>'Reporting',
			'insurances'=>$insurances,
			'report_templates'=>$report_templates,
			'report_header_name'=>$report_header_name,
			'report_icons'=>$report_icons,
			'report_header'=>$report_header
		);	
		
		$this->load->view('admin/admin-view',$data);

	}

	public function generate_formpage(){
	
		$report_header = $this->common->report_header();
		$report_header_name = $this->common->report_header_name();
		$report_icons = $this->common->report_icons();
		$report_templates = $this->master->getRecords('report_template');

		$insurances = $this->master->getRecords('bought_quote', array('status'=>'P'));

		$data = array(
			'middle_content'=>'manage-reporting-form',
			'title'=>'Bound Transactions Reporting',
			'insurances'=>$insurances,
			'report_templates'=>$report_templates,
			'report_header_name'=>$report_header_name,
			'report_icons'=>$report_icons,
			'report_header'=>$report_header
		);	
		
		$this->load->view('admin/admin-view',$data);


	}

	public function billings(){
		$date_from = $this->uri->segment('4');
		$date_to = $this->uri->segment('5');
		$date = date('d-m-Y');
			
		// output headers so that the file is downloaded rather than displayed
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=billing_report'.$date.'.csv');
		
		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
		
		// output the column headings
		fputcsv($output, array('Date of purchase', 'Agent', 'Consignee details', 'Quote Number', 'Premium'));
		
		$rows = $this->common->billing_report($date_from, $date_to);
		
		// loop over the rows, outputting them
		foreach($rows as $r=>$val){
			fputcsv($output, $val);
		}

	}

	public function agentchannel(){
		$date_from = $this->uri->segment('4');
		$date_to = $this->uri->segment('5');
		$date = date('d-m-Y');
		
		$rows = $this->common->agentchannel_report($date_from, $date_to);
//		echo json_encode($rows);
//		return false;
		
		
		// output headers so that the file is downloaded rather than displayed
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=agentchannel_report'.$date.'.csv');
		
		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
		
		// output the column headings
		fputcsv($output, array('Date of purchase', 'Agent Code', 'Agent Name', 'Consignee details', 'Quote Number', 'Premium'));
		
		
		// loop over the rows, outputting them
		foreach($rows as $r=>$val){
			fputcsv($output, $val);
		}

	}

	public function bordereaux(){
//		$r = $this->common->bordereaux_report();
//		header('Content-Type: application/json');
//		header('Access-Control-Allow-Origin: *');
//		echo json_encode($r, JSON_PRETTY_PRINT);
//		return false;

		$date_from = $this->uri->segment('4');
		$date_to = $this->uri->segment('5');
		
		$report_header = array();
		
		//date_from=28-09-2016&type=bordereaux&date_to=28-10-2016&report_template=&template_name=&report_header
		$report_template = $_GET['report_template'];
		$template_name = $_GET['template_name'];
		$report_header = (isset($_GET['report_header'])) ? $_GET['report_header'] : array();
		
		$nowtime = date('Y-m-d H:i:s');

		$store = array(
			'name'=>$template_name,
			'details'=>serialize($report_header)
		);
		
		if(!empty($template_name)){
			if($report_template == ''){
				$store['date_added'] = $nowtime;
				$this->master->insertRecord('report_template', $store);
			}
			else{
				$this->master->updateRecord('report_template', $store, array('id'=>$report_template));
			}
		}
		
		//fetch header title and, data name
		// $report_header_title = $this->common->report_header();
		// $report_header_name = $this->common->report_header_name();
		// $header = array();
		// $header_name = array();
		// if(count($report_header) > 0){
		// 	foreach($report_header as $r=>$value){
		// 		if($r < 29){
		// 			$header[] = $report_header_title[$value];
		// 			$header_name[] = $report_header_name[$value];
		// 		}
		// 	}
		// }
		// $rows = $this->common->bordereaux_report($date_from, $date_to, $header_name);
		// echo json_encode($rows);
		// return false;


		$date = date('d-m-Y');
			
		// output headers so that the file is downloaded rather than displayed
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=bordereaux_report'.$date.'.csv');
		
		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
		
		
		//fetch header title and, data name
		$report_header_title = $this->common->report_header();
		$report_header_name = $this->common->report_header_name();
		
		$header = array();
		$header_name = array();
		if(count($report_header) > 0){
			foreach($report_header as $r=>$value){
				if($r < 29){
					$header[] = $report_header_title[$value];
					$header_name[] = $report_header_name[$value];
				}
			}
		}
		
		// output the column headings
		fputcsv($output, $header);
		
		$rows = $this->common->bordereaux_report($date_from, $date_to, $header_name);
		
		// loop over the rows, outputting them
		foreach($rows as $r=>$val){
			fputcsv($output, $val);
		}

	}

	

}