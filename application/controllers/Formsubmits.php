<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formsubmits extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        echo 'test';
    }
    /***************************
    login
    ***************************/
    public function login()
    {

        $google_id   = $_POST['google_id'];
        $facebook_id = $_POST['facebook_id'];
        $email       = (!empty($_POST['email'])) ? $_POST['email'] : 'Anonymous';
        $password    = $_POST['password'];
        $utype       = $_POST['utype'];


        $agency   = $this->master->getRecords('agency', array(
            'email' => $email
        ));
        $customer = $this->master->getRecords('customers', array(
            'email' => $email
        ));
        $student  = $this->master->getRecords('students', array(
            'email' => $email
        ));

        $arr = array(
            'result' => 'not_exist',
            'google_id' => $google_id,
            'fb_id' => $facebook_id,
            'email' => $email
        );


        //get validation message from admin
        $action_message = $this->common->get_message('user_login');

        $arr['message'] = $action_message['error'];


        //default activity log
        $log_activity = array(
            'name' => $email . ' User not found',
            'type' => 'login',
            'details' => serialize($arr)
        );



        //if Agency
        if ($utype == 'Agency') {
            if (count($agency) > 0) {
                $arr['result']  = 'error';
                $arr['message'] = '<i class="fa fa-warning text-warning"></i> Wrong email or password.';
                foreach ($agency as $r => $value) {
                    if ($value['password'] == md5($password)) {
                        unset($value['password']);
                        $value['type']     = 'agency';
                        $value['location'] = $value['address'];
                        $arr['userdata']   = $value;

                        $value['multiple_account'] = 'no';
                        //include customer to switch
                        if (count($customer) > 0) {
                            $value['multiple_account'] = 'yes';
                        }

                        if ($value['enabled'] != 'N') {
                            $this->session->set_userdata($value);
                            $arr['result']  = 'ok';
                            $arr['message'] = $action_message['success'];
                        } else {
                            $arr['result']  = 'error';
                            $arr['message'] = '<i class="fa fa-warning text-warning"></i> Account not activated.';
                        }
                    }
                }
            }
        }
        //if customer
        else if ($utype == 'Customer') {
            if (count($customer) > 0) {
                $arr['result']  = 'error';
                $arr['message'] = '<i class="fa fa-warning text-warning"></i> Wrong email or password.';

                $log_activity = array(
                    'name' => $email . ' Wrong password',
                    'type' => 'login',
                    'details' => serialize($arr)
                );



                foreach ($customer as $r => $value) {
                    if ($value['password'] == md5($password)) {
                        unset($value['password']);
                        $value['name']    = $value['first_name'] . ' ' . $value['last_name'];
                        $value['type']    = 'customer';
                        $value['address'] = $value['location'];

                        $value['access_rights'] = (@unserialize($value['access_rights'])) ? unserialize($value['access_rights']) : $this->common->access_rights();

                        if ($value['super_admin'] == 'Y') {
                            $value['logged_admin']       = $value['name'];
                            $value['logged_admin_email'] = $value['email'];
                            $value['logged_admin_id']    = $value['id'];
                        }


                        $arr['userdata'] = $value;


                        $value['multiple_account'] = 'no';
                        //include agency to switch
                        if (count($agency) > 0) {
                            $value['multiple_account'] = 'yes';
                        }
                        if ($value['enabled'] != 'N') {
                            $this->session->set_userdata($value);
                            $arr['result']  = 'ok';
                            $arr['message'] = $action_message['success'];


                            //save quote if there's
                            $buy_inputs = $this->session->userdata('buy_inputs');
                            if (!empty($buy_inputs)) {
                                $details = $this->common->save_quote();
                            }

                            $log_activity = array(
                                'name' => $value['name'] . ' Login successful',
                                'type' => 'login',
                                'details' => serialize($value)
                            );

                        } else {
                            $arr['result']  = 'error';
                            $arr['message'] = '<i class="fa fa-warning text-warning"></i> Account not activated.';

                            $log_activity = array(
                                'name' => $value['name'] . ' Account not acitivated',
                                'type' => 'login',
                                'details' => serialize($value)
                            );
                        }



                    }


                }
            }
        }

        else {
            if (count($student) > 0) {
                $arr['result']  = 'error';
                $arr['message'] = '<i class="fa fa-warning text-warning"></i> Wrong email or password.';

                foreach ($student as $r => $value) {
                    if ($value['password'] == md5($password)) {
                        unset($value['password']);
                        $value['name']   = $value['first_name'] . ' ' . $value['last_name'];
                        $value['type']   = 'student';
                        //$value['address'] = $value['location'];
                        $arr['userdata'] = $value;

                        $value['multiple_account'] = 'no';

                        if ($value['enabled'] != 'N') {
                            $this->session->set_userdata($value);
                            $arr['result']  = 'ok';
                            $arr['message'] = $action_message['success'];
                        } else {
                            $arr['result']  = 'error';
                            $arr['message'] = '<i class="fa fa-warning text-warning"></i> Account not activated.';
                        }
                    }
                }
            }
        }


        $this->master->insertRecord('activity_log', $log_activity);


        echo json_encode($arr);

    }

    /***************************
    signup
    ***************************/
    public function signup()
    {

        //google_id=&facebook_id=&name=s&email=%40gmail.com&password=Darknite1
        $google_id   = $_POST['google_id'];
        $facebook_id = $_POST['facebook_id'];
        $name        = $_POST['name'];
        $first_name  = $_POST['first_name'];
        $last_name   = $_POST['last_name'];
        $email       = $_POST['email'];
        $password    = $_POST['password'];
        $utype       = $_POST['utype'];


        $business_name = $_POST['business_name'];
        $location      = $_POST['location'];
        $lat           = $_POST['lat'];
        $lng           = $_POST['lng'];
        $country_code  = $_POST['country_code'];
        $mobile        = ltrim($_POST['mobile'], '0');
        $contact_no    = $country_code . $mobile;

        //check where_from
        $refer_from       = isset($_POST['refer_from']) ? $_POST['refer_from'] : '';
        $append_getmethod = '';
        if ($refer_from != '') {
            $append_getmethod = '?refer_from=buy_form';
        }

        $nowtime = date('Y-m-d H:i:s');
        $id      = '';

        $google_img  = $_POST['google_img'];
        $fb_img      = $_POST['fb_img'];
        $social_type = $_POST['social_type'];
        $social_site = $_POST['social_site'];

        $result = 'error';
        $arr    = array(
            'google_id' => $google_id,
            'fb_id' => $facebook_id,
            'google_img' => $google_img,
            'fb_img' => $fb_img,
            'password' => md5($password),
            'email' => $email,
            'status' => 'N',
            'enabled' => 'N',
            'business_name' => $business_name,
            'location' => $location,
            'lat' => $lat,
            'lng' => $lng,
            'country_id' => $country_code,
            'calling_digits' => $mobile,
            'calling_code' => $country_code,
            'contact_no' => $contact_no,
            'date_added' => $nowtime
        );

        //get validation message from admin
        $action_message = $this->common->get_message('user_signup');


        $dbtable           = 'customers';
        $emailer_file_name = 'signup-mail-to-customer';
        $email_name        = $first_name . ' ' . $last_name;

        //check duplicate
        $dups = $this->master->getRecordCount($dbtable, array(
            'email' => $email
        ));

        if ($dups > 0) {
            $arr['message'] = 'User already exist.';
            $result         = 'duplicate';

        } else {

            //store name for student or customer
            $arr['first_name'] = $first_name;
            $arr['last_name']  = $last_name;

            $id = $this->master->insertRecord($dbtable, $arr, true);



            //admin emailer info
            $adminemail = $this->common->admin_email();

            //email settings
            $info_arr = array(
                'to' => $email,
                'from' => $adminemail,
                'subject' => 'Welcome to Transit Insurance',
                'view' => $emailer_file_name
            );


            $other_info = array(
                'password' => '',
                'emailer_file_name' => $emailer_file_name,
                'name' => $email_name,
                'agency_name' => $name,
                'user_name' => $email,
                'user_email' => $email,
                'link' => 'landing/verify_user/' . $id . '/' . md5($id) . '/' . $dbtable . $append_getmethod
            );

            $this->emailer->sendmail($info_arr, $other_info);

            $arr['other_info'] = $other_info;
            $arr['info_arr']   = $info_arr;
            $arr['message']    = $action_message['success'];
            $result            = 'ok';
        }

        $arr['result'] = $result;
        echo json_encode($arr);
    }





    /***************************
    update_student
    ***************************/
    public function update_student()
    {
        $inst_first_name = $_POST['inst_first_name'];
        $inst_last_name  = $_POST['inst_last_name'];
        $inst_email      = $_POST['inst_email'];
        $inst_id         = $_POST['inst_id'];


        $arr = array(
            'first_name' => $inst_first_name,
            'last_name' => $inst_last_name,
            'email' => $inst_email
        );
        $this->master->updateRecord('students', $arr, array(
            'id' => $inst_id
        ));

        $this->session->set_userdata($arr);

        //get validation message from admin
        $action_message = $this->common->get_message('generic_update_message');

        $arr['message'] = $action_message['success'];
        $result         = 'ok';

        $arr['result'] = $result;
        echo json_encode($arr);
    }


    /***************************
    update_customer
    ***************************/
    public function update_customer()
    {


        $inst_first_name = $_POST['inst_first_name'];
        $inst_last_name  = $_POST['inst_last_name'];
        $inst_email      = $_POST['inst_email'];
        $location        = $_POST['address'];
        $business_name   = $_POST['business_name'];
        $lat             = $_POST['lat'];
        $long            = $_POST['long'];
        $inst_id         = $_POST['inst_id'];
        $country_id      = $_POST['country_code'];
        $mobile          = ltrim($_POST['mobile'], '0');
        $country_short   = $_POST['country_short'];
        $about           = $_POST['about'];
        $country_short   = $_POST['country_short'];
        $calling_code    = $_POST['calling_code'];
        $contact_no      = $calling_code . $mobile;


        $arr = array(
            'first_name' => $inst_first_name,
            'last_name' => $inst_last_name,
            'email' => $inst_email,
            'location' => $location,
            'business_name' => $business_name,
            'lat' => $lat,
            'lng' => $long,
            'country_id' => $country_id,
            'calling_code' => $calling_code,
            'calling_digits' => $mobile,
            'contact_no' => $contact_no,
            'about' => $about
        );





        $this->master->updateRecord('customers', $arr, array(
            'id' => $inst_id
        ));


        $log_activity = array(
            'name' => $inst_first_name . ' updated his profile.',
            'type' => 'update_profile',
            'details' => serialize($arr)
        );
        $this->master->insertRecord('activity_log', $log_activity);


        $arr['address'] = $location;
        $this->session->set_userdata($arr);

        //get validation message from admin
        $action_message = $this->common->get_message('generic_update_message');

        $arr['message'] = $action_message['success'];
        $result         = 'ok';

        $arr['result'] = $result;
        echo json_encode($arr);

    }



    /***************************
    customer_genre
    ***************************/
    public function customer_genre()
    {
        $id     = $_POST['id'];
        $genres = array();
        if (isset($_POST['genres'])) {
            $genres = $_POST['genres'];
        }

        $arr = array(
            'dance_genres' => serialize($genres)
        );

        $this->master->updateRecord('customers', $arr, array(
            'id' => $id
        ));

        //get validation message from admin
        $action_message = $this->common->get_message('generic_update_message');

        $arr['message'] = $action_message['success'];
        $result         = 'ok';

        $this->session->set_flashdata($result, $arr['message']);
        $arr['result'] = $result;
        echo json_encode($arr);

    }

    /***************************
    update_agency
    ***************************/
    public function update_agency()
    {

        $agency_name     = $_POST['agency_name'];
        $agency_email    = $_POST['agency_email'];
        $address         = $_POST['address'];
        $lat             = $_POST['lat'];
        $long            = $_POST['long'];
        $agency_id       = $_POST['agency_id'];
        $country_id      = $_POST['country_code'];
        $mobile          = ltrim($_POST['mobile'], '0');
        $country_short   = $_POST['country_short'];
        $website         = $_POST['website'];
        $minimum_premium = $_POST['minimum_premium'];
        $background      = $_POST['background'];
        $facilities      = $_POST['facilities'];
        $calling_code    = $_POST['calling_code'];
        $contact_no      = $calling_code . $mobile;

        $result = 'error';

        $arr = array(
            'name' => $agency_name,
            'email' => $agency_email,
            'address' => $address,
            'lat' => $lat,
            'lng' => $long,
            'country_id' => $country_id,
            'calling_code' => $calling_code,
            'calling_digits' => $mobile,
            'contact_no' => $contact_no,
            'background' => $background,
            'facilities' => $facilities,
            'minimum_premium' => $minimum_premium,
            'website' => $website
        );


        $this->master->updateRecord('agency', $arr, array(
            'id' => $agency_id
        ));

        $this->session->set_userdata($arr);

        //get validation message from admin
        $action_message = $this->common->get_message('generic_update_message');

        $arr['message'] = $action_message['success'];
        $result         = 'ok';

        $arr['result'] = $result;
        echo json_encode($arr);
    }

    /***************************
    agency_update_password
    ***************************/
    public function agency_update_password()
    {
        $id = $_POST['id'];
        $pw = $_POST['pw'];

        $arr = array(
            'password' => md5($pw),
            'status' => 'Y'
        );

        $this->master->updateRecord('agency', $arr, array(
            'id' => $id
        ));

        //get validation message from admin
        $action_message = $this->common->get_message('save_password');

        $arr['message'] = $action_message['success'];
        $result         = 'ok';
        $arr['result']  = $result;
        echo json_encode($arr);
    }


    /***************************
    customer_update_password
    ***************************/
    public function customer_update_password()
    {
        $id = $_POST['id'];
        $timezone = (isset($_POST['timezone'])) ? $_POST['timezone'] : '32';
        $pw = $_POST['pw'];

        $arr = array(
            'timezone' => $timezone,
            'password' => md5($pw),
            'status' => 'Y'
        );

        $this->master->updateRecord('customers', $arr, array(
            'id' => $id
        ));

        //get validation message from admin
        $action_message = $this->common->get_message('save_password');

        $arr['message'] = $action_message['success'];
        $result         = 'ok';
        $arr['result']  = $result;
        echo json_encode($arr);
    }

    /***************************
    update_password
    ***************************/
    public function update_password()
    {
        $id   = $_POST['id'];
        $type = 'agency';
        $pw   = $_POST['pw'];

        $arr = array(
            'password' => md5($pw)
        );

        if (isset($_POST['type'])) {
            $type = $_POST['type'];
            $this->master->updateRecord('customers', $arr, array(
                'id' => $id
            ));
        } else {
            $this->master->updateRecord('agency', $arr, array(
                'id' => $id
            ));
        }



        //get validation message from admin
        $action_message = $this->common->get_message('save_password');

        $arr['message'] = $action_message['success'];
        $result         = 'ok';
        $arr['result']  = $result;

        echo json_encode($arr);
    }




    /***************************
    customers
    ***************************/
    public function resend_customer()
    {
        $id         = $_POST['id'];
        $customer   = $this->master->getRecords('customers', array(
            'id' => $id
        ));
        //admin emailer info
        $adminemail = $this->common->admin_email();

        //email settings
        $info_arr = array(
            'to' => $customer[0]['email'],
            'from' => $adminemail,
            'subject' => 'Welcome to Transit Insurance',
            'view' => 'registration-mail-to-customer',
            'emailer_file_name' => 'registration-mail-to-customer'
        );


        $other_info = array(
            'password' => '',
            'view' => 'registration-mail-to-customer',
            'emailer_file_name' => 'registration-mail-to-customer',
            'name' => $this->common->customer_name($customer[0]['id']),
            'user_name' => $customer[0]['email'],
            'user_email' => $customer[0]['email'],
            'link' => 'settings/verify/customers/' . $id . '/' . md5($id)
        );

        $this->emailer->sendmail($info_arr, $other_info);


        //get validation message from admin
        $action_message = $this->common->get_message('resend_confirmation');

        $arr['other_info'] = $other_info;
        $arr['info_arr']   = $info_arr;
        $arr['message']    = $action_message['success'];
        $result            = 'ok';

        echo json_encode($arr);
    }

    /***************************
    customers
    ***************************/
    public function customers()
    {
        $user_id = $this->session->userdata('id');
        $first   = $_POST['first'];
        $last    = $_POST['last'];
        $email   = $_POST['email'];
        $id      = $_POST['id'];
        $nowtime = date('Y-m-d H:i:s');

        $result = 'error';

        $arr = array(
            'first_name' => $first,
            'last_name' => $last,
            'email' => $email
        );

        //save update
        if ($id != '') {
            $this->master->updateRecord('customers', $arr, array(
                'id' => $id
            ));

            //get validation message from admin
            $action_message = $this->common->get_message('generic_update_message');

            $arr['message'] = $action_message['success'];
            $result         = 'ok';

        } else {
            $arr['date_added'] = $nowtime;
            $arr['status']     = 'N';
            $arr['agency_id']  = $user_id;
            $id                = $this->master->insertRecord('customers', $arr, true);

            //admin emailer info
            $adminemail = $this->common->admin_email();

            //email settings
            $info_arr = array(
                'to' => $email,
                'from' => $adminemail,
                'subject' => 'Welcome to Transit Insurance',
                'view' => 'registration-mail-to-customer',
                'emailer_file_name' => 'registration-mail-to-customer'
            );


            $other_info = array(
                'password' => '',
                'view' => 'registration-mail-to-customer',
                'emailer_file_name' => 'registration-mail-to-customer',
                'name' => $first . ' ' . $last,
                'user_name' => $email,
                'user_email' => $email,
                'link' => 'settings/verify/customers/' . $id . '/' . md5($id)
            );

            $this->emailer->sendmail($info_arr, $other_info);


            //get validation message from admin
            $action_message = $this->common->get_message('generic_add_message');

            $arr['other_info'] = $other_info;
            $arr['info_arr']   = $info_arr;
            $arr['message']    = $action_message['success'];
            $result            = 'ok';

        }

        $this->session->set_flashdata($result, $arr['message']);

        $arr['result'] = $result;
        echo json_encode($arr);


    }


    /***************************
    forgotpass
    ***************************/
    public function forgotpass()
    {
        $email = $_POST['email'];

        $user = $this->master->getRecords('agency', array(
            'email' => $email
        ));
        $type = 'agency';
        if (count($user) == 0) {
            $user = $this->master->getRecords('customers', array(
                'email' => $email
            ));
            $type = 'customers';
        }

        //get validation message from admin
        $action_message = $this->common->get_message('reset_password');

        $arr['message'] = $action_message['error'];
        $result         = 'error';

        if (count($user) > 0) {

            //generate random password

            $alphabet    = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
            $pass        = array(); //remember to declare $pass as an array
            $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache

            for ($i = 0; $i < 8; $i++) {

                $n      = rand(0, $alphaLength);
                $pass[] = $alphabet[$n];

            }

            $random_pass = implode($pass);


            $this->master->updateRecord($type, array(
                'password' => md5($random_pass)
            ), array(
                'id' => $user[0]['id']
            ));

            //admin emailer info
            $adminemail = $this->common->admin_email();

            //email settings
            $info_arr = array(
                'to' => $email,
                'from' => $adminemail,
                'subject' => 'Welcome to Transit Insurance',
                'view' => 'forget-password-mail-to-user',
                'emailer_file_name' => 'forget-password-mail-to-user'
            );


            $other_info = array(
                'password' => $random_pass,
                'view' => 'forget-password-mail-to-user',
                'emailer_file_name' => 'forget-password-mail-to-user',
                'name' => $user[0]['first_name'],
                'agency_name' => '',
                'user_name' => $email,
                'user_email' => $email,
                'link' => ''
            );

            $this->emailer->sendmail($info_arr, $other_info);
            $arr['other_info'] = $other_info;
            $arr['info_arr']   = $info_arr;

            $arr['message'] = $action_message['success'];
            $result         = 'ok';



        }

        $arr['result'] = $result;
        echo json_encode($arr);

    }

    /***************************
    classes
    ***************************/
    public function classes()
    {

        $user_id      = $this->session->userdata('id');
        $name         = $_POST['name'];
        $class_id     = $_POST['class_id'];
        $genres       = array();
        $dayweek      = array(); //$_POST['dayweek']; //array
        $sched_start  = array(); // $_POST['start'];
        $sched_end    = array(); // $_POST['end'];
        $location     = $_POST['location'];
        $lat          = $_POST['lat'];
        $long         = $_POST['long'];
        $cost         = $_POST['cost'];
        $max_students = $_POST['max_students'];
        $customers    = array();
        $nowtime      = date('Y-m-d H:i:s');
        $level        = 'Beginners';
        if (isset($_POST['level'])) {
            $level = $_POST['level'];
        }

        if (isset($_POST['genres'])) {
            $genres = $_POST['genres'];
        }

        if (isset($_POST['customers'])) {
            $customers = $_POST['customers'];
        }

        $result = 'error';

        $arr = array(
            'name' => $name,
            'genres' => serialize($genres),
            'location' => $location,
            'lat' => $lat,
            'lng' => $long,
            'customers' => serialize($customers),
            'cost' => $cost,
            'schedule' => serialize($dayweek),
            'sched_start' => serialize($sched_start),
            'sched_end' => serialize($sched_end),
            'max_students' => $max_students,
            'level' => $level
        );


        //save update
        if ($class_id != '') {
            $this->master->updateRecord('classes', $arr, array(
                'id' => $class_id
            ));

            //get validation message from admin
            $action_message = $this->common->get_message('generic_update_message');

            $arr['message'] = $action_message['success'];
            $result         = 'ok';

        } else {
            $arr['date_added'] = $nowtime;
            $arr['user_id']    = $user_id;
            $this->master->insertRecord('classes', $arr);

            //get validation message from admin
            $action_message = $this->common->get_message('generic_add_message');

            $arr['message'] = $action_message['success'];
            $result         = 'ok';

        }

        if (!isset($arr['message'])) {
            $arr['message'] = 'Something went wrong.';
        }

        $this->session->set_flashdata($result, $arr['message']);

        $arr['result'] = $result;
        echo json_encode($arr);

    }

    /***************************
    delete_tb
    ***************************/
    public function delete_tb()
    {
        $id = $_POST['id'];
        $tb = $_POST['table'];

        //delete associated data
        if ($tb == 'agency') {
            $users = $this->master->getRecords('customers', array(
                'agency_id' => $id
            ));
            if (count($users) > 0) {
                foreach ($users as $r => $value) {

                    $policies = $this->master->getRecords('biz_referral', array(
                        'broker_id' => $value['id']
                    ));
                    if (count($policies) > 0) {
                        foreach ($policies as $rr => $valuee) {
                            $this->master->deleteRecord('biz_referral', 'id', $valuee['id']);
                        }
                    }

                    $policies2 = $this->master->getRecords('biz_renewal_history', array(
                        'broker_id' => $value['id']
                    ));
                    if (count($policies2) > 0) {
                        foreach ($policies2 as $rr2 => $valuee2) {
                            $this->master->deleteRecord('biz_referral', 'id', $valuee2['id']);
                        }
                    }


                    $this->master->deleteRecord('customers', 'id', $value['id']);
                }
            }
            $brokerage = $this->master->getRecords('brokerage', array(
                'agency_id' => $id
            ));
            if (count($brokerage) > 0) {
                foreach ($brokerage as $r => $value) {
                    $this->master->deleteRecord('brokerage', 'id', $value['id']);
                }
            }
        }
        if ($tb == 'brokerage') {
            $users = $this->master->getRecords('customers', array(
                'brokerage_id' => $id
            ));
            if (count($users) > 0) {
                foreach ($users as $r => $value) {

                    //					$policies = $this->master->getRecords('biz_referral', array('broker_id'=>$value['id']));
                    //					if(count($policies) > 0){
                    //						foreach($policies as $rr=>$valuee){
                    //							$this->master->deleteRecord('biz_referral', 'id', $valuee['id']);
                    //						}
                    //					}
                    //
                    //					$policies2 = $this->master->getRecords('biz_renewal_history', array('broker_id'=>$value['id']));
                    //					if(count($policies2) > 0){
                    //						foreach($policies2 as $rr2=>$valuee2){
                    //							$this->master->deleteRecord('biz_referral', 'id', $valuee2['id']);
                    //						}
                    //					}

                    $this->master->deleteRecord('customers', 'id', $value['id']);

                }
            }
        }

        $this->master->deleteRecord($tb, 'id', $id);
        echo $id;
    }


    /***************************
    delete_tbx
    ***************************/
    public function delete_tbx()
    {
        $id = $_POST['id'];
        $tb = $_POST['table'];
        $this->master->deleteRecord($tb, 'id', $id);
        echo $id;
    }




    /***************************
    action_messages
    ***************************/
    public function action_messages()
    {
        $action_messages = $this->common->get_message();

        echo json_encode($action_messages);
    }



    /***************************
    social_login
    ***************************/
    public function social_login()
    {


        $type        = $_POST['type'];
        $id          = $_POST['id'];
        $email       = $_POST['email'];
        $img         = $_POST['img'];
        $action_type = $_POST['action_type'];
        $name        = $_POST['name'];

        $nowtime = date('Y-m-d H:i:s');

        $whr_user = array(
            'email' => $email,
            $type . '_id' => $id //social type
        );

        $whr_m_user = array(
            'email' => $email
        );

        $agency   = $this->master->getRecords('agency', $whr_user);
        $customer = $this->master->getRecords('customers', $whr_user);
        $student  = $this->master->getRecords('students', $whr_user);

        $agency2   = $this->master->getRecords('agency', $whr_m_user);
        $customer2 = $this->master->getRecords('customers', $whr_m_user);

        //get validation message from admin
        $action_message = $this->common->get_message('user_login');
        $message        = $action_message['error'];
        $result         = 'error';

        //detect actions
        if ($action_type == 'signup') {

            //get validation message from admin
            $action_message = $this->common->get_message('agency_signup');

            $message = $action_message['error'];
            $result  = 'error';
            if (count($agency) == 0) {

                $arr['status']       = 'N';
                $arr['enabled']      = 'N';
                $arr['email']        = $email;
                $arr['name']         = $name;
                $arr[$type . '_id']  = $id;
                $arr[$type . '_img'] = $img;
                $arr['date_added']   = $nowtime;

                $id = $this->master->insertRecord('agency', $arr, true);

                //admin emailer info
                $adminemail = $this->common->admin_email();

                //email settings
                $info_arr = array(
                    'to' => $email,
                    'from' => $adminemail,
                    'subject' => 'Welcome to Transit Insurance',
                    'view' => 'registration-mail-to-agency',
                    'emailer_file_name' => 'registration-mail-to-agency'
                );


                $other_info = array(
                    'password' => '',
                    'view' => 'registration-mail-to-agency',
                    'emailer_file_name' => 'registration-mail-to-agency',
                    'name' => $this->session->userdata('first_name'), //$first.' '.$last,
                    'agency_name' => '', //$name,
                    'user_name' => $email,
                    'user_email' => $email,
                    'link' => 'landing/verify_agency/' . $id . '/' . md5($id)
                );

                $this->emailer->sendmail($info_arr, $other_info);

                $message = $action_message['success'];
                $result  = 'ok';

            } //not exist
        }

        //if login
        else {

            if (count($agency) > 0) {
                $arr['result']  = 'error';
                $arr['message'] = '<i class="fa fa-warning text-warning"></i> Connect incorrect.';
                foreach ($agency as $r => $value) {
                    unset($value['password']);
                    $value['type']     = 'agency';
                    $value['location'] = $value['address'];
                    $arr['userdata']   = $value;

                    $value['multiple_account'] = 'no';
                    //include customer to switch
                    if (count($customer2) > 0) {
                        $value['multiple_account'] = 'yes';
                    }


                    if ($value['enabled'] != 'N') {
                        $this->session->set_userdata($value);
                        $result  = 'ok';
                        $message = $action_message['success'];
                    } else {
                        $arr['result']  = 'error';
                        $arr['message'] = '<i class="fa fa-warning text-warning"></i> Account not activated.';
                    }


                }
            } else if (count($customer) > 0) {
                $arr['result']  = 'error';
                $arr['message'] = '<i class="fa fa-warning text-warning"></i> Connect incorrect.';
                foreach ($customer as $r => $value) {

                    unset($value['password']);
                    $value['name']    = $value['first_name'] . ' ' . $value['last_name'];
                    $value['type']    = 'customer';
                    $value['address'] = $value['location'];
                    $arr['userdata']  = $value;

                    $value['multiple_account'] = 'no';
                    //include agency to switch
                    if (count($agency2) > 0) {
                        $value['multiple_account'] = 'yes';
                    }

                    if ($value['enabled'] != 'N') {
                        $this->session->set_userdata($value);
                        $result  = 'ok';
                        $message = $action_message['success'];
                    } else {
                        $arr['result']  = 'error';
                        $arr['message'] = '<i class="fa fa-warning text-warning"></i> Account not activated.';
                    }



                }
            } else if (count($student) > 0) {
                $arr['result']  = 'error';
                $arr['message'] = '<i class="fa fa-warning text-warning"></i> Connect incorrect.';
                foreach ($student as $r => $value) {

                    unset($value['password']);
                    $value['name']   = $value['first_name'] . ' ' . $value['last_name'];
                    $value['type']   = 'student';
                    //$value['address'] = $value['location'];
                    $arr['userdata'] = $value;

                    $value['multiple_account'] = 'no';
                    //include agency to switch
                    //						if(count($agency2) > 0){
                    //							$value['multiple_account'] = 'yes';
                    //						}


                    if ($value['enabled'] != 'N') {
                        $this->session->set_userdata($value);
                        $result  = 'ok';
                        $message = $action_message['success'];
                    } else {
                        $arr['result']  = 'error';
                        $arr['message'] = '<i class="fa fa-warning text-warning"></i> Account not activated.';
                    }

                }
            }
        }

        $arr['result']  = $result;
        $arr['action']  = $action_type;
        $arr['message'] = $message;
        echo json_encode($arr);

    }


    /***************************
    social_login
    ***************************/
    public function submit_schedule()
    {
        //class=18&max_student=33&start_day=Tue+7%2F7%2F2015&end_day=Tue+7%2F7%2F2015&start_time=6%3A44+PM&end_time=10%3A44+PM&duration=240&recur=&weekly_recure=3&days_in_week%5B%5D=1
        $user_id = $this->session->userdata('id');
        $type    = $this->session->userdata('type');

        $class       = $_POST['class'];
        $max_student = $_POST['max_student'];
        $start_day   = $_POST['start_day'];
        $end_day     = $start_day; //$_POST['end_day'];
        $start_time  = $_POST['start_time'];
        $end_time    = $_POST['end_time'];
        $duration    = $_POST['duration'];
        $recur       = '';
        if (isset($_POST['recur'])) {
            $recur = 'yeah';
        }

        $weekly_recure = $_POST['weekly_recure'];
        $days_in_week  = array();
        if (isset($_POST['days_in_week'])) {
            $days_in_week = $_POST['days_in_week'];
        }

        $schedule_date = array(
            $start_day,
            $end_day
        );
        $schedule_date = serialize($schedule_date);
        $schedule_time = array(
            $start_time,
            $end_time
        );
        $schedule_time = serialize($schedule_time);

        $start_time_format = $_POST['start_time_format'];
        $end_time_format   = $_POST['end_time_format'];

        $start_date_format = $_POST['start_date_format'];
        $end_date_format   = $start_date_format;

        $the_end_date_format = $_POST['end_date_format'];


        $nowtime = date('Y-m-d H:i:s');


        //get validation message from admin
        $action_message = $this->common->get_message('generic_add_message');


        $concat_startdate_time = $start_date_format . ' 00:00';
        $concat_enddate_time   = $end_date_format . ' 00:00';

        //format schedule date
        if ($start_time_format != '') {
            $concat_startdate_time = $start_date_format . ' ' . $start_time_format;
            $start_date_format     = $start_date_format . ' ' . $start_time_format . ':00'; //T
        }

        if ($end_time_format != '') {
            $concat_enddate_time = $end_date_format . ' ' . $end_time_format;
            $end_date_format     = $end_date_format . ' ' . $end_time_format . ':00'; //T
        }



        //run this if recur is not set set
        if ($recur == '') {
            $arr = array(
                'start' => $start_date_format,
                'end' => $end_date_format,
                'agency_id' => $user_id,
                'class_id' => $class,
                'max_student' => $max_student,
                'schedule_date' => $schedule_date,
                'schedule_time' => $schedule_time,
                'duration' => $duration,
                'day_in_week' => serialize($days_in_week),
                'status' => 'Y',
                'date_added' => $nowtime
            );

            $this->master->insertRecord('schedules', $arr);

            $test['the_schedules'] = $arr;
            $test['message']       = $action_message['success'];
            $test['result']        = 'ok';
            $this->session->set_flashdata($test['result'], $action_message['success']);
            echo json_encode($test);
            return false;

        }



        //get remaining days from the start day
        $current_year = date('Y');
        $endyr_date   = new DateTime($current_year . '-12-31 23:59');
        $strt_date    = new DateTime($start_date_format);
        $eend_date    = new DateTime($the_end_date_format);
        $diff         = $strt_date->diff($endyr_date);
        $diff2        = $strt_date->diff($eend_date);

        $remaining_days       = $diff->y * 365 + $diff->m * 30 + $diff->d + $diff->h / 24; // + $diff->i / 60;
        $remaining_endby_days = $diff2->y * 365 + $diff2->m * 30 + $diff2->d + $diff2->h / 24; // + ($diff2->i / 60)/24;


        $the_schedules = array();


        //$weekly_recure = $weekly_recure * 7;
        $until                   = ($remaining_days / $weekly_recure);
        $test['weekly_recure']   = $weekly_recure;
        $test['until']           = $until;
        $test['days_in_week']    = $days_in_week; //weekday
        $test['recurrence_type'] = 'if weekly';

        $test['message'] = 'if weekly';
        $test['status']  = 'if weekly';


        //End after x occurence
        //$weekly_looper = $remaining_endby_days;
        $weekly_looper = $weekly_recure;

        //apply recurrence
        $day_after_day             = 1;
        $current_week_number       = date('W', strtotime($concat_startdate_time));
        $curr_week_number          = 0;
        $test['week_number_count'] = array();



        $total_stored_sched = 0;
        for ($x = 0; $x < $remaining_days; $x++) { //loop until remaining days of the year
            $weekday_id      = date('w', strtotime($concat_startdate_time));
            $new_week_number = date('W', strtotime($concat_startdate_time));

            $test['week_number_count'][] = $new_week_number;


            if (($curr_week_number % $weekly_recure) == 0) { //modulus for weekly recur
                if (in_array($weekday_id, $days_in_week)) {

                    $arr             = array(
                        'start' => $start_date_format,
                        'end' => $end_date_format,
                        'agency_id' => $user_id,
                        'class_id' => $class,
                        'max_student' => $max_student,
                        'schedule_date' => $schedule_date,
                        'schedule_time' => $schedule_time,
                        'duration' => $duration,
                        'day_in_week' => serialize($days_in_week),
                        'status' => 'Y',
                        'date_added' => $nowtime
                    );
                    $the_schedules[] = $arr;
                    $this->master->insertRecord('schedules', $arr);
                }
            }


            //update number of week
            if ($current_week_number != $new_week_number) {
                $current_week_number = $new_week_number;
                $curr_week_number++;
            }


            //change date variables
            $start_added_date_time = strtotime($concat_startdate_time . ' + ' . $day_after_day . ' DAYS');
            $end_added_date_time   = strtotime($concat_enddate_time . ' + ' . $day_after_day . ' DAYS');

            $concat_startdate_time = date("Y-m-d H:i", $start_added_date_time);
            $concat_enddate_time   = date("Y-m-d H:i", $end_added_date_time);

            $start_time_format = substr($concat_startdate_time, -5);
            $end_time_format   = substr($concat_enddate_time, -5);
            $start_date_format = substr($concat_startdate_time, 0, 10);
            $end_date_format   = substr($concat_enddate_time, 0, 10);

            $start_date_format = $start_date_format . ' ' . $start_time_format . ':00'; //T
            $end_date_format   = $end_date_format . ' ' . $end_time_format . ':00'; //T


            //End by date
            if ($total_stored_sched == $weekly_looper) {
                $test['message']       = $action_message['success'];
                $test['the_schedules'] = $the_schedules;
                $test['result']        = 'ok';
                $this->session->set_flashdata($test['result'], $action_message['success']);
                echo json_encode($test);
                return false;
            }
            //End by date


            //increment stored scheds
            if (($curr_week_number % $weekly_recure) == 0) { //modulus for weekly recur
                if (in_array($weekday_id, $days_in_week)) {
                    $total_stored_sched++;
                }
            }



        } //end for loop


        //success if reach this section
        $test['message'] = $action_message['success'];
        $test['result']  = 'success';
        $this->session->set_flashdata($test['status'], $action_message['success']);

        //if no schedule added
        if (count($the_schedules) == 0) {
            $test['message'] = '<i class="fa fa-info text-warning"></i> No schedule was added. Please double check your recurring pattern.';
            $test['result']  = 'ok';
            $this->session->set_flashdata($test['result'], $test['message']);


        }

        $test['the_schedules'] = $the_schedules;
        echo json_encode($test);

    }


    public function remove_card()
    {
        $id     = $_POST['id'];
        $stripe = $_POST['stripe'];

        Stripe::setApiKey("sk_test_CiqlhOTQUSiBilafy4373MJe"); //sk_live_gs8DE2Zo8isW5HEcACTMIKAV

        $cu = Stripe_Customer::retrieve($stripe);
        $cu->delete();

        $arr = array(
            'stripe_id' => ''
        );

        $this->master->updateRecord('customers', $arr, array(
            'id' => $id
        ));

        //get validation message from admin
        $action_message = $this->common->get_message('customer_remove_card');
        $message        = $action_message['success'];
        $result         = 'ok';
        $this->session->set_flashdata($result, $message);

        $arr['result']  = $result;
        $arr['message'] = $message;
        echo json_encode($arr);
    }

    public function cargo_price()
    {
        //cargo-price=0.02&cargo-price-id=1
        $price = $_POST['cargo-price'];
        $id    = $_POST['cargo-price-id'];
        $this->master->updateRecord('cargo_category', array(
            'price' => $price
        ), array(
            'id' => $id
        ));
        echo json_encode(array(
            'id' => $id
        ));
    }


    public function inline_text_update()
    {
        $user_id    = $this->session->userdata('id');
        $text_input = $_POST['text_input'];
        $id         = $_POST['id'];
        $field_name = $_POST['field_name'];
        $db_name    = $_POST['db_name'];

        $this->master->updateRecord($db_name, array(
            $field_name => $text_input
        ), array(
            'id' => $id
        ));
        echo json_encode(array(
            'id' => $id
        ));

    }

    public function cargo_price_agency()
    {
        //cargo-price=0.02&cargo-price-id=1
        $user_id = $this->session->userdata('id');

        if ($user_id == '' || $this->session->userdata('logged_admin') != '') {
            $user_id = $this->common->default_cargo();
        }
        $type = $this->session->userdata('type');

        $agency_id = $_POST['cargo-agency-id'];
        $price     = $_POST['cargo-price'];
        $id        = $_POST['cargo-price-id'];
        $pricetype = $_POST['agency-price-type'];

        $db_name = 'agency';


        $theprice = ($price) ? $price : 0;
        if (isset($_POST['db-name'])) {
            $db_name = $_POST['db-name'];

            //format float for deductible
            if ($db_name == 'deductibles') {
                $theprice = floatval($theprice);
            }
        } else {
            $agency_prices      = $this->common->agency_prices($user_id, $pricetype);
            //set value to the index
            $agency_prices[$id] = $price;
            $theprice           = serialize($agency_prices);
        }


        if ($agency_id != '') {
            $this->master->updateRecord($db_name, array(
                $pricetype => $theprice
            ), array(
                'id' => $agency_id
            ));
        } else {
            $this->master->updateRecord('cargo_category', array(
                'price' => $price
            ), array(
                'id' => $id
            ));
        }
        echo json_encode(array(
            'id' => $id
        ));

    }

    public function buyform_to_session()
    {
        //check if premium approved
        $is_approved = $this->session->userdata('is_approved');

        $buy_inputs          = $_POST['buy_inputs'];
        $cargo_inputs        = $_POST['cargo_inputs'];
        $premium             = ($is_approved == 'yes') ? $this->session->userdata('premium') : $_POST['premium'];
        $cargo_price         = $_POST['cargo_price'];
        $currency            = $_POST['currency'];
        $selected_deductible = $_POST['selected_deductible'];
        $referral_id         = $this->session->userdata('referral_id');
        $max_insured         = (isset($_POST['max_insured'])) ? $_POST['max_insured'] : '';

        //format buy input array
        $buy_form_inputs = array();
        if (is_array($buy_inputs)) {
            foreach ($buy_inputs as $r => $value) {
                $buy_form_inputs[$value['name']] = $value['value'];
            }
        }

        //check currency value to max insured
        $default_cargo         = $this->common->db_field_id('admin', 'default_cargo', '2');
        $base_currency         = $this->common->base_currency($default_cargo);
        $insured_val           = (isset($buy_form_inputs['invoice'])) ? $buy_form_inputs['invoice'] : 0;
        $insured_val           = $this->common->numeric_currency($insured_val);
        $insured_converted_val = ($base_currency != $currency) ? $this->common->converted_value($currency, $base_currency, $insured_val) : $insured_val;

        $refer = false;

        if ($insured_converted_val > $max_insured) {
            $refer = true;
        }


        //add converted val to session
        $buy_inputs[] = array(
            'name' => 'converted_invoice',
            'value' => $insured_converted_val
        );
        //add default currency
        $buy_inputs[] = array(
            'name' => 'base_currency',
            'value' => $base_currency
        );

        //check if less than 15 fields / meaning disabled, do not update
        if(count($buy_inputs) < 15){
            $buy_inputs = $this->session->userdata('buy_inputs');
        }

        //add cargo inputs to buy inputs
        foreach ($cargo_inputs as $cargoi) {
            //$buy_inputs[] = $cargoi;
        }

        $arr = array(
            'buy_inputs' => $buy_inputs,
            'premium' => round($premium, 2),
            'premium_format' => number_format($premium, 2, '.', ','),
            'selected_deductible' => $selected_deductible,
            'cargo_price' => $cargo_price,
            'currency' => $currency,
            'refer' => $refer
            //'bindoption'=>$bindoption
        );
        if ($referral_id == '') {
            $this->session->set_userdata($arr);
        }
        //$arr['premium'] = $this->session->userdata('premium');
        echo json_encode($arr);

    }

    public function admin_refer_country()
    {



    }

    public function select_consignor()
    {
        $mycustomers = $_POST['mycustomers'];
        $customer    = $this->master->getRecords('agent_customers', array(
            'id' => $mycustomers
        ));

        if (count($customer) == 0) {
            $details = array(
                'first_name' => '',
                'last_name' => '',
                'business_name' => '',
                'location' => '',
                'lat' => '',
                'lng' => '',
                'country_id' => '',
                'contact_no' => '',
                'calling_code' => '',
                'calling_digits' => '',
                'email' => ''
            );
        } else {
            $details = unserialize($customer[0]['details']);
        }

        $details['id'] = $mycustomers;
        $arr['single'] = $details; //consignee

        $this->session->set_userdata($arr);
        echo json_encode($arr);
    }


    public function cargo_details_form()
    {
        $data       = $_POST['data'];
        $cargo      = $_POST['cargo'];

        //check if less than 15 fields / meaning disabled, do not update
        if(count($cargo) < 15){
            $cargo = $this->session->userdata('buy_inputs');
        }
        
        $buy_inputs = array_merge($cargo, $data);
        $this->session->set_userdata('buy_inputs', $buy_inputs);
        echo json_encode($buy_inputs);
    }

    public function single_purch_form()
    {
        $user_id      = $this->session->userdata('id');
        $is_approved      = $this->session->userdata('is_approved');
        $single       = $this->session->userdata('single');
        $agent_name = $this->session->userdata('first_name'); //(isset($single['email'])) ? $single['email'] : $this->session->userdata('email');
        $single_email = $this->session->userdata('email'); //(isset($single['email'])) ? $single['email'] : $this->session->userdata('email');

        if(empty($user_id)){
            $resdata['result']  = 'session_expired';
            $resdata['message'] = 'Your session expired. Please login again.';
            echo json_encode($resdata);
            return false;
        }

        $inputs       = $_POST['inputs'];
        $first        = $_POST['first'];
        $last         = $_POST['last'];
        $bname        = $_POST['bname'];
        $address      = $_POST['address'];
        $lat          = $_POST['lat'];
        $lng          = $_POST['lng'];
        $email        = $_POST['email'];
        $country_code = $_POST['country_code'];
        $ccode        = $_POST['ccode'];
        $mobile       = $_POST['mobile'];
        $notes_to_underwriter = (isset($_POST['notes'])) ? $_POST['notes'] : '';

        $info       = $_POST['info'];
        $crefer     = $_POST['crefer'];
        $buy_inputs = $this->session->userdata('buy_inputs');
        $premium    = $this->session->userdata('premium');
        $premium    = round($premium, 2);
        $nowtime    = date('Y-m-d H:i:s');


        $arr = array(
            'first_name' => $first,
            'last_name' => $last,
            'business_name' => $bname,
            'location' => $address,
            'lat' => $lat,
            'lng' => $lng,
            'country_id' => $country_code,
            'contact_no' => $ccode . ltrim($mobile, '0'),
            'calling_code' => $ccode,
            'calling_digits' => ltrim($mobile, '0'),
            'email' => $email
        );

        if ($user_id != '') {
            //if a consignor
            if ($info == 'consignor') {
                $this->master->updateRecord('customers', $arr, array(
                    'id' => $user_id
                ));
            }

            //if customer adding agent
            else if ($info == 'agent_customer') {
                $cust_info = array(
                    'agent_id' => $user_id,
                    'name' => $first . ' ' . $last,
                    'email' => $email,
                    'details' => serialize($arr),
                    'date_added' => $nowtime
                );
                $id        = '';
                if (isset($_POST['id'])) {
                    $id = $_POST['id'];
                }
                if ($id == '') {
                    $this->session->set_flashdata('ok', 'Customer successfully added.');
                    $this->master->insertRecord('agent_customers', $cust_info);
                } else {
                    $this->session->set_flashdata('ok', 'Customer successfully updated.');
                    $this->master->updateRecord('agent_customers', $cust_info, array(
                        'id' => $id
                    ));
                }
            }

        } else {
            $user_id = 0;
        }

        //check if consignee
        if ($info == 'consignee') {
            $consignee        = $arr;
            $arr              = array();
            $arr['consignee'] = $consignee;
            $to_session['consignee'] = $consignee;
        } else {
            $to_session['single'] = $arr;
            $to_session['address'] = $address;

            $arr['single']  = $arr;
            $arr['address'] = $address;
        }

        //store to session if not new customer
        if ($info != 'agent_customer') {
            $this->session->set_userdata($to_session);
        }


        //check if country needs referral
        if ($crefer != '' && ($is_approved != 'yes')) {
            $details = array(
                'buy_inputs' => $buy_inputs,
                'single_input' => $arr['single'],
                'premium' => $premium
            );


            $data = array(
                'customer_id' => $user_id,
                'details' => serialize($details),
                'status' => 'S',
                'email' => $single_email,
                'notes_to_underwriter'=>$notes_to_underwriter,
                'date_added' => $nowtime
            );

            $uuid         = $this->common->uuid();
            $data['uuid'] = $uuid;

            $booking_id = $this->master->insertRecord('bought_insurance', $data, true);

            //admin emailer info
            $adminemail = $this->common->admin_email();


            //email settings for agent
            $email_view = 'referral-notification-to-customer';
            $info_arr = array(
                'to' => $single_email,
                'from' => $adminemail,  
                'subject' => 'Welcome to Transit Insurance',
                'view' => $email_view,
                'emailer_file_name' => $email_view
            );

            $other_info = array(
                'password' => '',
                'view' => $email_view,
                'emailer_file_name' => $email_view,
                'name' => $agent_name, //$this->session->userdata('first_name'),
                'agency_name' => '', //$agency_info[0]['name'],
                'user_name' => $single_email,
                'user_email' => $single_email,
                'link' => 'dashboard' //landing/download/'.$user_id.'/'.$booking_id.'/'.md5($user_id)
            );
            if ($user_id == '0') {
                //$other_info['link'] = 'landing/reqdetails/'.$booking_id.'/'.md5($email);
            }

            $data['info_arr']   = $info_arr;
            $data['other_info'] = $other_info;
            $this->emailer->sendmail($info_arr, $other_info);

            //email settings for admin
            $email_view = 'referral-notification-to-admin';
            $info_arr = array(
                'to' => $this->common->recovery_admin(), //$adminemail,
                'from' => $adminemail,
                'subject' => 'Welcome to Transit Insurance',
                'view' => $email_view,
                'emailer_file_name' => $email_view
            );


            $other_info          = array(
                'password' => '',
                'view' => $email_view,
                'emailer_file_name' => $email_view,
                'name' => 'Admin', //$this->session->userdata('first_name'), //$first.' '.$last,
                'agency_name' => 'Admin',
                'user_name' => $adminemail,
                'user_email' => $adminemail,
                'link' => 'webmanager/dashboard/referrals'
            );
            $data['info_arr1']   = $info_arr;
            $data['other_info1'] = $other_info;
            $this->emailer->sendmail($info_arr, $other_info);


            //send to more admin emails
            $admin_info = $this->master->getRecords('admin', array(
                'id' => '2'
            ));
            $more_admin = (@unserialize($admin_info[0]['admin_emails'])) ? unserialize($admin_info[0]['admin_emails']) : array();

            if(!empty($more_admin)){
                foreach($more_admin as $r=>$value){
                    //email settings for agency
                    $info_arr['to'] = $value;
                    $rcount = $r+2;
                    $data['info_arr'.$rcount]   = $info_arr;
                    $data['other_info'.$rcount] = $other_info;
                    $this->emailer->sendmail($info_arr, $other_info);
                }
            }

            $arr['data'] = $data;
        }


        //get validation message from admin
        $action_message = $this->common->get_message('referral_request');
        $message        = $action_message['success'];
        $result         = 'ok';
        $booking_id = ''; //$this->save_transaction();

        if(isset($arr['consignee'])){
            $booking_id = $this->save_transaction();
        }

        $arr['result']  = $result;
        $arr['message'] = $message;
        $arr['booking_id'] = $booking_id;

        echo json_encode($arr);

    }

    public function rate_submit()
    {
        $user_id = $this->session->userdata('id');
        if ($user_id == '' || $this->session->userdata('logged_admin') != '') {
            $user_id = $this->common->default_cargo();
        }
        //$type = $this->session->userdata('type');

        $type = $_POST['type'];
        $rate = $_POST['rate'];
        //$rate = round($rate, 2);
        //$rate = floatval($rate);
        $this->master->updateRecord('agency', array(
            $type => $rate
        ), array(
            'id' => $user_id
        ));

        echo json_encode(array(
            'rate' => $rate
        ));
    }


    public function add_deductible()
    {
        $name  = $_POST['name'];
        $price = $_POST['price'];

        $user_id = $this->common->default_cargo();

        $arr = array(
            'agency_id' => $user_id,
            'deductible' => $name,
            //'the_default', 'N',
            'rate' => $price
        );
        $id  = $this->master->insertRecord('deductibles', $arr, true);

        $arr['result']  = 'ok';
        $arr['message'] = 'Deductible succesfully added.';
        echo json_encode($arr);
    }

    public function add_port_code()
    {
        $ccode    = $_POST['ccode'];
        $portcode = $_POST['portcode'];
        $portname = $_POST['portname'];

        $arr = array(
            'country_code' => $ccode,
            'port_code' => $portcode,
            'port_name' => $portname
        );

        $this->master->insertRecord('port_codes', $arr);

        $arr['result']  = 'ok';
        $arr['message'] = 'Port code succesfully added.';
        $this->session->set_flashdata($arr['result'], $arr['message']);
        echo json_encode($arr);

    }

    public function add_cargo_cat()
    {
        $code     = $_POST['code'];
        $name     = $_POST['name'];
        $price    = $_POST['price'];
        $referral = '';
        if (isset($_POST['referral'])) {
            $referral = 'Yes';
        }

        $user_id       = $this->common->default_cargo();
        $pricetype     = 'cargo_prices';
        $agency_prices = $this->common->agency_prices($user_id, $pricetype);
        $cargo_max     = $this->common->agency_prices($user_id, 'cargo_max');

        $arr = array(
            'hs_code' => $code,
            'description' => $name,
            'referral' => $referral
        );

        $id = $this->master->insertRecord('cargo_category', $arr, true);

        //set value to the index
        $agency_prices[$id] = $price;
        //$cargo_max[$id] = ($maxin) ? $maxin : 0;
        $this->master->updateRecord('agency', array(
            $pricetype => serialize($agency_prices)
        ), array(
            'id' => $user_id
        ));
        //$this->master->updateRecord('agency', array('cargo_max'=>serialize($cargo_max)), array('id'=>$user_id));

        //$arr['cargo_max'] = $cargo_max[$id];
        $arr['price']   = $agency_prices[$id];
        $arr['id']      = $id;
        $arr['result']  = 'ok';
        $arr['message'] = 'Cargo category succesfully added.';
        echo json_encode($arr);
    }

    public function currencies()
    {
        $this->load->view('front/currencies-stripe');
    }

    public function currs()
    {
        $arr    = $_POST['arr'];
        $curr[] = array();
        foreach ($arr as $r) {
            $r2   = substr($r, 0, 3);
            $arrs = array(
                'currency' => $r2,
                'name' => $r
            );
            $this->master->insertRecord('au_stripe_currencies', $arrs);
            $curr[] = $r2;
        }

        echo json_encode($curr);
    }

    public function reset_buyform()
    {
        $page = $this->uri->segment(3);
        $arr  = array(
            'buy_inputs',
            'booking_id',
            'premium',
            'premium_format',
            'selected_deductible',
            'cargo_price',
            'currency',
            'single',
            'consignee'
        );

        foreach ($arr as $key) {
            $this->session->unset_userdata($key);
        }

        redirect(base_url() . $page);
    }

    public function send_quote()
    {
        $first_name = $this->session->userdata('first_name');
        $last_name  = $this->session->userdata('last_name');
        $email      = $this->session->userdata('email');
        $email_to   = (isset($_POST['email'])) ? $_POST['email'] : $email;
        $buy_inputs = $this->session->userdata('buy_inputs');
        $b          = $this->common->buy_inputs($buy_inputs);
        $nowtime    = date('Y-m-d H:i:s');

        $query = array();

        foreach ($buy_inputs as $r => $value) {
            $title = $value['name'];
            $val   = $value['value'];

            $query[$title] = $val;
        }
        $query['date_sent'] = $nowtime;


        //admin emailer info
        $adminemail        = $this->common->admin_email();
        $emailer_file_name = 'quote-sending';
        //email settings for customer
        $info_arr          = array(
            'to' => $email_to,
            'from' => $adminemail,
            'subject' => 'Welcome to Transit Insurance',
            'view' => $emailer_file_name,
            'emailer_file_name' => $emailer_file_name
        );

        $other_info = array(
            'password' => '',
            'view' => $emailer_file_name,
            'emailer_file_name' => $emailer_file_name,
            'name' => $first_name,
            'details' => $b,
            'agency_name' => '', //$agency_info[0]['name'],
            'user_name' => $first_name . ' ' . $last_name,
            'user_email' => $email,
            'link' => ''//landing/sent_quote?' . http_build_query($query, '', '&') //&amp;
        );


        $data['info_arr2']  = $info_arr;
        $data['other_info'] = $other_info;
        $data['result']     = 'OK';
        $this->emailer->sendmail($info_arr, $other_info);

        //save quote on send quote
        $details = $this->common->save_quote();

        //$this->load->view('emails/standard-email-template', $other_info);
        echo json_encode($data);
    }

    public function save_quote()
    {
        $first_name = $this->session->userdata('first_name');
        $last_name  = $this->session->userdata('last_name');
        $email      = $this->session->userdata('email');
        $email_to   = $email;
        $buy_inputs = $this->session->userdata('buy_inputs');
        $b          = $this->common->buy_inputs($buy_inputs);
        $nowtime    = date('Y-m-d H:i:s');

        $query = array();

        foreach ($buy_inputs as $r => $value) {
            $title = $value['name'];
            $val   = $value['value'];

            $query[$title] = $val;
        }
        $query['date_sent'] = $nowtime;


        //admin emailer info
        $adminemail        = $this->common->admin_email();
        $emailer_file_name = 'quote-sending';
        //email settings for customer
        $info_arr          = array(
            'to' => $email_to,
            'from' => $adminemail,
            'subject' => 'Welcome to Transit Insurance',
            'view' => $emailer_file_name,
            'emailer_file_name' => $emailer_file_name
        );

        $other_info = array(
            'password' => '',
            'view' => $emailer_file_name,
            'emailer_file_name' => $emailer_file_name,
            'name' => $first_name,
            'details' => $b,
            'agency_name' => '', //$agency_info[0]['name'],
            'user_name' => $first_name . ' ' . $last_name,
            'user_email' => $email,
            'link' => '' //'landing/sent_quote?' . http_build_query($query, '', '&') //&amp;
        );

        $data['info_arr2']  = $info_arr;
        $data['other_info'] = $other_info;
        $data['result']     = 'OK';
        // $this->emailer->sendmail($info_arr, $other_info);

        $details = $this->common->save_quote();

        echo json_encode($details);
    }

    
    public function save_admin_tz(){
        $id        = $_POST['id'];
        $timezone        = $_POST['timezone'];

        $this->master->updateRecord('admin', array(
            'timezone' => $timezone
        ), array(
            'id' => $id
        ));

        echo json_encode($_POST);

    }
    public function save_tz(){
        $id        = $_POST['id'];
        $timezone        = $_POST['timezone'];

        $this->master->updateRecord('customers', array(
            'timezone' => $timezone
        ), array(
            'id' => $id
        ));

        echo json_encode($_POST);

    }


    public function make_first_underwriter()
    {
        $id        = $_POST['id'];
        $to        = $_POST['to'];
        $agency_id = $_POST['agency_id'];
        $brokers   = $this->master->getRecords('customers', array(
            'first_user' => 'Y'
        ));

        if (!empty($brokers)) {
            $this->master->updateRecord('customers', array(
                'first_user' => 'N'
            ), array(
                'id' => $brokers[0]['id']
            ));
        }

        $data = array(
            'first_user' => $to

        );
        $this->master->updateRecord('customers', $data, array(
            'id' => $id
        ));
        $data['message'] = 'Super Admin successfully updated.';

        echo json_encode($data);

    }


    public function activity_logged_pass()
    {
        $user_id = $this->session->userdata('logged_admin_id');

        $brokers = $this->master->getRecords('customers', array(
            'id' => $user_id
        ));

        if (count($brokers) == 0) {
            $brokers = $this->master->getRecords('admin', array(
                'id' => '2'
            ));
        }

        $password = $_POST['password'];

        $arr['result'] = 'notmatch';
        if (md5($password) == $brokers[0]['password']) {
            $arr['result'] = 'match';
            $this->session->set_userdata('activity_logged', 'Y');
        }

        echo json_encode($arr);
    }




    public function save_transaction()
    {
        $the_email = $this->session->userdata('email'); //(isset($single['email'])) ? $single['email'] : 
        $the_name = $this->session->userdata('first_name');
        $user_id = $this->session->userdata('id');
        $name    = $this->session->userdata('name');
        $single  = $this->session->userdata('single');
        $nowtime = date('Y-m-d H:i:s');
        $new_deductible    = $this->session->userdata('new_deductible');

        $data = array(
            'result'=>'error',
            'message'=>'Your session expired. Please login again.'
        );

        $booking_id = 0;

        $buy_inputs = $this->session->userdata('buy_inputs');

        if(!empty($user_id) && !empty($buy_inputs)){

            //get session data
            $details = array(
                'buy_inputs' => $this->session->userdata('buy_inputs'),
                'single_input' => $single,
                'consignee' => $this->session->userdata('consignee'),
                'premium' => $this->session->userdata('premium'),
                'new_deductible'=> $new_deductible
            );

            $data = array(
                'customer_id' => $user_id,
                'comment' => '',
                'details' => serialize($details),
                'status' => 'P',
                'saved' => 'Y',
                'email' => $the_email
            );

            $uuid         = $this->common->uuid();
            $data['uuid'] = $uuid;

            $data['date_added'] = $nowtime;

            //check if already saved
            $insurance_id = $this->session->userdata('insurance_id');

            if(!empty($insurance_id)){
                $booking_id = $insurance_id;
                $this->master->updateRecord('bought_insurance', $data, array('id'=>$insurance_id));
            }
            else{
                $booking_id = $this->master->insertRecord('bought_insurance', $data, true);
            }


            //activity log
            $log_activity = array(
                'name' => $name . ' saved new transaction.',
                'type' => 'transaction',
                'details' => serialize($data)
            );
            $this->master->insertRecord('activity_log', $log_activity);


            //admin emailer info
            $adminemail = $this->common->admin_email();

            //email settings for customer
            $email_view = 'send-certifiicate-to-user';
            $info_arr = array(
                'to'=>$the_email,
                'from'=>$adminemail,
                'subject'=>'Welcome to Transit Insurance',
                'view'=>$email_view,
                'emailer_file_name'=>$email_view,
            );

            $other_info = array(
                'password'=>'',
                'view'=>$email_view,
                'emailer_file_name'=>$email_view,
                'name'=>$name,
                'agency_name'=>'', //$agency_info[0]['name'],
                'user_name'=>$the_email,
                'user_email'=>$the_email,
                'link'=>'landing/certificate/'.$booking_id.'/'.md5($user_id) //confirm_quote/'.$user_id.'/'.$booking_id.'/'.md5($user_id)
            );


            $data['info_arr'] = $info_arr;
            $data['other_info'] = $other_info;
            $this->emailer->sendmail($info_arr,$other_info);

            //send to consignor
            $info_arr['to'] = $single['email'];
            $other_info['name'] = $single['first_name'];
            $other_info['user_name'] = $single['email'];
            $other_info['user_email'] = $single['email'];
            
            $data['info_arr2'] = $info_arr;
            $data['other_info2'] = $other_info;
            $this->emailer->sendmail($info_arr,$other_info);



            $data['result'] = 'success';

            $this->common->reset_transaction_session();
        }

        return $booking_id;

        // header('Content-Type: application/json');
        // header('Access-Control-Allow-Origin: *');

        // echo json_encode($data, JSON_PRETTY_PRINT); // $single['email'];
    }

    public function validate_agentcode()
    {
        $code = $_POST['code'];

        $letter = substr($code, 0, 1);
        $num    = substr($code, 1);
        $num    = (int) $num;

        $agent = $this->master->getRecords('brokerage', array(
            'id' => $num
        )); //, 'customer_type'=>'N'


        $arr['name'] = '';
        if (count($agent) > 0) {
            $this->session->set_userdata('agency_code', $num);
            $arr['name'] = $agent[0]['company_name'];
        } else {
            $this->session->unset_userdata('agency_code');
        }

        $arr['agent']  = $agent;
        $arr['num']    = $num;
        $arr['letter'] = $letter;
        $arr['code']   = $code;
        $arr['result'] = (count($agent) > 0) ? 'ok' : 'invalid';

        // header('Content-Type: application/json');
        // header('Access-Control-Allow-Origin: *');

        echo json_encode($arr); // $single['email'];
    }

    public function submit_contact()
    {
        $data = $_POST['data'];

        $arr     = array();
        $message = '<p><strong>Contact Details</strong></p>';
        foreach ($data as $r => $value) {
            $message .= ucfirst($value['name']) . ': ' . $value['value'] . ' <br/ >';

            $arr[$value['name']] = $value['value'];
        }



        //get validation message from admin
        $action_message = $this->common->get_message('a');
        $arr['message'] = $action_message['success'];



        //admin emailer info
        $adminemail        = $this->common->admin_email();
        $emailer_file_name = 'contact_us_to_admin';

        $to_email = $this->common->db_field_id('admin', 'recovery_email', 'admin', 'name'); //'jill@redskycargo.com';


        //email settings
        $info_arr = array(
            'to' => $to_email, //$arr['email'],
            'from' => $adminemail,
            'subject' => 'Greetings from Ebinder',
            'view' => $emailer_file_name
        );


        $other_info = array(
            'password' => '',
            'emailer_file_name' => $emailer_file_name,
            'name' => 'Admin', // $arr['name'],
            'agency_name' => $arr['name'],
            'user_name' => $arr['name'],
            'user_email' => $arr['name'],
            'details' => $message,
            'link' => 'webmanager'
        );

        $this->emailer->sendmail($info_arr, $other_info);


        //send to more admin emails
        $admin_info = $this->master->getRecords('admin', array(
            'id' => '2'
        ));
        $more_admin = (@unserialize($admin_info[0]['admin_emails'])) ? unserialize($admin_info[0]['admin_emails']) : array();

        if(!empty($more_admin)){
            foreach($more_admin as $r=>$value){
                //email settings for agency
                $info_arr['to'] = $value;
                $this->emailer->sendmail($info_arr, $other_info);
            }
        }

        $arr['other_info'] = $other_info;
        $arr['info_arr']   = $info_arr;
        echo json_encode($arr);

    }


    public function request_access_form(){
        //name=asdfsdfas&company=as&email=aw%40aw33.com&phone=asdfad
        $name = $_POST['name'];
        $company = $_POST['company'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];
        

        $arr     = array();
        $message = '<p><strong>Request Access Details</strong></p>';
        $message .= 'Name : ' . $name . ' <br/ >';
        $message .= 'Company : ' . $company . ' <br/ >';
        $message .= 'Email : ' . $email . ' <br/ >';
        $message .= 'Phone : ' . $phone . ' <br/ >';
        



        //get validation message from admin
        $action_message = $this->common->get_message('a');
        $arr['message'] = $action_message['success'];



        //admin emailer info
        $adminemail        = $this->common->recovery_admin();
        $emailer_file_name = 'request_access_to_admin';

        $to_email = $adminemail; //'jill@redskycargo.com';


        //email settings
        $info_arr = array(
            'to' => $to_email, //$arr['email'],
            'from' => $adminemail,
            'subject' => 'Greetings from Ebinder',
            'view' => $emailer_file_name
        );


        $other_info = array(
            'password' => '',
            'emailer_file_name' => $emailer_file_name,
            'name' => 'Admin', // $arr['name'],
            'agency_name' => $company,
            'user_name' => $name,
            'user_email' => $email,
            'details' => $message,
            'link' => 'webmanager'
        );

        $this->emailer->sendmail($info_arr, $other_info);


        //send to more admin emails
        $admin_info = $this->master->getRecords('admin', array(
            'id' => '2'
        ));
        $more_admin = (@unserialize($admin_info[0]['admin_emails'])) ? unserialize($admin_info[0]['admin_emails']) : array();

        if(!empty($more_admin)){
            foreach($more_admin as $r=>$value){
                //email settings for agency
                $info_arr['to'] = $value;
                $this->emailer->sendmail($info_arr, $other_info);
            }
        }

        $arr['other_info'] = $other_info;
        $arr['info_arr']   = $info_arr;
        echo json_encode($arr);

    }

    public function delete_user_api(){

        $id = $_POST['id'];
        $tb = $_POST['table'];

        $this->master->deleteRecord($tb, 'id', $id);
        echo json_encode($_POST);
    }


    public function access_admin(){
        $token = $_GET['token'];
        if($token == md5('Access Webmanager')){
            $row = $this->master->getRecords('admin');
            if(count($row) > 0){

                $_SESSION['logged_admin'] = $row[0]['name'];
                $_SESSION['logged_admin_email'] = $row[0]['email'];

                $this->session->set_userdata('logged_admin', $row[0]['name']);
                $this->session->set_userdata('logged_admin_email', $row[0]['email']);

                
                $log_activity = array(
                    'name'=>$row[0]['name'].' webmanager login by redsky',
                    'type'=>'login',
                    'details'=>serialize(array()) //$row[0])
                );
                $this->master->insertRecord('activity_log', $log_activity);

                redirect(base_url().'webmanager/dashboard');

            }
        }
    }
    
}
