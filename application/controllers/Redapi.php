<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Redapi extends CI_Controller
{


    public function __construct()
    {

        parent::__construct();

        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json');
    }


    public function get_all_agents(){
		$users = $this->master->getRecords('customers', array('customer_type'=>'N'));
        $brokerage = $this->master->getRecords('brokerage');
        $arr = null;

        if(!empty($users)){
            $arr = array();

            foreach($users as $u=>$user){
                $user['member_since'] = date_format(date_create($user['date_added']), 'd-m-Y');
                $user['name'] = $this->common->customer_name($user['id']);
                $user['company_name'] = $this->common->db_field_id('brokerage', 'company_name', $user['brokerage_id']);
                $user['status_text'] = ($user['status'] == 'Y') ? 'Verified' : 'Not Verified';
                $user['enabled_text'] = ($user['enabled'] == 'Y') ? 'Yes' : 'No';
                $user['source'] = $this->common->the_domain();
                $arr[] = $user;
            }
        }

        echo json_encode($arr);

		
    }

    public function update_agent(){
		$users = $this->master->getRecords('customers', array('customer_type'=>'N'));
		$brokerage = $this->master->getRecords('brokerage');
		
    }
}


?>
