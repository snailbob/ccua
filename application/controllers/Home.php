<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Home extends CI_Controller
{


    public function __construct()
    {

        parent::__construct();

    }

    public function index()
    {   

        $user_id = $this->session->userdata('id');

        if(!empty($user_id)){
            redirect('dashboard/quotes');
        }
        $reset = (isset($_GET['reset'])) ? $_GET['reset'] : 'yes';
        $data  = $this->common->home_data($reset);
        
        $this->load->view('frontpage/landing_meemeep', $data);

    }

    public function login()
    {

        $data['page_title'] = "Cloud-based Crisis Management System";

        $this->load->view('frontpage/header_view', $data);
        $this->load->view('frontpage/login_view', $data);
        $this->load->view('frontpage/footer_view');

    }

    public function forohfor()
    {

        $data['page_title'] = "Cloud-based Crisis Management System";

        $this->load->view('frontpage/header_view', $data);
        $this->load->view('frontpage/forohfor', $data);
        $this->load->view('frontpage/footer_view');

    }


    public function show_country_code()
    {
        $counrty_code = $_POST['country_short'];

        $mobile_code = $this->master->getRecords('country_t', array(
            'iso2' => $counrty_code
        ));

        $data['calling_code'] = '';
        $data['country_id']   = '';
        $data['result']       = 'empty';

        if (count($mobile_code) > 0) {
            $data['calling_code'] = $mobile_code[0]['calling_code'];
            $data['country_id']   = $mobile_code[0]['country_id'];
            $data['result']       = 'success';
        }

        echo json_encode($data);

    }


    public function currency_con()
    {
        $this->load->view('user/test-curr-conversion');
    }

    public function savecurr()
    {
        $arr = $_POST['arr'];

        $a = array();
        foreach ($arr as $r) {
            //$this->master->insertRecord('api_currencies', $r);
            $a[] = $r;
        }

        echo json_encode($a);
    }


    public function importexport()
    {
        $data = $this->common->home_data();

        $this->load->view('frontpage/meem/header_view', $data);
        $this->load->view('frontpage/meem/exportimport', $data);
        $this->load->view('frontpage/meem/footer_view', $data);

    }


    public function localtransit()
    {
        $data = $this->common->home_data();

        $this->load->view('frontpage/meem/header_view', $data);
        $this->load->view('frontpage/meem/localtransit', $data);
        $this->load->view('frontpage/meem/footer_view', $data);

    }


    public function claimevent()
    {
        $data = $this->common->home_data();

        $this->load->view('frontpage/meem/header_view', $data);
        $this->load->view('frontpage/meem/claimevent', $data);
        $this->load->view('frontpage/meem/footer_view', $data);

    }

    public function legal()
    {
        $data           = $this->common->home_data();
        $data['legals'] = $this->master->getRecords('admin');

        $this->load->view('frontpage/meem/header_view', $data);
        $this->load->view('frontpage/meem/legal', $data);
        $this->load->view('frontpage/meem/footer_view', $data);

    }

    public function aboutus()
    {
        $data = $this->common->home_data();

        $this->load->view('frontpage/meem/header_view', $data);
        $this->load->view('frontpage/meem/aboutus', $data);
        $this->load->view('frontpage/meem/footer_view', $data);

    }

    public function gotquestion()
    {
        $data = $this->common->home_data();

        $this->load->view('frontpage/meem/header_view', $data);
        $this->load->view('frontpage/meem/gotquestion', $data);
        $this->load->view('frontpage/meem/footer_view', $data);

    }

    public function contactus()
    {
        $data = $this->common->home_data();

        $this->load->view('frontpage/meem/header_view', $data);
        $this->load->view('frontpage/meem/contactus', $data);
        $this->load->view('frontpage/meem/footer_view', $data);

    }

}


?>
