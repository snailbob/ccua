<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function timezone(){
        $user_id = $this->session->userdata('id');

        $time_zone     = $this->master->getRecords('time_zone');

        $customer = $this->master->getRecords('customers', array(
            'id' => $user_id
        ));
        $customer_tz = (!empty($customer)) ? $customer[0]['timezone'] : '32';
        $customer_tz = (empty($customer_tz)) ? '32' : $customer_tz;

        $data = array(
            'view' => 'settings_timezone',
            'title' => 'Timezone',
            'time_zone' => $time_zone,
            'customer' => $customer,
            'customer_tz' => $customer_tz
        );
        $this->load->view('includes/user_view', $data);
    }


    /***************************
    profile
    ***************************/
    public function profile()
    {
        if ($this->session->userdata('id') == '') {
            redirect(base_url());
        }
        $user_id = $this->session->userdata('id');
        $type    = $this->session->userdata('type');

        $agency   = $this->master->getRecords('agency', array(
            'id' => $user_id
        ));
        $customer = $this->master->getRecords('customers', array(
            'id' => $user_id
        ));
        $student  = $this->master->getRecords('students', array(
            'id' => $user_id
        ));

        $countries = $this->master->getRecords('country_t');


        $data = array(
            'view' => 'settings_view',
            'title' => 'Profile',
            'countries' => $countries
        );

        if ($type == 'agency') {
            //get fields
            $the_fields = array();
            if (count($agency) > 0) {
                foreach ($agency[0] as $r => $value) {
                    if ($r == 'name' || $r == 'email') {
                        $the_fields[] = $r;
                    }
                }
            }

            $data['agency']     = $agency;
            $data['the_fields'] = $the_fields;
            $data['agency']     = $agency;



        } else if ($type == 'customer') {

            //get fields
            $the_fields = array();
            if (count($customer) > 0) {
                foreach ($customer[0] as $r => $value) {
                    if ($r == 'first_name' || $r == 'last_name' || $r == 'email') {
                        $the_fields[] = $r;
                    }
                }
            }

            $data['customer']   = $customer;
            $data['the_fields'] = $the_fields;
            $data['view']       = 'settings_customer_view';
        } else if ($type == 'student') {

            //get fields
            $the_fields = array();
            if (count($student) > 0) {
                foreach ($student[0] as $r => $value) {
                    if ($r == 'first_name' || $r == 'last_name' || $r == 'email') {
                        $the_fields[] = $r;
                    }
                }
            }

            $data['customer']   = $student;
            $data['the_fields'] = $the_fields;
            $data['view']       = 'settings_student_view';
        }

        $this->load->view('includes/user_view', $data);

    }



    /***************************
    verify
    ***************************/
    public function confirm_agency()
    {
        $id         = $this->uri->segment(3);
        $md5email   = $this->uri->segment(4);
        $agency     = $this->master->getRecords('agency', array(
            'id' => $id
        ));
        $countries  = $this->master->getRecords('country_t');
        //get fields
        $the_fields = array();
        if (count($agency) > 0) {
            foreach ($agency[0] as $r => $value) {
                if ($r == 'name' || $r == 'email') {
                    $the_fields[] = $r;
                }
            }
        }


        $data = array(
            'view' => 'agency_verify_view',
            'title' => 'Verify',
            'agency' => $agency,
            'the_fields' => $the_fields,
            'countries' => $countries
        );
        $this->load->view('includes/view', $data);


    }


    /***************************
    verify
    ***************************/
    public function verify()
    {
        $user   = $this->uri->segment(3);
        $id     = $this->uri->segment(4);
        $cryptt = $this->uri->segment(5);

        $customer  = $this->master->getRecords('customers', array(
            'id' => $id
        ));
        $countries = $this->master->getRecords('country_t');

        //get fields
        $the_fields = array();
        if (count($customer) > 0) {
            foreach ($customer[0] as $r => $value) {
                if ($r == 'first_name' || $r == 'last_name' || $r == 'email') {
                    $the_fields[] = $r;
                }
            }
        }


        $data = array(
            'view' => 'customer_verify_view',
            'title' => 'Verify',
            'customer' => $customer,
            'the_fields' => $the_fields,
            'countries' => $countries
        );
        $this->load->view('includes/view', $data);

    }

    /***************************
    show_country_code
    ***************************/
    public function show_country_code()
    {
        $counrty_code = $_POST['country_short'];

        $mobile_code = $this->master->getRecords('country_t', array(
            'iso2' => $counrty_code
        ));

        $data['calling_code'] = '';
        $data['country_id']   = '';
        $data['result']       = 'empty';

        if (count($mobile_code) > 0) {
            $data['calling_code'] = $mobile_code[0]['calling_code'];
            $data['country_id']   = $mobile_code[0]['country_id'];
            $data['result']       = 'success';
        }

        echo json_encode($data);

    }


    /***************************
    policydoc
    ***************************/
    public function policydoc()
    {
        $user_id    = $this->session->userdata('id');
        $name       = $this->session->userdata('name');
        $name       = ($name == '') ? 'Admin' : $name;
        $type       = $this->session->userdata('type');
        $output_dir = "uploads/policyd/";
        if (isset($_FILES["myfile"])) {
            //Filter the file types , if you want.
            if ($_FILES["myfile"]["error"] > 0) {
                echo 'error';
            } else {
                $file_name = $_FILES["myfile"]["name"];
                $file_name = substr($file_name, -3);

                if ($file_name != 'pdf') {
                    echo 'not_img';
                    return false;
                }

                $thefilename = uniqid() . str_replace(' ', '_', $_FILES["myfile"]["name"]);

                //move the uploaded file to uploads folder;
                move_uploaded_file($_FILES["myfile"]["tmp_name"], $output_dir . $thefilename); //$_FILES["myfile"]["name"]);

                $arr = array(
                    'name' => $thefilename
                );

                $this->master->insertRecord('policy_docs', $arr);


                //activity log
                $log_activity = array(
                    'name' => $name . ' uploaded new policy document.',
                    'type' => 'policydoc',
                    'details' => serialize($arr)
                );
                $this->master->insertRecord('activity_log', $log_activity);



                echo $thefilename;


            }
        }
    }

    /***************************
    update_avatar
    ***************************/
    public function update_avatar()
    {
        $user_id    = $this->session->userdata('id');
        $type       = $this->session->userdata('type');
        $output_dir = "uploads/avatars/";

        if (isset($_FILES["myfile"])) {
            //Filter the file types , if you want.
            if ($_FILES["myfile"]["error"] > 0) {
                echo 'error';
            } else {
                $file_name  = $_FILES["myfile"]["name"];
                $file_name2 = substr($file_name, -4);
                $file_name  = substr($file_name, -3);

                if ($file_name != 'jpg' && $file_name != 'gif' && $file_name != 'png' && $file_name != 'JPG' && $file_name != 'GIF' && $file_name != 'PNG' && $file_name2 != 'jpeg' && $file_name2 != 'JPEG') {
                    echo 'not_img';
                    return false;
                }


                $thefilename = uniqid() . str_replace(' ', '_', $_FILES["myfile"]["name"]);

                //move the uploaded file to uploads folder;
                move_uploaded_file($_FILES["myfile"]["tmp_name"], $output_dir . $thefilename);


                if (!isset($_POST['img_type'])) {

                    $arr = array(
                        'avatar' => $thefilename
                    );

                    //store to type of user db
                    if ($type != 'agency') {
                        $type = $type . 's';
                    }
                    $this->master->updateRecord($type, $arr, array(
                        'id' => $user_id
                    ));

                    $this->session->set_userdata('avatar', $thefilename);
                } else {
                    //					if($_POST['img_type'] == 'logo'){
                    //						$thefilename = 'crisisflo-logo-medium.png';
                    //						$output_dir = "assets/frontpage/corporate/images/";
                    //						//move the uploaded file to uploads folder;
                    //						move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir.$thefilename);
                    //
                    //					}
                }

                echo $output_dir . $thefilename;


            }

        }

    }


    /***************************
    save_profile_pic
    ***************************/
    public function save_profile_pic()
    {
        $img_url  = $_POST['img_url'];
        $img_name = $_POST['img_name'];
        $img_type = $_POST['img_type'];
        $user_id  = $this->session->userdata('id');
        $type     = $this->session->userdata('type');

        list($type, $img_url) = explode(';', $img_url);
        list(, $img_url) = explode(',', $img_url);
        $data = base64_decode($img_url);

        if ($img_type == 'logo') {
            $img_name = "assets/frontpage/corporate/images/crisisflo-logo-medium.png";
        }

        file_put_contents($img_name, $data);

        $arr = array(
            'a' => $img_url,
            'image' => $this->common->avatar($user_id, $type)
        );
        echo json_encode($arr);

    }

    /***************************
    add_stripe_customer
    ***************************/
    public function add_stripe_customer()
    {

        $user_id     = $this->session->userdata('id');
        $stripe_id   = $this->session->userdata('stripe_id');
        $agency_code = $this->session->userdata('agency_code');
        $email       = $this->session->userdata('email');
        $token       = $_POST['token'];
        $from_buy    = $_POST['from_buy'];

        $buy_inputs   = $this->session->userdata('buy_inputs');
        $single_input = $this->session->userdata('single'); //array();
        $consignee    = $this->session->userdata('consignee'); //array();

        $currency     = $this->session->userdata('currency');
        $premium      = $this->session->userdata('premium'); //round($_POST['premium'], 2);
        $premium      = round($premium, 2);
        $single_email = $_POST['email'];

        $card    = $_POST['card'];
        $nowtime = date('Y-m-d H:i:s');

        $referral_id = $this->session->userdata('referral_id');


        //get validation message from admin
        $action_message = $this->common->get_message('customer_add_card');
        $message        = $action_message['error'];
        $result         = 'error';

        Stripe::setApiKey("sk_test_CiqlhOTQUSiBilafy4373MJe"); //sk_live_gs8DE2Zo8isW5HEcACTMIKAV

        try {

            if ($from_buy == 'no') {

                // Create a Customer
                $customer = Stripe_Customer::create(array(
                    "description" => "transit_insurance_customer",
                    "email" => $this->session->userdata('email'),
                    "card" => $token // obtained with Stripe.js
                ));

                $arr = array(
                    'stripe_id' => $customer->id
                );

                $this->master->updateRecord('customers', $arr, array(
                    'id' => $user_id
                ));
                $this->session->set_userdata($arr);
                $message = $action_message['success'];
                $result  = 'ok';

            } //not from buy form
            else {
                //stripe charge data
                $charge_data = array(
                    "amount" => $premium * 100, // amount in cents, again
                    "currency" => strtolower($currency), // "usd",
                    //"customer" => $customer_info[0]['stripe_id'],
                    "description" => 'bought Single Marine Transit Policy'
                );

                //check if logged in
                if ($user_id != '' && $stripe_id != '') {
                    $customer_info           = $this->master->getRecords('customers', array(
                        'id' => $user_id
                    ));
                    $single_email            = $email;
                    $charge_data['customer'] = $customer_info[0]['stripe_id'];
                } else {
                    $charge_data['card'] = $token;
                    if ($user_id == '') {
                        $user_id = 0;
                    }
                }

                //check email if empty
                if ($single_email == '') {
                    $single_email = $email;
                }

                //charge card directly
                $charge = Stripe_Charge::create($charge_data);

                $details = array(
                    'buy_inputs' => $buy_inputs,
                    'single_input' => $single_input,
                    'consignee' => $consignee,
                    'premium' => $premium
                );

                $data = array(
                    'customer_id' => $user_id,
                    'details' => serialize($details),
                    'status' => 'P',
                    'agency_code' => ($agency_code == '') ? 0 : $agency_code,
                    'email' => $single_email
                );


                $sess_remove = array(
                    'buy_inputs' => '',
                    'single_input' => '',
                    'consignee' => '',
                    'premium' => '',
                    'premium_format' => '',
                    'selected_deductible' => '',
                    'cargo_price' => '',
                    'currency' => ''
                );

                //unset from session
                foreach ($sess_remove as $key => $value) {
                    $this->session->unset_userdata($key);
                }

                //check charge status
                if ($charge->status == 'succeeded') {
                    $data['date_purchased']   = $nowtime;
                    $data['stripe_charge_id'] = $charge->id;
                }

                //check if referred
                if ($referral_id == '') {
                    $uuid         = $this->common->uuid();
                    $data['uuid'] = $uuid;

                    $data['date_added'] = $nowtime;
                    $booking_id         = $this->master->insertRecord('bought_insurance', $data, true);
                    $arr['booking_id']  = $booking_id;
                } else {
                    $booking_id = $referral_id;
                    $this->master->updateRecord('bought_insurance', $data, array(
                        'id' => $booking_id
                    ));

                    $remove_sess = array(
                        'referral_id' => '',
                        'referral_comment' => ''
                    );
                    $this->session->set_userdata($remove_sess);
                }
                //admin emailer info
                $adminemail = $this->common->admin_email();

                //email settings for agency
                $info_arr = array(
                    'to' => $this->common->recovery_admin(), //$adminemail,
                    'from' => $adminemail,
                    'subject' => 'Welcome to Transit Insurance',
                    'view' => 'payment-notification-to-admin',
                    'emailer_file_name' => 'payment-notification-to-admin'
                );


                $other_info          = array(
                    'password' => '',
                    'view' => 'payment-notification-to-admin',
                    'emailer_file_name' => 'payment-notification-to-admin',
                    'name' => '', //$first.' '.$last,
                    'agency_name' => 'Admin',
                    'user_name' => $adminemail,
                    'user_email' => $adminemail,
                    'link' => 'webmanager/insurance'
                );
                $data['info_arr1']   = $info_arr;
                $data['other_info1'] = $other_info;
                $this->emailer->sendmail($info_arr, $other_info);

                //email settings for customer
                $info_arr = array(
                    'to' => $single_email,
                    'from' => $adminemail,
                    'subject' => 'Welcome to Transit Insurance',
                    'view' => 'payment-notification-to-customer',
                    'emailer_file_name' => 'payment-notification-to-customer'
                );

                $other_info = array(
                    'password' => '',
                    'view' => 'payment-notification-to-customer',
                    'emailer_file_name' => 'payment-notification-to-customer',
                    'name' => '', //$first.' '.$last,
                    'agency_name' => '', //$agency_info[0]['name'],
                    'user_name' => $single_email,
                    'user_email' => $single_email,
                    'link' => 'dashboard' //landing/download/'.$user_id.'/'.$booking_id.'/'.md5($user_id)
                );

                if ($user_id == '0') {
                    //$other_info['link'] = 'landing/reqdetails/'.$booking_id.'/'.md5($email);
                }

                $data['info_arr2']   = $info_arr;
                $data['other_info2'] = $other_info;
                $this->emailer->sendmail($info_arr, $other_info);


                //email certiifcate
                //email settings for customer
                $info_arr = array(
                    'to' => $single_email,
                    'from' => $adminemail,
                    'subject' => 'Welcome to Transit Insurance',
                    'view' => 'send-certifiicate-to-user',
                    'emailer_file_name' => 'send-certifiicate-to-user'
                );

                $other_info = array(
                    'password' => '',
                    'view' => 'send-certifiicate-to-user',
                    'emailer_file_name' => 'send-certifiicate-to-user',
                    'name' => '', //$first.' '.$last,
                    'agency_name' => '', //$agency_info[0]['name'],
                    'user_name' => $single_email,
                    'user_email' => $single_email,
                    'link' => 'landing/certificate/' . $booking_id
                );

                $data['info_arr3']   = $info_arr;
                $data['other_info3'] = $other_info;
                $this->emailer->sendmail($info_arr, $other_info);



                $arr['data'] = $data;

                //get validation message from admin
                $action_message = $this->common->get_message('buy_insurance');
                $message        = $action_message['success'];
                $result         = 'ok';
            }
        }
        catch (Stripe_CardError $e) {

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $message = $err['message'];

        }
        catch (Stripe_InvalidRequestError $e) {
        // Invalid parameters were supplied to Stripe's API

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $message = $err['message'];
        }
        catch (Stripe_AuthenticationError $e) {
        // Authentication with Stripe's API failed
        // (maybe you changed API keys recently)

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $message = $err['message'];
        }
        catch (Stripe_ApiConnectionError $e) {
        // Network communication with Stripe failed

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $message = $err['message'];
        }
        catch (Stripe_Error $e) {
        // Display a very generic error to the user, and maybe send
        // yourself an email

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $message = $err['message'];
        }
        catch (Exception $e) {
        // Something else happened, completely unrelated to Stripe

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $message = $err['message'];
        }

        $arr['result']  = $result;
        $arr['message'] = $message;
        echo json_encode($arr);
    }


    public function save_quote()
    {
        $user_id = $this->session->userdata('id');
        $name    = $this->session->userdata('name');
        $single  = $this->session->userdata('single');
        $nowtime = date('Y-m-d H:i:s');

        //get session data
        $details = array(
            'buy_inputs' => $this->session->userdata('buy_inputs'),
            'single_input' => $single,
            'consignee' => $this->session->userdata('consignee'),
            'premium' => $this->session->userdata('premium')
        );

        $data = array(
            'customer_id' => $user_id,
            'details' => serialize($details),
            'status' => 'S',
            'email' => $single['email']
        );

        $uuid         = $this->common->uuid();
        $data['uuid'] = $uuid;

        $data['date_added'] = $nowtime;
        $booking_id         = $this->master->insertRecord('bought_quote', $data, true);


        //admin emailer info
        $adminemail = $this->common->admin_email();

        //email settings for customer
        $info_arr = array(
            'to' => $single['email'],
            'from' => $adminemail,
            'subject' => 'Welcome to Transit Insurance',
            'view' => 'quote-send-to-consignor',
            'emailer_file_name' => 'quote-send-to-consignor'
        );

        $other_info = array(
            'password' => '',
            'view' => 'quote-send-to-consignor',
            'emailer_file_name' => 'quote-send-to-consignor',
            'name' => $single['first_name'] . ' ' . $single['last_name'],
            'agency_name' => '', //$agency_info[0]['name'],
            'user_name' => $single['email'],
            'user_email' => $single['email'],
            'link' => 'landing/confirm_quote/' . $user_id . '/' . $booking_id . '/' . md5($user_id)
        );


        $data['info_arr2']   = $info_arr;
        $data['other_info2'] = $other_info;
        $this->emailer->sendmail($info_arr, $other_info);


        $arr = array(
            'buy_inputs',
            'booking_id',
            'premium',
            'premium_format',
            'selected_deductible',
            'cargo_price',
            'currency',
            'single',
            'consignee'
        );

        foreach ($arr as $key) {
            $this->session->unset_userdata($key);
        }



        //activity log
        $log_activity = array(
            'name' => $name . ' submitted new quote.',
            'type' => 'quote',
            'details' => serialize($data)
        );
        $this->master->insertRecord('activity_log', $log_activity);



        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');

        echo json_encode($data, JSON_PRETTY_PRINT); // $single['email'];
    }


    public function check_student_card()
    {
        $user_id = $this->session->userdata('id');
        $type    = $this->session->userdata('type');

        $user_info = $this->master->getRecords('students', array(
            'id' => $user_id
        ));
        $status    = 'error';
        if (count($user_info) > 0) {
            if ($user_info[0]['stripe_id'] != '') {
                $status = 'ok';
            }
        }
        echo $status;

    }

}
