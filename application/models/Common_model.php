<?php

class Common_model extends CI_Model
{

    public function __construct()
    {

        parent::__construct();
        $this->set_timezone();
    }

	public function the_domain(){
		$url = $_SERVER["SERVER_NAME"];
		$account = str_replace(".redskycargo.com","",$url);
		return ($account != '' && $account != 'redskycargo.com' && $account != 'localhost') ? $account : '';
	}

    public function set_timezone(){
        $user_timezone = $this->session->userdata('timezone');
        $admin_info = $this->master->getRecords('admin', array(
            'id' => '2'
        ));
        $user_tz = (!empty($admin_info)) ? $admin_info[0]['timezone'] : '32';
        $user_tz = (empty($user_tz)) ? '32' : $user_tz;
        $user_tz = (!empty($user_timezone)) ? $user_timezone : $user_tz;

        $t = '(UTC+10:00)';

        // $user_tz = $this->session->userdata('timezone');
        $timezone = $this->master->getRecords('time_zone', array('id'=>$user_tz));

        if(count($timezone)){
            $t = $timezone[0]['utc_name'];
        }

        $offset = str_replace('(UTC', '', $t);
        $offset = str_replace(')', '', $offset);

        // Calculate seconds from offset
        list($hours, $minutes) = explode(':', $offset);
        $seconds = $hours * 60 * 60 + $minutes * 60;
        // Get timezone name from seconds
        $tz = timezone_name_from_abbr('', $seconds, 1);
        // Workaround for bug #44780
        if($tz === false) $tz = timezone_name_from_abbr('', $seconds, 0);
        // Set timezone
        date_default_timezone_set($tz);


        // echo date('Y-m-d H:i:s'); return false;
    }


    public function days_of_week()
    {
        $arr = array(
            'Sunday',
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday'
        );

        return $arr;

    }

    public function distanceCalculation($point1_lat, $point1_long, $point2_lat, $point2_long, $unit = 'km', $decimals = 2)
    {
        // Calculate the distance in degrees
        $point1_lat  = floatval($point1_lat);
        $point1_long = floatval($point1_long);
        $point2_lat  = floatval($point2_lat);
        $point2_long = floatval($point2_long);

        $degrees = rad2deg(acos((sin(deg2rad($point1_lat)) * sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat)) * cos(deg2rad($point2_lat)) * cos(deg2rad($point1_long - $point2_long)))));

        // Convert the distance in degrees to the chosen unit (kilometres, miles or nautical miles)
        switch ($unit) {
            case 'km':
                $distance = $degrees * 111.13384; // 1 degree = 111.13384 km, based on the average diameter of the Earth (12,735 km)
                break;
            case 'mi':
                $distance = $degrees * 69.05482; // 1 degree = 69.05482 miles, based on the average diameter of the Earth (7,913.1 miles)
                break;
            case 'nmi':
                $distance = $degrees * 59.97662; // 1 degree = 59.97662 nautic miles, based on the average diameter of the Earth (6,876.3 nautical miles)
        }
        return round($distance, $decimals);
    }


    public function get_message($name = NULL)
    {
        $message['id']      = '';
        $message['name']    = $name;
        $message['success'] = '<i class="fa fa-check-circle text-success"></i> Action successful.';
        $message['error']   = '<i class="fa fa-warning text-info"></i> Action failed to execute.';
        $message['info']    = '<i class="fa fa-info-circle"></i> No issue found.';
        $message['confirm'] = '<i class="fa fa-question-circle text-info"></i> Confirm action.';

        if ($name != NULL) {
            $mess = $this->master->getRecords('validation_messages', array(
                'name' => $name
            ));

            if (count($mess) > 0) {
                $message['id']      = $mess[0]['id'];
                $message['success'] = '<i class="fa fa-check-circle text-success"></i> ' . $mess[0]['success_message'];

                if ($mess[0]['error_message'] != '') {
                    $message['error'] = '<i class="fa fa-warning  text-warning"></i> ' . $mess[0]['error_message'];
                }

                if ($mess[0]['info_message'] != '') {
                    $message['info'] = '<i class="fa fa-info-circle  text-info"></i> ' . $mess[0]['info_message'];
                }

                if ($mess[0]['confirm_message'] != '') {
                    $message['confirm'] = '<i class="fa fa-question-circle text-info"></i> ' . $mess[0]['confirm_message'];
                }
            }

            return $message;

        } else {
            $messages = array();
            $mess     = $this->master->getRecords('validation_messages');
            $error    = array();
            $success  = array();
            $gloval   = array();
            foreach ($mess as $r => $value) {
                $confirm_fa              = '<i class="fa fa-question-circle text-info"></i> ';
                $success_fa              = '<i class="fa fa-check-circle text-success"></i> ';
                $err_fa                  = '<i class="fa fa-info-circle text-info"></i> ';
                $gloval[$value['name']]  = $confirm_fa . $value['confirm_message'];
                $error[$value['name']]   = $err_fa . $value['error_message'];
                $success[$value['name']] = $success_fa . $value['success_message'];
            }
            $messages['success'] = $success;
            $messages['error']   = $error;
            $messages['global']  = $gloval;
            return $messages;

        }
    }


    public function colors()
    {
        $arr = array(
            'bg-choco',
            'bg-lavander',
            'bg-maroon',
            'bg-deep',
            'bg-grass',
            'bg-blood'
        );

        return $arr;

    }

    public function avatar($id, $type = 'agency')
    {

        if ($type == 'agency') {
            $info    = $this->master->getRecords('agency', array(
                'id' => $id
            ));
            $default = base_url() . 'uploads/avatars/agency.jpg';
        }

        else if ($type == 'customer') {
            $info    = $this->master->getRecords('customers', array(
                'id' => $id
            ));
            $default = base_url() . 'uploads/avatars/download.jpg';
        } else {
            $info    = $this->master->getRecords('students', array(
                'id' => $id
            ));
            $default = base_url() . 'uploads/avatars/download.jpg';
        }

        if (count($info) > 0) {

            if ($info[0]['avatar'] != '') { //check if not default

                $default = base_url() . 'uploads/avatars/' . $info[0]['avatar'];

            }

        } // not existing user

        return $default;

    }

    public function class_name($id)
    {
        $class = $this->master->getRecords('classes', array(
            'id' => $id
        ));
        $name  = 'Not specified';
        if (count($class) > 0) {
            $name = $class[0]['name'];
        }
        return $name;
    }

    public function genre_name($id)
    {
        $genre = $this->master->getRecords('genres', array(
            'id' => $id
        ));
        $name  = 'Not specified';
        if (count($genre) > 0) {
            $name = $genre[0]['name'];
        }
        return $name;
    }

    public function agency_name($id)
    {
        $agency = $this->master->getRecords('agency', array(
            'id' => $id
        ));
        $name   = 'Not specified';
        if (count($agency) > 0) {
            $name = $agency[0]['name'];
        }
        return $name;
    }

    public function brokerage_info($id, $field = 'company_name')
    {
        $customer = $this->master->getRecords('brokerage', array(
            'id' => $id
        ));
        $name     = 'Not exist';
        if (!empty($customer)) {
            $name = (isset($customer[0][$field])) ? $customer[0][$field] : 'Unknown';
        }
        return $name;
    }

    public function customer_info($id, $field = 'first_name')
    {
        $customer = $this->master->getRecords('customers', array(
            'id' => $id
        ));
        $name     = 'Not exist';
        if (count($customer) > 0) {
            $name = (isset($customer[0][$field])) ? $customer[0][$field] : 'Unknown';
        }
        return $name;
    }

    public function customer_name($id)
    {
        $customer = $this->master->getRecords('customers', array(
            'id' => $id
        ));
        $name     = 'Not exist';
        if (count($customer) > 0) {
            $name = $customer[0]['first_name'] . ' ' . $customer[0]['last_name'];
        }
        return $name;
    }

    public function customer_status($status)
    {
        $stat = '<i class="fa fa-circle text-red"></i> Invitation not yet accepted';
        if ($status == 'Y') {
            $stat = '<i class="fa fa-circle text-success"></i> Invitation accepted';
        }
        return $stat;
    }

    public function quote_id($id)
    {
        $year   = date("Y");
        $format = sprintf("%'.04d", $id);

        return 'Q' . $format . $year;
    }

    public function db_field_id($db, $field, $id, $idfield = 'id')
    {
        $class = $this->master->getRecords($db, array(
            $idfield => $id
        ));
        $name  = 'Not specified';
        if (count($class) > 0) {
            if (isset($class[0][$field])) {
                $name = $class[0][$field];
            }
        }
        return $name;
    }

    public function genre_array($arr)
    {

        $new_arr = array();
        if ($arr != '') {
            $arr = unserialize($arr);
            if (count($arr) > 0) {
                foreach ($arr as $val) {
                    $new_arr[] = $this->genre_name($val);
                }
            }
        }
        return $new_arr;
    }

    public function customer_array($arr)
    {

        $new_arr = array();
        if ($arr != '') {
            $arr = unserialize($arr);
            if (count($arr) > 0) {

                foreach ($arr as $val) {
                    $new_arr[] = $this->customer_name($val);
                }
            }
        }
        return $new_arr;
    }

    public function display_array($arr)
    {
        $data = 'No records found';
        if ($arr != '') {
            if (!is_array($arr)) {
                $arr = unserialize($arr);
            }
            if (count($arr) > 0) {
                $data = '';
                $i    = 1;
                foreach ($arr as $val) {
                    $data .= $val;

                    if ($i < count($arr)) {
                        $data .= ', ';
                    }

                    $i++;
                }
            }

        }

        return $data;
    }


    public function active_referral_count($user_id = NULL)
    {
        $whr = array(
            // 'status !=' => 'P'
            'status' => 'S'
        );

        if ($user_id != NULL) {
            $whr['customer_id'] = $user_id;
        }
        $insurances = $this->master->getRecordCount('bought_insurance', $whr, '', array(
            'status' => 'DESC'
        ));
        return $insurances;
    }

    public function completed_transaction_count($user_id = NULL)
    {
        $whr = array(
            'status' => 'P'
        );

        if ($user_id != NULL) {
            $whr['customer_id'] = $user_id;
        }
        $insurances = $this->master->getRecordCount('bought_insurance', $whr, '', array(
            'status' => 'DESC'
        ));
        return $insurances;
    }

    public function the_cert_editor()
    {
        //set empty default
        $so       = '[[';
        $sog      = '[[';
        $sc       = ']]';
        $longline = 'INSURANCE DETAILS HERE';

        $arr = array(
            'consignor' => $so . 'consignor' . $sc,
            'consignee' => $so . 'consignee' . $sc,
            'shipmentdate' => $so . 'shipmentdate' . $sc,
            'insurance' => $so . 'insurance' . $sc,
            'transitfrom' => $so . 'transitfrom' . $sc,
            'cargocat' => $so . 'cargocat' . $sc,
            'portloading' => $so . 'portloading' . $sc,
            'portdischarge' => $so . 'portdischarge' . $sc,
            'transitto' => $so . 'transitto' . $sc,
            'description' => $so . 'description' . $sc,
            'longline' => $so . 'longline' . $sc,
            'shortline' => $so . 'shortline' . $sc,
            'shortlineg' => $sog . 'shortlineg' . $sc,
            'medline' => $sog . 'medline' . $sc,
            'deductible' => $so . 'deductible' . $sc,
            'currency' => 'AUD',
            'premium' => $so . 'premium' . $sc,
            'quoteid' => $so . 'quoteid' . $sc,
            'logolink' => $so . 'logolink' . $sc,
            'signlink' => $so . 'signlink' . $sc,
            'base_currency' => $so . 'base_currency' . $sc,
            'conv_insurance' => $so . 'converterted_insurance' . $sc,
            'surveyors' => $so . 'surveyors' . $sc,
            'uuid' => $so . 'uuid' . $sc,
            'policy_no' => $so . 'policy_no' . $sc,
            'date_bound' => $so . 'date_bound' . $sc,
            'vessel_name' => $so . 'vessel_name' . $sc
        );

        return $arr;
    }

    public function pdf_data_placeholder()
    {

        //set empty default
        $logolink = base_url() . 'assets/frontpage/corporate/images/crisisflo-logo-medium.png';
        $signlink = base_url() . 'assets/img/cert/cert-signiture.jpg';
        $so       = '<span style="background-color: #ffff00">';
        $sog      = '<span style="background-color: #c0c0c0">';
        $sc       = '</span>';
        $longline = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

        $medline = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

        $shortline = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        $consigns  = 'Name, Business Name and Address&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

        $arr = array(
            'consignor' => $so . $consigns . $sc,
            'consignee' => $so . $consigns . $sc,
            'shipmentdate' => $so . $shortline . $sc,
            'insurance' => $so . $shortline . $sc,
            'transitfrom' => $so . $shortline . $sc,
            'cargocat' => $so . $shortline . $sc,
            'portloading' => $so . $shortline . $sc,
            'portdischarge' => $so . $shortline . $sc,
            'transitto' => $so . $shortline . $sc,
            'description' => $so . $shortline . $sc,
            'longline' => $so . $longline . $sc,
            'shortline' => $so . $shortline . $sc,
            'shortlineg' => $sog . $shortline . $sc,
            'medline' => $sog . $medline . $sc,
            'deductible' => $so . $shortline . $sc,
            'currency' => $so . $shortline . $sc,
            'premium' => $so . $shortline . $sc,
            'quoteid' => $so . $shortline . $sc,
            'logolink' => $logolink,
            'signlink' => $signlink,
            'base_currency' => '', // $so.$shortline.$sc,
            'conv_insurance' => '', //$so.$shortline.$sc,
            'surveyors' => $so . $shortline . $sc,
            'uuid' => $so . $shortline . $sc,
            'policy_no' => $so . $shortline . $sc,
            'date_bound' => $so . $shortline . $sc,
            'vessel_name' => $so . $shortline . $sc

        );

        return $arr;
    }

    public function buy_inputs($b)
    {
        $insurance_details = '<br><br>';
        $arr               = array();
        $premium           = '';
        foreach ($b as $r => $value) {
            $title = $value['name'];
            $val   = $value['value'];

            $arr[$title] = $val;
        }

        $default_deductible = (isset($arr['default_deductible'])) ? $arr['default_deductible'] : 0;
        $dedd               = $this->common->db_field_id('deductibles', 'deductible', $default_deductible, 'rate');

        $val = $this->common->db_field_id('country_t', 'short_name', $arr['transitfrom'], 'country_id');
        $insurance_details .= 'Transit From: ' . $val . '<br>';

        $val = $this->common->db_field_id('country_t', 'short_name', $arr['transitto'], 'country_id');
        $insurance_details .= 'Transit To: ' . $val . '<br>';
        $insurance_details .= 'Invoice: ' . $arr['currency'] . '' . $arr['invoice'] . '<br>';
        $insurance_details .= 'Goods Description: ' . $arr['goods_desc'] . '<br>';
        $insurance_details .= 'Transportation Method: ' . $arr['transmethod'] . '<br>';
        $insurance_details .= 'Excess: ' . $arr['currency'] . '' . $dedd . '<br>';
        $insurance_details .= 'Premium: ' . $arr['currency'] . '' . $arr['premium'] . '<br>';

        return $insurance_details;
    }


    public function differ_days($date1, $date2)
    {
        $endyr_date = new DateTime($date2);
        $strt_date  = new DateTime($date1);
        $diff       = $strt_date->diff($endyr_date);

        $total = $diff->y * 365 + $diff->m * 30 + $diff->d + $diff->h / 24; // + $diff->i / 60;
        return round($total, 0);
    }



    public function the_cert_data($req_id)
    {
        $bought = $this->master->getRecords('bought_insurance', array(
            'id' => $req_id
        ));

        $arr = $this->pdf_data_placeholder();

        $so  = '<span style="background-color: #fff">';
        $sog = '<span style="background-color: #c0c0c0">';
        $sc  = '</span>';

        $premium = '0';

        //check if exist
        if (count($bought) > 0) {
            foreach ($bought as $r => $value) {
                $arr['uuid']       = $so . $value['uuid'] . $sc;
                $arr['policy_no']  = $so . 'TBC' . $sc; //$this->policy_id_format($value['id'])
                $date_bound        = date_format(date_create($value['date_added']), 'd/m/Y');
                $arr['date_bound'] = $date_bound;

                $details           = (@unserialize($value['details'])) ? unserialize($value['details']) : array();
                $buy_inputs        = (isset($details['buy_inputs'])) ? $details['buy_inputs'] : array();
                $insurance_details = array();
                foreach ($buy_inputs as $r => $value) {
                    $insurance_details[$value['name']] = $value['value'];
                }

                $single_input     = (isset($details['single_input'])) ? $details['single_input'] : array();
                $single_consignee = (isset($details['consignee'])) ? $details['consignee'] : array();

                if (!empty($details['single_input'])) {
                    $consignor = $single_input['first_name'] . ' ' . $single_input['last_name'] . '<br>';
                    $consignor .= $single_input['business_name'] . '<br>';
                    $consignor .= $single_input['location'];
                    $arr['consignor'] = $so . $consignor . $sc;
                }

                if (!empty($details['consignee'])) {
                    $consignee = $single_consignee['first_name'] . ' ' . $single_consignee['last_name'] . '<br>';
                    $consignee .= $single_consignee['business_name'] . '<br>';
                    $consignee .= $single_consignee['location'];
                    $arr['consignee'] = $so . $consignee . $sc;
                }

                if (isset($insurance_details['shipment_date'])) {
                    $arr['shipmentdate'] = $so . $insurance_details['shipment_date'] . $sc;
                }

                $insurance_curr = (isset($insurance_details['currency'])) ? $insurance_details['currency'] : 'AUD';

                $arr['premium']   = $so . $insurance_curr . number_format($details['premium'], 2, '.', ',') . $sc;
                $arr['currency']  = $insurance_curr;

                $insurance_details['invoice'] = (isset($insurance_details['invoice'])) ? $insurance_details['invoice'] : '';
                $arr['insurance'] = $so . $insurance_curr . $insurance_details['invoice'] . $sc;

                if (isset($insurance_details['base_currency'])) {
                    if ($insurance_curr != $insurance_details['base_currency']) {
                        $arr['base_currency']  = $so . $insurance_details['base_currency'] . $sc;
                        $conv_val              = number_format($insurance_details['converted_invoice'], 2, '.', ',');
                        $arr['conv_insurance'] = $so . $arr['base_currency'] . $conv_val . $sc;
                    }
                }


                $transitfrom = (isset($insurance_details['transitfrom'])) ? $insurance_details['transitfrom'] : '';

                $transfrom          = $this->common->db_field_id('country_t', 'short_name', $transitfrom, 'country_id');


                $arr['transitfrom'] = $so . $transfrom . $sc;

                $vessel_name     = (isset($insurance_details['vessel_name'])) ? $insurance_details['vessel_name'] : '';
                $arr['vessel_name'] = $so . $vessel_name . $sc;


                $cargocat     = (isset($insurance_details['cargocat'])) ? $insurance_details['cargocat'] : '';
                $cargocatname     = $this->cargo_name($cargocat);
                $arr['cargocat'] = $so . $cargocatname . $sc;

                if (isset($insurance_details['portloading'])) {
                    $portloading        = (is_numeric($insurance_details['portloading'])) ? $this->common->selectedport($insurance_details['portloading']) : $insurance_details['portloading'];
                    $arr['portloading'] = $so . $portloading . $sc;
                }

                if (isset($insurance_details['portdischarge'])) {
                    $portdischarge        = (is_numeric($insurance_details['portdischarge'])) ? $this->common->selectedport($insurance_details['portdischarge']) : $insurance_details['portdischarge'];
                    $arr['portdischarge'] = $so . $portdischarge . $sc;
                }

                $transsitto = (isset($insurance_details['transitto'])) ? $insurance_details['transitto'] : '';
                $transsitto2         = $this->common->db_field_id('country_t', 'short_name', $transsitto, 'country_id');
                $arr['transitto']   = $so . $transsitto2 . $sc;
                $arr['description'] = (isset($insurance_details['goods_desc'])) ? $so . $insurance_details['goods_desc'] . $sc : $so . $sc;

                $default_deductible = (isset($insurance_details['default_deductible'])) ? $insurance_details['default_deductible'] : 0;
                $dedd               = $this->common->db_field_id('deductibles', 'deductible', $default_deductible, 'rate');
                $arr['deductible']  = $so . $dedd . $sc;
                
                $arr['transmethod']  = (isset($insurance_details['transmethod'])) ? $so . $insurance_details['transmethod'] . $sc : $so . $sc;


                //get surveyors
                $transsitto_iso = $this->common->db_field_id('country_t', 'iso2', $transsitto, 'country_id');
                $surveyor       = $this->master->getRecords('surveyors', array(
                    'iso_code' => $transsitto_iso
                ));
                if (count($surveyor) > 0) {
                    $surv_text = $surveyor[0]['agent_name'] . '<br>';
                    $surv_text .= $surveyor[0]['full_main_address'] . '<br>';
                    $surv_text .= $surveyor[0]['main_phone_no'] . '<br>';
                    $surv_text .= $surveyor[0]['main_email'];
                    $arr['surveyors'] = $so . $surv_text . $sc;
                }

            }
        }

        return $arr;

    }

    public function the_quote_data($req_id, $no_highlight = '')
    {
        $bought = $this->master->getRecords('bought_quote', array(
            'id' => $req_id
        ));

        $arr = $this->pdf_data_placeholder();

        $so             = '<span style="background-color: #ffff00">';
        $sog            = '<span style="background-color: #c0c0c0">';
        $sc             = '</span>';
        $separator      = '<br>';
        $insurance_curr = 'AUD';
        if ($no_highlight != '') {
            $so        = '';
            $sog       = '';
            $sc        = '';
            $separator = ', ';
        }


        //get session data
        $details = array(
            'buy_inputs' => $this->session->userdata('buy_inputs'),
            'single_input' => $this->session->userdata('single'),
            'consignee' => $this->session->userdata('consignee'),
            'premium' => $this->session->userdata('premium')
        );

        $buy_inputs        = $details['buy_inputs'];
        $insurance_details = array();
        if (is_array($buy_inputs)) {
            foreach ($buy_inputs as $r => $value) {
                $insurance_details[$value['name']] = $value['value'];
            }
        }

        $single_input     = (isset($details['single_input'])) ? $details['single_input'] : array();
        $single_consignee = (isset($details['consignee'])) ? $details['consignee'] : array();

        //check if exist
        if (count($bought) > 0) {
            foreach ($bought as $r => $value) {
                $details    = unserialize($value['details']);
                $buy_inputs = $details['buy_inputs'];
                //$insurance_details = array();
                foreach ($buy_inputs as $r => $value) {
                    $insurance_details[$value['name']] = $value['value'];
                }

                $single_input     = (isset($details['single_input'])) ? $details['single_input'] : array();
                $single_consignee = (isset($details['consignee'])) ? $details['consignee'] : array();
                $insurance_curr   = (isset($insurance_details['currency'])) ? $insurance_details['currency'] : 'AUD';
                $arr['currency']  = $insurance_curr;
            }
        }

        if (count($single_consignee) > 0) {
            if (count($bought) > 0) {
                $arr['quoteid'] = $this->quote_id($bought[0]['id']);
            }

            if (isset($details['single_input'])) {
                $consignor = 'Name: ' . $single_input['first_name'] . ' ' . $single_input['first_name'] . $separator;
                $consignor .= 'Business Name: ' . $single_input['business_name'] . $separator;
                $consignor .= 'Address: ' . $single_input['location'];
                $arr['consignor'] = $so . $consignor . $sc;
            }

            if (isset($details['consignee'])) {
                $consignee = 'Name: ' . $single_consignee['first_name'] . ' ' . $single_consignee['first_name'] . $separator;
                $consignee .= 'Business Name: ' . $single_consignee['business_name'] . $separator;
                $consignee .= 'Address: ' . $single_consignee['location'];
                $arr['consignee'] = $so . $consignee . $sc;
            }

            if (isset($insurance_details['shipment_date'])) {
                $arr['shipmentdate'] = $so . $insurance_details['shipment_date'] . $sc;
            }

            $arr['premium']   = $so . $insurance_curr . number_format($details['premium'], 2, '.', ',') . $sc;

            $insurance_details['invoice'] = (isset($insurance_details['invoice'])) ? $insurance_details['invoice'] : '';
            $arr['insurance'] = $so . $insurance_curr . $insurance_details['invoice'] . $sc;

            if (isset($insurance_details['base_currency'])) {
                $base_curr = $insurance_details['base_currency'];
                if ($insurance_curr != $base_curr) {
                    $arr['base_currency']  = $so . $base_curr . $sc;
                    $conv_val              = number_format($insurance_details['converted_invoice'], 2, '.', ',');
                    $arr['conv_insurance'] = $so . $base_curr . $conv_val . $sc;
                }
            }

            $transitfrom = (isset($insurance_details['transitfrom'])) ? $insurance_details['transitfrom'] : '';
            $transfrom          = $this->common->db_field_id('country_t', 'short_name', $transitfrom, 'country_id');
            $arr['transitfrom'] = $so . $transfrom . $sc;

            $vessel_name     = (isset($insurance_details['vessel_name'])) ? $insurance_details['vessel_name'] : '';

            $tranmeth = (isset($insurance_details['transmethod'])) ? $insurance_details['transmethod'] : '';
            $arr['cargocat'] = $so . $tranmeth . ' - Name of Vessel\Aircraft: ' . $vessel_name . $sc;

            if (isset($insurance_details['portloading'])) {
                $portloading        = (is_numeric($insurance_details['portloading'])) ? $this->common->selectedport($insurance_details['portloading']) : $insurance_details['portloading'];
                $arr['portloading'] = $so . $portloading . $sc;
            }

            if (isset($insurance_details['portdischarge'])) {
                $portdischarge        = (is_numeric($insurance_details['portdischarge'])) ? $this->common->selectedport($insurance_details['portdischarge']) : $insurance_details['portdischarge'];
                $arr['portdischarge'] = $so . $portdischarge . $sc;
            }

            $transsitto = (isset($insurance_details['transitto'])) ? $insurance_details['transitto'] : '';

            $transsitto         = $this->common->db_field_id('country_t', 'short_name', $transsitto, 'country_id');
            $arr['transitto']   = $so . $transsitto . $sc;


            $goods_desc = (isset($insurance_details['goods_desc'])) ? $insurance_details['goods_desc'] : '';
            $arr['description'] = $so . $goods_desc . $sc;

            $default_deductible = (isset($insurance_details['default_deductible'])) ? $insurance_details['default_deductible'] : 0;
            $dedd               = $this->common->db_field_id('deductibles', 'deductible', $default_deductible, 'rate');
            $arr['deductible']  = $so . $dedd . $sc;
        }

        return $arr;

    }

    public function billing_report($date_from, $date_to)
    {
        $insurances = $this->master->getRecords('bought_quote', array(
            'status' => 'P'
        ));

        $billings = array();
        if (count($insurances) > 0) {
            foreach ($insurances as $r => $value) {
                $insurance = $this->common->the_quote_data($value['id'], 'no_highlight');

                $details           = unserialize($value['details']);
                $premium           = (isset($details['premium'])) ? $details['premium'] : 0;
                $premium           = number_format($premium, 2, '.', ',');
                $buy_inputs        = (isset($details['buy_inputs'])) ? $details['buy_inputs'] : array();
                $insurance_details = array();
                foreach ($buy_inputs as $bi => $bival) {
                    $insurance_details[$bival['name']] = $bival['value'];
                }

                $single_input = (isset($details['single_input'])) ? $details['single_input'] : array();
                $cust_name    = 'Unknown';
                if (isset($single_input['first_name']) && isset($single_input['last_name'])) {
                    $cust_name = $single_input['first_name'] . ' ' . $single_input['last_name'];
                }
                $transitto   = (isset($insurance_details['transitto'])) ? $this->common->db_field_id('country_t', 'short_name', $insurance_details['transitto'], 'country_id') : 'not specified';
                $transitfrom = (isset($insurance_details['transitfrom'])) ? $this->common->db_field_id('country_t', 'short_name', $insurance_details['transitfrom'], 'country_id') : 'not specified';


                $date_purch = explode(' - ', $insurance_details['shipment_date']);

                $agent = $this->common->customer_name($value['customer_id']);

                $billings[] = array(
                    'date' => date_format(date_create($date_purch[0]), 'd-m-Y'),
                    'purchase_date' => $date_purch[0],
                    'agent' => $agent,
                    'consignee' => $insurance['consignee'],
                    'qoute' => $this->quote_id($value['id']),
                    'premium' => '$' . $premium
                );
            }
        }


        //filter by date range
        $filtered_billings = array();
        if (count($billings) > 0) {
            foreach ($billings as $r => $value) {
                if (strtotime($value['date']) >= strtotime($date_from) && strtotime($value['date']) <= strtotime($date_to)) {

                    unset($value['date']);
                    $filtered_billings[] = $value;
                }
            }
        }

        //sort if not empty
        if (count($filtered_billings) > 0) {
            uasort($filtered_billings, array(
                $this,
                'date_compare'
            ));
        }

        return $filtered_billings;
    }


    public function agentchannel_report($date_from, $date_to)
    {
        $insurances = $this->master->getRecords('bought_insurance', array(
            'status' => 'P',
            'saved' => 'N'
        ));

        $billings = array();
        if (count($insurances) > 0) {
            foreach ($insurances as $r => $value) {
                $insurance = $this->common->the_quote_data($value['id'], 'no_highlight');

                $details           = unserialize($value['details']);
                $premium           = (isset($details['premium'])) ? $details['premium'] : 0;
                $premium           = number_format($premium, 2, '.', ',');
                $buy_inputs        = (isset($details['buy_inputs'])) ? $details['buy_inputs'] : array();
                $insurance_details = array();
                foreach ($buy_inputs as $bi => $bival) {
                    $insurance_details[$bival['name']] = $bival['value'];
                }

                $single_input = (isset($details['single_input'])) ? $details['single_input'] : array();
                $cust_name    = 'Unknown';
                if (isset($single_input['first_name']) && isset($single_input['last_name'])) {
                    $cust_name = $single_input['first_name'] . ' ' . $single_input['last_name'];
                }
                $transitto   = (isset($insurance_details['transitto'])) ? $this->common->db_field_id('country_t', 'short_name', $insurance_details['transitto'], 'country_id') : 'not specified';
                $transitfrom = (isset($insurance_details['transitfrom'])) ? $this->common->db_field_id('country_t', 'short_name', $insurance_details['transitfrom'], 'country_id') : 'not specified';

                $insurance_details['shipment_date'] = (isset($insurance_details['shipment_date'])) ? $insurance_details['shipment_date'] : '';

                $date_purch = explode(' - ', $insurance_details['shipment_date']);

                $agent = $this->common->customer_name($value['customer_id']);

                $value['date']          = date_format(date_create($date_purch[0]), 'd-m-Y');
                $value['purchase_date'] = $date_purch[0];

                $agency_code = ($value['agency_code'] != '0') ? $this->agentcode($value['agency_code']) : 'NA';
                $agent_name  = $this->customer_name($value['agency_code']);
                $billings[]  = array(
                    'date' => date_format(date_create($date_purch[0]), 'd-m-Y'),
                    'purchase_date' => $date_purch[0],
                    'agency_code' => $agency_code,
                    'agent_name' => $agent_name,
                    //'agent'=>$agent,

                    'consignee' => strip_tags($insurance['consignee']),
                    'qoute' => $this->quote_id($value['id']),
                    'premium' => '$' . $premium
                );
            }
        }


        //filter by date range
        $filtered_billings = array();
        if (count($billings) > 0) {
            foreach ($billings as $r => $value) {
                if (strtotime($value['date']) >= strtotime($date_from) && strtotime($value['date']) <= strtotime($date_to)) {

                    unset($value['date']);
                    $filtered_billings[] = $value;
                }
            }
        }

        //sort if not empty
        if (count($filtered_billings) > 0) {
            uasort($filtered_billings, array(
                $this,
                'date_compare'
            ));
        }

        return $filtered_billings;
    }

    public function bordereaux_report($date_from, $date_to, $header_name = array())
    {
        $insurances = $this->master->getRecords('bought_insurance', array(
            'status' => 'P'
        )); //bought_quote

        $billings = array();
        if (count($insurances) > 0) {
            foreach ($insurances as $r => $value) {
                $insurance = $this->common->the_cert_data($value['id'], 'no_highlight'); //the_quote_data

                $details           = unserialize($value['details']);
                $premium           = (isset($details['premium'])) ? $details['premium'] : 0;
                $premium           = number_format($premium, 2, '.', ',');
                $buy_inputs        = (isset($details['buy_inputs'])) ? $details['buy_inputs'] : array();
                $insurance_details = array();
                foreach ($buy_inputs as $bi => $bival) {
                    $insurance_details[$bival['name']] = $bival['value'];
                }

                $insurance = array_merge($insurance_details, $insurance);
                // $insurance['invoice'] = $insurance_details['invoice'];
                // $insurance['currency'] = $insurance_details['currency'];

                $single_input = (isset($details['single_input'])) ? $details['single_input'] : array();
                $insurance['cons_first_name']    = (isset($single_input['first_name'])) ? $single_input['first_name'] : 'Unknown';
                $insurance['cons_last_name']    = (isset($single_input['last_name'])) ? $single_input['last_name'] : 'Unknown';
                $insurance['cons_business_name']    = (isset($single_input['business_name'])) ? $single_input['business_name'] : 'Unknown';
                $insurance['cons_email']    = (isset($single_input['email'])) ? $single_input['email'] : 'Unknown';
                $insurance['cons_calling_code']    = (isset($single_input['calling_code'])) ? $single_input['calling_code'] : 'Unknown';
                $insurance['cons_calling_digits']    = (isset($single_input['calling_digits'])) ? $single_input['calling_digits'] : 'Unknown';
                // if (isset($single_input['first_name']) && isset($single_input['last_name'])) {
                //     $cust_name = $single_input['first_name'] . ' ' . $single_input['last_name'];
                // }

                $consignee = (isset($details['consignee'])) ? $details['consignee'] : array();
              
                $insurance['nee_first_name']    = (isset($consignee['first_name'])) ? $consignee['first_name'] : 'Unknown';
                $insurance['nee_last_name']    = (isset($consignee['last_name'])) ? $consignee['last_name'] : 'Unknown';
                $insurance['nee_business_name']    = (isset($consignee['business_name'])) ? $consignee['business_name'] : 'Unknown';
                $insurance['nee_email']    = (isset($consignee['email'])) ? $consignee['email'] : 'Unknown';
                $insurance['nee_calling_code']    = (isset($consignee['calling_code'])) ? $consignee['calling_code'] : 'Unknown';
                $insurance['nee_calling_digits']    = (isset($consignee['calling_digits'])) ? $consignee['calling_digits'] : 'Unknown';

                
                $transitto   = (isset($insurance_details['transitto'])) ? $this->common->db_field_id('country_t', 'short_name', $insurance_details['transitto'], 'country_id') : 'not specified';
                $transitfrom = (isset($insurance_details['transitfrom'])) ? $this->common->db_field_id('country_t', 'short_name', $insurance_details['transitfrom'], 'country_id') : 'not specified';

                $shipment_date = $value['date_added']; //(isset($insurance_details['shipment_date'])) ? $insurance_details['shipment_date'] : '';
                $date_purch = explode(' - ', $shipment_date);
                // $date_purch = (count($date_purch) < 2) ? explode('/', $shipment_date) : array();

                $agent = $this->common->customer_name($value['customer_id']);

                unset($insurance['longline']);
                unset($insurance['shortlineg']);
                unset($insurance['shortline']);
                unset($insurance['medline']);
                unset($insurance['logolink']);
                unset($insurance['signlink']);
                //				unset($insurance['conv_insurance']);
                //				unset($insurance['base_currency']);

                $insurance['agent']         = $agent;
                $insurance['quote_id']         = $this->policy_id_format($value['id'], 'Q');
                
                if((isset($date_purch[0])) && !is_bool(date_create($date_purch[0]))){
                    $insurance['uuid']       = $value['uuid'];
                    $insurance['policy_no']  = 'TBC'; //$this->policy_id_format($value['id'])


                    $single_input     = (isset($details['single_input'])) ? $details['single_input'] : array();
                    $single_consignee = (isset($details['consignee'])) ? $details['consignee'] : array();
    
                    // if (!empty($details['single_input'])) {
                    //     $consignor = $single_input['first_name'] . ' ' . $single_input['last_name'] . '<br>';
                    //     $consignor .= $single_input['business_name'] . '<br>';
                    //     $consignor .= $single_input['location'];
                    //     $arr['consignor'] = $so . $consignor . $sc;
                    // }
    
                    // if (!empty($details['consignee'])) {
                    //     $consignee = $single_consignee['first_name'] . ' ' . $single_consignee['last_name'] . '<br>';
                    //     $consignee .= $single_consignee['business_name'] . '<br>';
                    //     $consignee .= $single_consignee['location'];
                    //     $arr['consignee'] = $so . $consignee . $sc;
                    // }
                    
                    //customer brokerage
                    $brokerage_id = $this->common->db_field_id('customers', 'brokerage_id', $value['customer_id']);
                    $insurance['brokerage'] = ($brokerage_id != 'Not specified') ? $this->common->db_field_id('brokerage', 'company_name', $brokerage_id) : ' - ';
                    
                    $insurance['purchase_date'] = $date_purch[0]; //date_format(date_create($date_purch[0]), 'Y-m-d');
                    $insurance['date']          =  date_format(date_create($date_purch[0]), 'd-m-Y');

                    $insurance['vessel_name']    = (isset($insurance_details['vessel_name'])) ? $insurance_details['vessel_name'] : '';
                    $insurance['transit_begins'] = (isset($insurance_details['transit_begins'])) ? $insurance_details['transit_begins'] : '';
                    $insurance['transit_ends']   = (isset($insurance_details['transit_ends'])) ? $insurance_details['transit_ends'] : '';
                    $billings[]                  = $insurance;
                }

            }
        }


        //filter by date range
        $filtered_billings = array();
        if (count($billings) > 0) {
            foreach ($billings as $r => $value) {
                if (strtotime($value['date']) >= strtotime($date_from) && strtotime($value['date']) <= strtotime($date_to)) {

                    // unset($value['date']);



                    $filtered_billings[] = $value;
                }
            }
        }

        //sort if not empty
        if (count($filtered_billings) > 0) {
            uasort($filtered_billings, array(
                $this,
                'date_compare'
            ));
        }

        $final_filter = array();
        if (count($filtered_billings) > 0) {
            foreach ($filtered_billings as $r => $value) {
                $newval = array();
                // $newval['date'] = $value['date'];

                if (!empty($header_name)) {
                    foreach ($header_name as $hr => $hvalue) {
                        $newval[$hvalue] = (isset($value[$hvalue])) ? strip_tags($value[$hvalue]) : ''; 
                        $newval[$hvalue] = str_replace("&nbsp;", '', $newval[$hvalue]); 
                        $newval[$hvalue] = str_replace("&middot;", '-', $newval[$hvalue]); 
                        
                        //$value[$hvalue];
                    }
                }
                $final_filter[] = $newval;
            }
        }


        return $final_filter;
    }


    public function report_header()
    {
        $column_header = array(
            'Quote ID',
            'Date Bound',
            'Shipment Date',
            'Certificate Number',
            'Premium',
            'Excess',
            'Agent Name',
            'Brokerage',
            'Currency',
            'Insured Value',
            'Cargo Category',// (HS Code only…so not the text description)
            'Description of Goods',
            'Transportation',
            'Name of Vessel/Aircraft',
            'Port of Loading',
            'Port of Discharge',
            'Address where transit begins',
            'Address where transit ends',
            'Consignor\'s First Name',
            'Consignor\'s Last Name',
            'Consignor\'s Business Name',
            // 'Address where transit begins',
            'Consignor\'s Email',
            'Consignor\'s Country Calling Code',
            'Consignor\'s Contact Number',
            'Consignee\'s First Name',
            'Consignee\'s Last Name',
            'Consignee\'s Business Name',
            // 'Address where transit ends',
            'Consignee\'s Email',
            'Consignee\'s Country Calling Code',
            'Consignee\'s Contact Number',
            

            // 'Consignor Details',
            // 'Consignee Details',
            // 'Shipment Date',
            // 'Insured Value',
            // 'Transit From',
            // 'Cargo Category',
            // 'Port of Loading',
            // 'Port of Discharge',
            // 'Transit To',
            // 'Description',
            // 'Deductible',
            // 'Currency',
            // 'Premium',
            // 'Quote Number/ID',
            // 'Base Currency',
            // 'Converted Insured Value',
            // 'Agent',
            // 'Purchase Date',
            // 'Name of Vessel/Aircraft ',
            // 'Address transit begins',
            // 'Address transit ends '
        );

        return $column_header;
    }

    public function report_icons()
    {
        $column_header = array(
            'fa-user',
            'fa-child',
            'fa-calendar',
            'fa-usd',
            'fa-map-marker',
            'fa-list-alt',
            'fa-ship',
            'fa-ship',
            'fa-map-marker',
            'fa-file-text-o',
            'fa-minus-circle',
            'fa-money',
            'fa-check-circle-o',
            'fa-quote-left',
            'fa-plus-circle',
            'fa-picture-o',
            'fa-child',
            'fa-calendar',
            'fa-ship',
            'fa-map-marker',
            'fa-map-marker'
        );

        return $column_header;
    }


    public function report_header_name()
    {
        $column_header = array(
            'quote_id',
            'date',
            'shipmentdate',
            'uuid',
            'premium',
            'deductible',
            'agent',
            'brokerage',
            'currency',
            'invoice',
            'cargocat',
            'description',
            'transmethod',
            'vessel_name',
            'portloading',
            'portdischarge',
            'transit_begins',
            'transit_ends',

            'cons_first_name',
            'cons_last_name',
            'cons_business_name',
            'cons_email',
            'cons_calling_code',
            'cons_calling_digits',

            'nee_first_name',
            'nee_last_name',
            'nee_business_name',
            'nee_email',
            'nee_calling_code',
            'nee_calling_digits'

            // 'consignor',
            // 'consignee',
            // 'insurance',
            // 'transitfrom',
            // 'transitto',
            // 'currency',
            // 'quoteid',
            // 'base_currency',
            // 'conv_insurance',
            // 'agent',
            // 'purchase_date',
            // 'lat',
            // 'lng',
            // 'converted_invoice',
            // 'cargo_max_val',
            // 'goods_desc',
            // 'goods_descx',
            // 'goods_descx'

        );

        return $column_header;
    }

    public function date_compare($a, $b)
    {
        $t1 = strtotime($a['purchase_date']);
        $t2 = strtotime($b['purchase_date']);
        return $t1 - $t2;
    }

    public function selectedport($id)
    {
        $txt = '';

        if (is_numeric($id)) {
            $port_codes = $this->master->getRecords('port_codes', array(
                'id' => $id
            ));

            if (count($port_codes) > 0) {
                $txt = $port_codes[0]['country_code'] . ' - ' . $port_codes[0]['port_name'] . '(' . $port_codes[0]['port_code'] . ')';
            }
        } else {
            $txt = $id;
        }
        return $txt;
    }

    public function policy_request_status($status, $saved = 'N')
    {
        $stat = '<i class="fa fa-circle text-muted"></i> Referred';
        if ($status == 'P') {
            $stat = '<i class="fa fa-circle text-success"></i> Bound';
            if ($saved == 'Y') {
                $stat = '<i class="fa fa-circle text-warning"></i> Bound';
            }
        }
        else if ($status == 'A') {
            $stat = '<i class="fa fa-circle text-info"></i> Approved';
        } else if ($status == 'R') {
            $stat = '<i class="fa fa-circle text-danger"></i> Declined';
        } else if ($status == 'D') {
            $stat = '<i class="fa fa-circle text-warning"></i> Saved';
        }
        return $stat;
    }

    public function quote_status($status)
    {
        $stat = '<i class="fa fa-circle text-muted"></i> Referred';
        if ($status == 'P') {
            $stat = '<i class="fa fa-circle text-success"></i> Bound';
        } else if ($status == 'A') {
            $stat = '<i class="fa fa-circle text-info"></i> Approved';
        } else if ($status == 'R') {
            $stat = '<i class="fa fa-circle text-danger"></i> Declined';
        } else if ($status == 'D') {
            $stat = '<i class="fa fa-circle text-warning"></i> Saved';
        }
        return $stat;
    }


    public function agency_customers($agency_id)
    {
        $customer  = $this->master->getRecords('customers', array(
            'agency_id' => $agency_id
        ));
        $customers = array();


        if (count($customer) > 0) {
            foreach ($customer as $r => $value) {
                $arr = array(
                    'id' => $value['id'],
                    'name' => $value['first_name'] . ' ' . $value['last_name'],
                    'avatar' => $this->common->avatar($value['id'], 'customer')
                );

                $customers[] = $arr;
            }
        }

        return $customers;
    }


    public function agency_prices($agency_id, $type)
    {
        $agency = $this->master->getRecords('agency', array(
            'id' => $agency_id
        ));
        $mini   = array();

        if (count($agency) > 0) {
            $min = $agency[0][$type];
            if ($min != '') {
                $mini = unserialize($min);
            }
        }
        return $mini;
    }


    public function agency_minimum_premium($agency_id)
    {
        $agency = $this->master->getRecords('agency', array(
            'id' => $agency_id
        ));
        $mini   = 0;

        if (count($agency) > 0) {
            $mini = $agency[0]['minimum_premium'];

        }
        return $mini;
    }

    public function agency_max_insured($agency_id)
    {
        $agency = $this->master->getRecords('agency', array(
            'id' => $agency_id
        ));
        $mini   = 0;

        if (count($agency) > 0) {
            $mini = str_replace(',', '', $agency[0]['max_insurance']);

        }
        return $mini;
    }

    public function base_currency($agency_id)
    {
        $agency = $this->master->getRecords('agency', array(
            'id' => $agency_id
        ));
        $mini   = 0;

        if (count($agency) > 0) {
            $mini = ($agency[0]['base_currency'] != '') ? $agency[0]['base_currency'] : 'AUD';
        }
        return $mini;
    }

    public function converted_value($from, $to, $amount)
    {
        $url = 'http://currency-api.appspot.com/api/' . $from . '/' . $to . '.json?amount=' . $amount;

        $result = file_get_contents($url);
        $result = json_decode($result);

        $value = $amount;
        if ($result->success) {
            $value = $result->amount;
        }

        return $value;
    }

    public function numeric_currency($amount)
    {

        $b = str_replace(',', '', $amount);

        if (is_numeric($b)) {
            $amount = $b;
        }

        return $amount;
    }

    public function cargo_name($cargo_id)
    {
        $cargo_category = $this->master->getRecords('cargo_category', array(
            'id' => $cargo_id
        ));

        $name = 'Not specified';
        if (count($cargo_category) > 0) {
            $name = $cargo_category[0]['hs_code'] . ' &middot; ' . $cargo_category[0]['description'];
        }
        return $name;
    }

    public function transportations()
    {
        $arr = array(
            'Sea Freight',
            'Air Freight',
            'Road Freight',
            'Combination' // of Sea, Air and Road'
        );
        return $arr;
    }

    public function transportations_icon()
    {
        $arr = array(
            'fa fa-ship',
            'fa fa-plane',
            'fa fa-truck',
            'fa fa-globe'
        );
        return $arr;
    }

    public function admin_email()
    {
        $admin = $this->master->getRecords('admin');
        return $admin[0]['email'];
    }

    public function recovery_admin()
    {
        $admin = $this->master->getRecords('admin');
        return $admin[0]['recovery_email'];
    }

    public function default_cargo()
    {
        $admin = $this->master->getRecords('admin');
        return $admin[0]['default_cargo'];
    }


    public function access_rights()
    {
        $arr = array(
            'Referrals',
            'Transactions',
            'Manage Brokers',
            'Manage Customers',
            'Rating',
            'Manage Port Codes',
            'Manage Certifcate',
            'Policy Document',
            'Reporting',
            'Outgoing Emails',
            'Manage Content',
            'Settings'
        );

        return $arr;
    }



    public function encrypt($q)
    {
        $cryptKey = 'TMhh4G1UrC4j9QtxVSk06qORx2aD93Ye';
        $qEncoded = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), $q, MCRYPT_MODE_CBC, md5(md5($cryptKey))));
        return ($qEncoded);
    }

    public function decrypt($q)
    {
        $cryptKey = 'TMhh4G1UrC4j9QtxVSk06qORx2aD93Ye';
        $qDecoded = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), base64_decode($q), MCRYPT_MODE_CBC, md5(md5($cryptKey))), "\0");
        return ($qDecoded);
    }


    public function country_format()
    {
        $countries = $this->master->getRecords('country_t', array(), '*', array(
            'country_id' => 'ASC'
        ));
        $arr       = array();
        foreach ($countries as $r => $value) {
            $value['short_name_iso2'] = $value['short_name'] . ' (+' . $value['calling_code'] . ')';
            $arr[]                    = $value;
        }
        return $arr;
    }

    public function currencies_format()
    {
        $currencies = $this->master->getRecords('api_currencies', array(
            'in_stripe' => 'Y'
        ));

        $arr = array();

        foreach ($currencies as $r => $value) {
            $value['country_code']  = substr($value['currency'], 0, 2);
            $value['name_currency'] = $value['name'] . ' (' . $value['currency'] . ')';
            $arr[]                  = $value;
        }

        return $arr;
    }

    public function buy_mob($u, $t)
    {
        $user_id       = (isset($u)) ? $u : $this->session->userdata('id');
        $customer_type = (isset($t)) ? $u : $this->session->userdata('customer_type');

        $cargos          = $this->master->getRecords('cargo_category');
        $transportations = $this->common->transportations();
        $countries       = $this->country_format();
        $currencies      = $this->currencies_format();

        $admin_info  = $this->master->getRecords('admin');
        $the_agency  = $this->master->getRecords('agency', array(
            'id' => $admin_info[0]['default_cargo']
        ));
        $deductibles = $this->master->getRecords('deductibles', array(
            'agency_id' => $admin_info[0]['default_cargo']
        ), '', array(
            'not_default' => 'DESC'
        ));
        $default_ded = array(
            'rate' => '1',
            'deductible' => '1'
        );
        if (count($deductibles) > 0) {
            $default_ded['rate'] = $deductibles[0]['rate'];
            //$deductibles[0]['deductible'] = str_replace(',','', $deductibles[0]['deductible']);
        }

        $minimum_premium  = $this->common->agency_minimum_premium($admin_info[0]['default_cargo']);
        $max_insured      = $this->common->agency_max_insured($admin_info[0]['default_cargo']);
        $base_currency    = $this->common->base_currency($admin_info[0]['default_cargo']);
        $country_currency = ($the_agency[0]['country_currency']) ? $the_agency[0]['country_currency'] : 'Y';

        $agency_id = '0';
        if (count($the_agency) > 0) {
            $agency_id = $the_agency[0]['id'];
        }

        $policy_docs = $this->master->getRecords('policy_docs', '', '*', array(
            'id' => 'DESC'
        ));

        $cargo_prices          = $this->common->agency_prices($agency_id, 'cargo_prices');
        $cargo_max             = $this->common->agency_prices($agency_id, 'cargo_max');
        $transportation_prices = $this->common->agency_prices($agency_id, 'transportation_prices');
        $country_prices        = $this->common->agency_prices($agency_id, 'country_prices');
        $country_referral      = $this->common->agency_prices($agency_id, 'country_referral');
        $zone_multiplier       = $this->common->agency_prices($agency_id, 'zone_multiplier');

        $data = array(
            'transportations' => $transportations,
            'countries' => $countries,
            'currencies' => $currencies,
            'cargo_prices' => $cargo_prices,
            'cargo_max' => $cargo_max,
            'transportation_prices' => $transportation_prices,
            'country_prices' => $country_prices,
            'country_referral' => $country_referral,
            'zone_multiplier' => $zone_multiplier,
            'the_agency' => $the_agency,
            'deductibles' => $deductibles,
            'default_ded' => $default_ded,
            'minimum_premium' => $minimum_premium,
            'max_insured' => $max_insured,
            'base_currency' => $base_currency,
            'country_currency' => $country_currency,
            'title' => ($customer_type == 'N') ? 'Get Quote' : 'Buy Insurance',
            'view' => 'buy_view',
            'policy_docs' => $policy_docs,
            'cargos' => $cargos
        );

        //transit form vars
        //get session values
        $buy_inputs  = $this->session->userdata('buy_inputs');
        $premium     = $this->session->userdata('premium');
        $cargo_price = $this->session->userdata('cargo_price');
        $bindoption  = $this->session->userdata('bindoption');
        $useremail   = $this->session->userdata('email');
        $email       = '';
        $trans_icon  = $this->common->transportations_icon();


        $mycustomers = array();

        if ($customer_type == 'N') {
            $mycustomers = $this->master->getRecords('agent_customers', array(
                'agent_id' => $user_id
            ));
        }


        $shipment_date      = '';
        $vessel_name        = '';
        $transitfrom        = '';
        $portloading        = '';
        $transitto          = '';
        $portdischarge      = '';
        $cargocat           = '';
        $goods_desc         = '';
        $goods_count        = 300;
        $transmethod        = '';
        $currency           = '';
        $invoice            = '';
        $cargo_max_val      = '';
        $default_deductible = '';

        if ($buy_inputs != '') { //count($buy_inputs) > 0
            foreach ($buy_inputs as $r => $value) {
                ${$value['name']} = $value['value'];
            }
            $goods_count = $goods_count - strlen($goods_desc);

        }
        if ($useremail != '') {
            $email = $useremail;
        }

        //update value of default ded when not found
        $existing_ded        = $this->master->getRecordCount('deductibles', array(
            'rate' => $default_deductible
        ));
        $selected_deductible = ($default_deductible) ? $default_deductible : $default_ded['rate'];

        if ($existing_ded == 0) {
            $selected_deductible = $default_ded['rate'];
            $default_deductible  = $selected_deductible;
        }

        $data['premium']             = $premium;
        $data['cargo_price']         = $cargo_price;
        $data['cargo_max_val']       = $cargo_max_val;
        $data['bindoption']          = $bindoption;
        $data['useremail']           = $useremail;
        $data['email']               = $email;
        $data['buy_inputs']          = $buy_inputs;
        $data['trans_icon']          = $trans_icon;
        $data['selected_deductible'] = $selected_deductible;

        $data['shipment_date'] = $shipment_date;
        $data['vessel_name']   = $vessel_name;
        $data['transitfrom']   = $transitfrom;
        $data['portloading']   = $portloading;
        $data['transitto']     = $transitto;
        $data['portdischarge'] = $portdischarge;
        $data['cargocat']      = $cargocat;
        $data['goods_desc']    = $goods_desc;
        $data['goods_count']   = $goods_count;
        $data['transmethod']   = $transmethod;
        $data['currency']      = $currency;
        $data['invoice']       = $invoice;
        $data['mycustomers']   = $mycustomers;
        $data['view']          = 'buy_insurance';


        return $data;


    }


    public function agentcode($id)
    {
        $agentcode = 'A';
        $format_id = sprintf("%03d", $id);
        $agentcode = $agentcode . $format_id;
        return $agentcode;
    }

    public function brokercode($id)
    {
        $agentcode = 'B';
        $format_id = sprintf("%03d", $id);
        $agentcode = $agentcode . $format_id;
        return $agentcode;
    }


    public function save_quote()
    {
        $user_id    = $this->session->userdata('id');
        $booking_id = $this->session->userdata('booking_id');
        $single     = $this->session->userdata('single');
        $email      = $this->session->userdata('email');
        $nowtime    = date('Y-m-d H:i:s');

        $buy_inputs = $this->session->userdata('buy_inputs');
        $consignee  = $this->session->userdata('consignee');
        $premium    = $this->session->userdata('premium');
        //get session data
        $details    = array(
            'buy_inputs' => $buy_inputs,
            'single_input' => (is_array($single)) ? $single : array(),
            'consignee' => (is_array($consignee)) ? $consignee : array(),
            'premium' => $premium
        );

        $data = array(
            'customer_id' => $user_id,
            'details' => serialize($details),
            'status' => 'D',
            'email' => (isset($single['email'])) ? $single['email'] : $email
        );

        $data['date_added'] = $nowtime;

        if ($booking_id != '') {
            $this->master->updateRecord('bought_insurance', $data, array(
                'id' => $booking_id
            ));
        } else {
            $uuid         = $this->common->uuid();
            $data['uuid'] = $uuid;

            $booking_id = $this->master->insertRecord('bought_insurance', $data, true);
            $this->session->set_userdata('booking_id', $booking_id);
        }

        return $details;
    }

    public function reset_transaction_session(){
        //reset on page refreshed
        $arr = array(
            'buy_inputs',
            'booking_id',
            'premium',
            'premium_format',
            'selected_deductible',
            'cargo_price',
            'currency',
            'single',
            'consignee',
            'is_approved',
            'new_deductible',
            'insurance_id',
            'refer'
        );

        
        foreach ($arr as $key) {
            $this->session->unset_userdata($key);
        }

        return true;
    }


    public function home_data($reset = 'yes')
    {


        if ($reset == 'yes') {
            $this->reset_transaction_session();
        }



        $cargos          = $this->master->getRecords('cargo_category');
        $transportations = $this->common->transportations();
        $countries       = $this->master->getRecords('country_t');
        $currencies      = $this->master->getRecords('api_currencies', array(
            'in_stripe' => 'Y'
        ));

        $admin_info  = $this->master->getRecords('admin');
        $the_agency  = $this->master->getRecords('agency', array(
            'id' => $admin_info[0]['default_cargo']
        ));
        $deductibles = $this->master->getRecords('deductibles', array(
            'agency_id' => $admin_info[0]['default_cargo']
        ), '', array(
            'not_default' => 'DESC'
        ));
        $default_ded = array(
            'rate' => '1',
            'deductible' => '1'
        );
        if (count($deductibles) > 0) {
            $default_ded['rate'] = $deductibles[0]['rate'];
            //$deductibles[0]['deductible'] = str_replace(',','', $deductibles[0]['deductible']);
        }

        $minimum_premium  = $this->common->agency_minimum_premium($admin_info[0]['default_cargo']);
        $max_insured      = $this->common->agency_max_insured($admin_info[0]['default_cargo']);
        $base_currency    = $this->common->base_currency($admin_info[0]['default_cargo']);
        $country_currency = ($the_agency[0]['country_currency']) ? $the_agency[0]['country_currency'] : 'Y';
        $agency_id        = '0';
        if (count($the_agency) > 0) {
            $agency_id = $the_agency[0]['id'];
        }

        $policy_docs = $this->master->getRecords('policy_docs', '', '*', array(
            'id' => 'DESC'
        ));

        $cargo_prices          = $this->common->agency_prices($agency_id, 'cargo_prices');
        $cargo_max             = $this->common->agency_prices($agency_id, 'cargo_max');
        $transportation_prices = $this->common->agency_prices($agency_id, 'transportation_prices');
        $country_prices        = $this->common->agency_prices($agency_id, 'country_prices');
        $country_referral      = $this->common->agency_prices($agency_id, 'country_referral');
        $zone_multiplier       = $this->common->agency_prices($agency_id, 'zone_multiplier');

        $data = array(
            'cargos' => $cargos,
            'transportations' => $transportations,
            'countries' => $countries,
            'currencies' => $currencies,
            'cargo_prices' => $cargo_prices,
            'cargo_max' => $cargo_max,
            'transportation_prices' => $transportation_prices,
            'country_prices' => $country_prices,
            'country_referral' => $country_referral,
            'zone_multiplier' => $zone_multiplier,
            'the_agency' => $the_agency,
            'deductibles' => $deductibles,
            'default_ded' => $default_ded,
            'minimum_premium' => $minimum_premium,
            'max_insured' => $max_insured,
            'base_currency' => $base_currency,
            'country_currency' => $country_currency,
            'admin_info' => $admin_info,
            'title' => 'Buy Insurance',
            'view' => 'buy_view',
            'policy_docs' => $policy_docs
        );

        //transit form vars
        //get session values
        $buy_inputs  = $this->session->userdata('buy_inputs');
        $premium     = $this->session->userdata('premium');
        $cargo_price = $this->session->userdata('cargo_price');
        $bindoption  = $this->session->userdata('bindoption');
        $useremail   = $this->session->userdata('email');
        $email       = '';
        $trans_icon  = $this->common->transportations_icon();

        $user_id       = $this->session->userdata('id');
        $customer_type = $this->session->userdata('customer_type');
        $mycustomers   = array();

        if ($customer_type == 'N') {
            $mycustomers = $this->master->getRecords('agent_customers', array(
                'agent_id' => $user_id
            ));
        }


        $transit_begins     = '';
        $transit_ends       = '';
        $shipment_date      = '';
        $vessel_name        = '';
        $transitfrom        = '';
        $portloading        = '';
        $transitto          = '';
        $portdischarge      = '';
        $cargocat           = '';
        $goods_desc         = '';
        $goods_count        = 300;
        $transmethod        = '';
        $currency           = ($the_agency[0]['base_currency']) ? $the_agency[0]['base_currency'] : 'AUD';
        $invoice            = '';
        $cargo_max_val      = '';
        $default_deductible = '';

        if ($buy_inputs != '') { //count($buy_inputs) > 0
            foreach ($buy_inputs as $r => $value) {
                ${$value['name']} = $value['value'];
            }
            $goods_count = $goods_count - strlen($goods_desc);

        }
        if ($useremail != '') {
            $email = $useremail;
        }

        //update value of default ded when not found
        $existing_ded        = $this->master->getRecordCount('deductibles', array(
            'rate' => $default_deductible
        ));
        $selected_deductible = ($default_deductible) ? $default_deductible : $default_ded['rate'];

        if ($existing_ded == 0) {
            $selected_deductible = $default_ded['rate'];
            $default_deductible  = $selected_deductible;
        }

        $data['premium']             = $premium;
        $data['cargo_price']         = $cargo_price;
        $data['cargo_max_val']       = $cargo_max_val;
        $data['bindoption']          = $bindoption;
        $data['useremail']           = $useremail;
        $data['email']               = $email;
        $data['buy_inputs']          = $buy_inputs;
        $data['trans_icon']          = $trans_icon;
        $data['selected_deductible'] = $selected_deductible;

        $data['transit_begins'] = $transit_begins;
        $data['transit_ends']   = $transit_ends;
        $data['shipment_date']  = $shipment_date;
        $data['vessel_name']    = $vessel_name;
        $data['transitfrom']    = $transitfrom;
        $data['portloading']    = $portloading;
        $data['transitto']      = $transitto;
        $data['portdischarge']  = $portdischarge;
        $data['cargocat']       = $cargocat;
        $data['goods_desc']     = $goods_desc;
        $data['goods_count']    = $goods_count;
        $data['transmethod']    = $transmethod;
        $data['currency']       = $currency;
        $data['invoice']        = $invoice;
        $data['mycustomers']    = $mycustomers;



        //new menu
        $data['homebiz1'] = $this->session->userdata('homebiz1');
        $data['homebiz2'] = $this->session->userdata('homebiz2');
        $data['homebiz3'] = $this->session->userdata('homebiz3');
        $data['homebiz4'] = $this->session->userdata('homebiz4');
        $data['homebiz5'] = $this->session->userdata('homebiz5');
        $data['homebiz6'] = $this->session->userdata('homebiz6');
        $data['homebiz7'] = $this->session->userdata('homebiz7');

        $thepage          = (isset($_GET['tab'])) ? $_GET['tab'] : '1';
        $data['thepage']  = $thepage;
        $theindex         = $thepage - 1;
        $data['theindex'] = $theindex;


        $themenu         = array(
            array(
                'name' => 'Transit',
                'data' => $data['homebiz1'],
                'target' => 'tab-transit'
            ),
            //			array(
            //				'name'=>'Cargo',
            //				'data'=>$data['homebiz1'],
            //				'target'=>'tab-cargo'
            //			),
            array(
                'name' => 'Quote',
                'data' => $data['homebiz2'],
                'target' => 'tab-quote'
            ),
            array(
                'name' => 'Bind',
                'data' => $data['homebiz3'],
                'target' => 'tab-details'
            ),
            //			array(
            //				'name'=>'Confirm',
            //				'data'=>$data['homebiz4'],
            //				'target'=>'tab-confirm'
            //			),
            // array(
            //     'name' => ($this->session->userdata('customer_type') == 'N') ? 'Send' : 'Payment',
            //     'data' => $data['homebiz5'],
            //     'target' => 'tab-payment'
            // ),
            array(
                'name' => 'Done',
                'data' => $data['homebiz5'],
                'target' => 'tab-done'
            )
        );
        $data['themenu'] = $themenu;


        return $data;


    }

    public function surveyors_fields()
    {
        $col = array(
            'iso_code',
            'common_name',
            'agent_name',
            'main_address_line',
            'main_address_line_2',
            'main_address_line_3',
            'main_city',
            'state',
            'postcode',
            'main_country',
            'full_main_address',
            'main_email',
            'after_hours',
            'main_phone_no',
            'main_fax_no',
            'contact_mobile_no'
        );

        return $col;

    }

    public function uuid()
    {
        $section = file_get_contents('https://www.uuidgenerator.net/api/version1', NULL, NULL, 0);
        return $section;
    }

    public function policy_id_format($id, $type = 'P')
    {
        $sprint = sprintf("%04d", $id);
        return $type . $sprint; //'P'.$yr.$mo.$sprint;
    }


}

?>
