var action_messages = '';
var current_class_booked = {};

//get action messages
$(document).ready(function(e) {
    $.post(
        base_url + 'formsubmits/action_messages', {},
        function(res) {
            console.log(res);
            action_messages = res;
        },
        'json'
    ).error(function(err) {
        console.log(err);
    });

    $('.toggle_check').on('click', function(){
        var $self = $(this);
        var toToggle = $self.data('name');
        var isChecked = $('[name="'+toToggle+'"]').is(':checked');
        console.log(isChecked, toToggle);
        $('[name="'+toToggle+'"]').prop('checked', !isChecked);

        $('[name="'+toToggle+'"]').closest('.btn').removeClass('btn-primary active').addClass('btn-default');

        if(!isChecked){
            $('[name="'+toToggle+'"]').closest('.btn').removeClass('btn-default').addClass('btn-primary');
        }
    });

    //location of user
    var startPos;
    var geoSuccess = function(position) {
        startPos = position;

        console.log(startPos.coords.latitude, startPos.coords.longitude, startPos)

        $.getJSON('http://api.geonames.org/countryCode', {
            lat: startPos.coords.latitude,
            lng: startPos.coords.longitude,
            username: 'snailbob',
            type: 'JSON'
        }, function(result) {
            setTimeout(function() {
                console.log($('#currency').data('country'), result);
                if ($('#currency').data('country') == 'Y') {
                    $('option[data-country="' + result.countryCode + '"]').prop('selected', true);
                    $('[name="currency"]').selectpicker('refresh');
                }
            }, 1500);
            //alert(result.countryCode);
        });


        //		document.getElementById('startLat').innerHTML = startPos.coords.latitude;
        //		document.getElementById('startLon').innerHTML = startPos.coords.longitude;
    };
    var geoError = function(error) {
        console.log('Error occurred. Error code: ' + error.code);
        // error.code can be:
        //   0: unknown error
        //   1: permission denied
        //   2: position unavailable (error response from location provider)
        //   3: timed out
    };

    if (typeof(get_location) !== 'undefined') {
        if (get_location == 'Y') {
            navigator.geolocation.getCurrentPosition(geoSuccess, geoError);
        }
    }



});


//trigger map change
var show_map = function() {
    setTimeout(function() {
        //trigger location to display map onload
        $('.request_quote_location').trigger("geocode");
    }, 2);

}; //show_map



function FormatCurrency(ctrl) {
    //Check if arrow keys are pressed - we want to allow navigation around textbox using arrow keys
    if (event.keyCode == 37 || event.keyCode == 38 || event.keyCode == 39 || event.keyCode == 40) {
        return;
    }

    var val = ctrl.value;

    val = val.replace(/,/g, "")
    ctrl.value = "";
    val += '';
    x = val.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';

    var rgx = /(\d+)(\d{3})/;

    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }

    ctrl.value = x1 + x2;
}

function CheckNumeric() {
    return event.keyCode >= 48 && event.keyCode <= 57;
}

var getCountryCalling = function(country_short, $form) {
    $.ajax({
        type: "POST",

        url: base_url + "settings/show_country_code",

        data: {
            country_short: country_short
        },

        dataType: 'json',

        success: function(data) {
            console.log(data);

            $form.find('.addon-shortcode').html('+' + data.calling_code);
            $form.find('[name="country_code"]').val(data.country_id);
            //$('[name="country_short"]').val(data);
            $form.find('[name="country_code"]').selectpicker('refresh');
        },
        error: function(err) {
            console.log(err);
        }
    });

};

var deleteRecord = function(el, id, table) {
    var mydataTb = $('.mydataTb').DataTable();
    $.post(
        base_url + 'formsubmits/delete_tb', {
            id: id,
            table: table
        },
        function(res) {
            console.log(res);
            el.closest('tr').addClass('bg-danger').fadeOut('fast', function() {

                mydataTb.destroy();
                $.when($(this).remove()).then(function() {

                    mydataTb = $('.mydataTb').DataTable({
                        "bPaginate": true,
                        "bLengthChange": false,
                        "bFilter": false,
                        "bInfo": true
                    });
                });

            });
        }

    );
};

var errForm = function(err) {
    console.log(err);
};


var showSummary = function(data) {
    console.log(data);
    var $mdl = $('#summaryModal');
    $mdl.find('.instructor_info').html('<p class="text-center text-muted"><i class="fa fa-spinner fa-spin fa-2x"></i><br>Loading..</p>');

    $.post(
        base_url + 'landing/schedule_info', {
            data: data
        },
        function(res) {
            $mdl.find('.view_holder').html(res);
        }
    );

    $mdl.modal('show');

};

//scroller detect plugin
(function($) {
    $.fn.hasScrollBar = function() {
        return this.get(0).scrollHeight > this.height();
    }
})(jQuery);


var video_count = 1;
var videoPlayer = document.getElementById("bgvid");
var video_file = [];
var vid1 = base_url + 'assets/videos/shutterstock_v6441026.mp4';
var vid2 = base_url + 'assets/videos/shutterstock_v5239256.mp4';
video_file.push(vid1);
video_file.push(vid2);

var switchVid = function() {

    var mod = video_count % 2;

    var nextVideo = video_file[mod];
    console.log(nextVideo);
    console.log(video_count);
    videoPlayer.src = nextVideo;
    videoPlayer.play();
    video_count++;

};


$(document).ready(function(e) {


    //request for login, signup, add record
    var submitForm = function(passto, data, form) { //(submit link, data to pass, form selector)
        $(form).find('button[type="submit"]').button('loading');

        $.post(
            base_url + 'formsubmits/' + passto,
            data,
            function(res) {
                console.log(res);
                console.log(passto);

                if (res.result == 'ok') {
                    if (passto == 'login') {
                        $(form).find('.btn[type="submit"]').html('<i class="fa fa-check-circle"></i> Success!');
                        setTimeout(function() {
                            $(form).find('.btn[type="submit"]').html('<i class="fa fa-spinner fa-pulse"></i> Success!');

                            //console.log((typeof(res.userdata.logged_admin_id) === 'undefined')); return false;
                            if (typeof(res.userdata.logged_admin_id) === 'undefined') {
                                window.location.href = base_url + 'dashboard';
                            } else {
                                window.location.href = base_url + 'webmanager/dashboard';
                            }
                            
                        }, 700);

                    } else {
                        $(form).find('button[type="submit"]').html('<i class="fa fa-check-circle"></i> Success!');

                        if (passto == 'update_agency' || passto == 'update_password' || passto == 'update_customer' || passto == 'update_student') {
                            bootbox.alert('<h4>' + res.message + '</h4>');
                            $(form).find('.btn').button('reset');
                            console.log('sf');
                        } else if (passto == 'customer_update_password' || passto == 'agency_update_password') {
                            console.log('yea');
                            bootbox.alert('<h4>' + res.message + '</h4>', function() {
                                // $('#loginModal').modal('show');
                                window.location.href = base_url;
                            });
                        } else if (passto != 'signup' && passto != 'forgotpass') {
                            window.location.reload(true);

                        } else {
                            $('.modal').modal('hide');
                            bootbox.alert('<h4>' + res.message + '</h4>');
                        }


                    }
                } else {
                    if (passto == 'login') {
                        $(form).closest('.modal-body').find('.alert span.my-text').html(res.message);
                        $(form).closest('.modal-body').find('.alert').removeClass('hidden alert-success alert-info').addClass('alert-danger');
                        $(form).find('.btn').button('reset');
                    } else {
                        bootbox.alert('<h4>' + res.message + '</h4>');
                        $(form).find('.btn').button('reset');
                        console.log('ahh');
                    }

                }

            },
            'json'
        ).error(function(err) {
            console.log(err);
        });


    };

    $('[name="same_transit_begins"]').on('change', function() {
        var $self = $(this);
        var loc = $('[name="transit_begins"]').val();
        $self.closest('form').find('[name="address"]').val('');
        if ($self.val() == 'y') {
            $self.closest('form').find('[name="address"]').val(loc);
        }
    });

    $('[name="same_transit_ends"]').on('change', function() {
        var $self = $(this);
        var loc = $('[name="transit_ends"]').val();
        $self.closest('form').find('[name="address"]').val('');
        if ($self.val() == 'y') {
            $self.closest('form').find('[name="address"]').val(loc);
        }
    });

    $('.js-close-menu-trigger2').on('click', function() {
        $('.js-close-menu-trigger').click();
    });


    //quotes
    $('.confirm_quote_btn').on('click', function() {
        var $self = $(this);
        var id = $self.data('id');
        var uid = $self.data('uid');
        bootbox.confirm('<h4 class="modal-title">Confirm quote.</h4>', function(e) {
            if (e) {
                $.post(
                    base_url + 'landing/confirm_quote_status', {
                        id: id,
                        uid: uid
                    },
                    function(res) {
                        console.log(res);
                        $self.html('<i class="fa fa-file-text"></i> Confirmed').addClass('disabled');
                    },
                    'json'
                ).error(function(err) {
                    console.log(err);
                });

            }
        });
        return false;
    });

    $('.bind_quote_btn').on('click', function() {
        var $self = $(this);
        var id = $self.data('id');

        bootbox.confirm('<h4 class="modal-title">Confirm to bind quote.</h4>', function(e) {
            if (e) {
                $.post(
                    base_url + 'landing/confirm_quote_bind', {
                        id: id
                    },
                    function(res) {
                        console.log(res);
                        $self.html('Bound').addClass('disabled');
                    },
                    'json'
                ).error(function(err) {
                    console.log(err);
                });

            }
        });
        return false;
    });

    //progressbar-clickable
    $(document).on('click', '.progressbar-clickable li', function() {
        var target = $(this).data('target');
        $(this).addClass('active').siblings().removeClass('active');
        $('.progressbar-control').find('[href="' + target + '"]').click();
    });

    $(document).on('click', '.life-list-clickable a', function() {
        var target = $(this).data('target');
        console.log(target, 'life=eads');
        $(this).closest('li').addClass('complete').siblings().removeClass('complete');
        $('.progressbar-control').find('[href="#' + target + '"]').click();
    });

    $(document).on('click', 'ul.progressbar-main li.done-muted', function() {
        var $self = $(this);
        var target = $self.data('target');
        console.log(target);
        $('#' + target).addClass('active').siblings().removeClass('active');
        $self.removeClass('done').addClass('active').siblings().removeClass('active');
        $self.nextAll().removeClass('done');
        $self.prevAll().addClass('done-muted done');
    });

    //	$(document).on('click', 'ul.life-list li', function(){
    //		var $self = $(this);
    //		var target = $self.data('target');
    //		console.log(target);
    //
    //		if($('#'+target).hasClass('complete') || $('#'+target).hasClass('current')){
    //			$('#'+target).addClass('current').siblings().removeClass('current');
    //			$self.removeClass('complete').addClass('current').siblings().removeClass('current');
    //			$self.nextAll().removeClass('complete');
    //			$self.prevAll().addClass('complete');
    //
    //		}
    //
    //	});


    $('[data-toggle="tooltip"]').tooltip();
    //format number with comma
    $('[name="invoice"], .input-currency').on('keypress', function() {
        CheckNumeric();
    });
    $('[name="invoice"], .input-currency').on('keyup', function() {
        FormatCurrency(this);
    });


    $('.keyboard .keyboard__number').on('click', function() {
        var $self = $(this);
        var num = $self.html();
        var $input = $(document).find('.active-numpad');
        var val = $input.val();
        val = val + num;
        $input.val(val);
        $input.focus();
        setTimeout(function() {
            $input.keyup();
        }, 0);

        console.log(num, val);

    });

    $('.keyboard .keyboard__delete').on('click', function() {
        var $input = $(document).find('.active-numpad');
        var val = $input.val();
        val = val.slice(0, -1);
        $input.val(val);
        $input.focus();
        setTimeout(function() {
            $input.keyup();
        }, 0);

    });

    //	$('[name="invoice"], .input-currency').on('focus', function(){
    //		console.log('yeah');
    //		var $self = $(this);
    //		$('input').removeClass('active-numpad');
    //		$self.addClass('active-numpad');
    //
    //		$('.numpad').show('fast');
    //	});

    $('body').on('click', function(e) {
        if ($(e.target).hasClass('keyboard__number') || $(e.target).hasClass('keyboard__delete') || $(e.target).is('input')) {
            console.log('yeah bdrop');
        } else {
            $('.numpad').fadeOut('fast');
        }

    });




    $('.input-search').on('keyup', function() {
        var rex = new RegExp($(this).val(), 'i');
        var $items = $(this).closest('.modal-body').find('form');
        $items.find('.col-xs-6').hide();
        $items.find('.col-xs-6').filter(function() {
            return rex.test($(this).text());
        }).show();
    });

    $('.input-search-table').on('keyup', function() {
        var rex = new RegExp($(this).val(), 'i');
        var $items = $(this).closest('.modal-body').find('table tbody');
        $items.find('tr').hide();
        $items.find('tr').filter(function() {
            return rex.test($(this).text());
        }).show();
    });


    $('.price-text').on('click', function() {
        var $self = $(this);
        $self.addClass('hidden');
        $self.siblings().removeClass('hidden');
    });

    $('.price_form, .text_form').find('a').on('click', function() {
        $(this).closest('form').addClass('hidden').siblings().removeClass('hidden');
        return false;
    });

    $('#sendquote_form').validate({
        rules: {
            email: {
                email: true,
                required: true
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            element.closest('.form-group').append(error);
        },
        submitHandler: function(form) {
            console.log($(form).serialize());

            $.post(
                base_url + 'formsubmits/send_quote',
                $(form).serialize(),
                function(res) {
                    console.log(res);
                    $(form).closest('.modal').modal('hide');
                    $(form).find('input').val('');
                    window.location.href = base_url + 'dashboard/quotes';
                    // bootbox.alert('<h4>Success! Quote successfully sent.</h4>');
                },
                'json'
            ).error(function(err) {
                console.log(err);
            });
        }

    });

    $('.save-quote-btn').on('click', function() {
        var logged = $(this).data('id');
        console.log(logged);
        if (logged) {
            bootbox.confirm('<h4>Confirm you wish to save quote.</h4>', function(e) {
                if (e) {
                    console.log('yea');
                    $.post(
                        base_url + 'formsubmits/save_quote', {},
                        function(res) {
                            console.log(res);
                            window.location.href = base_url + 'dashboard/quotes';
                            // bootbox.alert('<h4>Success! Quote successfully saved.</h4>');
                        },
                        'json'
                    ).error(function(err) {
                        console.log(err);
                    });


                }
            });
            return false;
        } else {
            $('#quoteSave').modal('show');
            return false;
        }
    });

    $('#contact_form').validate({
        ignore: [],
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-warning');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-warning');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            element.closest('.form-group').append(error);
        },
        submitHandler: function(form) {
            $(form).find('button[type="submit"]').button('loading');

            $.post(
                base_url + 'formsubmits/submit_contact', {
                    data: $(form).serializeArray()
                },
                function(res) {
                    console.log(res);
                    bootbox.alert(res.message, function() {
                        $(form).closest('.modal').modal('hide');
                    });
                    $(form).find('button[type="submit"]').html('Submitted!');
                },
                'json'
            ).error(function(err) {
                console.log(err);
                $(form).find('button[type="submit"]').button('reset');
            });
        }

    });


    $('.text_form').each(function(index, element) {
        $(this).validate({
            rules: {
                text_input: {
                    required: true
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                // element.closest('.form-group').append(error);
            },
            submitHandler: function(form) {

                $(form).find('button').button('loading');
                var newval = $(form).find('input[type="text"]').val();
                console.log($(form).serialize());
                $.post(
                    base_url + 'formsubmits/inline_text_update',
                    $(form).serialize(),
                    function(res) {
                        console.log(res);
                        $(form).addClass('hidden').siblings().removeClass('hidden').html(newval);
                        $(form).find('button').button('reset');
                    },
                    'json'
                ).error(function(err) {
                    console.log(err);
                });


            }
        });
    });

    $('.price_formx').each(function(index, element) {
        $(this).validate({
            rules: {
                'cargo-price': {
                    required: true,
                    number: true
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                // element.closest('.form-group').append(error);
            },
            submitHandler: function(form) {

                $(form).find('button').addClass('disabled');
                var newval = $(form).find('input[type="text"]').val();
                console.log($(form).serialize());
                $.post(
                    base_url + 'formsubmits/cargo_price_agency',
                    $(form).serialize(),
                    function(res) {
                        console.log(res);
                        $(form).addClass('hidden').siblings().removeClass('hidden').html(newval);
                        $(form).find('button').removeClass('disabled');
                    },
                    'json'
                ).error(function(err) {
                    console.log(err);
                });


            }
        });
    });

    $('.form-rate').each(function(index, element) {
        $(this).validate({
            rules: {
                rate: {
                    required: true,
                    number: true
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                element.closest('.form-group').append(error);
            },
            submitHandler: function(form) {
                var $btn = $(form).find('[type="submit"]');
                var type = $(form).find('[name="type"]').val();
                var rate = $(form).find('[name="rate"]').val();
                $btn.button('loading');
                console.log($(form).serialize());

                $.post(
                    base_url + 'formsubmits/rate_submit',
                    $(form).serialize(),
                    function(res) {
                        console.log(res);
                        $btn.button('reset');
                        $('.' + type).html(res.rate);
                        $('.modal').modal('hide');
                    },
                    'json'
                ).error(function(err) {
                    console.log(err);
                });
            }
        });
    });


    //check if user has completed profile
    if (uri_1 == 'dashboard') {
        if (user_name != '' || (user_address == '') || stripe_id == '') {
            //			var text = 'Welcome to Transit Insurance! Please complete your account setup to buy Single Marine Transit Policy.';
            //			bootbox.alert('<h4>'+text+'</h4>', function(){
            //				//window.location.href = base_url+'settings/profile';
            //			});
        }
    }

    //multiple modal
    $(document).on('show.bs.modal', '.modal', function() {
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function() {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 340);
    });


    //open login
    if (uri_1 == '' || location.hash == '#login') {
        $('#loginModal').modal('show');
    }

    if (uri_1 == '' && location.hash == '#forgot') {
        $('#forgotModal').modal('show');
    }

    if (uri_1 == '' && location.hash == '#buyform') {
        var $mdl = $('#buyNowModal');
        //direct to quote tab
        var linksource = $('#login_form').find('[name="from_buyform"]').val();

        if (linksource == 'req_buy_form') {

            //proceed to next tab
            var $tabpane = $('#tab-transit');
            var $progressbar = $('#progressbar');
            var premium = $('#cargotab_form').find('[name="premium"]').val();
            $mdl.find('.well.well-premium h1').html('$' + premium);
            $mdl.find('.show_form_btn').data('premium', premium);

            $tabpane.removeClass('active').next().addClass('active');
            $progressbar.find('.active').removeClass('active').addClass('done-muted done').next().addClass('active');

            var $progressbar2 = $('.lifecycle .list');
            $progressbar2.find('.current').removeClass('current').addClass('complete').next().addClass('current');

            // $mdl.modal('show');
        }
        $mdl.modal('show');
    }


    $('.buy_regnow_btn').on('click', function(e) {
        $('.modal').modal('hide');
        $('.refer_from').val('buy_form');
        $('#signupModal').modal('show');
        e.preventDefault();
    });

    $('.buy_thanks_btn').on('click', function(e) {
        $('.modal').modal('hide');
        var $mdl = $('#buyNowModal');
        //direct to quote tab
        var linksource = $('#login_form').find('[name="from_buyform"]').val();
        if (linksource == 'req_buy_form') {
            //proceed to next tab
            var $tabpane = $('#tab-transit');
            var $progressbar = $('#progressbar');
            var premium = $('#cargotab_form').find('[name="premium"]').val();
            $mdl.find('.well.well-premium h1').html('$' + premium);
            $mdl.find('.show_form_btn').data('premium', premium);

            $tabpane.removeClass('active').next().addClass('active');
            $progressbar.find('.active').removeClass('active').addClass('done-muted done').next().addClass('active');
            var $progressbar2 = $('.lifecycle .list');
            $progressbar2.find('.current').removeClass('current').addClass('complete').next().addClass('current');


        }
        $mdl.modal('show');
        e.preventDefault();
    });

    $('.buy_transit_btn').on('click', function() {
        var $self = $(this);
        console.log($self, 'wwww');

        if (user_id != '') {

            var $mdl = $('#buyNowModal');
            //direct to quote tab
            var linksource = $('#login_form').find('[name="from_buyform"]').val();
            if (linksource == 'req_buy_form') {
                //proceed to next tab
                var $tabpane = $('#tab-transit');
                var $progressbar = $('#progressbar');
                var premium = $('#cargotab_form').find('[name="premium"]').val();
                $mdl.find('.well.well-premium h1').html('$' + premium);
                $mdl.find('.show_form_btn').data('premium', premium);

                $tabpane.removeClass('active').next().addClass('active');
                $progressbar.find('.active').removeClass('active').addClass('done-muted done').next().addClass('active');

                var $progressbar2 = $('.lifecycle .list');
                $progressbar2.find('.current').removeClass('current').addClass('complete').next().addClass('current');

            }

            $mdl.modal('show');

        } else {
            $('#checkSingleModal').modal('show');
        }
    });

    //click signup_btn
    $('.signup_btn').on('click', function() {
        $('.modal.in').modal('hide');

        $('#signupModal').modal('show');
        return false;
    });

    //click login
    $('.login_btn').on('click', function() {
        $('.modal.in').modal('hide');
        var $mdl = $('#loginModal');
        $mdl.find('[name="from_buyform"]').val('');
        $mdl.find('[name="refer_from"]').val('');
        $mdl.modal('show');
        return false;
    });

    //click forgot
    $('.forgot_btn').on('click', function() {
        $('.modal.in').modal('hide');
        $('#forgotModal').modal('show');
        return false;
    });


    //datatables
    var mydataTb = $('.mydataTb').DataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": false,
        "bInfo": true,
        "language": {
            "emptyTable": "No data available"
        }
    });

    //datatables
    var mydataTb2 = $('.mydataTb2').DataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": true
    });

    //prevent add alphabet in number field
    $('input[type="number"]').keyup(function(event) {
        // skip for arrow keys
        if (event.which >= 37 && event.which <= 40) {
            event.preventDefault();
        }

        $(this).val(function(index, value) {
            return value;
        });
    });

    $('.show_form_btn').on('click', function() {
        var $self = $(this);
        var premium = $self.data('premium');
        var buy_inputs = $('#buy_form').serializeArray(); //cargotab_form
        var email = $('#buy_form').find('[name="email"]').val();
        $.post(
            base_url + 'formsubmits/buyform_to_session', {
                buy_inputs: buy_inputs,
                premium: premium
            },
            function(res) {
                console.log(res);
            },
            'json'
        ).error(function(err) {
            console.log(err);
        });

        if (user_name == '') {

            $('.modal').modal('hide');
            $('#signupModal').find('[name="email"]').val(email);
            $('#signupModal').modal('show');
        } else {
            //check if had added stripe
            if (stripe_id != '') {
                bootbox.confirm('<h4>' + action_messages.global.buy_insurance + '</h4>', function(e) {
                    if (e) {
                        $self.button('loading');
                        console.log('eyh');
                        $.post(
                            base_url + 'landing/stripe_paynow', {
                                buy_inputs: buy_inputs,
                                premium: premium
                            },
                            function(res) {
                                console.log(res);
                                $('.modal').modal('hide');
                                setTimeout(function() {
                                    bootbox.alert('<h4>' + action_messages.success.buy_insurance + '</h4>', function() {
                                        window.location.href = base_url + 'dashboard';

                                    });
                                    $self.button('reset');
                                }, 500);

                            },
                            'json'
                        ).error(function(err) {
                            console.log(err);
                        });
                    } //if confirm
                });
            } else {
                bootbox.alert('<h4>' + action_messages.error.buy_insurance + '</h4>', function() {
                    window.location.href = base_url + 'settings/profile/payment';
                });
            }
        }

        //$(this).closest('div').addClass('hidden').siblings('form').removeClass('hidden');
    });


    $('.mycustomer_form').validate({
        //ignore: [],
        rules: {
            mycustomers: {
                required: true
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            element.closest('.form-group').append(error);
        },
        submitHandler: function(form) {
            //$(form).find('button[type="submit"]').button('loading');

            $.post(
                base_url + 'formsubmits/select_consignor',
                $(form).serialize(),
                function(res) {
                    console.log(res);
                    //$(form).find('button[type="submit"]').button('reset');

                    var $single_form = $('#single_purchase_form');
                    var udata = res.single;
                    $single_form.find('[name="first"]').val(udata.first_name);
                    $single_form.find('[name="last"]').val(udata.last_name);
                    $single_form.find('[name="bname"]').val(udata.business_name);
                    $single_form.find('[name="address"]').val(udata.location);
                    $single_form.find('[name="lat"]').val(udata.lat);
                    $single_form.find('[name="lng"]').val(udata.lng);
                    $single_form.find('[name="email"]').val(udata.email);
                    $single_form.find('[name="country_code"]').val(udata.country_id);
                    $single_form.find('.addon-shortcode').html('+' + udata.calling_code);
                    $single_form.find('[name="mobile"]').val(udata.calling_digits);

                    $single_form.find('[name="country_code"]').selectpicker('refresh');

                },
                'json'
            ).error(function(err) {
                console.log(err);
            });


        }
    });


    $('.mycustomer_form').find('select').on('change', function() {
        $(this).closest('form').submit();
    });


    $('#buy_form').validate({
        ignore: [],
        rules: {
            //			shipment_date: {
            //				required: true
            //			},
            //			vessel_name: {
            //				required: true
            //			},
            transitfrom: {
                required: true
            },
            //			portloading: {
            //				required: true
            //			},
            transitto: {
                required: true
            },
            //			portdischarge: {
            //				required: true
            //			},
            currency: {
                required: true
            },
            invoice: {
                required: true,
                number: true
            },

            cargocat: {
                required: true
            },
            goods_desc: {
                required: true
            },
            transmethod: {
                required: true
            },
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            element.closest('.form-group').append(error);
        },
        submitHandler: function(form) {
            //			$(form).find('button[type="submit"]').button('loading');
            var data = $(form).serialize();
            console.log(data);
            //			console.log($(form).find('[name="cargocat"]').length);
            //			if($(form).find('[name="cargocat"]').length == 0){
            //				$(form).find('.fg-cargo-cat').addClass('has-error');
            //				$(form).find('.fg-cargo-cat').append('<span for="cargocat" class="help-block">This field is required.</span>');
            //
            //				return false;
            //			}
            //			else{
            //				$(form).find('.fg-cargo-cat span.help-block').remove();
            //			}

            var $mdl = $(form).closest('.modal');
            var $tabpane = $(form).closest('.tab-pane');
            var $progressbar = $('#progressbar');
            var $progressbar2 = $('.lifecycle .list');
            var $tabcontent = $(form).closest('.tab-content');
            //$tabcontent.find('.well.well-premium h1').html('<small>'+currency+'</small>'+res.premium_format);

            $tabpane.removeClass('active').next().addClass('active');
            $progressbar.find('.active').removeClass('active').addClass('done-muted done').next().addClass('active');
            $progressbar2.find('.current').removeClass('current').addClass('complete').next().addClass('current');

        }
    });


    $('#cargotab_form').validate({
        ignore: [],
        rules: {
            transitfrom: {
                required: true
            },
            transitto: {
                required: true
            },
            currency: {
                required: true
            },
            invoice: {
                required: true,
                number: true
            },

            cargocat: {
                required: true
            },
            goods_desc: {
                required: true
            },
            transmethod: {
                required: true
            },
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            element.closest('.form-group').append(error);
        },
        submitHandler: function(form) {
            $(form).find('button[type="submit"]').button('loading');
            var transitform = '#cargotab_form'; //#buy_form';

            var $mdl = $(form).closest('.modal');
            var $tabpane = $(form).closest('.tab-pane');
            var $progressbar = $('#progressbar');
            var $tabcontent = $(form).closest('.tab-content');

            //from cargo form
            var cargo_price = $(form).find('[name="cargocat"]').find(':selected').data('info');
            cargo_price = cargo_price.split(",");
            var bindoption = cargo_price[2];
            var cargo_max_val = cargo_price[1];
            cargo_price = parseFloat(cargo_price[0]);
            var transmethod = $(form).find('[name="transmethod"]:checked').data('price');
            transmethod = parseFloat(transmethod);

            //from transit form
            var transitfrom = $(transitform).find('[name="transitfrom"]').find(':selected').data('price');
            transitfrom = parseFloat(transitfrom);
            var transitto = $(transitform).find('[name="transitto"]').find(':selected').data('price');
            transitto = parseFloat(transitto);


            var transit_to_refer = $(transitform).find('[name="transitto"]').find(':selected').data('referral');
            //transit_to_refer = parseFloat();
            var transit_from_refer = $(transitform).find('[name="transitfrom"]').find(':selected').data('referral');
            //transit_from_refer = parseFloat(transit_from_refer);

            var currency = $(transitform).find('[name="currency"]').val();
            var invoice = $(transitform).find('[name="invoice"]').val();
            var max_insured = $(transitform).find('[name="invoice"]').data('max');
            max_insured = parseFloat(max_insured);
            invoice = invoice.replace(/,/g, "");
            invoice = parseFloat(invoice);



            //display in quote tab
            var transitfrom_name = $(transitform).find('[name="transitfrom"]').find(':selected').data('text');
            var transitto_name = $(transitform).find('[name="transitto"]').find(':selected').data('text');
            var goods_desc_text = $(form).find('[name="goods_desc"]').val(); //$('.thumbnail-cargo').text(); //
            // var goods_desc_text = goods_desc_text.substring(1);
            var insured_value = $(transitform).find('[name="invoice"]').val();
            var deductible_excess = $('[name="deductible_dd"]').find(':selected').text();

            $('.cargo_description').text(goods_desc_text);
            $('.transit_from').text(transitfrom_name);
            $('.transit_to').text(transitto_name);
            $('.insured_value').text('$' + insured_value);
            $('.excess_value').text(deductible_excess);


            //from cargo form
            var default_ded = $(form).find('[name="default_deductible"]').val();
            default_ded = parseFloat(default_ded);
            var base_rate = $(form).find('[name="base_rate"]').val();

            base_rate = parseFloat(base_rate);
            var base_premium = base_rate * invoice;
            var minimum_premium = $(form).find('[name="minimum_premium"]').val();
            var total = base_premium * cargo_price * transmethod * transitto * transitfrom * default_ded; // * invoice;
            total = Math.round(total * 100) / 100;
            console.log(base_premium, cargo_price, transmethod, transitto, transitfrom, default_ded, base_rate, invoice);
            console.log(total);
            if (total < minimum_premium) {
                total = minimum_premium;
            }

            //			$(form).append('<input type="hidden" name="bindoption" value="'+bindoption+'" />');
            $(form).find('[name="cargo_price"]').val(cargo_price);
            $(form).find('[name="cargo_max_val"]').val(cargo_max_val);
            $(form).find('[name="premium"]').val(total);
            var premium = total;
            var buy_inputs = $(transitform).serializeArray();
            var cargo_inputs = $(form).serializeArray();

            $('[name="deductible_dd"]').val(default_ded);

            $.post(
                base_url + 'formsubmits/buyform_to_session', {
                    buy_inputs: buy_inputs,
                    cargo_inputs: cargo_inputs,
                    premium: premium,
                    cargo_price: cargo_price,
                    currency: currency,
                    selected_deductible: default_ded,
                    max_insured: max_insured
                        //bindoption: '', //bindoption
                },
                function(res) {
                    console.log(res);
                    var linksource = $('#login_form').find('[name="from_buyform"]').val();
                    //check if country needs referral
                    console.log(transit_to_refer, transit_from_refer, cargo_max_val, res.refer);

                    var params = window.location.search.substr(1);
                    var needs_refer = ((transit_to_refer == 'yes' || transit_from_refer == 'yes') || (cargo_max_val != '') || res.refer == true);

                    if (needs_refer && params == '') {
                        // || ((max_insured != 0) && (invoice > max_insured))
                        if (linksource == 'req_buy_form') {
                            //proceed to next tab
                            $tabcontent.find('.well.well-premium h1').html('<small>' + currency + '</small>' + res.premium_format);

                            $tabpane.removeClass('active').next().addClass('active');
                            $progressbar.find('.active').removeClass('active').addClass('done-muted done').next().addClass('active');
                            var $progressbar2 = $('.lifecycle .list');
                            $progressbar2.find('.current').removeClass('current').addClass('complete').next().addClass('current');

                        } else {
                            $mdl.modal('hide');
                            $('#referCountryModal').modal('show');
                        }
                    } else {
                        //proceed to next tab
                        $tabcontent.find('.well.well-premium h1').html('<small>' + currency + '</small>' + res.premium_format);
                        $tabpane.removeClass('active').next().addClass('active');
                        $progressbar.find('.active').removeClass('active').addClass('done-muted done').next().addClass('active');

                        var $progressbar2 = $('.lifecycle .list');
                        $progressbar2.find('.current').removeClass('current').addClass('complete').next().addClass('current');


                    }
                    $(form).find('button[type="submit"]').button('reset');

                },
                'json'
            ).error(function(err) {
                console.log(err);
            });

        }
    });

    $('[name="deductible_dd"]').on('change', function() {
        var $self = $(this);
        var form = '#cargotab_form';
        var transitform = '#cargotab_form'; //#buy_form';

        var newded = $self.val();
        newded = parseFloat(newded);



        var $mdl = $(form).closest('.modal');
        var $tabpane = $(form).closest('.tab-pane');
        var $progressbar = $('#progressbar');
        var $tabcontent = $(form).closest('.tab-content');

        //from cargo form
        var cargo_price = $(form).find('[name="cargocat"]').find(':selected').data('info');
        cargo_price = cargo_price.split(",");
        var bindoption = cargo_price[2];
        var cargo_max_val = cargo_price[1];
        cargo_price = parseFloat(cargo_price[0]);
        var transmethod = $(form).find('[name="transmethod"]:checked').data('price');
        transmethod = parseFloat(transmethod);

        //from transit form
        var transitfrom = $(transitform).find('[name="transitfrom"]').find(':selected').data('price');
        transitfrom = parseFloat(transitfrom);
        var transitto = $(transitform).find('[name="transitto"]').find(':selected').data('price');
        transitto = parseFloat(transitto);

        var transit_to_refer = $(transitform).find('[name="transitto"]').find(':selected').data('referral');
        transit_to_refer = parseFloat(transit_to_refer);
        var transit_from_refer = $(transitform).find('[name="transitfrom"]').find(':selected').data('referral');
        transit_from_refer = parseFloat(transit_from_refer);

        var currency = $(transitform).find('[name="currency"]').val();
        var invoice = $(transitform).find('[name="invoice"]').val();
        invoice = invoice.replace(/,/g, "");
        invoice = parseFloat(invoice);

        //from cargo form
        var default_ded = $(form).find('[name="default_deductible"]').val();
        default_ded = parseFloat(default_ded);
        var base_rate = $(form).find('[name="base_rate"]').val();

        base_rate = parseFloat(base_rate);
        var base_premium = base_rate * invoice;
        var minimum_premium = $(form).find('[name="minimum_premium"]').val();
        var total = base_premium * cargo_price * transmethod * transitto * transitfrom * newded; //default_ded; // * invoice;
        total = Math.round(total * 100) / 100;
        console.log(base_premium, cargo_price, transmethod, transitto, transitfrom, newded, base_rate, invoice);
        console.log(total);
        if (total < minimum_premium) {
            total = minimum_premium;
        }


        $(form).find('[name="premium"]').val(total);
        $(form).find('[name="default_deductible"]').val(newded);
        var buy_inputs = $(transitform).serializeArray();
        var cargo_inputs = $(form).serializeArray();
        $.post(
            base_url + 'formsubmits/buyform_to_session', {
                buy_inputs: buy_inputs,
                cargo_inputs: cargo_inputs,
                premium: total,
                cargo_price: cargo_price,
                currency: currency,
                selected_deductible: newded
            },
            function(res) {
                console.log(res);
                var $mdl = $(form).closest('.modal');
                var $tabpane = $(form).closest('.tab-pane');
                var $progressbar = $('#progressbar');
                var $tabcontent = $(form).closest('.tab-content');
                $tabcontent.find('.well.well-premium h1').html('<small>' + currency + '</small>' + res.premium_format);

            },
            'json'
        ).error(function(err) {
            console.log(err);
        });

    });

    $('.select_port_btn').on('click', function() {
        var $self = $(this);
        var type = $self.data('type');
        var selected = $('#' + type).val();
        var $mdl = $('#portModal');
        var $country = $('[name="transitfrom"]');
        if (type != 'portloading') {
            $country = $('[name="transitto"]');
        }
        var iso = $country.find(':selected').data('iso');
        $mdl.find('.genre_list').html('<p class="text-muted text-center" style="margin-top: 50px;"><i class="fa fa-spinner fa-spin"></i></p>');
        $.post(
            base_url + 'landing/getports', {
                iso: iso,
                selected: selected
            },
            function(res) {
                $mdl.find('.genre_list').html(res);
                $mdl.find('[name="type"]').val(type);
                console.log(res);
            }
        ).error(function(err) {
            console.log(err);
        });

        $mdl.modal('show');
        return false;
    });

    $('.clickable-cargo-table tbody').find('tr').on('click', function() {
        var $self = $(this);
        var id = $self.data('value');
        var price = $self.data('value');
        var refer = $self.data('max');
        var uninsurable = $self.data('bindoption');
        var desc = $self.data('text');
        var $mdl = $self.closest('.modal');
        var $mdl2 = $('#buyNowModal');

        $mdl.modal('hide');

        $('[name="cargocat"]').val(id);
        $('[name="cargocat"]').selectpicker('refresh');

        //add to well<i class="fa fa-times close-cargo pull-right"></i> title="'+desc+'"
        $('.genre_well').html('');
        var text = '<div class="thumbnail thumbnail-cargo" data-id="' + id + '"><button type="button" aria-label="Close" class="close close-cargo"><span aria-hidden="true">&times;</span></button>' + desc + '</div> ';
        $('.genre_well').html(text).show();

        $self.addClass('active').siblings().removeClass('active');
    });

    $('.custom_port_btn').on('click', function() {
        var $self = $(this);
        var $mdl = $self.closest('.modal');
        var type = $mdl.find('[name="type"]').val();
        var custom_val = $self.closest('div').find('input').val();

        $('#' + type).val(custom_val);
        $('#' + type).siblings('a').hide();
        $('#' + type).siblings('.btn-group').show().children().first().html(custom_val);
        $mdl.modal('hide');
        $self.closest('div').find('input').val('');
    });

    $('[name="transitfrom"]').on('change', function() {
        var $self = $(this);
        var $port_btn = $('[data-type="portloading"]');
        $port_btn.siblings('input').val('');
        $port_btn.show().siblings('.btn-group').hide();

        if ($self.val() != '') {
            $port_btn.removeClass('disabled');

        } else {
            $port_btn.addClass('disabled');
        }


    });


    $('[name="transitto"]').on('change', function() {
        var $self = $(this);
        var $port_btn = $('[data-type="portdischarge"]');
        $port_btn.siblings('input').val('');
        $port_btn.show().siblings('.btn-group').hide();

        if ($self.val() != '') {
            $port_btn.removeClass('disabled');

        } else {
            $port_btn.addClass('disabled');
        }

    });


    //initialize programmed select picker
    $('select').selectpicker();

    if ($('.bootstrap-select .bs-searchbox').length > 0) {

        $('.bootstrap-select .bs-searchbox').addClass('inner-addon right-addon');
        $('.bootstrap-select .bs-searchbox').prepend('<i class="glyphicon glyphicon-search"></i>');
    }

    $('.single_purchase_form').each(function(index, element) {
        $(this).validate({
            ignore: [],
            rules: {
                // first: {
                // 	required: true
                // },
                // last: {
                // 	required: true
                // },
                // bname: {
                // 	required: true
                // },
                address: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                element.closest('.form-group').append(error);
            },
            submitHandler: function(form) {
                //check either name bname only
                var $name = $(form).find('[name="first"]');
                var $bname = $(form).find('[name="bname"]');
                var name = $(form).find('[name="first"]').val()+' '+$(form).find('[name="last"]').val();
                var fname = $(form).find('[name="first"]').val();
                var lname = $(form).find('[name="last"]').val();
                var bname = $(form).find('[name="bname"]').val();

                $name.closest('form').find('.name-biz-error').addClass('hidden');

                if ((fname == '' || lname == '') && bname == '') {
                    $name.closest('form').find('.name-biz-error').removeClass('hidden');

                    // bootbox.alert('Name and Business Name cannot be empty at once.');
                    $(form).find('button[type="submit"]').button('reset');
                    return false;
                }

                var data = {
                    ccode: $(form).find('[name="country_code"]').find(':selected').data('code'),
                    inputs: $(form).serializeArray(),
                    crefer: ''
                };

                data.info = $(form).find('[name="type"]').val();

                //check if needs refer
                var params = window.location.search.substr(1);
                if ($(form).find('[name="refer_country"]').length > 0 && params == '') {
                    $(form).find('button[type="submit"]').button('loading');


                    data.crefer = 'yes';
                    data = $(form).serialize() + '&' + $.param(data);
                    $.post(
                        base_url + 'formsubmits/single_purch_form',
                        data,
                        function(res) {
                            console.log(res);
                            window.location.href = base_url + 'dashboard/quotes';
                            // $('.modal').modal('hide');
                            // bootbox.alert('<h4 class="modal-title">' + action_messages.success.referral_request + '</h4>');
                            // $(form).find('button[type="submit"]').button('reset');
                        },
                        'json'
                    ).error(function(err) {
                        console.log(err);
                    });

                    return false;
                }

                var actiontype = data.info;
                data = $(form).serialize() + '&' + $.param(data);
                $(form).find('button[type="submit"]').button('loading');

                if (actiontype == 'consignee') {
                    //set loading button
                    $('.policy_btn').button('loading');

                    //var cargo_details_form input
                    $cd_form = $('#cargo_details_form');

                    var cddata = {};
                    cddata.shipment_date = $cd_form.find('[name="shipment_date"]').val();
                    cddata.vessel_name = $cd_form.find('[name="vessel_name"]').val();
                    cddata.portloading = $cd_form.find('[name="portloading"]').val();
                    cddata.portdischarge = $cd_form.find('[name="portdischarge"]').val();

                    data = data + '&' + $.param(cddata);
                    console.log(data, 'policy_details');
                }

                $.post(
                    base_url + 'formsubmits/single_purch_form',
                    data,
                    function(res) {
                        console.log(res);

                        $(form).find('button[type="submit"]').button('reset');

                        if(res.result == 'session_expired'){
                            window.location.href = base_url;
                        }

                        //check if agent_customer is added
                        if (actiontype == 'agent_customer') {
                            window.location.reload(true);
                        } else if (actiontype == 'consignee') {
                            var $mdl = $(form).closest('.modal');
                            var $tabcontent = $(form).closest('.tab-content-buy');
                            var $tabpane = $(form).closest('.tab-pane');
                            var $progressbar = $('#progressbar');

                            if(res.booking_id){
                                //set download link for cert_currency_btn
                                $('.cert_currency_btn').attr('href', base_url+'landing/certificate/'+res.booking_id);
                            }

                            $(window).scrollTop(0);

                            $tabcontent.find('.confirm-holder').html('<p class="text-center text-muted"><i class="fa fa-spinner fa-spin fa-2x"></i><br>Loading..</p>');
                            $tabcontent.find('.confirm-holder').load(base_url + 'landing/confirmbuy_info');

                            $tabpane.removeClass('active').next().addClass('active');
                            $progressbar.find('.active').removeClass('active').addClass('done-muted done').next().addClass('active');

                            var $progressbar2 = $('.lifecycle .list');
                            $progressbar2.find('.current').removeClass('current').addClass('complete').next().addClass('current');

                            //reset policy btn
                            $('.policy_btn').button('reset');
                        } else {
                            //detect consignee form is validated
                            var $consignee = $('#consignee_form input:lt(7)');
                            var nee_count = 0;
                            $consignee.each(function(index, element) {
                                if ($(this).val() == '') {
                                    nee_count++;
                                }
                            });

                            if (nee_count > 0) {
                                $('#consignee_form').submit();
                                return false;
                            }

                        }


                    },
                    'json'
                ).error(function(err) {
                    console.log(err);
                });
            }
        });
    }); //each .single_purchase_form

    $('#tz_form').validate({
        ignore: [],
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            element.closest('.form-group').append(error);
        },
        submitHandler: function(form) {
            $(form).find('button[type="submit"]').button('loading');

            $.post(
                base_url + 'formsubmits/save_tz',
                $(form).serialize(),
                function(res) {
                    console.log(res);
                    $(form).find('button[type="submit"]').html('Saved');

                    setTimeout(function(){
                        $(form).find('button[type="submit"]').button('reset');
                    },2500);
                },
                'json'
            ).error(function(err) {
                console.log(err);
            });
        }
    });

    $('#cargo_details_form').validate({
        ignore: [],
        rules: {
            shipment_date: {
                required: true
            },
            portloading: {
                required: true
            },
            portdischarge: {
                required: true
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            element.closest('.form-group').append(error);
        },
        submitHandler: function(form) {
            $.post(
                base_url + 'formsubmits/cargo_details_form', {
                    data: $(form).serializeArray(),
                    cargo: $("#cargotab_form").serializeArray()
                },
                function(res) {
                    console.log(res);
                },
                'json'
            ).error(function(err) {
                console.log(err);
            });
        }
    });

    //submit merge form for consignor and consignee
    $('.policy_btn').on('click', function() {
        var $self = $(this);
        var $cargo_details = $('#cargo_details_form input.form-control:lt(4)');
        var $consignor = $('#single_purchase_form input.form-control:lt(7)');
        var $consignee = $('#consignee_form input.form-control:lt(7)');

        var nor_count = 0;
        $cargo_details.each(function(index, element) {
            if ($(this).val() == '') {
                nor_count++;
            }
        });
        $('#cargo_details_form').submit();

        $consignor.each(function(index, element) {
            if ($(this).val() == '') {
                nor_count++;
            }
        });
        $('#single_purchase_form').submit();

        console.log(nor_count);

        setTimeout(function(){
            $('#consignee_form').submit();
        }, 1000);
        
        // if (nor_count > 1) {
        //     return false;
        // }

    });

    $('.save_quote_transaction_btn').on('click', function() {
        var $self = $(this);
        $self.button('loading');

        setTimeout(function(){
            $self.html('<i class="fa fa-check-circle"></i> Success!');
            window.location.href = base_url + 'dashboard/transactions?new=yes';
        }, 1500);

        return false;

        // $.post(
        //     base_url + 'formsubmits/save_transaction', {},
        //     function(res) {
        //         console.log(res);
        //         $self.html('<i class="fa fa-check-circle"></i> Success!');
                
        //         window.location.href = base_url + 'dashboard/transactions?new=yes';

        //         // bootbox.alert('<h4 class="modal-title"><i class="fa fa-check-circle text-success"></i> Quote saved successfully.</h4>', function() {
        //         //     window.location.href = base_url + 'dashboard/transactions';
        //         // });
        //     }
        // ).error(function(err) {
        //     console.log(err);
        // });

    });

    $('[name="agent_code"]').on('change', function() {
        var $self = $(this);
        $('.validate-agentcode-btn').click();
        console.log($self.val());
    });

    $('.validate-result-text').on('click', function() {
        $self = $(this);
        if ($self.hasClass('valid')) {
            $self.addClass('disabled btn-success').removeClass('btn-link').html('Confirmed!');
        }
    });

    $('.validate-agentcode-btn').on('click', function() {
        var $self = $(this);
        var code = $('[name="agent_code"]').val();
        var $txt = $('.validate-result-text');
        $txt.html('');
        $self.button('loading');
        $.post(
            base_url + 'formsubmits/validate_agentcode', {
                code: code
            },
            function(res) {
                console.log(res);
                if (res.result == 'ok') {
                    $self.html('<i class="fa fa-check-circle"></i> Success!');
                    $txt.addClass('valid').html('<i class="fa fa-check-circle text-success"></i> ' + res.name + ' - Click to confirm');
                    //bootbox.alert('<h4 class="modal-title"><i class="fa fa-check-circle text-success"></i> Agent code valid.</h4>');
                } else {
                    $self.button('reset');
                    $txt.html('Agent code invalid. Please try again.');
                    $txt.removeClass('disabled btn-success valid').addClass('btn-link');
                }
            },
            'json'
        ).error(function(err) {
            console.log(err);
            $self.button('reset');
        });


    });

    $('.agent_save_btn').on('click', function() {
        var $self = $(this);
        $self.button('loading');


        $.post(
            base_url + 'settings/save_quote', {},
            function(res) {
                console.log(res);
                $self.html('<i class="fa fa-check-circle"></i> Success!');
                bootbox.alert('<h4 class="modal-title"><i class="fa fa-check-circle text-success"></i> Quote emailed to consignor.</h4>', function() {
                    window.location.href = base_url + 'dashboard/quotes';
                });
            }
        ).error(function(err) {
            console.log(err);
        });
    });

    $(document).on('click', '.confirmbuy_update_btn', function() {
        var $self = $(this);
        var target = $self.data('target');
        var progress = $self.data('progress');

        $('#' + target).addClass('active').siblings().removeClass('active');
        $('ul.progressbar-main li:eq(' + progress + ')').removeClass('done').addClass('active').siblings().removeClass('active');
        $('ul.progressbar-main li:eq(' + progress + ')').nextAll().removeClass('done');
        console.log(progress);
        return false;
    });

    $('.a_consignee').on('click', function() {
        var type = $(this).data('type');
        var address = $('#single_purchase_form').find('[name="address"]').val();

        $('#consignee_form').find('input[type="text"], select').val('');

        $(this).closest('div').fadeOut('fast', function() {
            $('#consignee_form').removeClass('hidden');
        });

        if (type != 'yes') {
            address = '';
        }

        $('#consignee_form').find('[name="address"]').val(address);
        return false;
    });

    $('.quote_continue_btn').on('click', function() {
        var $self = $(this);
        var logged_id = $self.data('id');
        var $progressbar = $('#progressbar');
        var $tabpane = $self.closest('.tab-pane');
        var $mdl = $self.closest('.modal');
        $tabpane.removeClass('active').next().addClass('active');
        $progressbar.find('.active').removeClass('active').addClass('done-muted done').next().addClass('active');
        var $progressbar2 = $('.lifecycle .list');
        $progressbar2.find('.current').removeClass('current').addClass('complete').next().addClass('current');


    });

    $('.buy_back_btn').on('click', function(e) {
        var $self = $(this);
        var $progressbar = $('#progressbar');
        var $tabpane = $self.closest('.tab-pane');
        var $mdl = $self.closest('.modal');
        $tabpane.removeClass('active').prev().addClass('active');
        $progressbar.find('.active').removeClass('active done').prev().removeClass('done').addClass('active');

        var $progressbar2 = $('.lifecycle .list');
        $progressbar2.find('.current').removeClass('current complete').prev().removeClass('complete').addClass('current');

        e.preventDefault();
    });

    //Form forgot password validate
    $('#forgot_form').validate({
        rules: {
            email: {
                required: true,
                email: true
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            element.closest('.form-group').append(error);
        },
        submitHandler: function(form) {
            var data = $(form).serialize();
            console.log(data);
            submitForm('forgotpass', data, form);
        }
    });

    $('#customer_password_form').validate({
        ignore: [],
        rules: {
            pw: {
                required: true,
                minlength: 6
            },
            pw2: {
                required: true,
                equalTo: '#pw'
            },
            terms: {
                required: true,
            },
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            element.closest('.form-group').append(error);
        },
        submitHandler: function(form) {
            var data = $(form).serialize();
            console.log(data);
            submitForm('customer_update_password', data, form);
        }

    });


    $('#agency_password_form').validate({
        rules: {
            pw: {
                required: true,
                minlength: 6
            },
            pw2: {
                required: true,
                equalTo: '#pw'
            },
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            element.closest('.form-group').append(error);
        },
        submitHandler: function(form) {
            var data = $(form).serialize();
            console.log(data);
            submitForm('agency_update_password', data, form);
        }

    });


    $('#student_profile_form').validate({
        rules: {
            inst_first_name: {
                required: true
            },
            inst_last_name: {
                required: true
            },
            inst_email: {
                required: true,
                email: true
            },
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            element.closest('.form-group').append(error);
        },
        submitHandler: function(form) {
            var data = $(form).serialize();
            console.log(data);
            submitForm('update_student', data, form);
        }
    });

    $('#customer_profile_form').validate({
        rules: {
            inst_first_name: {
                required: true
            },
            inst_last_name: {
                required: true
            },
            inst_email: {
                required: true,
                email: true
            },
            address: {
                required: true
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            element.closest('.form-group').append(error);
        },
        submitHandler: function(form) {
            var calling_code = $(form).find('[name="country_code"]').find(':selected').attr('data-code');
            var data = {
                calling_code: calling_code
            };

            data = $(form).serialize() + '&' + $.param(data);
            console.log(calling_code);
            console.log(data);
            submitForm('update_customer', data, form);
        }
    });
    $('#agency_profile_form').validate({
        rules: {
            studio_name: {
                required: true
            },
            studio_email: {
                required: true,
                email: true
            },
            address: {
                required: true
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            element.closest('.form-group').append(error);
        },
        submitHandler: function(form) {
            var calling_code = $(form).find('[name="country_code"]').find(':selected').attr('data-code');
            var data = {
                calling_code: calling_code
            };

            data = $(form).serialize() + '&' + $.param(data);
            console.log(calling_code);
            console.log(data);
            submitForm('update_agency', data, form);
        }

    });

    $('#request_access_form').validate({
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            element.closest('.form-group').append(error);
        },
        submitHandler: function(form) {

            data = $(form).serialize(); // + '&' + $.param(data);
            console.log(data);
            $.post(
                base_url + 'formsubmits/request_access_form',
                data,
                function(res) {
                    console.log(res);
                    $('.modal').modal('hide');
                    bootbox.alert('<h4 class="modal-title">' + 'Request access successfully sent.' + '</h4>');
                    $(form).find('button[type="submit"]').button('reset');
                },
                'json'
            ).error(function(err) {
                console.log(err);
            });        }

    });

    $('#signup_form').validate({
        rules: {
            first_name: {
                required: true
            },
            last_name: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            email2: {
                required: true,
                equalTo: "#signup_email"
            },
            password: {
                required: true,
                minlength: 6
            },
            password2: {
                required: true,
                equalTo: "#signup_password"
            },
            terms: {
                required: true,
            },
        },
        messages: {
            email2: {
                equalTo: "Email does not match"
            },
            password2: {
                equalTo: "Password does not match"
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            element.closest('.form-group').append(error);
        },
        submitHandler: function(form) {
            var data = $(form).serialize();
            //console.log(data);
            submitForm('signup', data, form);
        }

    });

    //	$('.dropdown-form').find('a').on('click', function(){
    //		var $self = $(this);
    //		var text = $self.data('type');
    //		$self.closest('.dropdown').find('.u_type').html(' - '+text);
    //		$self.closest('li').addClass('active').siblings().removeClass('active');
    //		$self.closest('.form-group').find('input').val(text);
    //
    //	});

    $('.dropdown-login').find('a').on('click', function() {
        var $self = $(this);
        var $self2 = $('.dropdown-login');
        var text = $self.data('type');
        $self2.closest('.dropdown').find('.u_type').html(' - ' + text);
        $self2.find('a[data-type="' + text + '"]').closest('li').addClass('active').siblings().removeClass('active');
        $self2.closest('.form-group').find('input').val(text);

    });

    $('.dropdown-signup').find('a').on('click', function() {
        var $self = $(this);
        var $self2 = $('.dropdown-signup');
        var text = $self.data('type');

        if (text == 'Studio') {
            $('.studio_only').show();
            $('.notfor_studio').hide();
            $('.notfor_studio').find('input').val('a');
            $('.studio_only').find('input').val('');
        } else {
            $('.studio_only').hide();
            $('.notfor_studio').show();
            $('.notfor_studio').find('input').val('');
            $('.studio_only').find('input').val('a');
        }


        $self2.closest('.dropdown').find('.u_type').html(' - ' + text);
        $self2.find('a[data-type="' + text + '"]').closest('li').addClass('active').siblings().removeClass('active');
        $self2.closest('.form-group').find('input').val(text);

    });




    $('#login_form').validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true
            },
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            element.closest('.form-group').append(error);
        },
        submitHandler: function(form) {
            var data = $(form).serialize();
            console.log(data);
            submitForm('login', data, form);
        }


    });


    //remove multiple scroll for modal
    $(document).on('hidden.bs.modal', '.modal', function() {
        if ($('.modal:visible').length > 0) {
            setTimeout(function() {
                $('body').addClass('modal-open');
            }, 330);
        }
    });


    //open modal to add record
    $('.openmodal_add_btn').on('click', function() {
        var $self = $(this);
        var mdl = $self.data('target');

        $(mdl).find('input[type="text"]').val('');
        $(mdl).find('input[type="number"]').val('');
        $(mdl).find('select').val('');
        $(mdl).find('input[type="checkbox"]').prop('checked', false);
        $(mdl).find('select').selectpicker('refresh');
        $(mdl).find('.addon-shortcode').html('+');

        $(mdl).modal('show');
        return false;
    });

    $(document).on('click', '.update_btn', function(e) {
        var $self = $(this);
        var data = $self.data();
        var mdl = data.target;
        console.log(data);

        var keys = [];
        for (var k in data) {
            keys.push(k);
        }
        console.log(keys);
        for (var i = 0; i < keys.length; i++) {
            console.log(keys[i], data[keys[i]]);
            $(mdl).find('.' + keys[i]).val(data[keys[i]]);
        }
        $(mdl).find('.addon-shortcode').html('+' + data.calling_code);
        $(mdl).find('select').selectpicker('refresh');


        $(mdl).modal('show');
        e.preventDefault();
    });

    //confirm message to delete record
    $(document).on('click', '.delete_btn', function(e) {
        var $self = $(this);
        var id = $self.data('id');
        var table = $self.data('table');

        bootbox.confirm('<h4>' + action_messages.global.generic_delete_message + '</h4>', function(e) {
            if (e) {
                //alert(id);
                deleteRecord($self, id, table);
            }
        });
        e.preventDefault();
    });



    //add mouse hover to dropdown
    $('.dropdown.my-dropdown').hover(function() {
        $(this).addClass('open');
    }, function() {
        $(this).removeClass('open');
    });

    //select country code
    $(document).on('change', '[name="country_code"]', function() {
        var ccode = $(this).find(':selected').attr('data-code');
        var country_id = $(this).val();
        $('.addon-shortcode').html('+' + ccode);
        $('[name="country_code"]').val(country_id);
        console.log('arr');
    });

    $('[name="country_referral[]"]').on('change', function() {
        var $self = $(this);
        var country_id = $self.val();
        var data = {
            country_id: country_id,
            type: 'unset'
        };
        if ($self.is(':checked')) {
            data.type = 'set';
        }

        $.post(
            base_url + 'webmanager/cargo/country_referral',
            data,
            function(res) {
                console.log(res);
            },
            'json'
        ).error(function(err) {
            console.log(err);
        });

    });

    $(document).on('change', '[name="cargo_referral[]"]', function() {
        var $self = $(this);
        var id = $self.val();
        var data = {
            id: id,
            type: ''
        };
        if ($self.is(':checked')) {
            data.type = 'Yes';
        }

        $.post(
            base_url + 'webmanager/cargo/cargo_referral',
            data,
            function(res) {
                console.log(res);
            },
            'json'
        ).error(function(err) {
            console.log(err);
        });

    });




    if (uri_1 == 'search') {
        var width = $(window).width();
        if (width >= 993) {
            $('body').css('overflow', 'hidden');
            $('body').css('scroll-y', 'hidden');
        }

        $(window).resize(function() {
            if ($(window).width() < 993) {

                $('body').css('overflow', 'auto');
                $('body').css('scroll-y', 'auto');
            } else {
                $('body').css('overflow', 'hidden');
                $('body').css('scroll-y', 'hidden');
            }
        });
    }


    //user_geolocation ,"form#customer_profile_form"
    //$(document).on('focusin', '.user_geolocation', function(){
    $('.user_geolocation').geocomplete({
            details: "ul.map-details",
            detailsAttribute: "data-geo"

        })
        .bind("geocode:result", function(event, result) {

            var $form = $(this).closest('form'); //('#agency_profile_form');

            var comp_length = result.address_components.length - 1;
            var country_short = $('[data-geo="country_short"]').text(); //result.address_components[comp_length].short_name; //$form.find('[name="country_short"]').val(
            var location_lat = result.geometry.location.lat();
            var location_long = result.geometry.location.lng();

            $form.find('[name="lat"]').val(location_lat);
            $form.find('[name="long"]').val(location_long);
            $form.find('[name="lng"]').val(location_long);

            console.log(result, result.address_components, country_short);
            getCountryCalling(country_short, $form);

        });

    $('.instructor_geolocation').geocomplete({
            details: "form#customer_profile_form"
        })
        .bind("geocode:result", function(event, result) {

            var $form = $('#customer_profile_form');

            var country_short = $form.find('[name="country_short"]').val();

            var location_lat = result.geometry.location.lat();
            var location_long = result.geometry.location.lng();

            $(this).siblings('[name="lat"]').val(location_lat);
            $(this).siblings('[name="long"]').val(location_long);

            console.log(result);
            getCountryCalling(country_short, $form);

        });


    //});


    //geocomplete
    $('.geocomplete').geocomplete()
        .bind("geocode:result", function(event, result) {
            var location_lat = result.geometry.location.lat();
            var location_long = result.geometry.location.lng();
            $(".geocomplete").siblings('[name="lat"]').val(location_lat);
            $(".geocomplete").siblings('[name="long"]').val(location_long);
        });

    //geocomplete
    $('.input_location').geocomplete()
        .bind("geocode:result", function(event, result) {

            var $self = $(this); // = result.geometry.location.lat();
            var location_lat = result.geometry.location.lat();
            var location_long = result.geometry.location.lng();
            $self.siblings('[name="lat"]').val(location_lat);
            $self.siblings('[name="long"]').val(location_long);
        });


    setTimeout(function() {
        //trigger location to display map onload
        $('.request_quote_location').trigger("geocode");
    }, 2);


    $('[name="searchtype"]').bootstrapSwitch({
        onText: 'Studio',
        offText: 'Class',
        onColor: 'info',
        offColor: 'info',
        labelText: '<i class="fa fa-search"></i>',
        labelWidth: 15,
        onSwitchChange: function(event, state) {
            show_map();
            //			if(state){
            //				$('.more_filter').find('.form-group').slideDown('fast');
            //			}
            //			else{
            //				$('.more_filter').find('.form-group').slideUp('fast');
            //			}
        }
    });


    //submit form on click red-btn for searching class
    $('#search_class_form').find('a').on('click', function() {
        //form submit to validate if empty
        if ($(this).closest('form').find('input').val() == '') {
            $(this).closest('form').submit();
        } else {
            window.location.href = base_url + 'buy'; //?' + $(this).closest('form').serialize();
        }
        return false;
    });

    $('#search_class_form').validate({
        rules: {
            location: {
                required: true
            },
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            //element.closest('.form-group').append(error);
        },
        submitHandler: function(form) {
            window.location.href = base_url + 'buy'; //?searchtype=studio&' + $(form).serialize();
        }

    });


    //maps and autocomplete address
    $(".request_quote_location").geocomplete()
        .bind("geocode:result", function(event, result) {
            //if(window.location.hash == false){
            //}
            $('.panel-map').css('margin-top', '0px');
            $('.tasks_holder').html('');



            var the_loc = $(".request_quote_location").val();


            var stateObj = {};

            window.history.pushState(stateObj, '', 'search?' + $('#request_form').serialize());

            //console.log(result.address_components[2].short_name);
            console.log(result.geometry.location);
            var location_lat = result.geometry.location.lat();
            var location_long = result.geometry.location.lng();
            $(".request_quote_location").siblings('[name="lat"]').val(location_lat);
            $(".request_quote_location").siblings('[name="long"]').val(location_long);
            //working maps sample
            var geocoder;
            var map;
            var places;
            var markers = [];


            var infowindow = new google.maps.InfoWindow({
                content: '',
                maxWidth: 400,
                minWidth: 300
            });
            // fetch Places JSON from /data/places
            // loop through and populate the map with markers
            var fetchPlaces = function() {

                var the_data = {
                    //				"the_loc": the_loc,
                    "location_lat": location_lat,
                    "location_long": location_long
                };

                the_data = $('#request_form').serialize() + "&" + $.param(the_data);

                //check type of search to toggle filters
                if ($('[name="searchtype"]').is(':checked')) {
                    $('.more_filter .form-group').slideUp('fast');
                } else {
                    $('.more_filter .form-group').slideDown('fast');
                }
                console.log(the_data);
                var result_table = $('.space_datatable').DataTable();

                $('.space_datatable').hide().closest('#DataTables_Table_0_wrapper').hide();
                $('.space_results').show().html('<p class="lead text-muted text-center" style="padding-top: 150px;"><i class="fa fa-spinner fa-spin"></i><br />Loading...</p>');

                $.ajax({
                    type: "POST",
                    url: base_url + 'landing/search_list',
                    dataType: 'json',
                    data: the_data,
                    success: function(response) {
                        console.log(response);
                        if (response.status == 'OK') {

                            places = response.places;
                            var space_html = '';


                            // loop through places and add markers
                            for (p in places) {

                                var btn_data = 'data-id="' + places[p].id + '" data-studio_id="' + places[p].studio_id + '" data-type="' + response.searchtype + '"';

                                var action_btn_class = 'view_studio_profile_btn';
                                var action_btn_text = 'View studio\'s full profile';
                                //toggle view for class and studio
                                if (response.searchtype == 'class') {

                                    action_btn_class = 'req_book_btn';
                                    action_btn_text = 'Book Now';

                                    space_html += '<tr ' + btn_data + ' class="req_book_btnx">';
                                    space_html += '<td class="hidden">';
                                    space_html += places[p].distance;
                                    space_html += '</td>';

                                    space_html += '<td>';
                                    space_html += '<div class="row md-gutter">';
                                    space_html += '  <div class="col-sm-5">';


                                    space_html += '<div class="fb-profile img-thumbnail">';
                                    space_html += '    <img align="left" class="fb-image-lg" src="' + places[p].image + '" alt="studio" />';
                                    space_html += '    <img align="left" class="fb-image-profile img-circle" src="' + places[p].seeker_img + '" alt="User image example"/>';
                                    space_html += '    <div class="fb-profile-text">';
                                    space_html += '       <h1>' + places[p].name + '</h1>';
                                    space_html += '        <p>' + places[p].studio_name + '</p>';
                                    space_html += '    </div>';
                                    space_html += '</div>';


                                    space_html += '  </div>';
                                    space_html += '  <div class="col-sm-7">';
                                    space_html += '      <div class="caption req_details">';
                                    space_html += '        <h4 class="text-info">Details</h4>';
                                    space_html += '        <p>Location: ' + places[p].geo_name + '</p>';

                                    //								space_html += '        <p>Schedule: ';
                                    //								if(places[p].schedule.length > 0){
                                    //									for(var i = 0; i < places[p].schedule.length; i++){
                                    //										space_html += places[p].schedule[i]+' <small>('+places[p].sched_start[i]+' to '+places[p].sched_end[i]+')</small>';
                                    //										if((i+1) != places[p].schedule.length){
                                    //											space_html += ', ';
                                    //										}
                                    //									}
                                    //								}
                                    //								space_html += '</p>';

                                    space_html += '        <p>Genre(s): ';
                                    if (places[p].genres_name.length > 0) {
                                        for (var i = 0; i < places[p].genres_name.length; i++) {
                                            space_html += places[p].genres_name[i];
                                            if ((i + 1) != places[p].genres_name.length) {
                                                space_html += ', ';
                                            }
                                        }
                                    }
                                    space_html += '</p>';

                                    space_html += '        <p>Instructor(s): ';
                                    if (places[p].instructors_name.length > 0) {
                                        for (var i = 0; i < places[p].instructors_name.length; i++) {
                                            var inst_id = places[p].instructors_id;
                                            space_html += '<a href="#" class="view_inst_btn" data-id="' + inst_id[i] + '">' + places[p].instructors_name[i] + '</a>';
                                            if ((i + 1) != places[p].instructors_name.length) {
                                                space_html += ', ';
                                            }
                                        }
                                    } else {
                                        space_html += 'No instructors listed.';
                                    }
                                    space_html += '</p>';

                                    space_html += '        <p>Cost: $' + places[p].cost + '</p>';
                                    space_html += '        <p>Level: ' + places[p].level + '</p>';
                                    space_html += '        <p>Distance: ' + places[p].distance + ' km.</p>';


                                    space_html += '        <p><a href="#" class="btn btn-primary ' + action_btn_class + ' btn-sm ' + places[p].button_class + '" ' + btn_data + ' role="button"><i class="fa fa-file"></i> ' + action_btn_text + '</a></p>'; //'+places[p].button_text+'


                                    space_html += '      </div>';
                                    space_html += '  </div>';
                                    space_html += '</div>';

                                    space_html += '</td>';
                                    space_html += '</tr>';

                                }
                                //view for studio
                                else {
                                    space_html += '<tr ' + btn_data + ' class="req_book_btnx">';
                                    space_html += '<td class="hidden">';
                                    space_html += places[p].distance;
                                    space_html += '</td>';

                                    space_html += '<td>';
                                    space_html += '<div class="row">';
                                    space_html += '  <div class="col-sm-4 md-gutter">';


                                    space_html += '<div class="fb-profile">';
                                    space_html += '    <img align="left" class="fb-image-lg img-thumbnail" src="' + places[p].image + '" alt="studio image" />';
                                    //space_html += '    <img align="left" class="fb-image-profile img-circle" src="'+places[p].seeker_img+'" alt="User image example"/>';
                                    space_html += '    <div class="fb-profile-text">';
                                    //space_html += '       <h4>'+places[p].name+'</h4>';
                                    //space_html += '        <p>'+places[p].studio_name+'</p>';
                                    space_html += '    </div>';
                                    space_html += '</div>';


                                    space_html += '  </div>';
                                    space_html += '  <div class="col-sm-8">';
                                    space_html += '      <div class="captionx req_details">';
                                    space_html += '        <h4 class="text-info">' + places[p].name + '</h4>';
                                    space_html += '        <p>Location: ' + places[p].geo_name + '</p>';

                                    space_html += '        <p>Genre(s): ';
                                    if (places[p].genres_name.length > 0) {
                                        for (var i = 0; i < places[p].genres_name.length; i++) {
                                            space_html += places[p].genres_name[i];
                                            if ((i + 1) != places[p].genres_name.length) {
                                                space_html += ', ';
                                            }
                                        }
                                    }

                                    space_html += '        <p>Distance: ' + places[p].distance + ' km.</p>';
                                    //space_html += '        <p>Background: '+places[p].background+'</p>';
                                    //space_html += '        <p>Facilities: '+places[p].facilities+'</p>';


                                    space_html += '        <p><a href="#" class="btn btn-primary ' + action_btn_class + ' btn-sm ' + places[p].button_class + '" ' + btn_data + ' role="button"><i class="fa fa-file"></i> ' + action_btn_text + '</a></p>'; //'+places[p].button_text+'


                                    space_html += '      </div>';
                                    space_html += '  </div>';
                                    space_html += '</div>';

                                    space_html += '</td>';
                                    space_html += '</tr>';
                                }


                                //create gmap latlng obj
                                tmpLatLng = new google.maps.LatLng(places[p].geo[0], places[p].geo[1]);

                                // make and place map maker.
                                var marker = new google.maps.Marker({
                                    map: map,
                                    position: tmpLatLng,
                                    title: places[p].name + " by " + places[p].seeker_name,
                                    //icon: base_url + 'assets/img/google-maps-marker.png'
                                });

                                var map_marker_content = '<div id="content" style="text-align: center;"><img src="' + places[p].image + '" alt="Space Image" class="img-thumbnail ' + action_btn_class + '" ' + btn_data + ' style="width: 150px; margin: 0 auto;"><p><b>' + places[p].name;

                                if (response.searchtype == 'class') {
                                    map_marker_content += ' by ' + places[p].studio_name + '</b><br>Cost: $' + places[p].cost;
                                }

                                map_marker_content += '<br><button class="btn btn-xs btn-primary ' + action_btn_class + '" ' + btn_data + ' style="margin-top: 5px;"><i class="fa fa-file"></i> ' + action_btn_text + '</button></p></div>';


                                bindInfoWindow(marker, map, infowindow, map_marker_content);

                                // not currently used but good to keep track of markers
                                markers.push(marker);

                            }

                            //append html
                            if (space_html != '') {

                                result_table.destroy();

                                $('.space_results').hide();
                                $('.space_datatable').show().closest('#DataTables_Table_0_wrapper').show();
                                $('.space_datatable').find('tbody').html(space_html);
                                result_table = $('.space_datatable').DataTable({
                                    "bLengthChange": false,
                                    "searching": false
                                });

                            } else {
                                $('.space_datatable').hide().closest('#DataTables_Table_0_wrapper').hide();
                                $('.space_results').show().html('<p class="lead text-muted text-center" style="padding-top: 150px;">No match found.</p>');
                            }


                        }
                    },
                    error: function(err) {
                        console.log(err);
                    }
                })


            };



            // binds a map marker and infoWindow together on click
            var bindInfoWindow = function(marker, map, infowindow, html) {
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.setContent(html);
                    infowindow.open(map, marker);
                });

            }



            var initialize = function() {

                // create the geocoder
                geocoder = new google.maps.Geocoder();

                // set some default map details, initial center point, zoom and style
                var mapOptions = {
                    center: new google.maps.LatLng(location_lat, location_long),
                    zoom: 7,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    scrollwheel: false
                };

                // create the map and reference the div#map-canvas container
                map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

                // fetch the existing places (ajax)
                // and put them on the map
                fetchPlaces();

                // Limit the zoom level
                google.maps.event.addListener(map, 'zoom_changed', function() {
                    if (map.getZoom() < 5) map.setZoom(5);
                });

                google.maps.event.addListener(map, "click", function(event) {
                    infowindow.close();
                });

                google.maps.event.addListener(infowindow, 'domready', function() {
                    //$(".gm-style-iw").next("div").hide();
                    $(".gm-style-iw").css("left", function() {
                        return ($(this).parent().width() - $(this).width());
                    }).next("div").remove();
                });

            }


            // when page is ready, initialize the map!
            //google.maps.event.addDomListener(window, 'load', initialize);
            initialize();


        }); //end of geocomplete search


    //add_class_sched_btn

    $(document).on('click', '.add_class_sched_btn', function() {
        var $self = $(this);
        var data = $self.data();

        //check user type
        if (user_type != 'student') {
            bootbox.alert('<h4>' + action_messages.error.confirm_booking + '</h4>', function() {
                $('#loginModal').modal('show');
            });
        } else {
            //check student card
            $.post(
                base_url + 'settings/check_student_card', {},
                function(res) {
                    console.log(res);
                    if (res != 'ok') {
                        bootbox.alert('<h4><i class="fa fa-info-circle text-info"></i> Please add payment method to continue class booking.</h4>', function() {
                            window.location.href = base_url + 'settings/profile/payment';
                        });

                    } else {
                        showSummary(data);
                    }
                }
            );
        }

        return false;
    });

    $(document).on('click', '.book_schedule_btn', function() {
        var $self = $(this);
        var data = $self.data();
        data = $.param(data);
        console.log(data);
        //return false;
        $self.button('loading');

        $.post(
            base_url + 'landing/stripe_paynow',
            data,
            function(res) {
                console.log(res);
                $('.modal').modal('hide');
                setTimeout(function() {
                    $self.button('reset');
                    bootbox.alert('<h4>' + res.message + '</h4>');
                }, 500);

            },
            'json'
        ).error(function(err) {
            console.log(err);
        });

    });

    //show studio
    $(document).on('click', '.req_book_btn, .view_studio_profile_btn', function() {
        var $self = $(this);
        var data = $self.data();
        current_class_booked = data;
        console.log(data);
        var param_data = $.param(data);

        var scrollto = '';
        if (data.type == 'class') {
            var scrollto = '#classes';
        }

        var win = window.open(base_url + 'studio?' + param_data + scrollto, '_blank');
        win.focus();

        return false;
    });




    //show referral_info_btn details
    $('body').on('click', '.referral_info_btn', function(e) {
        var $self = $(this);
        var id = $self.data('id');
        var mdl = $self.data('target');
        $(mdl).find('.txt_details').html('<p class="text-center text-muted"><i class="fa fa-spinner fa-spin fa-2x"></i><br>Loading..</p>');

        $(mdl).find('.txt_details').load(base_url + 'landing/refferal_info/' + id);

        $(mdl).modal('show');
        e.preventDefault();
    });

    //show referral_info_btn details
    $('body').on('click', '.trans_info_btn', function(e) {
        var $self = $(this);
        var id = $self.data('id');
        var mdl = $self.data('target');
        $(mdl).find('.txt_details').html('<p class="text-center text-muted"><i class="fa fa-spinner fa-spin fa-2x"></i><br>Loading..</p>');

        $(mdl).find('.txt_details').load(base_url + 'landing/trans_info/' + id);

        $(mdl).modal('show');
        e.preventDefault();
    });

    //show agentcust details
    $(document).on('click', '.view_agentcust_btn', function(e) {
        var $self = $(this);
        var id = $self.data('id');
        var mdl = $self.data('target');
        $(mdl).find('.txt_details').html('<p class="text-center text-muted"><i class="fa fa-spinner fa-spin fa-2x"></i><br>Loading..</p>');

        $(mdl).find('.txt_details').load(base_url + 'landing/agentcust_info/' + id, function() {});

        $(mdl).modal('show');
        e.preventDefault();
    });

    //show terms modal
    $('.terms_btn').on('click', function() {
        var $self = $(this);
        var $mdl = $('#termsModal');

        $mdl.find('.text_content').html('<p class="text-center text-muted"><i class="fa fa-spinner fa-spin fa-2x"></i><br>Loading..</p>');

        $mdl.find('.text_content').load(base_url + 'landing/legal_text/terms');

        $mdl.modal('show');
        return false;
    });

    //show privacy_btn modal
    $('.privacy_btn').on('click', function() {
        var $self = $(this);
        var $mdl = $('#privacyModal');

        $mdl.find('.text_content').html('<p class="text-center text-muted"><i class="fa fa-spinner fa-spin fa-2x"></i><br>Loading..</p>');

        $mdl.find('.text_content').load(base_url + 'landing/legal_text/privacy');

        $mdl.modal('show');
        return false;
    });



    //show terms modal
    $('.terms_btn2').on('click', function() {
        var $self = $(this);
        var $mdl = $('#termsModal');

        $mdl.find('.text_content').html('<p class="text-center text-muted"><i class="fa fa-spinner fa-spin fa-2x"></i><br>Loading..</p>');

        $mdl.find('.text_content').load(base_url + 'landing/legal_text/terms2');

        $mdl.modal('show');
        return false;
    });

    //show privacy_btn modal
    $('.privacy_btn2').on('click', function() {
        var $self = $(this);
        var $mdl = $('#privacyModal');

        $mdl.find('.text_content').html('<p class="text-center text-muted"><i class="fa fa-spinner fa-spin fa-2x"></i><br>Loading..</p>');

        $mdl.find('.text_content').load(base_url + 'landing/legal_text/privacy2');

        $mdl.modal('show');
        return false;
    });


    $('#request_form').find('.form-group').hover(function() {
        $(this).find('.reset_btn').removeClass('hidden');

    }, function() {
        $(this).find('.reset_btn').addClass('hidden');

    });

    $('.reset_btn').on('click', function() {
        $(this).closest('.form-group').find('input').prop('checked', false);
        $(this).closest('.form-group').find('label.btn').removeClass('active btn-primary').addClass('btn-default');
        $('.request_quote_location').trigger("geocode");
        return false;
    });



    $('#request_form').on('submit', function() {
        var $self = $(this);
        var newUrl = location.href.replace($self.serialize());
        console.log(newUrl);

        show_map();
        console.log($self.serialize());

        return false;

    });



    //profile upload
    $("#myFormAvatar").ajaxForm({
        beforeSend: function() {
            $(".progress-profile").show();
            //clear everything
            $(".bar-profile").width('0%');
            $(".percent-profile").html("0%");
        },
        uploadProgress: function(event, position, total, percentComplete) {
            $(".bar-profile").width(percentComplete + '%');
            $(".percent-profile").html(percentComplete + '%');


        },
        success: function() {
            $(".bar-profile").width('100%');
            $(".percent-profile").html('100%');

        },
        complete: function(response) {
            if (response.responseText == 'not_img') {
                $(".progress-profile").hide();
                bootbox.alert('<p class="lead">File selected is not a valid image.</p>');

            } else {
                $('#avatar_name').val(response.responseText);
                $('.bootstrap-modal-cropper img').attr('src', base_url + '' + response.responseText);
                $("#cropImg").find('.modal-body').css('min-height', '560');
                $("#cropImg").modal('show');
            }
            console.log(response.responseText);

        },
        error: function() {
            bootbox.alert('<h4> Unable to upload file. Please refresh and try again.</h4>');
        }

    });
    //end ajax file upload


    if (uri_1 == 'settings' && uri_2 == 'profile' || (uri_3 == 'logo')) {
        //crop on a modal
        var $modal = $("#cropImg"),
            $image = $modal.find(".bootstrap-modal-cropper > img"),
            originalData = {};


        var aspratio = 146 / 61;
        if ($('#customer_img').length > 0) {
            aspratio = 1 / 1;
        }

        $modal.on("shown.bs.modal", function() {
            $image.cropper({
                aspectRatio: aspratio,
                multiple: true,
                data: originalData,

                strict: true,
                guides: false,
                highlight: false,
                dragCrop: false,
                cropBoxMovable: false,
                cropBoxResizable: false,

                done: function(data) {
                    console.log(data);
                }
            });
        }).on("hidden.bs.modal", function() {
            originalData = $image.cropper("getData");
            $image.cropper("destroy");
        });




        //save crop image
        $('.sav_crop').click(function(e) {
            var aspect = {
                width: 146,
                height: 61
            };
            var hey = $image.cropper('getCroppedCanvas', aspect).toDataURL();
            //$image.cropper("getDataURL", "image/jpeg");
            console.log(hey);
            $(this).addClass('disabled');
            $(this).html('Loading...');

            var data = {
                "img_url": hey,
                "img_name": $('#avatar_name').val(),
                "img_type": $('#img_type').val()
            };

            data = $.param(data); // $(this).serialize() + "&" +

            $.ajax({
                type: "POST",
                url: base_url + "settings/save_profile_pic",
                dataType: 'json',
                data: data,
                success: function(data) {
                    console.log(data);
                    window.location.reload();
                },
                error: function(err) {
                    console.log(err);
                }
            });

        });
    } //crop is for settings/profile


    $('.remove_selected_btn').on('click', function() {
        var $self = $(this);
        $self.closest('div').hide().siblings('input').val('');
        $self.closest('div').siblings('a').show();
        return false;
    });

    $('.radio_list_form').on('submit', function() {
        var $self = $(this);
        var type = $self.find('[name="type"]').val();
        var $checked = $self.find('input:checked');

        if ($checked.length > 0) {
            var txt = $checked.siblings('span').html();
            $('#' + type).val($checked.val());
            $('#' + type).siblings('a').hide();
            $('#' + type).siblings('.btn-group').show().children().first().html(txt);
        }
        return false;
    });


    $('#filter_genre_form').on('submit', function() {
        var $self = $(this);
        var checked = $self.find('input:checked');
        var name = 'genre[]';

        if ($self.find('input[type="radio"]').length > 0) {
            var name = 'cargocat';
        }

        if (checked.length > 0) {
            $('.genre_inputs').html('');
            $('.genre_well').html('');
            $(checked).each(function(index, element) {
                var data = $(this).data();
                //data = $.param(data);
                data = $.map(data, function(value, index) {
                    return [value];
                });
                var the_input = '<input type="hidden" data-info="' + data + '" name="' + name + '" value="' + $(element).val() + '"/>';
                $('.genre_inputs').append(the_input);
                console.log(the_input);

                var gntext = $(element).siblings('span').html();
                var title = $(element).closest('a').attr('title');
                var text = '<span class="label label-info label-lg" title="' + title + '" data-id="' + $(element).val() + '">' + gntext + ' <i class="fa fa-times close-gnre"></i></span> ';

                $('.genre_well').append(text);
            });

            $('.genre_well').show();
        } else {
            $('.genre_inputs').html('<input type="hidden" name="' + name + '" value=""/>');
            $('.genre_well').hide();
        }

        //$self.closest('.modal').modal('hide');
        $('#request_form').submit();
        return false;
    });


    $(document).on('click', '.close-cargo', function() {
        var $self = $(this);
        $('[name="cargocat"]').val('');
        $('[name="cargocat"]').selectpicker('refresh');
        $('.genre_well').hide().html('');
    });

    $(document).on('click', '.close-gnre', function() {
        var $self = $(this);
        var id = $self.closest('span').data('id');
        $('.genre_inputs').find('[value="' + id + '"]').remove();
        $self.closest('span').remove();
        if ($('.genre_well').children().length == 0) {
            $('.genre_well').hide();
        }

        $('#filter_genre_form').find('[value="' + id + '"]').prop('checked', false);
        $('#filter_genre_form').find('[value="' + id + '"]').closest('.genre_thumbs').removeClass('active');

        $('#formModal').find('[value="' + id + '"]').prop('checked', false);
        $('#formModal').find('[value="' + id + '"]').closest('.genre_thumbs').removeClass('active');
        $('#request_form').submit();
    });


    $('#request_form').find('[type="radio"], [type="checkbox"]').on('change', function() {
        $('#request_form').submit();
    });

    //resend instructor
    $('.resend_inst_btn').on('click', function() {
        var $self = $(this);
        var form = $self;
        var id = $self.data('id');
        bootbox.confirm('<h4>' + action_messages.global.resend_confirmation + '</h4>', function(e) {
            if (e) {
                //var data = $(form).serialize();
                //console.log(data);
                submitForm('resend_instructor', {
                    id: id
                }, form);
            }
        });
        return false;
    });

    $('.btn-group label').children().on('change', function() {
        var $self = $(this);
        if ($self.is(":checked")) {
            $self.closest('label').removeClass('btn-default').addClass('btn-primary');
            console.log('checked');
            if ($self.is('[type="radio"]')) {
                $self.closest('label').siblings().removeClass('btn-primary').addClass('btn-default');
            }
        } else {
            console.log('notchecked');
            $self.closest('label').removeClass('btn-primary').addClass('btn-default');
        }
    });


    $(document).on('click', '.genre_list a', function() {
        var $self = $(this);
        var $form = $self.closest('form');

        if(!$self.closest('div').siblings().find('a').hasClass('disabled')){
            //if radio button
            if ($self.find('input[type="radio"]').length > 0) {
                $self.find('input').prop('checked', true);
                //$self.closest('div').siblings().find('input').prop('checked', false);
                $self.closest('div').siblings().find('a').removeClass('active');
                $self.addClass('active');

            } else { //if checkbox
                if ($self.find('input').is(':checked')) {
                    $self.find('input').prop('checked', false);
                    $self.removeClass('active');
                } else {
                    $self.find('input').prop('checked', true);
                    $self.addClass('active');
                }
            }
            //auto submit cargo
            if ($form.is('#filter_genre_form') || $form.hasClass('radio_list_form')) {
                $form.submit();
                $self.closest('.modal').modal('hide');
            }


        }


        return false;
    });


    //toggle class schedule time
    $('[name="dayweek[]"]').on('click', function() {
        if ($(this).is(':checked')) {
            $(this).closest('.checkbox').next().show();
            $(this).closest('.checkbox').next().find('input').first().attr('name', 'start[]');
            $(this).closest('.checkbox').next().find('input').last().attr('name', 'end[]');
        } else {
            $(this).closest('.checkbox').next().hide();
            $(this).closest('.checkbox').next().find('input').removeAttr('name');
        }
    });


    $('[name="usertype"]').bootstrapSwitch({
        size: 'small',
        onText: 'Studio',
        offText: 'Instructor',
        onColor: 'info',
        offColor: 'info',
        labelText: '<i class="fa fa-user"></i>',
        labelWidth: 15
    });

    //detect type of social button
    $('.omb_btn-facebook, .omb_btn-google').on('click', function() {
        var $self = $(this);
        var type = $self.data('type');
        var site = $self.data('site');
        $('[name="social_type"]').val(type);
        $('[name="social_site"]').val(site);
        console.log(type);
    });


    //date time picker
    $('.timepicker').datetimepicker({
        format: 'LT'
    });

    //date time picker
    $('.input-date').datetimepicker({
        format: 'DD/MM/YYYY', //YYYY/M/D - dddd
        minDate: moment()
    });

    //character counter
    $('.input-count-char').on('keypress', function(event) {
        var $self = $(this);
        var value = $self.val();
        var count = $self.val().length;
        if (count == 300) {
            return false;
        }
    });

    //character counter
    $('.input-count-char').on('keyup', function() {
        var $self = $(this);
        var value = $self.val();
        var count = 300 - $self.val().length;
        if (count < 0) {
            value = value.slice(0, 300);
            $self.val(value);
            count = 0;
        }
        $self.siblings('span.small').html(count + ' characters remaining.');
    });

    $('input[name="start_time"]').on('dp.change', function() {
        var duration = $('[name="duration"]').val();
        var start_time = $(this).val();
        var new_time = moment('2013-02-08 ' + start_time).add(duration, 'minutes').format('h:mm A');

        console.log(new_time);
        $('input[name="end_time"]').val(new_time);
    });

    $('select[name="duration"]').on('change', function() {
        var start_time = $('[name="start_time"]').val();
        var duration = $(this).val();
        var nowdate = moment().format('YYYY-MM-DD');
        var new_time = moment(nowdate + ' ' + start_time).add(duration, 'minutes').format('h:mm A');

        console.log(new_time);
        $('input[name="end_time"]').val(new_time);

    });

    $('input[name="end_time"]').on('dp.change', function() {
        $('select[name="duration"]').val('0');
    });




    //schedule scripts
    var current_year = moment().format('YYYY');
    var next_year = parseInt(current_year) + 1;
    $('input[name="start_day"]').datetimepicker({
        format: 'ddd M/D/YYYY',
        maxDate: moment(current_year + '-12-31'),
        minDate: moment()
    });

    $('input[name="end_day"]').datetimepicker({
        format: 'ddd M/D/YYYY',
        maxDate: moment(next_year + '-12-31'),
        minDate: moment()
    });


    var next_year = parseInt(current_year) + 1;
    $('input[name="date_from"]').datetimepicker({
        format: 'DD-MM-YYYY'
    });

    $('input[name="date_to"]').datetimepicker({
        format: 'DD-MM-YYYY',
        // minDate: moment().add(30, 'd')
    });


    //link both date pickers
    $('input[name="date_from"]').on("dp.change", function(e) {
        $('input[name="date_to"]').data("DateTimePicker").minDate(e.date);
    });
    $('input[name="date_to"]').on("dp.change", function(e) {
        $('input[name="date_from"]').data("DateTimePicker").maxDate(e.date);
    });

    //generate class max students
    $('[name="class"]').on('change', function() {
        var $self = $(this);
        var maxx = $self.find(':selected').data('max');
        $self.closest('form').find('[name="max_student"]').val(maxx);
    });


    $('[name="recur"]').bootstrapSwitch({
        size: 'small',
        onText: 'Recurring',
        offText: 'Single',
        onColor: 'info',
        offColor: 'info',
        labelText: '<i class="fa fa-calendar-o fa-fw"></i>',
        labelWidth: 15,
        onSwitchChange: function(event, state) {
            if (state) {
                $('.weekly_settings').slideDown('fast');
            } else {
                $('.weekly_settings').slideUp('fast');
            }
        }
    });


    $('[name="toggle_recur"]').on('change', function() {});




    //Form schedule_form validate
    $('#schedule_form').validate({
        rules: {
            class: {
                required: true
            },
            //			max_student: {
            //				required: true
            //			},
            start_day: {
                required: true
            },
            end_day: {
                required: true
            },
            start_time: {
                required: true
            },
            end_time: {
                required: true
            },
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            element.closest('.form-group').append(error);
        },
        submitHandler: function(form) {

            var nowdate = moment().format('YYYY-MM-DD');

            var start_time = $('input[name="start_time"]').val();
            if (start_time != '') {
                start_time = moment(nowdate + ' ' + start_time).format('HH:mm');
            }

            var end_time = $('input[name="end_time"]').val();
            if (end_time != '') {
                end_time = moment(nowdate + ' ' + end_time).format('HH:mm');
            }


            var start_date = $('input[name="start_day"]').val();
            start_date = moment(start_date).format('YYYY-MM-DD');

            var end_date = moment(start_date).format('YYYY-MM-DD');
            //			var end_date = $('input[name="end_day"]').val();
            //			if(end_date !=''){
            //				end_date = moment(end_date).format('YYYY-MM-DD');
            //			}

            var data = {
                "start_time_format": start_time,
                "end_time_format": end_time,
                "start_date_format": start_date,
                "end_date_format": end_date
            };

            data = $(form).serialize() + '&' + $.param(data);

            //check recur if checked
            var recur = $(form).find('[name="recur"]').is(':checked');
            var weekly_recure = $(form).find('[name="weekly_recure"]').val();
            var daysinweek = $(form).find('[name="days_in_week[]"]:checked').length;
            if (recur) {
                if (daysinweek > 0 && weekly_recure != '') {
                    $('.weekly_settings').find('span').hide();
                } else {
                    $('.weekly_settings').find('span').show();
                    return false;
                }
            }

            console.log(data);
            submitForm('submit_schedule', data, form);
        }
    });

    $('.remove_card_btn').on('click', function() {
        var $self = $(this);
        var form = $self.closest('form');
        var id = $self.data('id');
        var stripe = $self.data('stripe');

        bootbox.confirm('<h4>' + action_messages.global.customer_remove_card + '</h4>', function(e) {
            if (e) {
                var data = {
                    id: id,
                    stripe: stripe
                };
                console.log(data);
                data = $.param(data);
                submitForm('remove_card', data, form);
            }
        });
        return false;
    });

}); //document.ready


if (((uri_2 == 'profile' && stripe_id == '') || (uri_1 == 'buy' || uri_1 == '')) && customer_type != 'N') { // && user_type == 'student'
    //buy insurance for user already added payment method
    $('.payment_logged_btn').on('click', function() {
        var $self = $(this);
        /* Visual feedback */
        $self.html('Processing <i class="fa fa-spinner fa-pulse"></i>').prop('disabled', true);

        var token = ''; //response.id;
        var card = ''; //response.card;
        var datta = {
            token: token,
            card: card
        };

        //detect whether from buy or not
        var from_buy = 'yes';
        var email = '';
        datta.from_buy = from_buy;
        datta.email = email;
        // AJAX - you would send 'token' to your server here.

        console.log(datta);

        //		return false;

        $.post(
                base_url + 'settings/add_stripe_customer',
                $.param(datta),
                function(res) {
                    console.log(res);
                },
                'json'
            )
            .error(function(err) {
                console.log(err);
            })
            // Assign handlers immediately after making the request,
            .done(function(data, textStatus, jqXHR) {
                $('.modal').modal('hide');
                setTimeout(function() {
                    bootbox.alert('<h4>' + data.message + '</h4>', function() {
                        window.location.href = base_url + 'dashboard';
                    });
                }, 360);
                if (data.result == 'ok') {
                    $self.html('<i class="fa fa-check-circle"></i> Success').prop('disabled', true);
                } else {
                    $self.html('Submit').prop('disabled', false);
                }
                console.log(data);
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                $self.html('There was a problem').removeClass('success').addClass('error');
            });

    });

    //stripe add payment method - card
    var $form = $('#payment-form');
    $form.on('submit', payWithStripe);

    /* If you're using Stripe for payments */
    function payWithStripe(e) {
            e.preventDefault();

            /* Visual feedback */
            $form.find('[type=submit]').html('Validating <i class="fa fa-spinner fa-pulse"></i>');

            var PublishableKey = 'pk_test_QI3QYkCIFf8kQyDImRoBgjKh'; //pk_live_Fy5rAmIEkuTT4IBD4COuBXzw // Replace with your API publishable key , for redskycargo
            Stripe.setPublishableKey(PublishableKey);

            /* Create token */
            var expiry = $form.find('[name=cardExpiry]').payment('cardExpiryVal');
            var ccData = {
                number: $form.find('[name=cardNumber]').val().replace(/\s/g, ''),
                cvc: $form.find('[name=cardCVC]').val(),
                name: $form.find('[name=name]').val(),
                exp_month: expiry.month,
                exp_year: expiry.year
            };

            Stripe.card.createToken(ccData, function stripeResponseHandler(status, response) {
                if (response.error) {
                    /* Visual feedback */
                    $form.find('[type=submit]').html('Try again');
                    /* Show Stripe errors on the form */
                    $form.find('.payment-errors').text(response.error.message);
                    $form.find('.payment-errors').closest('.row').show();
                } else {
                    /* Visual feedback */
                    $form.find('[type=submit]').html('Processing <i class="fa fa-spinner fa-pulse"></i>').prop('disabled', true);
                    /* Hide Stripe errors on the form */
                    $form.find('.payment-errors').closest('.row').hide();
                    $form.find('.payment-errors').text("");
                    // response contains id and card, which contains additional card details
                    console.log(response.id);
                    console.log(response.card);


                    var token = response.id;
                    var card = response.card;
                    var datta = {
                        token: token,
                        card: card
                    };

                    //detect whether from buy or not
                    var from_buy = 'no';
                    var email = '';
                    if ($form.find('[name="from_buy"]').length > 0) {
                        from_buy = 'yes';
                        email = $('.single_purchase_form').find('[name="email"]').val();
                    }
                    datta.from_buy = from_buy;
                    datta.email = email;
                    // AJAX - you would send 'token' to your server here.

                    console.log(datta);
                    //				console.log(base_url+'settings/add_stripe_customer');
                    //				return false;

                    $.post(
                            base_url + 'settings/add_stripe_customer',
                            $.param(datta),
                            function(res) {
                                console.log(res);
                            },
                            'json'
                        )
                        .error(function(err) {
                            console.log(err);
                        })
                        // Assign handlers immediately after making the request,
                        .done(function(data, textStatus, jqXHR) {

                            if (data.result == 'ok') {
                                $form.find('[type=submit]').html('<i class="fa fa-check-circle"></i> Success').prop('disabled', true);
                                if (typeof(data.booking_id) !== 'undefined') {
                                    // $('.modal').modal('hide');
                                    var message = '<h3 class="text-center"><i class="fa fa-check-circle" style="color: green"></i> Confirm your payment was successful. You can now download your Certificate of Insurance by clicking on the below link. <br> <br><div class="row"><div class="col-md-4 col-md-offset-4"><a href="' + base_url + 'landing/certificate/' + data.booking_id + '" target="_blank" class="btn btn-link btn-block">Download my certificate</a></div></div></h3>';

                                    setTimeout(function() {
                                        //tab-done
                                        //proceed to next tab
                                        var $tabpane = $('#tab-payment');
                                        var $tabpane2 = $('#tab-done');
                                        var $progressbar = $('#progressbar');

                                        $tabpane2.find('.done_message').html(message);
                                        $tabpane.removeClass('active').next().addClass('active');
                                        $progressbar.find('.active').removeClass('active').addClass('done-muted done').next().addClass('active');

                                        var $progressbar2 = $('.lifecycle .list');
                                        $progressbar2.find('.current').removeClass('current').addClass('complete').next().addClass('current');

                                        // bootbox.alert('<h4>'+message+'</h4>', function(){
                                        // 	window.location.href = base_url+'dashboard';
                                        // });
                                    }, 360);

                                } else if (from_buy == 'yes') {
                                    $('.modal').modal('hide');
                                    setTimeout(function() {
                                        bootbox.alert('<h4>' + data.message + '</h4>', function() {
                                            window.location.href = base_url + 'dashboard';
                                        });
                                    }, 360);
                                } else {
                                    bootbox.alert('<h4>' + data.message + '</h4>', function() {
                                        window.location.href = base_url + '#buyform';
                                    });
                                }

                            } else {
                                $form.find('[type=submit]').html('Submit').prop('disabled', false);
                                bootbox.alert('<h4>' + data.message + '</h4>');
                            }
                            console.log(data);
                        })
                        .fail(function(jqXHR, textStatus, errorThrown) {
                            $form.find('[type=submit]').html('There was a problem').removeClass('success').addClass('error');
                            /* Show Stripe errors on the form */
                            $form.find('.payment-errors').text('Try refreshing the page and trying again.');
                            $form.find('.payment-errors').closest('.row').show();
                        });
                }
            });
        }
        /* Fancy restrictive input formatting via jQuery.payment library*/
    $('input[name=cardNumber]').payment('formatCardNumber');
    $('input[name=cardCVC]').payment('formatCardCVC');
    $('input[name=cardExpiry').payment('formatCardExpiry');

    /* Form validation using Stripe client-side validation helpers */
    jQuery.validator.addMethod("cardNumber", function(value, element) {
        return this.optional(element) || Stripe.card.validateCardNumber(value);
    }, "Please specify a valid credit card number.");

    jQuery.validator.addMethod("cardExpiry", function(value, element) {
        /* Parsing month/year uses jQuery.payment library */
        value = $.payment.cardExpiryVal(value);
        return this.optional(element) || Stripe.card.validateExpiry(value.month, value.year);
    }, "Invalid expiration date.");

    jQuery.validator.addMethod("cardCVC", function(value, element) {
        return this.optional(element) || Stripe.card.validateCVC(value);
    }, "Invalid CVC.");

    validator = $form.validate({
        rules: {
            cardNumber: {
                required: true,
                cardNumber: true
            },
            cardExpiry: {
                required: true,
                cardExpiry: true
            },
            cardCVC: {
                required: true,
                cardCVC: true
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
            $(element).closest('.form-control').removeClass('success').addClass('error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
            $(element).closest('.form-control').removeClass('error').addClass('success');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            element.closest('.form-group').append(error);
        }

    });

    paymentFormReady = function() {
        if ($form.find('[name=cardNumber]').hasClass("success") &&
            $form.find('[name=cardExpiry]').hasClass("success") &&
            $form.find('[name=cardCVC]').val().length > 1) {
            return true;
        } else {
            return false;
        }
    }

    $form.find('[type=submit]').prop('disabled', true);
    var readyInterval = setInterval(function() {
        if (paymentFormReady()) {
            $form.find('[type=submit]').prop('disabled', false);
            clearInterval(readyInterval);
        }
    }, 250);

} //if profile page and student user


jQuery(document).ready(function() {
    "use strict";
    var options = {};
    options.ui = {
        container: "#customer_password_form",
        showVerdictsInsideProgressBar: true,
        viewports: {
            progress: ".pwstrength_viewport_progress"
        }
    };
    var options2 = {};
    options2.ui = {
        container: "#signup_form",
        showVerdictsInsideProgressBar: true,
        viewports: {
            progress: ".pwstrength_viewport_progress"
        }
    };
    //	options.common = {
    //		debug: true,
    //		onLoad: function () {
    //			$('#messages').text('Start typing password');
    //		}
    //	};

    if ($('#pw').length > 0) {
        $('#pw').pwstrength(options);
    }

    if ($('#signup_password').length > 0) {
        console.log('signup_password');
        $('#signup_password').pwstrength(options2);
    }
});
