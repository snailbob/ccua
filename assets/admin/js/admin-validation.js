// JavaScript Document
$(document).ready(function() {

    $('.content_text').on('click', function() {
        $(this).hide();
        $('.edit_content').show();
        $('.edit_content').focus();
        $('.submit_content').show();
    });


    $('[name="ctype"]').bootstrapSwitch({
        onText: 'Customer',
        offText: 'Agency',
        onColor: 'info',
        offColor: 'info',
        labelWidth: 15,
    });

    $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            $('.edit_content').hide();
            $('.content_text').show();
            $('.submit_content').hide();
        } // esc
    });


    $('.new_admin_email').on('click', function(){
        var tr = $(this).closest('.form-group').clone(); 
        //.removeClass().addClass('tr-' + res.id).insertAfter('.' + tr_ln);
        tr.find('.new_admin_email').hide();
        tr.find('input').val('');
        tr.find('.delete_admin_email').removeClass('hidden');
        $('.email-lists').append(tr);
        return false;
    });

    $(document).on('click', '.delete_admin_email', function(){
        $(this).closest('.form-group').remove();
    });

    $(document).on('click', '.reject_btn', function() {
        var $self = $(this);
        var id = $self.closest('form').find('[name="id"]').val();
        var comment = $self.closest('form').find('[name="comment"]').val();
        $('.modal').modal('hide');
        bootbox.confirm('<h4 class="modal-title">Confirm you wish to reject selected request.</h4>', function(e) {
            if (e) {
                window.location.href = base_url + 'webmanager/dashboard/reject_request/' + id+'?comment='+comment;
            }
        });
    });

    $('.send_certificate_btn').on('click', function(e) {
        var $self = $(this);
        var id = $self.data('id');
        $('.modal').hide();
        bootbox.confirm('<h4 class="modal-title">Confirm you wish to send certificate to selected customer.</h4>', function(e) {
            if (e) {
                $.post(
                    base_url + 'webmanager/dashboard/send_certificate', {
                        id: id
                    },
                    function(res) {
                        console.log(res);
                        bootbox.alert('<h4 class="modal-title"><i class="fa fa-check-circle text-success"></i> Insurance Certificate successfully sent.</h4>');
                    },
                    'json'
                ).error(function(res) {
                    console.log(res);
                });
            }
        });

        e.preventDefault();
    });

    $('.generate_report_btn').on('click', function(e) {
        var $self = $(this);
        var title = $self.data('title');
        var type = $self.data('type');
        var $mdl = $('#myModal');

        $mdl.find('.row-bordereaux').addClass('hidden');
        if (type == 'bordereaux') {
            $mdl.find('.row-bordereaux').removeClass('hidden');
        }

        $mdl.find('.modal-title').html(title);
        $mdl.find('.report-type').val(type);
        $mdl.modal('show');
        e.preventDefault();
    });

    $('.generate_report_form').on('submit', function() {
        var $form = $(this);
        var form_data = $form.serialize()
        console.log(form_data); //return false;
        var date_from = $form.find('[name="date_from"]').val();
        var date_to = $form.find('[name="date_to"]').val();
        var type = $form.find('[name="type"]').val();
        console.log(base_url + 'webmanager/reporting/' + type + '/' + date_from + '/' + date_to);
        window.location.href = base_url + 'webmanager/reporting/' + type + '/' + date_from + '/' + date_to + '?' + form_data;
        $form.closest('.modal').modal('hide');
        return false;
    });



    $('[name="report_template"]').on('change', function() {
        var $self = $(this);
        var inputs = $self.find('option:selected').data('inputs');
        var name = $self.find('option:selected').data('name');
        $('[name="template_name"]').val(name);

        $('[name="report_header[]"]').prop('checked', false).closest('.btn').removeClass('btn-primary active').addClass('btn-default');
        for (i = 0; i < inputs.length; i++) {
            console.log(inputs[i]);
            $('.report-attributes').find('[value="' + inputs[i] + '"]').prop('checked', true).closest('.btn').addClass('btn-primary active');
        }

        console.log(name, inputs);
    });

    $('[name="country_currency"]').on('change', function() {
        var checked = $('[name="country_currency"]:checked').length;
        $.post(
            base_url + 'webmanager/cargo/save_country_currency', {
                checked: (checked > 0) ? 'Y' : 'N'
            },
            function(res) {
                console.log(res);

            },
            'json'
        ).error(function(err) {
            console.log(err);
        });
    });


    $('[name="home_btn_options"]').on('change', function() {
        var $self = $(this);
        console.log($self.val());

        $.post(
            base_url + 'webmanager/contents/buy_button_show', {
                checked: $self.val()
            },
            function(res) {
                console.log(res);
                bootbox.alert('Buy button settings updated');
            },
            'json'
        ).error(function(err) {
            console.log(err);
        });

    });

    $('#admin_change_password').validate({
        ignore: [],

        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',

        errorPlacement: function(error, element) {
            element.closest('div').append(error);
        },
        // submitHandler: function(form) {
        //     $(form).find('button[type="submit"]').button('loading');
        //     return true;
        // }
    });

    $('#base_curr_form').validate({
        ignore: [],
        rules: {
            currency: {
                required: true
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',

        errorPlacement: function(error, element) {
            element.closest('.form-group').append(error);
        },
        submitHandler: function(form) {
            $(form).find('button[type="submit"]').button('loading');
            $.post(
                base_url + 'webmanager/cargo/save_currency',
                $(form).serialize(),
                function(res) {
                    console.log(res);
                    $('.base_currency').html($(form).find('[name="currency"]').val());
                    $(form).closest('.modal').modal('hide');
                    $(form).find('button[type="submit"]').button('reset');

                },
                'json'
            ).error(function(err) {
                console.log(err);
            });
        }
    });

    $('#add_port_form').validate({
        rules: {
            ccode: {
                required: true
            },
            portcode: {
                required: true,
                maxlength: 3
            },
            portname: {
                required: true
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',

        errorPlacement: function(error, element) {
            element.closest('.form-group').append(error);
        },
        submitHandler: function(form) {
            $(form).find('button[type="submit"]').button('loading');
            console.log($(form).serialize());
            submitForm('add_port_code', $(form).serialize(), form);
        }
    });

    $('#agency_form').validate({
        rules: {
            name: {
                required: true
            },
            email: {
                required: true,
                email: true
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',

        errorPlacement: function(error, element) {
            element.closest('.form-group').append(error);
        },
        submitHandler: function(form) {
            $(form).find('button[type="submit"]').button('loading');
            $.post(
                base_url + 'webmanager/users/add',
                $(form).serialize(),
                function(res) {
                    console.log(res);
                    window.location.reload(true);
                    $(form).find('button[type="submit"]').button('reset');
                },
                'json'
            ).error(function(err) {
                console.log(err);
            });
        }
    });

    $('#customer_form').validate({
        ignore: [],
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',

        errorPlacement: function(error, element) {
            element.closest('.form-group').append(error);
        },
        submitHandler: function(form) {
            $(form).find('button[type="submit"]').button('loading');
            $.post(
                base_url + 'webmanager/customers/add',
                $(form).serialize(),
                function(res) {
                    console.log(res);
                    window.location.reload(true);
                    $(form).find('button[type="submit"]').button('reset');
                },
                'json'
            ).error(function(err) {
                console.log(err);
            });
        }
    });

    $('#admin_tz_form').validate({
        ignore: [],
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            element.closest('.form-group').append(error);
        },
        submitHandler: function(form) {
            $(form).find('button[type="submit"]').button('loading');

            $.post(
                base_url + 'formsubmits/save_admin_tz',
                $(form).serialize(),
                function(res) {
                    console.log(res);
                    $(form).find('button[type="submit"]').html('Saved');

                    setTimeout(function(){
                        $(form).find('button[type="submit"]').button('reset');
                    },2500);
                },
                'json'
            ).error(function(err) {
                console.log(err);
            });
        }
    });
    

    $('#add_cargo_form').validate({
        rules: {
            name: {
                required: true
            },
            price: {
                required: true,
                number: true
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            element.closest('.form-group').append(error);
        },
        submitHandler: function(form) {
            $(form).find('button').button('loading');


            console.log($(form).serialize());
            $.post(
                base_url + 'formsubmits/add_cargo_cat',
                $(form).serialize(),
                function(res) {
                    console.log(res);
                    var $cargo_mdl = $('#cargoCategoryModal');
                    var tr_ln = $cargo_mdl.find('tbody tr').last().attr('class');

                    var tr = $cargo_mdl.find('tbody tr').first().clone().removeClass().addClass('tr-' + res.id).insertAfter('.' + tr_ln);

                    $cargo_mdl.find('.tr-' + res.id + ' .price-text:eq(0)').html(res.hs_code);
                    $cargo_mdl.find('.tr-' + res.id + ' .price-text:eq(1)').html(res.description);
                    $cargo_mdl.find('.tr-' + res.id + ' .price-text:eq(2)').html(res.price);
                    var referr = false;
                    if (res.referral == 'Yes') {
                        referr = true;
                    }
                    $cargo_mdl.find('.tr-' + res.id + ' [type="checkbox"]').prop('checked', referr);
                    $cargo_mdl.find('.tr-' + res.id + ' [type="checkbox"]').val(res.id);
                    $cargo_mdl.find('.delete_btn').data('id', res.id);
                    console.log(tr);
                    $(form).find('input').val('');
                    $(form).find('button').button('reset');
                    $(form).closest('.modal').modal('hide');
                },
                'json'
            ).error(function(err) {
                console.log(res);
            });

            //submitForm('add_cargo_cat', $(form).serialize(), form);

        }
    });

    $('#add_ded_form').validate({
        rules: {
            name: {
                required: true,
                number: true
            },
            price: {
                required: true,
                number: true
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            element.closest('.form-group').append(error);
        },
        submitHandler: function(form) {

            $(form).find('button').button('loading');
            console.log($(form).serialize());

            submitForm('add_deductible', $(form).serialize(), form);

        }
    });


});


function submitSubject(id) {
    $('.form_subj').find('button.btn-primary').html('Loading..').addClass('disabled');
    var subb = $('#subject_name').val();
    if (subb == '') {
        bootbox.alert('<p class="lead">Please add subject</p>');
    } else {

        $.ajax({
            type: "POST",

            url: base_url + 'webmanager/emails/update_subject',

            data: {
                subject: subb,
                id: id

            },

            success: function(data) {
                console.log(data);
                $('#subjj').html(subb);
                $('.form_subj').find('button.btn-primary').html('Saved').removeClass('disabled');
                setTimeout(function() {
                    $('.form_subj').hide();
                    $('#subjj').show();
                    $('.form_subj').find('button.btn-primary').html('Submit');
                }, 1000);
            }

        });


    }
}


function userActivate(inc, user_id) {
        var actt = '';
        if (inc == 'Y') {
            actt = '<p class="lead">Please confirm you wish to activate agency?</p>';
        } else {
            actt = '<p class="lead">Please confirm you wish to deactivate agency?</p>';
        }

        bootbox.confirm(actt, function(e) {

            if (e) {
                window.location.assign(base_url + "webmanager/users/activate/" + inc + "/" + user_id);

            }
        });

    } //userActivate

function defaultAgency(agency_id) {
    var actt = '<p class="lead">Please confirm you wish to make cargo category of this agency default?</p>';
    bootbox.confirm(actt, function(e) {
        if (e) {
            window.location.assign(base_url + "webmanager/cargo/makedefault/" + agency_id);

        }
    });
};

function customerActivate(inc, user_id) {
        var actt = '';
        if (inc == 'Y') {
            actt = '<p class="lead">Please confirm you wish to activate customer?</p>';
        } else {
            actt = '<p class="lead">Please confirm you wish to deactivate customer?</p>';
        }

        bootbox.confirm(actt, function(e) {

            if (e) {
                window.location.assign(base_url + "webmanager/customers/activate/" + inc + "/" + user_id);

            }
        });

    } //userActivate


function userDelete(user_id) {
        var del_mess = '<p class="lead">Please confirm you wish to delete agency?</p>';


        bootbox.confirm(del_mess, function(e) {

            if (e) {
                window.location.assign(base_url + "webmanager/users/delete/" + user_id);

            }
        });

    } //userDelete

function customerDelete(user_id, type) {
        var del_mess = '<p class="lead">Please confirm you wish to delete user?</p>';


        bootbox.confirm(del_mess, function(e) {

            if (e) {
                window.location.assign(base_url + "webmanager/customers/delete/" + user_id + "/" + type);

            }
        });

    } //userDelete



function deleteFaq(id) {
        var del_mess = '<p class="lead">Please confirm you wish to delete selected FAQ?</p>';


        bootbox.confirm(del_mess, function(e) {

            if (e) {
                window.location.assign(base_url + "webmanager/faq/delete/" + id);

            }
        });

    } //deleteFaq



function getFaq(id) {

    var data = {
        "id": id
    };
    data = $(this).serialize() + "&" + $.param(data);

    $.ajax({
        type: "POST",

        url: base_url + "webmanager/faq/get_info",

        dataType: 'json',

        data: data,

        success: function(data) {
            console.log(data);
            $('#question').val(data.question);
            $('#answer').val(data.answer);
            $('#faq_id').val(id);
            $('#faqModal').modal('show');

        }
    });

}


function userDetails(id) {

    var $mdl = $('#infoModal');
    $mdl.find('.agency_info').html('<p class="text-center text-muted"><i class="fa fa-spinner fa-spin fa-2x"></i><br>Loading..</p>');

    $mdl.find('.agency_info').load(base_url + 'landing/agency_info/' + id, function() {});

    $mdl.modal('show');
}


function customerDetails(id) {

    var $mdl = $('#infoModal');
    $mdl.find('.customer_info').html('<p class="text-center text-muted"><i class="fa fa-spinner fa-spin fa-2x"></i><br>Loading..</p>');

    $mdl.find('.customer_info').load(base_url + 'landing/customer_info/' + id, function() {});

    $mdl.modal('show');
}


//additional validations
$.validator.addMethod("alphanumeric", function(value, element) {
    return this.optional(element) || /^\w+$/i.test(value);
}, "Letters, numbers, and underscores only please");


$(document).ready(function(e) {


    $(document).on('click', '.underwriter-first-user', function() {
        var $self = $(this);
        var id = $self.data('id');
        var info = $self.data('info');
        var to = $self.data('to');
        var agency_id = $self.data('agency_id');
        console.log(info);

        var txt = (to == 'Y') ? 'Confirm make user super admin?' : 'Confirm unassign super admin?';
        bootbox.confirm(txt, function(e) {
            if (e) {
                $.post(
                    base_url + 'formsubmits/make_first_underwriter', {
                        id: id,
                        agency_id: agency_id,
                        to: to
                    },
                    function(res) {
                        console.log(res);
                        bootbox.alert(res.message, function() {
                            window.location.reload(true);
                        });
                    },
                    'json'
                ).error(function(res) {
                    console.log(res);
                });
            }
        });

    });


    $(document).on('click', '.customer-update', function() {
        var $self = $(this);
        var id = $self.data('id');
        var arr = $self.data('arr');
        var $form = $('#customer_form');

        $form.find('[name="id"]').val(id);
        $form.find('[name="first"]').val(arr.first_name);
        $form.find('[name="last"]').val(arr.last_name);
        $form.find('[name="bname"]').val(arr.business_name);
        $form.find('[name="brokerage_id"]').val(arr.brokerage_id);
        $form.find('[name="email"]').val(arr.email);
        $form.find('[name="brokerage_id"]').selectpicker('refresh');
        $form.find('[name="agency_id"]').val(arr.agency_id);
        $form.find('[name="agency_id"]').closest('.form-group').hide();

        if (arr.access_rights.length > 0) {
            $form.find('[name="access_rights[]"]').prop('checked', false).closest('label').addClass('btn-default').removeClass('active btn-primary');
            for (var i = 0; i < arr.access_rights.length; i++) {
                $form.find('[value="' + arr.access_rights[i] + '"]').prop('checked', true).closest('label').removeClass('btn-default').addClass('active btn-primary');
            }
        } else {
            $form.find('[name="access_rights[]"]').prop('checked', true).closest('label').removeClass('btn-default').addClass('active btn-primary');
        }


        $form.closest('.modal').modal('show');
        console.log(arr);


    });

    //load #myActivityLogModal
    var $myActivityLogModal = $('#myActivityLogModal');
    var activityLogged = $myActivityLogModal.data('logged');
    if (activityLogged != 'Y') {
        $myActivityLogModal.modal('show');
    }

    $(document).on('click', '.customer-update', function() {
        var $self = $(this);
        var id = $self.data('id');
        var arr = $self.data('arr');
        var $form = $('#customer_form');

        $form.find('[name="id"]').val(id);
        $form.find('[name="first"]').val(arr.first_name);
        $form.find('[name="last"]').val(arr.last_name);
        $form.find('[name="brokerage_id"]').val(arr.brokerage_id);
        $form.find('[name="email"]').val(arr.email);
        $form.find('[name="brokerage_id"]').selectpicker('refresh');
        $form.find('[name="agency_id"]').val(arr.agency_id);
        $form.find('[name="agency_id"]').closest('.form-group').hide();

        if (arr.access_rights.length > 0) {
            $form.find('[name="access_rights[]"]').prop('checked', false).closest('label').addClass('btn-default').removeClass('active btn-primary');
            for (var i = 0; i < arr.access_rights.length; i++) {
                $form.find('[value="' + arr.access_rights[i] + '"]').prop('checked', true).closest('label').removeClass('btn-default').addClass('active btn-primary');
            }
        } else {
            $form.find('[name="access_rights[]"]').prop('checked', true).closest('label').removeClass('btn-default').addClass('active btn-primary');
        }


        $form.closest('.modal').modal('show');
        console.log(arr);


    });


    $(document).on('click', '.delete_btn_confirm', function() {
        var $self = $(this);
        var $mdl = $('#deleteModal');
        $mdl.find('[name="id"]').val($self.data('id'));
        $mdl.modal('show');
    });

    $('#delete_form').validate({
        ignore: [],
        rules: {
            'delete': {
                required: true,
                equalTo: '#delete2'
            }
        },

        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            //element.closest('div').append(error);
        },
        submitHandler: function(form) {
            $(form).find('[type="submit"]').button('loading');
            var id = $(form).find('[name="id"]').val();
            var $self = $('.delete_btn_confirm[data-id="' + id + '"]');
            var table = $self.data('table');
            console.log(id, $self);
            //deleteRecord($self, id, table);

            var mydataTb = $('.mydataTb').DataTable();
            $.post(
                base_url + 'formsubmits/delete_tb', {
                    id: id,
                    table: table
                },
                function(res) {
                    console.log(res);
                    $self.closest('tr').addClass('bg-danger').fadeOut('fast', function() {
                        $('.modal.in').modal('hide');
                        $(form).find('[type="submit"]').button('reset');
                        $(form).find('[name="delete"]').val('');

                        mydataTb.destroy();
                        $.when($(this).remove()).then(function() {

                            mydataTb = $('.mydataTb').DataTable({
                                "bPaginate": true,
                                "bLengthChange": false,
                                "bFilter": false,
                                "bInfo": true
                            });
                        });

                    });
                }

            ).error(function(err) {
                console.log(err);
            });

        }
    });



    $(document).on('click', '.view_details_btn', function() {
        var $mdl = $('#infoModal');
        var $self = $(this);
        var info = $self.data('info');
        var $container = $mdl.find('.the-info');
        console.log(info);

        var mess = 'Name: ' + info.company_name;
        mess += '<br>Address: ' + info.address;
        mess += '<br>Date added: ' + info.date_added;
        mess += '<br><br><strong>Primary contact</strong><br>First Name: ' + info.first;
        mess += '<br>Last: ' + info.last;
        mess += '<br>Email: ' + info.email;
        mess += '<br>Phone: ' + info.phone;
        // mess += '<br>Unique Link: ' + base_url + 'landing/brokerform/' + info.id;
        $container.html(mess);
        //		for (var key in info) {
        //		  if (info.hasOwnProperty(key)) {
        //			$mdl.find('[name="'+key+'"]').val(info[key]);
        //			console.log(key + " -> " + info[key]);
        //		  }
        //		}
        $mdl.modal('show');
    });


    $(document).on('click', '.brokerage-update', function() {
        var $self = $(this);
        var id = $self.data('id');
        var arr = $self.data('info');
        var $form = $('#brokerage_form');
        $form.find('[name="id"]').val(id);
        $form.find('[name="company_name"]').val(arr.company_name);
        $form.find('[name="address"]').val(arr.address);
        $form.find('[name="lat"]').val(arr.lat);
        $form.find('[name="lng"]').val(arr.lng);
        $form.find('[name="first"]').val(arr.first);
        $form.find('[name="last"]').val(arr.last);
        $form.find('[name="email"]').val(arr.email);
        $form.find('[name="phone"]').val(arr.phone);
        //$form.find('.row-prime-contact').hide();

        $form.closest('.modal').modal('show');
        console.log(arr);
    });


    $('#brokerage_form').validate({
        ignore: [],
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-warning');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-warning');
        },
        errorElement: 'span',
        errorClass: 'help-block',

        errorPlacement: function(error, element) {
            element.closest('.form-group').append(error);
        },
        submitHandler: function(form) {

            $(form).find('button[type="submit"]').button('loading');

            $.post(
                base_url + 'webmanager/agency/save_brokerage', {
                    data: $(form).serializeArray()
                },
                function(res) {
                    console.log(res);
                    window.location.reload(true);
                    //$(form).find('button[type="submit"]').button('reset');
                },
                'json'
            ).error(function(err) {
                console.log(err);

                $(form).find('button[type="submit"]').button('reset');
            });


        }
    });

    $('.price-text').on('click', function() {
        var $self = $(this);
        $self.addClass('hidden');
        $self.siblings().removeClass('hidden');
        $self.siblings().find('[name="cargo-price"]').focus();
    });

    $('.price_form').find('a').on('click', function() {
        $(this).closest('form').addClass('hidden').siblings().removeClass('hidden');
        return false;
    });

    $('.price_form').each(function(index, element) {
        $(this).validate({
            rules: {
                'cargo-price': {
                    required: true,
                    number: true
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                // element.closest('.form-group').append(error);
            },
            submitHandler: function(form) {

                $(form).find('button').addClass('disabled');
                var newval = $(form).find('[name="cargo-price"]').val();
                console.log($(form).serialize());
                $.post(
                    base_url + 'formsubmits/cargo_price_agency',
                    $(form).serialize(),
                    function(res) {
                        console.log(res);
                        $(form).addClass('hidden').siblings().removeClass('hidden').html(newval);
                        $(form).find('button').removeClass('disabled');
                    },
                    'json'
                ).error(function(err) {
                    console.log(err);
                });


            }
        });


    });



    $('#password_form').validate({
        ignore: [],
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            //element.closest('div').append(error);
        },
        submitHandler: function(form) {
            $(form).find('button').button('loading');
            $.post(
                base_url + 'formsubmits/activity_logged_pass',
                $(form).serialize(),
                function(res) {
                    console.log(res);
                    if (res.result == 'match') {
                        window.location.reload(true);
                    } else {
                        bootbox.alert('Password didn\'t match. Please try again.');
                        $(form).find('button').button('reset');
                    }
                },
                'json'
            ).error(function(err) {
                console.log(err);
            });
        }
    });

    $('#zone_multi_form').validate({
        rules: {
            'zone[]': {
                required: true,
                number: true
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            //element.closest('div').append(error);
        },
        submitHandler: function(form) {
            $(form).find('button').button('loading');
            $.post(
                base_url + 'webmanager/cargo/save_zone',
                $(form).serialize(),
                function(res) {
                    console.log(res);
                    $(form).find('button').button('reset');
                    $(form).closest('.modal').modal('hide');
                },
                'json'
            ).error(function(err) {
                console.log(err);
            });
        }
    });

    $('[name="ded_default[]"]').on('change', function() {
        var $self = $(this);
        var agency_id = $self.data('agency');
        var id = $self.val();
        if ($self.is(':checked')) {
            $self.closest('tr').siblings().find('input[type="checkbox"]').prop('checked', false);
            $.post(
                base_url + 'webmanager/cargo/deductible_default', {
                    agency_id: agency_id,
                    id: id
                },
                function(res) {
                    console.log(res);
                },
                'json'
            ).error(function(err) {
                console.log(err);
            });
        }
    });

    $('.table-datatable').dataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": false,
        "bInfo": true
    });

    $('.activitylog-datatable').dataTable({
        "bPaginate": true,
        "bLengthChange": true,
        "pageLength": 50,
        "bFilter": false,
        "bInfo": true
    });


    $('.review_policy_btn').on('click', function(e) {
        var $self = $(this);
        var id = $self.data('id');
        var premium = $self.data('premium');
        var comment = $self.data('comment');
        var $mdl = $('#reviewModal');
        $mdl.find('[name="id"]').val(id);
        $mdl.find('[name="premium"]').val(premium);
        $mdl.find('[name="comment"]').html(comment);
        $mdl.modal('show');
        e.preventDefault();
    });



    $('#about_form').on('submit', function() {
        var about_content = $("#summernote").code();
        $self = $(this);
        $self.find('button.btn-sky').addClass('disabled').html('Saving changes..');

        $.post(
            base_url + 'webmanager/pages/save_about', {
                about_content: about_content
            },
            function(res) {
                console.log(res);
                $self.find('button.btn-sky').html('Saved!!');

                setTimeout(function() {
                    $self.find('button.btn-sky').removeClass('disabled').html('Submit');
                }, 2000);

            }
        );

        return false;
    });

    //uploadPolicyForm
    $("#uploadPolicyForm").ajaxForm({
        beforeSend: function() {
            $(".progress-profile").show();
            //clear everything
            $(".bar-profile").width('0%');
            $(".percent-profile").html("0%");
        },
        uploadProgress: function(event, position, total, percentComplete) {
            $(".bar-profile").width(percentComplete + '%');
            $(".percent-profile").html(percentComplete + '%');


        },
        success: function() {
            $(".bar-profile").width('100%');
            $(".percent-profile").html('100%');

        },
        complete: function(response) {
            if (response.responseText == 'not_img') {
                $(".progress-profile").hide();
                bootbox.alert('<p class="lead">File selected is not a valid file.</p>');

            } else {
                $('.policydoc').html('<a href="' + base_url + 'uploads/policyd/' + response.responseText + '" target="_blank" class=""><i class="fa fa-file-pdf-o"></i> ' + response.responseText.substring(13) + '</a> <small><i class="fa fa-times-circle text-muted delete_policy_btn"></i></small>');
            }
            console.log(response.responseText);

        },
        error: function() {
            bootbox.alert('<h4> Unable to upload file. Please refresh and try again.</h4>');
        }

    });
    //end ajax file upload

    $(document).on('click', '.delete_policy_btn', function() {
        bootbox.confirm('<h4 class="modal-title">Confirm you wish to delete policy document.</h4>', function(e) {
            if (e) {
                $.post(
                    base_url + 'webmanager/policydoc/deletedoc', {},
                    function(res) {
                        console.log(res);
                        $('.policydoc').html('');
                    }
                );
            }

        });
        return false;
    });


    $('.summernote').summernote({
        height: 370,
        oninit: function() {
            //$("div.note-editor button[data-event='codeview']").click();
        }
    });


    $('.save_contents_btn').on('click', function() {
        var $self = $(this);
        var field = $self.data('field');
        var title = $self.data('title');
        var text = $self.closest('div').find('.summernote').code();
        $self.button('loading');
        console.log(text);
        $.post(
            base_url + 'webmanager/insurance/savetexts', {
                text: text,
                field: field
            },
            function(res) {
                console.log(res);
                bootbox.alert('<h4 class="modal-title">Changes to ' + title + ' successully saved!</h4>');
                $self.button('reset');
            },
            'json'
        ).error(function(err) {
            console.log(err);
        });

        return false;
    });

    $('.geolocation').geocomplete()
        .bind("geocode:result", function(event, result) {

            var location_lat = result.geometry.location.lat();
            var location_long = result.geometry.location.lng();

            var $form = $(this).closest('form');

            $form.find('[name="lat"]').val(location_lat);
            $form.find('[name="lng"]').val(location_long);

            console.log(result);

        });



    $('.save_certificate_btn').on('click', function() {
        var $self = $(this);
        var text = $self.closest('div').find('.summernote').code();
        $self.button('loading');
        //console.log(cert);
        $.post(
            base_url + 'webmanager/insurance/savetexts', {
                text: text,
                field: 'certificate_template'
            },
            function(res) {
                console.log(res);
                bootbox.alert('Changes to certificate template successully saved!');
                $self.button('reset');
            },
            'json'
        ).error(function(err) {
            console.log(err);
        });
        return false;
    });


    $('.filter-portcode').find('a').on('click', function(e) {
        var $self = $(this);
        var field = $self.data('field');
        $self.closest('form').find('[name="search_by"]').val(field);
        $self.closest('li').addClass('active').siblings().removeClass('active');
        e.preventDefault();
    });

});
