//Used in form validation - if the form is ready to be submitted (no errors), will go ahead and submit it.
//Otherwise, won't submit the form
var goahead = true;
var whattheheck = true;
var submitForm = true;

//Swap image
function swap(d) {
	if (!d[0].osrc) d[0].osrc = d[0].src; // keep an old copy
	var src = d[0].osrc;

	d[0].src = src.replace(/\.png/, '-mo.png'); // use a pattern
	d[0].src = d[0].src.replace(/\.gif/, '-mo.gif');
	d[0].onmouseout = function() {
		d[0].src = d[0].osrc; // restore
	};

}

function getURLParameter(name) {
    return decodeURI(
        (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,null])[1]
    );
}

function hasAttribute(element,attribute) {
	var attr = element.attr(attribute);
	
	if (typeof attr !== 'undefined' && attr !== false) {
		return true;
	}
	
	return false;
}

function switchAccounts(link, redirect) {
	$.ajax({
		type: "POST",
		url: link,
		success: function(data) {  
			window.destinationHref = redirect;
			window.location.href = redirect; 	
		}
	});
}

function submitWithMember(formId, userId)
{
	var form = $("#" + formId);
	form.attr('action', form.val() + "?memberUserId=" + userId);

	form.submit();
}

function remove_comment(commentId)
{
	var form = $("#epact-deleteusercomment-form");

	var input = $(form).find("input[name='commentId']");

	input.val(commentId);

	$(form).ajaxSubmit( function() {
		location.reload();
	});
}

function toggle_advanced_filter()
{
	var form = $('#advanced-filter-form');
	var status = form.css('display');

	if(status == 'none')
	{
		$("#advanced-filter-form-link span:first").html('&#9660;');
		form.fadeIn(400, function() {
			form.css('display', 'block');
		});
		$('#basic-filter-form').hide();
		$("#advanced-filter-form-link-container").css("margin-left","56px");
		//$("#advanced-filter-form-title").show();
		$('#advancedSearchTerm-search').val($("#searchTerm-search").val());
	}
	else
	{
		$("#advanced-filter-form-link span:first").html('&#9658;');
		form.css('display', 'none');
		$('#orgMenu-search').css('display', 'block');
		//$("#advanced-filter-form-title").hide();
		$("#advanced-filter-form-link-container").css("margin-left","56px");
		$('#basic-filter-form').fadeIn(400, function() { $('#basic-filter-form').css("display", "block"); } );
		if($("#advanced-filter-form-link").data('advanced') == true)
		{
			$('#remove-option').show();
		}
		if($("#status.advancedOrgListMultiSelect input[type='checkbox']:checked").length > 1)
		{
			$('#status.orgListSelect').val("ALL");
		}
	}
}

function show_advanced_filter()
{
	var form = $('#advanced-filter-form');
	var status = form.css('display');
	if(status == 'none')
	{
		$("#advanced-filter-form-link span:first").html('&#9660;');
		form.fadeIn(400, function() {
			form.css('display', 'block');
		});
		$('#basic-filter-form').hide();
		$("#advanced-filter-form-link-container").css("margin-left","0px");
		$("#advanced-filter-form-title").show();
		$('#advancedSearchTerm-search').val($("#searchTerm-search").val());
	}
}

function add_medication_column(row, name, size, span)
{
	var column = $('<div style="display:inline-block; vertical-align:top;">');

	column.appendTo(row);

	var group = $('<div id="control-for-'+name + size+'" class="control-group control-for-'+name + size+'">');

	group.appendTo(column);

	$('<label class="control-label"></label>').appendTo(group);

	var controls = $('<div class="controls">');

	controls.appendTo(group);

	$('<input class="ignore" type="text" name="' + name + size + '" data-index="'+size+'" />').appendTo(controls);
}

function remove_medication(link, index)
{
	var len = 0;

	$.each($("#medications-list .medication-row"), function(index, value) {
		if($(value).css('display') != 'none')
			len++;
	});

	var div = $(link).closest("div");

	var row = $(div).closest(".medication-row");

	if( ($(row).find("input[value='new']").length) )
	{
		$(row).remove();
	}
	else
	{
		$('<input class="ignore" type="hidden" name="deleteMed' + index + '" value="true" />').appendTo(div);

		row.css("display", "none");
	}

	if(len <= 1)
	{
		add_medication();
	}

	$(".btn-save").html("Save");     		
	$(".btn-save").removeAttr("disabled");
	$(".save-next,.save-next-incomplete").hide(); 
}

function add_medication()
{
	var div = $("#medications-list");

	var size = $("#medications-list > .medication-row").size() + 1;

	var row = $('<div class="medication-row row med-index-'+size+'">');

	row.appendTo(div);
	var wrap = $('<div>');
	$('<input type="hidden" value="new" name="'+name+'Id' + size + '" />').appendTo(wrap);
	wrap.appendTo(row);

	add_medication_column(row, "medication", size, 3);

	add_medication_column(row, "dose", size, 3);

	add_medication_column(row, "instructions", size, 3);

	var col = $('<div class="remove-medication" style="display:inline-block; float:right;">');

	$('<span data-index="'+size+'"><i class="glyphicon glyphicon-remove" style="margin-top: 10px"></i></span>"').appendTo(col);

	$(col).appendTo(row);

	$("#medications-list-header").css('display', 'block');

	$(".btn-save").html("Save");     		
	$(".btn-save").removeAttr("disabled");
	$(".save-next,.save-next-incomplete").hide(); 

	$("input[name^=dose], input[name^=instructions]").unbind("input");
	$("input[name^=dose], input[name^=instructions]").bind("input", function(){
		var medicationRow = $(this).data("index");
		if ($(".med-index-"+medicationRow+" input[name=dose"+medicationRow+"]").val() || $(".med-index-"+medicationRow+" input[name=instructions"+medicationRow+"]").val())
		{
			$(".med-index-"+medicationRow+" input[name=medication"+medicationRow+"]").attr("required","true");
		} 
		else
		{
			$(".med-index-"+medicationRow+" input[name=medication"+medicationRow+"]").removeAttr("required");
		}
	});
	$(".remove-medication span").unbind("click").click(function(e){
		var index = $(this).data("index");
		if (index && index != null)
			remove_medication(this, index);
	});
}



/*function save_form_to_session(formId)
{
	var form = $("#" + formId);
	var input = $("<input>").attr("type", "hidden").attr("name", "saveToSession").val("true");
	form.append(input);

         $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: form.serialize(),
            async: false
         });
}*/

function setCookie(c_name,value,exTime)
{

	var exdate=new Date();
	exdate.setTime(exdate.getTime() + exTime);


	var c_value=escape(value) + ((exTime==null) ? "" : ";  expires="+exdate.toUTCString());
	document.cookie=c_name + "=" + c_value;
}

//When head.js is ready
function checkTimezone() {
	//calculate the offset with GMT-0

	var dTimezone = new Date() ;
	var iDiffMinutes = dTimezone.getTimezoneOffset();

	var tz = jstz.determine(); // Determines the time zone of the browser client

	//Store the offset on a cookie

	setCookie("ePact-TZ",tz.name(),900000); // 15 minutes
}

function fadeAlertMessages() {
	window.fadeAlert = setTimeout(function() {
		$('.alert-danger:not(.no-fade), .alert-success:not(.no-fade), .alert-error:not(.no-fade)').slideUp();
	}, 7000 );
}

head.ready(function() {

	// Make sure the dom is ready
	jQuery(document).ready(function($) {


		/*=============================================
        =            Epact Module                     =
        =============================================*/
		var epact = {

				// Define init method
				init: function() {
					/**
					 *
					 * Wire up rss feed functionality
					 *
					 **/
					var showPicker = false;
					checkTimezone();

					/*=============================================
                  =            Legacy JS                        =
                  =============================================*/

					if (!($('body').hasClass("corporate") || $('body').hasClass("landing") || $('body').hasClass("login") || $('body').hasClass("register") || $('body').hasClass("registerConfirm")
							|| $('body').hasClass("forgotPassword") || $('body').hasClass("resetPassword") || $('body').hasClass("accountLockout") || $('body').hasClass("getStarted") || $('body').hasClass("orgExpressInterest")
							|| $('body').hasClass("resetPasswordSent") || $('body').hasClass("forgotPasswordUpdate") || $('body').hasClass("permissionDenied") || $('body').hasClass("unactivated") 
							|| $('body').hasClass("oldInvite") || $('body').hasClass("forgotPasswordSubmit") || $('body').hasClass("forgotPasswordUpdateSubmit"))) {
						// Initializes Bootstrap modal manager
						$('body').modalmanager();

						// Sets max height of modals
						$.fn.modal.defaults.maxHeight = function(){
							// subtract the height of the modal header and footer
							return $(window).height() - 300; 
						};					}
		
					$(".medical-dropdown").change(function() {
						if ($(this).val() == "optclinic") {
							$("#form-clinic").show();
							$("#form-doctor").hide();
							$("#medical-form-submit").show();
						} else if ($(this).val() == "optdoctor") {
							$("#form-clinic").hide();
							$("#form-doctor").show();
							$("#medical-form-submit").show();
						} else if ($(this).val() == "optselect") {
							$("#form-clinic").hide();
							$("#form-doctor").hide();
							$("#medical-form-submit").hide();
						}
					});

					// forces IE not to cache results
					$.ajaxSetup({
						cache: false
					});

					var $body = $(window.document.body);

					function displaySavePopup(formName, isComplete) {

						var additionalInfo = "";

						// Display incomplete message
						if (!(isComplete)) {
							additionalInfo = languageStrings.required_information_missing;
						} 

						if ((formName == "#epact-dependant-contact-form") || (formName == "#epact-contactinfo-form")) {
							$(".save-message").html("<span class='glyphicon glyphicon-ok-sign'></span> <span class='save-alert-message'>" + languageStrings.contact_info_saved + "</span>");
						} else if ((formName == "#epact-details-form") || (formName == "#epact-dependant-details-form")) {
							$(".save-message").html("<span class='glyphicon glyphicon-ok-sign'></span> <span class='save-alert-message'>" + languageStrings.details_info_saved + "</span>");         	  		
						} else if ((formName == "#epact-medicalinfo-form") || (formName == "#epact-dependant-medical-form")) {
							$(".save-message").html("<span class='glyphicon glyphicon-ok-sign'></span> <span class='save-alert-message'>" + languageStrings.medical_info_saved + " " + additionalInfo + "</span>");
						} else if ((formName == "#epact-dependant-guardian-add-form") || (formName == "#epact-guardian-add-form")) {
							$(".save-message").html("<span class='glyphicon glyphicon-plus-sign'></span> <span class='save-alert-message'>" + languageStrings.legal_guardian_added + "</span>");
						} else if ((formName == "#epact-dependant-altguardian-add-form") || (formName == "#epact-create-emergency-contact-form") || 
								((formName == "#epact-adult-add-form") && ($("body").hasClass("dependantContacts")))) {
							$(".save-message").html("<span class='glyphicon glyphicon-plus-sign'></span> <span class='save-alert-message'>" + languageStrings.emergency_contact_added + "</span>");
						} else if ((formName == "#epact-create-out-of-area-contact-form") || (formName == "#epact-dependant-outofarea-add-form")) {
							$(".save-message").html("<span class='glyphicon glyphicon-ok-sign'></span> <span class='save-alert-message'>" + languageStrings.ooa_contact_added + "</span>"); 	  	
						} else if (formName == "#epact-dependant-contacts-form") {
							$(".save-message").html("<span class='glyphicon glyphicon-ok-sign'></span> <span class='save-alert-message'>" + languageStrings.contacts_info_saved + "</span>"); 	  	
						} else if (formName == "familyWizard") {
							$(".save-message").html("<span class='glyphicon glyphicon-ok-sign'></span> <span class='save-alert-message'>" + languageStrings.family_info_saved + "</span>");	  	
						} else {
							return false;
						}

						$(".save-alert").slideDown("fast", function() {
							setTimeout(function(){
								$(".save-alert").slideUp();
							},7000);
						});
					}

					function bodyFreezeScroll() {
						var bodyWidth = $body.innerWidth();
						$body.css('overflow', 'hidden');
						$body.css('marginRight', ($body.css('marginRight') ? '+=' : '') + ($body.innerWidth() - bodyWidth));
					}

					function bodyUnfreezeScroll() {
						var bodyWidth = $body.innerWidth();
						$body.css('marginRight', '-=' + (bodyWidth - $body.innerWidth()));
						$body.css('overflow', 'auto');
					}

//					$(".modal").on('show', function () {
//					var top = $("body").scrollTop(); $("body").css('position','fixed').css('overflow','hidden').css('top',-top).css('width','100%').css('height',top+5000);
//					}).on("hide.bs.modal", function () {
//					var top = $("body").position().top; $("body").css('position','relative').css('overflow','auto').css('top',0).scrollTop(-top);
//					});

					$.validator.setDefaults({

						errorPlacement: function(error, element) {
							if (element.attr('type') === 'radio') {
								error.insertAfter(
										element.siblings('.radio-error'));
							} else if ((element.attr('id') == "dobMonths") || (element.attr('id') == "dobDays") || (element.attr('id') == 'dobYears')) {
								$("#dobDateError").show();
							} else if (element.attr('name') == "healthNum") {
								error.insertAfter('.inline-healthNum');  
							} else if (element.attr('name') == "primaryPhoneIsMobile-select") {
								if (!($("label[for=primaryPhone].error").length) || !($("label[for=primaryPhone].error").is(":visible"))) {
									$(".primaryPhone-mobileTypeError").show();
								}
							}
							else if (element.attr('name') == "secondaryPhoneIsMobile-select") {
								if (!($("label[for=secondaryPhone].error").length) || !($("label[for=secondaryPhone].error").is(":visible"))) {
									$(".secondaryPhone-mobileTypeError").show();
								}
							}
							else if (element.attr('name') == "primaryPhoneOtherIsMobile-select") {
								if (!($("label[for=primaryPhoneOther-field].error").length) || !($("label[for=primaryPhoneOther-field].error").is(":visible"))) {
									$(".primaryPhoneOther-mobileTypeError").show();
								}
							}
							else if (element.attr('name') == "secondaryPhoneOtherIsMobile-select") {
								if (!($("label[for=secondaryPhoneOther-field].error").length) || !($("label[for=secondaryPhoneOther-field].error").is(":visible"))) {
									$(".secondaryPhoneOther-mobileTypeError").show();
								}   
							} else if (element.attr('name') == "altWorkPhoneNumberIsMobile-select") {
								if (!($("label[for=altWorkPhoneNumber-field].error").length) || !($("label[for=altWorkPhoneNumber-field].error").is(":visible"))) {
									$(".altWorkPhoneNumber-mobileTypeError").show();
								}     		 
							} else if ((element.attr('name') == "campaignStartDate") || (element.attr('name') == "campaignEndDate")) {
								error.insertAfter('#' + element.attr('name') + 'Display');
							} else if ((element.attr('name') == "primaryPhone") || (element.attr('name') == "secondaryPhone") || (element.attr('name') == "secondaryPhoneOther")
									|| (element.attr('name') == "primaryPhoneOther") || (element.attr('name') == "altWorkPhoneNumber")){
								error.insertAfter(
										element.siblings('select.phoneType'));
								$("#mobileTypeError").hide();
							} else if ((element.attr('id') == "tetanusMonths") || (element.attr('id') == "tetanusDays") || (element.attr('id') == 'tetanusYears')) {
								$("#tetanusDateError").show();
							} else if ((element.attr('id') == "physicalMonth") || (element.attr('id') == "physicalDay") || (element.attr('id') == 'physicalYear')) {
								$("#physicalDateError").show();
							} else if (element.attr('name') == "acceptTerms")  {
								error.insertAfter('#terms-text');
							} else if ((element.attr('name') == "email1") || (element.attr('name') == "email2") || (element.attr('name') == "email3")) {
								error.insertAfter('#' + element.attr('name') + '-badge');
							} else if(element.attr("class").indexOf("datepickeralt") >= 0) {
								error.insertAfter(element.siblings("button"));
							} else {
								error.insertAfter(element);
							}
						}
					});

					$("select.phoneType").change(function() {
						var phoneType = "." + $(this).data("mobilefor") + "-mobileTypeError";
						$(this).parents("form").find(phoneType).hide();  
					});
					
					// woohoo calendar fun
					$('.datepicker').each(function() {
						var timestamp = parseInt($(this).val());
						var alt = $(this).closest('div').find('.datepickeralt'); // use an alt display so we can save in timestamp format
						$(this).datepicker({
							showOn: 'button',
							buttonText: '<span class="glyphicon glyphicon glyphicon-calendar"></span>',
							altField: alt,
							altFormat: "MM dd, yy",
							dateFormat: "@",
							changeMonth: true,
							changeYear: true,
							yearRange: "-100:+0",
							maxDate: new Date(),
							onClose: function(dateText, inst) {
						      showPicker = false;
						    }
						});
						if(timestamp && timestamp != NaN)
						{
							// set the initial value for the display
							$(alt).val($.datepicker.formatDate("MM dd, yy", new Date(timestamp)));

						}
						var picker = $(this);
						$(alt).click(function(eventObj) {
							picker.datepicker('show');
							$(alt).blur();
						});
					});
					
					$(".phoneType").change(function() {

						var checkboxName = $(this).data("mobilecheckbox");

						if ($(this).val() == "mobile") {
							$(this).parents("form").find("input[name=" + checkboxName + "]").prop('checked',true);
						} else {
							$(this).parents("form").find("input[name=" + checkboxName + "]").prop('checked',false);
						}
					});

					$(".addAnotherNumber").click(function () {
						if (!($(this).attr("disabled"))) {
							$(this).parents("form").find("#control-for-secondaryPhone").show();
							$(this).hide();
							$(this).parents("form").find(".secondaryPhoneIsMobile-select").attr("notEqual","select");
							$(this).parents("form").find("input[name=secondaryPhone]").attr("required","");
						}
					});

					$(".sharedAddress").on("change keypress paste", function(){   
						$(this).parents("form").addClass("changedAddress");
					});

					$(".sharedAddress").keydown(function(e){
						// Delete or backspace key
						if(e.keyCode == 46 || e.keyCode == 8) {
							$(this).parents("form").addClass("changedAddress");
						}
					});

					// Enables/disables the add another number button
					$("input[name=primaryPhone]").on("input", function(){
						if ($(this).val() != "") {
							$(this).parents("form").find(".addAnotherNumber").removeAttr("disabled");
						} else {
							$(this).parents("form").find(".addAnotherNumber").attr("disabled");
						}
					});


					$("input[name=primaryPhoneOther]").each(function() {

						if ($(this).parents("form").attr("id") == "epact-adult-add-form") {
							$(this).attr("required","");
						}

						if (($(this).val() != "") || ($(this).parents("form").attr("id") == "epact-adult-add-form")){
							$(this).parents("form").find("input:radio[name=primaryPhone][value ='other']").prop('checked', true);
							$(this).parents("form").find(".primaryPhoneOtherIsMobile-select").attr("notEqual","select");
						} else {
							$(this).parents("form").find("input:radio[name=primaryPhone]:first").prop('checked', true);
							$(this).parents("form").find(".primaryPhoneOtherIsMobile-select").removeAttr("notEqual");
						}
					});

					$("input[name=primaryPhoneOther]").on("input", function(){
						if ($(this).val() != "") {
							$(this).parents("form").find("input:radio[name=primaryPhone][value ='other']").prop('checked', true);
							$(this).parents("form").find(".primaryPhoneOtherIsMobile-select").attr("notEqual","select");
							$(this).parents("form").find(".dependantAddAnotherNumber").removeAttr("disabled");
						} else {
							$(this).parents("form").find("input:radio[name=primaryPhone]:first").prop('checked', true);
							$(this).parents("form").find(".primaryPhoneOtherIsMobile-select").removeAttr("notEqual");
							$(this).parents("form").find(".dependantAddAnotherNumber").attr("disabled","");
						}
					});

					$("input[name=secondaryPhone]").each(function() {
						if ($(this).val() != "") {
							$(this).parents("form").find("input:radio[name=secondaryPhone][value ='']").prop('checked', true);
							$(this).parents("form").find(".secondaryPhoneIsMobile-select").attr("notEqual","select");
						} else {
							$(this).parents("form").find("input:radio[name=secondaryPhone][value ='none']").prop('checked', true);
							$(this).parents("form").find(".secondaryPhoneIsMobile-select").removeAttr("notEqual");
						}
					});

					$("input[name=secondaryPhone]").on("input", function(){
						if ($(this).val() != "") {
							$(this).parents("form").find("input:radio[name=secondaryPhone][value ='']").prop('checked', true);
							$(this).parents("form").find(".secondaryPhoneIsMobile-select").attr("notEqual","select");
						} else {
							$(this).parents("form").find("input:radio[name=secondaryPhone][value ='none']").prop('checked', true);
							$(this).parents("form").find(".secondaryPhoneIsMobile-select").removeAttr("notEqual");
						}
					});

					$("input[name=secondaryPhoneOther]").each(function() {
						if ($(this).val() != "") {
							if ($(this).parents("form").find(".contextual-work").length) {
								$(this).parents("form").find(".contextual-work").css("margin-top","-40px");
							}
							$(this).parents("form").find("input:radio[name=secondaryPhone][value ='other']").prop('checked', true);
							$(this).parents("form").find(".secondaryPhoneOtherIsMobile-select").attr("notEqual","select");
						} else {
							$(this).parents("form").find("input:radio[name=secondaryPhone][value ='none']").prop('checked', true);
							$(this).parents("form").find(".secondaryPhoneOtherIsMobile-select").removeAttr("notEqual");
						}
					});

					$("input[name=secondaryPhoneOther]").change(function() {
						if ($(this).val() != "") {
							$(this).parents("form").find("input:radio[name=secondaryPhone][value ='other']").prop('checked', true);
							$(this).parents("form").find(".secondaryPhoneOtherIsMobile-select").attr("notEqual","select");
						} else {
							$(this).parents("form").find("input:radio[name=secondaryPhone][value ='none']").prop('checked', true);
							$(this).parents("form").find(".secondaryPhoneOtherIsMobile-select").removeAttr("notEqual");
						}
					});

					// Shows/hides the same address text field in forms
					$('#filter-status .orgListSelect').change(function() {
						var filterOption = $(this).attr("name");
						var filterValue = document.getElementById(filterOption).options[document.getElementById(filterOption).selectedIndex].value;

						var form = $('#epact-filter-org-member-list-form, #epact-filter-org-group-list-form, #epact-filter-org-admin-list-form');

						form.attr('action', form.val() + "?" + filterOption + "=" + filterValue);
						form.submit();
					});

					$('#filetype-dropdown').change(function() {
						if($(this).val() == 'OTHER')
						{
							$('input[name="fileTypeOtherValue"]').css("display", "inline");
						}
						else
						{
							$('input[name="fileTypeOtherValue"]').css("display", "none");
						}
					});

					if($('#filetype-dropdown').val() != "OTHER")
					{
						$('input[name="fileTypeOtherValue"]').css("display", "none");
					}

					$("input[name=altWorkPhoneNumber]").each(function() {
						if ($(this).val() != "") {
							$(this).parents("form").find("input:radio[name=workPhone][value ='other']").prop('checked', true);
							$(this).parents("form").find(".altWorkPhoneNumberIsMobile-select").attr("notEqual","select");
						} else {
							$(this).parents("form").find(".altWorkPhoneNumberIsMobile-select").removeAttr("notEqual");
						}
					});

					$("input[name=altWorkPhoneNumber]").change(function() {
						if ($(this).val() != "") {
							$(this).parents("form").find("input:radio[name=workPhone][value ='other']").prop('checked', true);
							$(this).parents("form").find(".altWorkPhoneNumberIsMobile-select").attr("notEqual","select");
						} else {
							$(this).parents("form").find(".altWorkPhoneNumberIsMobile-select").removeAttr("notEqual");
						}
					});			
					
					//$('#epact-dependant-medical-form').validate();

					$('#epact-dependant-clinic-add-form').validate();
					$('#epact-dependant-clinic-add-form').ajaxForm(function() { 
						if (goahead == true) {
							var redirect = document.URL + " #medical-providers";
							document.getElementById('success-message').style.display = 'block';
							document.getElementById('success-message').innerHTML=languageStrings.clinic_add_success;

							$('#manage-medicalproviders').modal('hide'); 
							$('#medical-providers').load(redirect);
							document.getElementById('success-message').scrollIntoView()
						} 
					}); 

					$('#epact-dependant-doctor-add-form').validate();
					$('#epact-dependant-doctor-add-form').ajaxForm(function() { 
						if (goahead == true) {
							var redirect = document.URL + " #medical-providers";
							document.getElementById('success-message').style.display = 'block';
							document.getElementById('success-message').innerHTML=languageStrings.doctor_add_success;
							$('#manage-medicalproviders').modal('hide'); 
							$('#medical-providers').load(redirect);
							document.getElementById('success-message').scrollIntoView()
						} 
					});
					
					$(".dependantAddAnotherNumber").click(function() {

						if (!($(this).attr("disabled"))) {
							if ($(this).parents("form").find(".contextual-work").length) {
								$(this).parents("form").find(".contextual-work").css("margin-top","-40px");
							}

							$(this).parents("form").find("#control-for-secondaryPhoneOther").show();
							$(this).parents("form").find(".secondaryPhoneOtherIsMobile-select").attr("notEqual","select");
							$(this).parents("form").find("input[name=secondaryPhoneOther]").attr("required","");
							$(this).hide();
						}
					});

					$("input[type=text][name=secondaryPhone]").each(function() {
						if ($(this).val() == "") {
							$(this).parents("form").find("#control-for-secondaryPhone").hide();
						} else {
							$(this).parents("form").find(".addAnotherNumber").hide();
							$(this).parents("form").find("#control-for-secondaryPhone").show();
						}
					});

					$("input[type=text][name=secondaryPhoneOther]").each(function() {
						if ($(this).val() == "") {
							$(this).parents("form").find("#control-for-secondaryPhoneOther").hide();
						} else {
							$(this).parents("form").find(".dependantAddAnotherNumber").hide();
							$(this).parents("form").find("#control-for-secondaryPhoneOther").show();
						}
					});

					$(".removeNumber").click(function (e) {
						e.preventDefault();
						$(this).parents("form").find(".secondaryPhoneIsMobile-select").removeAttr("notEqual");
						$(this).parents("form").find(".secondaryPhoneIsMobile-select").val("select");
						$(this).parents("form").find("input[name=secondaryPhone]").val("");
						$(this).parents("form").find("input[name=secondaryPhone]").removeAttr("required");
						$(this).parents("form").find(".control-for-secondaryPhone").hide();
						$(this).parents("form").find(".addAnotherNumber").show();
					});


					$(".removeDependantNumber").click(function (e) {
						e.preventDefault();
						if ($(this).parents("form").find(".contextual-work").length) {
							$(this).parents("form").find(".contextual-work").css("margin-top","-95px");
						}

						$(this).parents("form").find(".secondaryPhoneOtherIsMobile-select").removeAttr("notEqual");
						$(this).parents("form").find(".secondaryPhoneOtherIsMobile-select").val("select");
						$(this).parents("form").find("input[name=secondaryPhoneOther]").val("");
						$(this).parents("form").find("input[name=secondaryPhoneOther]").removeAttr("required");
						$(this).parents("form").find("input[name=secondaryPhoneOther]").val("");
						$(this).parents("form").find("input:radio[name=secondaryPhone][value ='none']").prop('checked', true);
						$(this).parents("form").find(".control-for-secondaryPhoneOther").hide();
						$(this).parents("form").find(".dependantAddAnotherNumber").show();
					});

					$(".edit-mode-content .myepact-person-right, .edit-mode-content .cursor-icon").click(function() {
						$(this).parents(".myepact-person").find(".checkbox-float > input[type=checkbox]").trigger('click');
					});

					$("a.submitWithMember").click(function(e){
						e.preventDefault();
						var fid = $(this).data("fid");
						var uid = $(this).data("uid");
						submitWithMember(fid,uid);
					});
					
					$("a.removeComment").click(function(e){
						e.preventDefualt();
						var cid = $(this).data("cid");
						remove_comment(cid);
					});
					
					$("a.previousPage").click(function(e){
						e.preventDefault();
						history.go(-1);
					});
					
					$("a.preventDefault").click(function(e){
						e.preventDefault();
					});
					
					$('#manage-medicalproviders').on('shown.bs.modal', function() {
					    $("#medical-form-submit").hide();
					});
					
					//TODO: This is temporal, untill we implement the org administrable waivers
					$("#control-for-CAMPERS_COMMITMENT a").click(function(e){
						e.preventDefault();
						$('#campers-message').modal('show');
					});
					$("#control-for-PARENTS_COMMITMENT a").click(function(e){
						e.preventDefault();
						$('#parents-message').modal('show');
					});
					$("#control-for-MEDICAL_AUTHORIZATION a").click(function(e){
						e.preventDefault();
						$('#medical-auth-message').modal('show');
					});
					$("#control-for-AGREES_TO_HEAD_LICE_INFO a").click(function(e){
						e.preventDefault();
						$('#head-lice-message').modal('show');
					});
					$("#control-for-AGREES_TO_HEAD_LICE_INFO a").click(function(e){
						e.preventDefault();
						$('#head-lice-message').modal('show');
					});
					$("#control-for-photo_video_consent_assignment_release a").click(function(e){
						e.preventDefault();
						$('#photo-video-message').modal('show');
					});
					
					$("#remove-photo-yes").click(function() {
						var onWizard = false;
						// If you're accessing the modal from the wizard
						if ($("body").hasClass("details") || $("body").hasClass("contact")) {onWizard = true;}

						$("#epact-removeprofilepic-form").ajaxSubmit(function() {

							// If they're removing their photo on a mobile phone, go back to the previous page after completion
							if (mobilePhone) {
								window.destinationHref =  referer;
								window.location.href = referer; 	
							} else {
								if (onWizard) {
									var redirect = window.location.pathname + " #profile-pic-section";
									$("#profile-pic-section").load(redirect, function() {
										$("#photo-updated").hide();
										$("#photo-hide").show();      
										$("#remove-profile-picture").modal("hide"); 
									});	

								} else {
									window.destinationHref = window.location.href;
									location.reload();
								}
							}


						});
					});

					$('.dropdown-menu').on('click', function(e) {
						if($(this).hasClass('dropdown-menu-form')) {
							e.stopPropagation();
						}
					});     	

					// Activates the upload photo modal
					$("#upload-photo-button-popup").click(function(){

						var referer = $(this).data("referer");
						var onWizard = false;
						// If you're accessing the modal from the wizard
						if ($("body").hasClass("details") || $("body").hasClass("contact")) {onWizard = true;}

						if ($("input[name=imageupload]").length && $("input[name=imageupload]")[0].files.length && $("input[name=imageupload]")[0].files[0].size > 3145728)
						{
							$("#error-profile-pic-size").show();
						}
						else
						{
							$("#epact-profilepic-form").ajaxSubmit({
								beforeSubmit: function() {
									$("body").css("cursor", "progress");
									$(".popup-loader").show();
									$("#success-profile-pic").hide();
									$("#error-profile-pic").hide();
									$("#upload-photo-button-popup").attr("disabled","true");
								},

								error: function() {
									$("#success-profile-pic").hide();
									$("#error-profile-pic").show();
									$("body").css("cursor", "default");
									$("#upload-photo-button-popup").removeAttr("disabled");
									$(".popup-loader").hide();
								},

								success: function(responseText) { 

									if (responseText != "failure") {

										$("#error-profile-pic").hide();
										$("#success-profile-pic").show();

										if (onWizard) {          	             				
											var redirect = window.location.pathname + " #profile-pic-section";
											$("#profile-pic-section").load(redirect, function(){ 
												$("#photo-removed").hide();
												$("#photo-updated").show();      
												$("#change-profile-picture").modal("hide"); 
											}); 

										} else {	
											window.destinationHref = window.location.href;
											location.reload();
										}          	            	    	
									} else {

										$("#error-profile-pic").show();
										$("#success-profile-pic").hide();
									}

									$("body").css("cursor", "default");
									$("#upload-photo-button-popup").removeAttr("disabled");
									$(".popup-loader").hide();
								}  	 
							}); 
						}
					});

					$("#input-upload-profile").change(function() {
						$("#upload-photo-button-popup").removeAttr("disabled");
					});

					$("#upload-photo-button").click(function(){
						document.getElementById("input-upload-profile").click();
					});

					$("#upload-file-button-popup").click(function(){
						$("input[name=fileTypeOtherValue]").addClass("ignore");
						var referer = $(this).data("referer");
						var onWizard = true;
						// If you're accessing the modal from the wizard
						if ($("body").hasClass("details") || $("body").hasClass("contact")) {onWizard = true;}
						if ($("input[name=fileTypeOtherValue]:visible").length && !$("input[name=fileTypeOtherValue]:visible").val())
						{
							$("form#epact-dependant-userfile-form").valid();
						}
						else if ($("input[name=fileupload]").length && $("input[name=fileupload]")[0].files.length && $("input[name=fileupload]")[0].files[0].size > 3145728)
						{
							$("#error-file-size").show();
						}
						else
						{ 
							$("#epact-dependant-userfile-form").ajaxSubmit({
								beforeSubmit: function() {
									$(".popup-loader").show();
									$("#success-file-upload").hide();
									$("#error-file-upload").hide();
									$("#upload-file-button-popup").attr("disabled","true");
								},

								error: function() {
									$("#success-file-upload").hide();
									$("#error-file-upload").show();
									$("body").css("cursor", "default");
									$("#upload-file-button-popup").removeAttr("disabled");
									$(".popup-loader").hide();
								},

								success: function(responseText) { 

									if (responseText != "failure") {

										$("#error-file-upload").hide();
										$("#success-file-upload").show();

										if (onWizard) {          	             				
											var redirect = window.location.pathname + " #file-actions";
											$("#file-actions").load(redirect, function(){  
												$(this).children().unwrap();
												$("#upload-file-modal").modal("hide"); 
												$.getScript(scriptURL); 
											}); 

										} else {	
											window.destinationHref = window.location.href;
											location.reload();
										}          	            	    	
									} else {

										$("#error-file-upload").show();
										$("#success-file-upload").hide();
									}

									$("body").css("cursor", "default");
									$("#upload-file-button-popup").removeAttr("disabled");
									$(".popup-loader").hide();
								}  	 
							});
						}
					});

					$("input[name=fileupload], select#filetype-dropdown").change(function() {
						$("#error-file-name-long").hide();
						if ($("input[name=fileupload]")[0].files[0] && $("input[name=fileupload]")[0].files[0].name.length <= 32 && $("select#filetype-dropdown").find(":selected").val().toLowerCase() != "select") {
							$("#upload-file-button-popup").removeAttr("disabled");
						} else if($("input[name=fileupload]")[0].files[0] && $("input[name=fileupload]")[0].files[0].name.length > 32) {
							$("#error-file-name-long").show();
						}

					});

					$("#upload-file-modal .modal-footer #upload-button-container").remove();
					$("#upload-file-modal .modal-footer .btn").before($(".modal-body #upload-button-container"));

					$(".remove-file-btn").click(function(e){
						e.preventDefault();
						$.ajax({
							url: $(this).data("href"),
							cache: false,
							type: "POST",
							success: function(data) {
								var redirect = window.location.pathname + " #file-actions";
								$("#file-actions").load(redirect, function(){  
									$(this).children().unwrap();
									$.getScript(scriptURL); 
								});
							}
						});
					});
					
					if ($(".countryList").length)
					{		
						var selectedCountry = ".provinceList-" + $(".countryList").val();
						$('.province, .province-label').hide();
						$('.province').removeAttr("name");
						$('.province').removeAttr("required");

						$(".error[for='provinceList-US'], .error[for='provinceList-CA']").hide();
						if ($(selectedCountry).children('option').length != 0) {
							$(selectedCountry + ", .province-label").show();
							$(selectedCountry).prop("name", "province");
						}
					}
					
					if ($(".workCountryList").length)
					{		
						var selectedCountry = ".workProvinceList-" + $(".workCountryList").val();
						$('.workProvince, .workProvince-label').hide();
						$('.workProvince').removeAttr("name");
						$('.workProvince').removeAttr("required");

						$(".error[for='workProvinceList-US'], .error[for='workProvinceList-CA']").hide();
						if ($(selectedCountry).children('option').length != 0) {
							$(selectedCountry + ", .workProvince-label").show();
							$(selectedCountry).prop("name", "workProvince");
						}
					}

					// Resets the mouse pointer to default
					$("body").css("cursor", "default");

					if ($(".popup-loader").length) {
						$(".popup-loader").hide();
					}

					if ($(".submit-ajax").length) {
						$(".submit-ajax").removeAttr("disabled");
					}

					window.onbeforeunload = function () {
						$("body").css("cursor", "default");
					}; 

					$(".filter-label").click(function() {		
						$(this).parents(".filter-option").find("input[type=checkbox]").trigger('click');
					});

					$(".filter-option input[type=checkbox]").click(function() {
						if ($(this).is(":checked")) {
							$(this).parents(".filter-option").addClass("selected");
						} else {
							$(this).parents(".filter-option").removeClass("selected");
						}
					});

					$("[data-toggle=modal]").click(function(ev) {

						ev.preventDefault();
						// load the url and show modal on success
						$( $(this).attr('data-target') + " .modal-body").load($(this).attr("href"), function() { 
							$($(this).attr('data-target')).modal("show"); 
						});
					});

					// Loop and apply standard form validation
					$('.form-login').each(function() {

						$(this).validate({submitHandler: function(form) { 

							var formName = "#" + form.name;
							var redirectPage= $(formName).data("redirectpage");

							$("#login-loader").show();
							$("body").css("cursor", "progress");
							$(formName).ajaxSubmit({

								error: function() {
									$("body").css("cursor", "default");
									$("#login-loader").hide();
								},         

								success: function(response) {  	
									// Directs them to the lockout page if their account is locked out
									if (response.indexOf("accountLockout home accountLockout") != -1) {
										window.destinationHref =  lockoutLink;
										window.location.href = lockoutLink;
									}
									$.getScript(scriptVarsURL).done(function() {
									var loggedIn = false;
									$("body").css("cursor", "default");
									$("#login-loader").hide();

									var $data=$(response);
									var strippedResponse = $data.filter('#switchAccountVars').html();

									$("#hiddendiv").html(strippedResponse);      
									
									if (outOfDate) {
										$("#login-error-text").text("Page was out of date. Please try again.");
										$("#login-error").show();
										$("#login-form-hidden").load(document.URL + " #login-form-hidden");
									} else if (loggedInUserEmail == "unavailable") {
										$("#login-error-text").text("The username or password is incorrect.");
										$("#login-error").show();
									} else if (loggedInUserEmail == "admin@epactnetwork.com") {	
										switchAccounts(switchAdminLink,listOrgsLink);    
									} else {
										// If the user is an admin of any orgs, display the switch account overlay

										if (adminMode) {
											// Calls the switch acconut overlay if the user is an admin for an org
											$.ajax({
												url: switchAccountLink,
												cache: false,
												success: function(data) {
														var failure = null;

														$("#switch-account .modal-body").load(switchAccountLink, '', function(){
															$.getScript(scriptURL);
															$("#switch-account").modal('show');
															$("#full-menu").css("left","-9px");
															$(".navbar-static-top").css("position","relative");
															$(".navbar-static-top").css("left","-9px");
														});
												}
											});
										} else { 
											window.destinationHref = defaultLink;
											window.location.href = defaultLink;
										}
									}
									});
								}
							});
						}
						});          
					});

					// Resets the save button back to enabled if the user changes a form on the wizard
					$(".form-validate-ajax :input").on("input change", function(){
						$(".btn-save").html("Save");     		
						$(".btn-save").removeAttr("disabled");
						$(".save-next,.save-next-incomplete").hide();  	        		
					});



					$(".form-validate-ajax :input").keydown(function(e){            	
						// Delete or backspace key
						if(e.keyCode == 46 || e.keyCode == 8) {
							$(".btn-save").html("Save");     		
							$(".btn-save").removeAttr("disabled");
							$(".save-next,.save-next-incomplete").hide();  	
						}
					});


					// Closes the save alert message
					$(".save-close-btn").click(function() {
						$(this).parents(".save-alert").slideUp();
					});

					$(".close-message-alert").click(function() {
						$(this).parents(".message-alert-content").slideUp();
					});

					$("#shared-address-yes").unbind("click").click(function() {
						$("#shared-address-modal").modal('hide');
						var changedForm = $(this).data("changedform");
						$(changedForm).removeClass("changedAddress");
						submitAjaxForm(changedForm, true);
					});

					$("#shared-address-no").unbind("click").click(function() {
						var changedForm = $(this).data("changedform");

						$('<input>').attr({
							type: 'hidden',
							name: 'updateWithNewAddress',
							value: 'true'
						}).appendTo(changedForm + ' > #form-id-stuff');

						$(changedForm).removeClass("changedAddress");
						$("#shared-address-modal").modal('hide');
						submitAjaxForm(changedForm, true);
					});



					// Validates and submits form through ajax
					function submitAjaxForm(formName, isValid) {

						var reloadDiv = $(formName).data("reloaddiv");
						var modalDiv = $(formName).data("modaldiv");
						var redirectPage= $(formName).data("redirectpage");
						var saveForm = $(formName).data("saveform");

						$(formName).ajaxSubmit({

							error: function() {
								$(".popup-error").show();
								$(".btn-save").removeAttr("disabled");
								$("body").css("cursor", "default");
								$(".submit-ajax").removeAttr("disabled");
								$(".popup-loader").hide();
							},         

							success: function() {	
								$.getScript(scriptVarsURL).done(function() {
									if (redirectPage != null) {		    	            
										window.destinationHref = redirectPage;
										window.location.href = redirectPage;	      
										// If this is a form on the wizard which we need to save
									} else if (saveForm != null) {	 		        				
										if ($(".btn-save").length) {
											$("#dependant-endnav-left").load(window.location.pathname + " #dependant-endnav-left", function() {
												$("#dependant-endnav-left").children().unwrap();
												$.getScript(scriptURL);   
											});									
										}

										$("body").css("cursor", "default");

										var hiddenVariables = document.URL + " #form-id-stuff";
										$("#form-id-stuff").load(hiddenVariables);


										// If we're on the medical info page and they're missing a form or doctor info isn't complete
										if (!(isValid) || ($("#medical-requirement").length && $("#medical-requirement").is(":visible"))) {
											displaySavePopup(formName, false);
										} else {
											displaySavePopup(formName, true);
										}

									} else {
										var redirect = window.location.pathname + " " + reloadDiv;

										// Update the save next/back buttons at the bottom of the wizard page
										if ($(".btn-save").length) {
											$(".btn-save").html("Save");     		
											$(".btn-save").removeAttr("disabled");			
											$(".save-next,.save-next-incomplete").hide(); 
										}

										// If we have to refresh a section on the page
										if ($(reloadDiv).length) {
											$(reloadDiv ).load(redirect, function() {
												$.getScript(scriptURL);   
												$("body").css("cursor", "default");
												$(".popup-loader").hide();					            	
												$(".popup-error").hide();
												$(".submit-ajax").removeAttr("disabled");

												$(modalDiv).modal('hide');

												if(typeof $(formName).find("input[name=email]").val() !== 'undefined' ) {
													displaySavePopup(formName, true);
												}
											});   
										} else {
											$("body").css("cursor", "default");
											$(".popup-loader").hide();
											$(".popup-error").hide();
											$(".submit-ajax").removeAttr("disabled");
											$(modalDiv).modal('hide');	
										}
									}
								});
							}
						});
					}


					// Attachs custom validate function to form
					$('.form-validate-ajax').each(function() {

						var formyName = $(this).attr("id");

						if (formyName == "edit-campaign-form") {
							$.validator.setDefaults({ 
								ignore: [],
								// any other default options and/or rules
							});
						}

						$(this).attr("data-modaldiv", ("#" + $(this).parents(".modal").attr("id")));

						// Validator for all wizard pages that must have required information filled out, ie not the medical info pages
						if (!(($(this).attr("id") == "epact-medicalinfo-form") || ($(this).attr("id") == "epact-dependant-medical-form"))) {
							$(this).validate({

								invalidHandler: function(event, validator) {
									$("body").css("cursor", "progress");

									// If this is not a modal, hide the save next message
									if (!($(this).hasClass("form-modal"))) {
										$(".save-next").hide();
									}
								},
								submitHandler: function(form) { 
									var formName = "#" + form.name;

									if (!($("body").hasClass("profileContacts"))) {
										$(".btn-save").html(" <img src='" + loadWhiteGIF + "'> Saving");
										$(".btn-save").attr("disabled","");
										$(".submit-ajax").attr("disabled","true");
									}
									// If the user changed a shared address, display the popup
									if ($(formName).hasClass("changedAddress")) {
										$("#shared-address-modal").modal('show');
									} else {
										$("body").css("cursor", "progress");
										$(".popup-loader").show();
										submitAjaxForm(formName, true);
									}
								}
							});

						} else {         
							$(this).validate({

								invalidHandler: function(event, validator) {

									var formName = "#" + formyName;

									// If the user hits the save button instead of the initial incomplete error check
									if (!($("#medical-section").hasClass("incomplete"))) {
										$(".btn-save").html(" <img src='" + loadWhiteGIF + "'> Saving");
										$(".btn-save").attr("disabled","");
										$(".submit-ajax").attr("disabled","true");
									}

									// If the user changed a shared address, display the popup
									if ($(formName).hasClass("changedAddress")) {
										$("#shared-address-modal").modal('show');
									} else {
										$("body").css("cursor", "progress");
										$(".popup-loader").show();

										if (!($("#medical-section").hasClass("incomplete"))) {
											submitAjaxForm(formName, false);
										}
									}
								},

								submitHandler: function(form) { 
									var formName = "#" + form.name;

									$(".btn-save").html(" <img src='" + loadWhiteGIF + "'> Saving");
									$(".btn-save").attr("disabled","");
									$(".submit-ajax").attr("disabled","true");

									// If the user changed a shared address, display the popup
									if ($(formName).hasClass("changedAddress")) {
										$("#shared-address-modal").modal('show');
									} else {
										$("body").css("cursor", "progress");
										$(".popup-loader").show();
										submitAjaxForm(formName, true);
									}
								}
							});
						} 

					});

					$('.form-mobile').each(function() {        

						var newReferer = $(this).data("referer");
						$(this).validate({
							submitHandler: function(form) {
								var formName = "#" + form.name;
								$("body").css("cursor", "progress");
								$(".mobile-delete, .submit-mobile").attr("disabled","true");
								$(".popup-loader-mobile").show();

								$(formName).ajaxSubmit({

									error: function() {
										$("body").css("cursor", "default");
										$(".submit-mobile, .mobile-delete").removeAttr("disabled");
										$(".popup-loader-mobile").hide();
									},         

									success: function() {    
										window.destinationHref = newReferer;
										window.location.href = newReferer; 
									}
								});
							}
						});
					});

					// Ensures that users cannot use their own email for emergency contacts
					$("#epact-adult-add-form, #epact-adult-edit-form, #epact-outofarea-edit-form, #epact-create-out-of-area-contact-form, " +
							"#epact-editemergencycontact-form, #epact-create-emergency-contact-form, #epact-dependant-guardian-edit-form, " +
							"#epact-dependant-guardian-add-form, #epact-guardian-add-form, #epact-guardian-edit-form, " +
							"#epact-dependant-outofarea-edit-form, #epact-dependant-altguardian-edit-form, #epact-dependant-outofarea-add-form, " +
					"#epact-dependant-altguardian-add-form, #epact-overview-contact-form").each(function() {
						$(this).find("input[name=email]").attr("sameEmail", "true");
					});

//					// Loop and apply standard form validation
					$('.form-validate').each(function() {

						// Allows hidden fields to be validated for the campaign form
						if (($(this).attr("id") == "create-campaign-form") || ($(this).attr("id") == "edit-campaign-form")) {
							$(this).validate({ignore: []});
						} else {
							$(this).validate();
						}

						if ($("input[name=primaryPhone]").length) {
							if($("input[name=primaryPhone]").is(':radio')){  
								$("input[name=primaryPhoneOther]").rules("add", "phoneUS"); 
							} else {
								$("input[name=primaryPhone]").rules("add", "phoneUS"); 
							}
						}       


						// If this form is a register form

						if (($(this).attr("id") == "epact-inviteregistration-form") || ($(this).attr("id") == "epact-resetpassword-form") || ($(this).attr("id") == "epact-forgotpasswordupdate-form") || ($(this).attr("id") == "epact-registration-form")){
							if ($("input[name=registerPassword]").length) {
								$("input[name=registerPassword]").rules("add", "validPassword"); 
							}
							if ($("input[name=updatePassword]").length) {
								$("input[name=updatePassword]").rules("add", "validPassword"); 
							}
						}

						if ($(this).attr("id") =="epact-accountpassword-form") {
							if ($("input[name=newPassword]").length) {
								$("input[name=newPassword]").rules("add", "validPassword"); 
							}
						}


						if ($("input[name=secondaryPhone]").length) {
							if($("input[name=secondaryPhone]").is(':radio')){  
								$("input[name=secondaryPhoneOther]").rules("add", "phoneUS"); 
							} else {
								$("input[name=secondaryPhone]").rules("add", "phoneUS"); 
							}
						}

						if ($("input[name=altWorkPhoneNumber]").length) {
							$("input[name=altWorkPhoneNumber]").rules("add", "phoneUS"); 
						}

						if ($("input[name=clinicContact]").length) {
							$("input[name=clinicContact]").rules("add", "phoneUS"); 
						}

						if ($("input[name=doctorContact]").length) {
							$("input[name=doctorContact]").rules("add", "phoneUS"); 
						}  

						if ($("input[name=dentistContact]").length) {
							$("input[name=dentistContact]").rules("add", "phoneUS"); 
						}    
					});

					
				    $("#close-switch-account").click(function() {
				    	var logoutLink = $(this).data("logout");
						$.ajax({
							type: "POST",
							url: logoutLink,
							success: function(data) {  
								$("#login-form-hidden").load(window.location.pathname + " #login-form-hidden", function() {
									$('#switch-account').modal('hide'); 
								});
							}
						});
				    });

					// Ensures that the user really really wants to delete that contact
					$(".confirm-delete-modal").unbind("click").click(function(){
						if(!confirm(languageStrings.getText("confirm_delete"))){
							return false;
						} else {
							var formName = $(this).data("formname");
							$(formName).validate();
						}
					});

					$(".province-starred").hide();

					$('.form-validate-ajax, .form-mobile').each(function() {
						if ($("input[name=primaryPhone]").length) {
							if($("input[name=primaryPhone]").is(':radio')){  
								$("input[name=primaryPhoneOther]").rules("add", "phoneUS"); 
							} else {
								$("input[name=primaryPhone]").rules("add", "phoneUS"); 
							}
						}       

						if ($("input[name=secondaryPhone]").length) {
							if($("input[name=secondaryPhone]").is(':radio')){  
								$("input[name=secondaryPhoneOther]").rules("add", "phoneUS"); 
							} else {
								$("input[name=secondaryPhone]").rules("add", "phoneUS"); 
							}
						}

						if ($("input[name=altWorkPhoneNumber]").length) {
							$("input[name=altWorkPhoneNumber]").rules("add", "phoneUS"); 
						}

						if ($("input[name=clinicContact]").length) {
							$("input[name=clinicContact]").rules("add", "phoneUS"); 
						}

						if ($("input[name=doctorContact]").length) {
							$("input[name=doctorContact]").rules("add", "phoneUS"); 
						}  

						if ($("input[name=dentistContact]").length) {
							$("input[name=dentistContact]").rules("add", "phoneUS"); 
						}   
					});  
					
					// If the user is logged in, log them out after 30 minutes of inactivty
					if (!($("body").hasClass("home")) || (document.body.className == "accountSettings home accountSettings")) {
						idleTime = 0;
						$(document).ready(function () {
							//Increment the idle time counter every minute.
							var idleInterval = setInterval(timerIncrement, 60000); // 1 minute
						});
						function timerIncrement() {
							idleTime = idleTime + 1;	            	    
							if (idleTime > 29) { // 30 minutes

								// After 30 minutes and one second	    	
								setTimeout(function (){
									window.destinationHref = window.location.href;
									location.reload();
								}, 1000); 
							}
						}


						if (typeof(Zenbox) !== "undefined") {
							Zenbox.init({
								dropboxID:   "20241486",
								url:         "https://epactnetwork.zendesk.com",
								tabTooltip:  "Ask Us",
								tabImageURL: "https://assets.zendesk.com/external/zenbox/images/tab_ask_us_right.png",
								tabColor:    "#1b99cc",
								tabPosition: "Right"
							});
						}
					}


					// Goes back to the previous page
					$(".mobile-back").click(function() {
						window.destinationHref = referer;
						window.location.href = referer; 
					});

					// Handles popup delete submissions from mobile devices
					$(".mobile-delete").click(function() {

						var goDelete = confirm(languageStrings.getText("confirm_delete"));

						if (goDelete) {
							$("body").css("cursor", "progress");
							$(".mobile-delete, .submit-mobile").attr("disabled","true");
							$(".popup-loader-delete").show();

							var formName = $(this).data("formname");

							$(formName).ajaxSubmit({

								error: function() {
									$("body").css("cursor", "default");
									$(".submit-mobile, .mobile-delete").removeAttr("disabled");
									$(".popup-loader-delete").hide();
								},         

								success: function() {   
									window.destinationHref = referer;
									window.location.href = referer; 
								}
							});
						} else {
							return false;
						}
					});

					// Handles popup submissions from mobile devices
					$(".submit-mobile").click(function() {
						var formName = $(this).data("formname");
						$(formName).submit();
					});

				/*	$(".select-button").click(function() {
						$("body").css("cursor", "progress");
						$(".select-button").attr("disabled","true");
						$(".popup-loader").show();

						var formName = $(this).data("formname");
						var reloadDiv = $(this).data("reloaddiv");

						if (!($(reloadDiv).length)) {
							reloadDiv = $(this).data("reloaddiv2");
						}

						$(formName).ajaxSubmit({
							beforeSubmit: function() {							
							},
							
							error: function() {
								$(".select-warning").show();
								$("body").css("cursor", "default");
								$(".select-button").removeAttr("disabled");
								$(".popup-loader").hide();
							},         

							success: function() {  
								$(reloadDiv).load((window.location.pathname + " " + reloadDiv), function() {		            	
									$.getScript(scriptURL); 
									$("body").css("cursor", "default");
									$(".select-button").removeAttr("disabled");
									$(".select-warning").hide();
									$(".popup-loader").hide();

									if ($(".btn-save").length) {
										$(".btn-save").html("Save");     		
										$(".btn-save").removeAttr("disabled");
										$(".save-next,.save-next-incomplete").hide(); 
									}
								});    
							}
						});
					});*/

					// Binds a form to ajax and validates it if needed - used for popups that are used in more than one page
					function setAjaxForm(formName, options) {
						$(formName).ajaxForm(function() { 
							if (goahead == true) {

								if ((options['urlFactor']) && (document.URL.indexOf(options['urlFactor']) >= 1)) {

									var redirect = document.URL + " " + options['reloadDiv2'];

									if (options['modalDiv']) { $(options['modalDiv']).modal('hide'); }
									$(options['reloadDiv2']).load(redirect, function() {
										$.getScript(scriptURL); 
										$("body").css("cursor", "default");
										$(".select-button").removeAttr("disabled");
									});  
								} else {

									var redirect = document.URL + " " + options['reloadDiv'];
									if (options['modalDiv']) { $(options['modalDiv']).modal('hide'); }
									if ($(options['reloadDiv']).length) {
										$(options['reloadDiv']).load(redirect, function() {
											$.getScript(scriptURL);    		
											$("body").css("cursor", "default");
											$(".select-button").removeAttr("disabled");
										});    
									}
								}			
							};
						}); 

					}

					// Enables menu dropdowns
					$('.dropdown-toggle').dropdown();

					// If the user doesn't click on any users, will automatically let them print from a range
					$(".print-submit").click(function(event){
						var selectedUsers = $("input:checkbox:checked").length;

						if (selectedUsers == $("#select-all").data("allmembers") && $("#select-all").data("maxusers") > 1) {
							window.destinationHref = $("#select-all").data("printrange");
							window.location.href = $("#select-all").data("printrange");
							return false;
						} else if ($('#unselect-everything').data("printrange") != null) {
							window.destinationHref = $("#unselect-everything").data("printrange");
							window.location.href = $("#unselect-everything").data("printrange");
							return false;
						}
					});

					// Shows/hides the same address text field in forms
//					$('.orgListSelect').not('#searchBy').change(function() {
//					var filterOption = $(this).attr("name");
//					var filterValue = document.getElementById(filterOption).options[document.getElementById(filterOption).selectedIndex].value;

//					var form = $('#epact-filter-org-member-list-form, #epact-filter-org-group-list-form, #epact-filter-org-admin-list-form');

//					form.attr('action', form.val() + "?" + filterOption + "=" + filterValue);
//					form.submit();
//					});

					if($('#advanced-filter-form').css('display') != 'none')
					{
						var parent = $('#searchTerm-search').parent();
						parent.css("display", "none");
						$('#multiselect-status').css("display", "none");
					}


					$('.deleteTooltip, .cannotEditTooltip, .cannotEditTooltipIcon').tooltip();

					$("#hasMedicalAlert-y").each(function() {
						if ($(this).is(':checked')) {
							$(".medicalNotes-section").show();
							$("textarea[name=medicalNotes]").attr("required","");

						} else {
							$(".medicalNotes-section").hide();
							$("textarea[name=medicalNotes]").removeAttr("required");
						}
					});
					$("#hasMedicalAlert-y").click(function() {
						$(".medicalNotes-section").show();
						$("textarea[name=medicalNotes]").attr("required","");

					});

					$("#hasMedicalAlert-n").click(function() {
						$(".medicalNotes-section").hide();
						$("textarea[name=medicalNotes]").removeAttr("required");
					});

					// If the user is on a page that has a "work location" field in it, displays/hides the new available at work fields
					$('select[name="workLocation"]').each(function() {
						if ($(this).val() == "office") {
							$(".workPhone").attr("required","");

						} else {
							$(".workPhone").removeAttr("required");

						}
					});
					// If the user is on a page that has a "work location" field in it, displays/hides the new available at work fields on change
					$('select[name="workLocation"]').on("change keypress paste", function(){
						if ($(this).val() == "office") {
							$(".availableAtWork-section").show();
							$(".workPhone").attr("required","");
						} else {
							$(".availableAtWork-section").hide();
							$(".workPhone").removeAttr("required");

						}
					});

					// Shows the available at work section in forms
					$('.availableAtWork-true').click(function(){
						$(this).parents("form").find(".workLocation-section").show();
						$(this).parents("form").find('.workLocation-select').attr("notEqual","unset");
					});

					// Hides the available at work section in forms
					$('.availableAtWork-false').click(function(){
						$(this).parents("form").find(".workLocation-section").hide();
						$(this).parents("form").find(".availableAtWork-section").hide();
						$(this).parents("form").find('#altWorkPhoneNumber-field').removeAttr("required");
						$(this).parents("form").find('.workLocation-select').removeAttr("notEqual");
						$(this).parents("form").find(".workPhone").removeAttr("required");
					});

					// Initally shows/hides the available at work section on page load
					$('.availableAtWork-true').each(function(){
						if ($(this).is(':checked')) {
							$(this).parents("form").find(".workLocation-section").show();
							$(this).parents("form").find('.workLocation-select').attr("notEqual","unset");
						} else{
							$(this).parents("form").find(".availableAtWork-section").hide();
							$(this).parents("form").find(".workLocation-section").hide();
							$(this).parents("form").find('#altWorkPhoneNumber-field').removeAttr("required");
							$(this).parents("form").find('.workLocation-select').removeAttr("notEqual");
						}
					});

					// Hides the same address section in forms
					$('.sameAddress-true').click(function(){
						$(".sameAddress-section").hide();
						$(".sameAddress-fields").each(function() {
							$(this).removeAttr("required");
							if ($(this).attr("name") == "country") {
								$(this).attr("notEqual");
							}
						});
					});

					// Displays the same address section in forms
					$('.sameAddress-false').click(function(){
						$(".sameAddress-section").show();

						$(".sameAddress-fields").each(function() {

							if ($(this).attr("name") == "country") {
								$(this).attr("notEqual","select");
							}
							$(this).attr("required","");
						});
					});

					$("input[name=primaryPhoneIsMobile], input[name=secondaryPhoneIsMobile], input[name=altWorkPhoneNumberIsMobile]," +
					"input[name=primaryPhoneOtherIsMobile], input[name=secondaryPhoneOtherIsMobile]").each(function() {

						var mobileSelect = "." + $(this).attr("name") + "-select";

						if ($(this).parents("form").find(mobileSelect).length) {
							if ($(this).is(":checked")) {
								$(this).parents("form").find(mobileSelect).val("mobile");
							} else {

								var mobileFor = "input[name=" + $(this).parents("form").find(mobileSelect).data("mobilefor") + "]";

								if ($(this).parents("form").find(mobileFor).val() == "") {
									$(this).parents("form").find(mobileSelect).val("select");
								} else {
									$(this).parents("form").find(mobileSelect).val("landline");
								}
							}
						}
					});

					$("input[name=secondaryPhoneOther]").on("input", function(){
						if ($(this).val() != "") {
							$(this).parents("form").find("input:radio[name=secondaryPhone][value ='other']").prop('checked', true);
						} else {
							$(this).parents("form").find("input:radio[name=secondaryPhone][value ='none']").prop('checked', true);
						}
					});

					$(".cancel-select").click(function() {

						var reloadDiv = $(this).data("reloaddiv");
						var reloadURL = window.location.pathname + " " + reloadDiv;

						$(reloadDiv).load(reloadURL, function() {
							$.getScript(scriptURL); 
						});    
					});

					$("#country-required").each(function() {
						$(".province-starred").show();
						$(".province").attr("notequal","select");
					});

					// Initally shows/hides the same address section on page load
					$('.sameAddress-true').each(function(){

						$(this).parents("form").find(".starred").show();
						$(this).parents("form").find(".province-starred").show();
						$(this).parents("form").find(".province").attr("notequal","select");

						if ($(this).is(':checked')) {
							$(".sameAddress-fields").each(function() {
								$(this).removeAttr("required");
							});
						} else{
							$(".sameAddress-fields").each(function() {
								$(this).attr("required","");
							});
						}
					});

					// Removes the validate on email if checked
					$('.noemail').click(function() {
						if ($(this).is(':checked')) {
							$(this).parents("form").find(".control-for-email").hide();
							$(this).parents("form").find(".email").removeAttr("required");
							$(this).parents("form").find(".email").val("");
							$(this).parents("form").find(".email").removeAttr("name");
							$(this).parents("form").find("input[name=email]").val("");

						} else {
							$(this).parents("form").find(".control-for-email").show();
							$(this).parents("form").find(".email").attr("name", "email");
							$(this).parents("form").find(".email").attr("required", "");
						}
					});

					// Removes the validate on email on modal load
					$('.noemail').each(function() {
						if ($(this).is(':checked')) {
							$(this).parents("form").find(".control-for-email").hide();
							$(this).parents("form").find(".email").removeAttr("required");
							$(this).parents("form").find(".email").val("");
							$(this).parents("form").find(".email").removeAttr("name");
						} else {
							$(this).parents("form").find(".control-for-email").show();
							$(this).parents("form").find(".email").attr("name", "email");
							$(this).parents("form").find(".email").attr("required", "");
						}
					});

					// Submits form and proceeds to the previous page
					$(".submit-ajax").unbind("click").click(function() {
						var formName = $(this).data("formname");
						$(formName).submit();
					});

					// Submits the medical provider form
					$(".submit-ajax-medical").unbind("click").click(function() {	
						if ($(".medical-dropdown").val() == "optclinic") {
							$("form#epact-clinic-add-form").submit();

						} else if ($(".medical-dropdown").val() == "optdoctor") {
							$("form#epact-doctor-add-form").submit();
						}
					});               	

					// Adds/removes work province dropdown based on whether that work country has provinces/states
					$(".workCountryList").on("change keypress keydown keyup paste", function(){

						var selectedCountry = ".workProvinceList-" + $(this).val();
						$('.workProvince, .workProvince-label').hide();
						$('.workProvince').removeAttr("name");

						$(".error[for='provinceList-US'], .error[for='provinceList-CA']").hide();
						if ($(selectedCountry).children('option').length != 0) {
							$(selectedCountry + ", .workProvince-label").show();
							$(selectedCountry).prop("name", "workProvince");
							$(selectedCountry).prop("selectedIndex", 0);
						} 					
					});

					// Adds/removes province dropdown based on whether that country has provinces/states
					$(".countryList").on("change keypress keydown keyup paste", function(){		
						var selectedCountry = ".provinceList-" + $(this).val();

						$('.province, .province-label').hide();
						$('.province').removeAttr("name");
						$('.province').removeAttr("required");

						$(".error[for='provinceList-US'], .error[for='provinceList-CA']").hide();
						if ($(selectedCountry).children('option').length != 0) {
							$(selectedCountry + ", .province-label").show();
							$(selectedCountry).prop("name", "province");
						}
					});

					$(".countryList").on("change", function(){		
						$('.province').not(this).prop("selectedIndex", 0);
//						$('.province').not(this).removeAttr("notEqual");
//						$('.province').not(this).removeAttr("required");
					});

					$(".workCountryList").on("change", function(){		
						$('.workProvince').not(this).prop("selectedIndex", 0);
					});

					// Adds/removes province dropdown based on whether that country has provinces/states
					$(".province").each(function(){		
						if (!($(this).val() == "select")) {					
							$(this).parents("form").find("#province-label").show();
							$(this).prop("name", "province");
							$(this).show();
						}
					});


					// Adds/removes province dropdown based on whether that country has provinces/states
					$(".workProvince").each(function(){		
						if (!($(this).val() == "select")) {					
							$(this).prop("name", "workProvince");
							$(this).show();
						}
					});

					// Shows/hides the available at work text field in forms
					$('.primaryPhone').click(function() {

						if($(this).val() == "other") {
							$(this).parents("form").find("#control-for-primaryPhoneOther").show();
							$(this).parents("form").find(".primaryPhoneOther-field").attr("required","");
						}else {
							$(this).parents("form").find("#control-for-primaryPhoneOther").hide();
							$(this).parents("form").find(".primaryPhoneOther-field").removeAttr("required");
						}
					});

//					// Shows/hides the available at work text field in forms
//					$('.secondaryPhone').click(function() {

//					if($(this).val() == "other") {
//					$(this).parents("form").find('.control-for-secondaryPhoneOther').show();
//					$(this).parents("form").find('.secondaryPhoneOther-field').attr("required","");

//					}else {
//					$(this).parents("form").find('.control-for-secondaryPhoneOther').hide();
//					$(this).parents("form").find('.secondaryPhoneOther-field').removeAttr("required");
//					}
//					});

					$(".workPhone").click(function() {
						if ($(this).is(':checked')) {
							if ($(this).prop('id') == "workPhone-work") {					
								$(this).parents("form").find('#altWorkPhoneNumber-field').attr("required","");
								$(this).parents("form").find('#control-for-altWorkPhoneNumber').show();
								$(this).parents("form").find('.altWorkPhoneNumberIsMobile-select').attr("notEqual","select");
							} else {
								$(this).parents("form").find('#altWorkPhoneNumber-field').removeAttr("required");
								$(this).parents("form").find('#control-for-altWorkPhoneNumber').hide();
								$(this).parents("form").find('.altWorkPhoneNumberIsMobile-select').removeAttr("notEqual");						
							}						
						} 
					});

					$(".workPhone").each(function() {
						if ($(this).is(':checked')) {
							if ($(this).prop('id') == "workPhone-work") {
								$(this).parents("form").find('#altWorkPhoneNumber-field').attr("required","");
								$(this).parents("form").find('.altWorkPhoneNumberIsMobile-select').attr("notEqual","select");
							} else {
								$(this).parents("form").find('#altWorkPhoneNumber-field').removeAttr("required");
								$(this).parents("form").find('.altWorkPhoneNumberIsMobile-select').removeAttr("notEqual");												
							}
						} 
					});

					// Resets the org list form to have no filters
					$('#searchTerm-remove-option').click(function() {
						$("#searchTerm-search").val("");
						$(this).val("reset");
						$('#epact-filter-org-member-list-form, #epact-filter-org-group-list-form, #epact-filter-org-admin-list-form').submit();
					});

					$('#advancedSearchTerm-remove-option').click(function() {
						$("#advancedSearchTerm-search").val("");
						$("#searchBy").val("name");
						$('#epact-advanced-filter-org-member-list-form, #epact-advanced-filter-org-group-list-form, #epact-advanced-filter-org-admin-list-form').submit();
					});

					// Resets the org list form to have no filters
					$('#memberId-remove-option').click(function() {
						$("#memberId-search").val("");
						$('#epact-filter-org-member-list-form, #epact-filter-org-group-list-form, #epact-filter-org-admin-list-form').submit();
					});

					$("#searchTerm-search").keypress(function(event) {
						if (event.which == 13) {
							event.preventDefault();
							$('#epact-filter-org-member-list-form, #epact-filter-org-group-list-form, #epact-filter-org-admin-list-form').submit();
						}
					});

					// Resets the organization member list form status/division filters
					$('.removeFilterURL').click(function() {
						document.getElementsByName('status')[0].value = "ALL";
						document.getElementsByName('division')[0].value = "ALL";
						document.getElementsByName('medical')[0].value = "NONE";
						$("#searchTerm-search").val("");
						$('#epact-filter-org-member-list-form, #epact-filter-org-group-list-form, #epact-filter-org-admin-list-form').submit();
					});


					$('#physicalDate').each(function() {

						// If the field is blank, set the selected dropdown indicies to default
						if ($(this).val() == "") {
							$('#physicalYear').prop('selectedIndex', 0);
							$('#physicalMonth').prop('selectedIndex', 0);
							$('#physicalDay').prop('selectedIndex', 0);
						} else {

							if (isNaN($(this).val())) {
								$(this).val("");
							} else {
								var d = new Date($(this).val());
								var newPhysicalYear = d.getUTCFullYear();
								var newPhysicalMonth = d.getUTCMonth() + 1;
								var newPhysicalDay = d.getUTCDate();

								$('#physicalYear').val(newPhysicalYear);
								$('#physicalMonth').val(newPhysicalMonth);
								$('#physicalDay').val(newPhysicalDay);
							}
						}
					});

					// limits the input based on the maxlength attribute, this is by default no supported by input type number
					$("input[type=number][maxlength]").on('keydown keyup',function(){ 
						var $that = $(this),
						maxlength = $that.attr('maxlength')
						if($.isNumeric(maxlength)){
							$that.val($that.val().substr(0, maxlength));
						};
					});

					$("#physicalYear").on("change keypress paste", function(){
						var d = new Date($('#physicalYear').val(), $('#physicalMonth').val()-1, $('#physicalDay').val());

						$("#physicalDate").val(d.getTime());

						if (isNaN($("#physicalDate").val())) {
							$("#physicalDate").val("");
						}

						if ( ($("#physicalDay").val() != "select") && ($("#physicalMonth").val() != "select") && ($(this).val() != "select")) {
							$("#physicalDateError").hide();
						} 	
					});

					$("#physicalMonth").on("change keypress paste", function(){
						var d = new Date($('#physicalYear').val(), $('#physicalMonth').val()-1, $('#physicalDay').val());
						$("#physicalDate").val(d.getTime());

						if (isNaN($("#physicalDate").val())) {
							$("#physicalDate").val("");
						}

						if ( ($("#physicalYear").val() != "select") && ($("#physicalDay").val() != "select") && ($(this).val() != "select")) {
							$("#physicalDateError").hide();
						} 	
					});	

					$("#physicalDay").on("change keypress paste", function(){
						var d = new Date($('#physicalYear').val(), $('#physicalMonth').val()-1, $('#physicalDay').val());
						$("#physicalDate").val(d.getTime());

						if (isNaN($("#physicalDate").val())) {
							$("#physicalDate").val("");
						}

						if ( ($("#physicalYear").val() != "select") && ($("#physicalMonth").val() != "select") && ($(this).val() != "select")) {
							$("#physicalDateError").hide();
						} 			
					});				

					// Copies the value from the day dropdown field to the original text field
					$('#dobDays').on("change keypress paste", function(){
						document.getElementById("databaseDay").value = document.getElementById("dobDays").value;

						if ( ($("#dobMonths").val() != "select") && ($("#dobYears").val() != "select") && ($(this).val() != "select")) {
							$("#dobDateError").hide();
						}
					});

					// Copies the value from the month dropdown field to the original text field
					$('#dobMonths').on("change keypress paste", function(){
						document.getElementById("databaseMonth").value = document.getElementById("dobMonths").value;

						if ( ($("#dobDays").val() != "select") && ($("#dobYears").val() != "select") && ($(this).val() != "select")) {
							$("#dobDateError").hide();
						}
					});

					// Copies the value from the year dropdown field to the original text field
					$('#dobYears').on("change keypress paste", function(){
						document.getElementById("databaseYear").value = document.getElementById("dobYears").value;

						if ( ($("#dobDays").val() != "select") && ($("#dobMonths").val() != "select") && ($(this).val() != "select")) {
							$("#dobDateError").hide();
						} 
					});


					// Copies the value from the day dropdown field to the original text field
					$('#tetanusDays').on("change keypress paste", function(){
						document.getElementById("databaseTetanusDay").value = document.getElementById("tetanusDays").value;
					});

					// Copies the value from the month dropdown field to the original text field
					$('#tetanusMonths').on("change keypress paste", function(){
						document.getElementById("databaseTetanusMonth").value = document.getElementById("tetanusMonths").value;
					});

					// Copies the value from the year dropdown field to the original text field
					$('#tetanusYears').on("change keypress paste", function(){
						document.getElementById("databaseTetanusYear").value = document.getElementById("tetanusYears").value;
					});

					// Validates and submits forms using ajax
					$('.submitbutton').click(function() {

						// Needs to revalidate the form based on form validation change
						var formRevalidate = $(this).data("formname");
						$(formRevalidate).validate();

						// Ensures that it doesn't sumbit both the select and add form at once
						if ($(this).data("formname") == "#epact-dependant-outofarea-add-form") {
							$("#oop-select").remove();
						}
					});

					if (document.body.className && document.body.className.indexOf("orgMemberList") != -1) {

						$('#addMembersSelect').tooltip();

						$('#advanced-filter-form-link').click(function(e){
							e.preventDefault();
							toggle_advanced_filter(this);
						});
						
						$("td.org-row").click(function() {
							if (!($(this).hasClass("edit-row")) && !($(this).hasClass("checkbox-row")) && !($(this).hasClass("status-row"))) {
								$(this).parents("tr").find("input[type=checkbox]").trigger('click');
							}
						});

						if ($("#searchTerm-search").length) {
							if ($("#searchTerm-search").val() != "") {
								$('#remove-option').show();
							}
						}

						if ($("#advancedSearchTerm-search").length) {
							if ($("#advancedSearchTerm-search").val() != "") {
								$('#advancedSearchTerm-remove-div').show();
							}
						}

						if ($("#memberId-search").length) {
							if ($("#memberId-search").val()  != "") {
								$("#memberId-remove-div").show();
							}    
						}

						$("#hasFiles-search, #hasComments-search").on('change', function() {
							if($(this).is(':checked'))
								$(this).val("true");
							else
								$(this).val("false");
						});

						$(".removeGroupMember").unbind("click").click(function(e) {
							var r = confirm(languageStrings.getText("confirm_remove_group_member"));
							if (r==true)
							{
								var fid = $(this).data("fid");
								var uid = $(this).data("uid");
								submitWithMember(fid,uid);
							}
						});

						$(".removeAdmin").unbind("click").click(function(e) {
							e.preventDefault();
							var r = confirm(languageStrings.getText("confirm_remove_admin"));
							if (r==true)
							{
								var fid = $(this).data("fid");
								var uid = $(this).data("uid");
								submitWithMember(fid,uid);
							}
						});

						$(".no-tab").click(function() {
							window.destinationHref = $(this).data("redirect");
							window.location.href = $(this).data("redirect");
						});

						// Grabs data from the URL of the page and uses it to determine which column is being sorted
						var urlParams;
						(window.onpopstate = function () {
							var match,
							pl     = /\+/g,  // Regex for replacing addition symbol with a space
							search = /([^&=]+)=?([^&]*)/g,
							decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
							query  = window.location.search.substring(1);

							urlParams = {};
							while (match = search.exec(query))
								urlParams[decode(match[1])] = decode(match[2]);
						})();

						var columnName = (urlParams["sortColumn"]);
						var columnSorted = "sort-column-".concat(columnName);

						// Used for toggling display of dropdowns for status/division
						if ((document.URL).indexOf("status") > 0 && !((document.URL).indexOf("status=ALL") > 0)) {
							$("select[name=status]").show();
							$("#remove-option").show();
						} else if ((document.URL).indexOf("division") > 0) {
							$("select[name=division]").show();
							$("#remove-option").show();
						} else if ((document.URL).indexOf("search") > 0) {


						} /*else{
                            document.getElementsByName("status")[0].selectedIndex=0;
                            document.getElementsByName("division")[0].selectedIndex=0;
                    }*/

						// Adds an up/down arrow based on the sorted column
						if (document.getElementById(columnSorted)) {
							if (urlParams["sortOrder"] == "ASC") {
								document.getElementById(columnSorted).innerHTML = document.getElementById(columnSorted).innerHTML + " <span class='glyphicon glyphicon-arrow-up'></span>";
							} else{
								document.getElementById(columnSorted).innerHTML = document.getElementById(columnSorted).innerHTML + " <span class='glyphicon glyphicon-arrow-down'></span>";
							}
						}

					}

					if (document.body.className == "orgMemberEditComment organization orgMemberEditComment") {
						//TODO: "delete" needs to be translatable
						$('#control-group-for-epact-editusercomment-form-submit input').after('<input type="button" class="btn btn-danger" id="delete" style="float:right; margin-right:10px;" value="Delete"></input>');
						$("#delete").bind("click",function() {
							$('#epact-deleteusercomment-form-submit').click();
						});
					}

					if (document.body.className == "editMemberManually organization editMemberManually") {
						$("#email1-status, #email2-status, #email3-status").each(function() {
							var relatedBadge = $(this).attr("data-badgeId");
							var messageStatus = $(this).attr("data-status");

							$(relatedBadge).html($(this).html());

							switch(messageStatus) {
							case "QUEUED":
								$(relatedBadge).addClass("label-warning");
								break;
							case "DELIVERED":
								$(relatedBadge).addClass("label-info");
								break;
							case "OPENED":
								$(relatedBadge).addClass("label-success");
								break;
							case "UNDELIVERED":
							case "UNSUBSCRIBED":
							case "SPAMREPORTED":
							case "DROPPED":
							case "BOUNCED":
								$(relatedBadge).addClass("label-danger");
								break;
							}
							$(relatedBadge).css("display","inline-block");
						});
					}

					if (document.body.className == "manualReminders admin manualReminders") {
						$("#epact-reset-reminders-form input[type=checkbox]").click(function() {
							if($(this).is(':checked')) {
								$("#epact-reset-reminders-form").find(":submit").removeClass("btn-warning");
								$("#epact-reset-reminders-form").find(":submit").addClass("btn-danger");
								$("#epact-reset-reminders-form").find(":submit").attr("value","Invalidate Reminders");

							} else {
								$("#epact-reset-reminders-form").find(":submit").removeClass("btn-danger");
								$("#epact-reset-reminders-form").find(":submit").addClass("btn-warning");
								$("#epact-reset-reminders-form").find(":submit").attr("value","Reset Reminders");
							}
						});
					}
					
					if (document.body.className == "dependantShare dependantprofile share") {
						$("input").bind("change", function(){
							if (!$("#epact-dependant-share-form input[type=checkbox]:not(:checked)").length)
							{
								$("input.submitbutton").removeAttr("disabled");
							}
							else 
							{
								$("input.submitbutton").attr("disabled", true);
							}
						}); 

						if (!$("#epact-dependant-share-form input[type=checkbox]:not(:checked)").length || !$("#epact-dependant-share-form input[type=checkbox]").length)
						{
							$("input.submitbutton").removeAttr("disabled");
						}
						
						$("a.waiver-link").click(function(e){
							e.preventDefault();
							var id = $(this).attr("id");
							$('#waiver-'+id).modal('show');
						});
					}

					if (document.body.className == "myEpact profile myEpact") {

						function changeSign(ele) {
							if (ele.hasClass("icon-plus-sign")) {
								ele.removeClass("icon-plus-sign");
								ele.addClass("icon-minus-sign");
							} else {
								ele.removeClass("icon-minus-sign");
								ele.addClass("icon-plus-sign");
							}
						}

						$("#expand-notifications").click(function() {
							$("#notifications-section").slideToggle();
							changeSign($(this));
						});  	

						$("#expand-requests").click(function() {
							$("#requests-section").slideToggle();
							changeSign($(this));
						}); 

						$("#expand-myhousehold").click(function() {
							$("#myhousehold-section").slideToggle();
							changeSign($(this));
						}); 

						$("#expand-mynetwork").click(function() {
							$("#mynetwork-section").slideToggle();
							changeSign($(this));
						}); 

						$("#expand-myresponsibilities").click(function() {
							$("#myresponsibilities-section").slideToggle();
							changeSign($(this));
						}); 

						$("#expand-myorgs").click(function() {
							$("#myorgs-section").slideToggle();
							changeSign($(this));
						}); 
					}

					if (document.body.className == "profileContact profile contact" || document.body.className == "dependantContact dependantprofile contact")  {

						$(".workPhone").click(function() {
							if ($(this).is(':checked')) {
								if ($(this).prop('id') == "workPhone-work") {					
									$('#altWorkPhoneNumber-field').attr("required","");
								} else {
									$('#altWorkPhoneNumber-field').removeAttr("required");

								}
							} 
						});

						$(".workPhone").each(function() {
							if ($(this).is(':checked')) {
								if ($(this).prop('id') == "workPhone-work") {
									$('#altWorkPhoneNumber-field').attr("required","");
								} else {
									$('#altWorkPhoneNumber-field').removeAttr("required");
								}
							} 
						});
						
						if($("#workProvinceList-CA:visible").length || $("#workProvinceList-US:visible").length)
						{
							$("#workProvince-label").show();
						}
					}
					if (document.body.className == "editMemberManually organization editMemberManually") {
						// Submits form and proceeds to the previous page
						$("#edit-save").click(function() {
							$('#epact-edit-member-form').data("redirectpage", $("#edit-save").data("redirect"));
							$('#epact-edit-member-form').validate();
							redirect = $("#edit-save").data("redirect");
							$('form#epact-edit-member-form').submit();
						});
					}

					if (document.body.className == "addMembersToGroup organization addMembersToGroup") {
						var select = $("#epact-add-members-to-group-form select[name='groupId']");
						select.on("change keypress paste", function(){

							if ($(this).val() == "NEW") {
								$(".control-group").show();
								$("input[name=groupName]").attr("required","");
							} else {
								$("#control-for-groupName, #control-for-startTime, #control-for-endTime,#control-for-sameDate, .description-section").hide();
								$("input[name=groupName]").removeAttr("required");
							}
						});

						if(select.val() == "NEW") {
							$("#control-for-groupName").show();
							$("input[name=groupName]").attr("required","");
						}
					}

					if (document.body.className == "editOrganizationGroup organization editOrganizationGroup") {
						// Submits form and proceeds to the previous page
						$("#edit-save").click(function() {
							$('#epact-edit-group-form').data("redirectpage", $("#edit-save").data("redirect"));
							$('#epact-edit-group-form').validate();
							redirect = $("#edit-save").data("redirect");
							$('form#epact-edit-group-form').submit();
						});
					}

					if ($("body").hasClass("editCampaign")) {

						// Submits form and proceeds to the previous page
						$("#edit-save").click(function() {
							$('#edit-campaign-form').data("redirectpage", $("#edit-save").data("redirect"));
							$('#edit-campaign-form').validate();
							redirect = $("#edit-save").data("redirect");
							$('form#edit-campaign-form').submit();
						});
					}

					if ($("body").hasClass("editApplicationOrganization")) {

						// Submits form and proceeds to the previous page
						$("#edit-save").click(function() {
							$('#edit-application-organization-form').data("redirectpage", $("#edit-save").data("redirect"));
							$('#edit-application-organization-form').validate();
							redirect = $("#edit-save").data("redirect");
							$('form#edit-application-organization-form').submit();
						});
					}
					if ($("body").hasClass("editApplication")) {

						// Submits form and proceeds to the previous page
						$("#edit-save").click(function() {
							$('#edit-application-form').data("redirectpage", $("#edit-save").data("redirect"));
							$('#edit-application-form').validate();
							redirect = $("#edit-save").data("redirect");
							$('form#edit-application-form').submit();
						});
					}
					if (document.body.className == "editAdminManually organization editAdminManually") {
						// Submits form and proceeds to the previous page
						$("#edit-save").click(function() {
							$('#epact-edit-admin-form').data("redirectpage", $("#edit-save").data("redirect"));
							$('#epact-edit-admin-form').validate();
							redirect = $("#edit-save").data("redirect");
							$('form#epact-edit-admin-form').submit();
						});
					}

					if ($("body").hasClass("overviewUserConnectionEdit")) {

						// Ensures that primary phone numbers are required for adults
						if ($("#profile-right").hasClass("adult")) {
							$("input[name=primaryPhoneOther]").attr("required","");
							$(".primaryPhoneOtherIsMobile-select").attr("notEqual","select"); 
						}

						// Submits form and proceeds to the previous page
						$("#contact-save").click(function() {
							if (!hasAttribute($(this),"disabled"))
							{
								$('#epact-overview-contact-form').data("redirectpage", $("#contact-save").data("redirect"));
								$('#epact-overview-contact-form').validate();
								redirect = $("#contact-save").data("redirect");
								$('form#epact-overview-contact-form').submit();
							}
						});
					}
					if (document.body.className == "dependantOverviewMedical dependantprofile overviewMedical") {

						// Submits form and proceeds to the previous page
						$("#medical-save").click(function() {
							$('#epact-dependant-medical-form').data("redirectpage", $("#medical-save").data("redirect"));
							//$('#epact-dependant-medical-form').valid();
							redirect = $("#medical-save").data("redirect");
							//$("form#epact-dependant-medical-form *").addClass("ignore");
							$('form#epact-dependant-medical-form').submit();
							//$("form#epact-dependant-medical-form *").removeClass("ignore");
//							$("form#epact-dependant-medical-form").valid();
						});
					}


					if ($("body").hasClass("profileContacts")) {
						// Displays the contacts saved popup
						$("#contacts-no-org").click(function() {
							displaySavePopup("#epact-dependant-contacts-form", true);
							$(".btn-save").html("<span class='glyphicon glyphicon-ok'></span> Saved");     		
							$(".btn-save").attr("disabled","");
							$(".save-next").show();
						});
					}

					if ($("body").hasClass("family")) {
						// Displays the contacts saved popup
						$("#family-save").click(function() {
							displaySavePopup("familyWizard", true);
							$(".btn-save").html("<span class='glyphicon glyphicon-ok'></span> Saved");     		
							$(".btn-save").attr("disabled","");
							$(".save-next").show();
						});  
					}        

					if ((document.body.className == "profileContacts profile contacts") || (document.body.className == "myEpact profile myEpact")) {

						// Cancels the emergency contact edit mode
						$(".cancel-emergency-button").click(function() {
							var heyhey = document.URL + " #emergency-contacts";
							$('#emergency-contacts').load(heyhey, function() {
								$.getScript(scriptURL); 
							});              
						});


						setAjaxForm('#epact-outofarea-delete-form',{"reloadDiv": '#oop-contacts',"modalDiv":'#edit-oopcontact'});               
						setAjaxForm('#epact-deleteemergencycontact-form',{"reloadDiv":'#emergency-contacts',"modalDiv":'#edit-emergencycontact'}); 

						// Ensures only one out of area contact is selected
						$(".oopSelect").each(function()
								{
							$(this).change(function() {
								if ($(this).prop('checked')) {
									$(".oopSelect").prop('checked',false);
									$(this).prop('checked',true);
								}
							});
								});

					}

					if (document.body.className ==  "forgotPasswordUpdate home forgotPasswordUpdate") {
					}

					// Organization folder validations
					if (document.body.className ==  "addMembersManually organization addMembersManually") {}
					if (document.body.className ==  "editMemberManually organization editMemberManually") {}

					if (document.body.className ==  "orgMemberListPrint organization orgMemberListPrint") {
						document.getElementById("epact-print-org-member-list-form-submit").className += " btn-small";
					}
					
					if (document.body.className && document.body.className.indexOf("orgMemberList") != -1) {

						// Adds/removes initial highlight on table row whether or not it is selected
						$('.orgcheck').each(function(){
							var rowName = ".cell-" + $(this).data("checknum");
							if ($(this).prop('checked')) {
								$(rowName).css("background", "rgb(250, 250, 190)");
							} else {
								$(rowName).css("background", "");
							}
						});

						// Adds/removes highlight on table row
						$(".orgcheck").change(function(){                		
							var rowName = ".cell-" + $(this).data("checknum");
							if ($(this).prop('checked')) {
								$(rowName).css("background", "rgb(250, 250, 190)");
								if ($("#addMembersSelected").length) {
									$("#addMembersSelect").hide();
									$("#addMembersSelected").show();
								}

								if($("#total-results").length) {
									var newResults = parseInt($("#total-results").text()) + 1;
									if (newResults == parseInt($("#total-results").attr("data-original"))) {
										$("#all-results").show();
									}

									$("#total-results").text(newResults);

								}
							} else {

								if ($(".orgcheck:checked").length > 0) {
									if ($("#addMembersSelected").length) {
										$("#addMembersSelect").hide();
										$("#addMembersSelected").show();
									}              				
								} else {
									if ($("#addMembersSelected").length) {
										$("#addMembersSelected").hide();
										$("#addMembersSelect").show();
									}   			
								}

								$(rowName).css("background", "");
								$(rowName).css("color","");

								if($("#total-results").length) {
									var newResults = parseInt($("#total-results").text()) - 1;
									$("#total-results").text(newResults);
									$("#all-results").hide();
								}
								$('#select-everything').hide();
							}
						});

						// Adds a highlight to all rows and selects them all
						$('#select-all').click(function() {
							if (document.getElementById("row-1")) {
								$('.orgcheck').prop('checked', true);
								$('.org-row').css("background", "rgb(250, 250, 190)");

								if ($("#addMembersSelected").length) {
									$("#addMembersSelect").hide();
									$("#addMembersSelected").show();
								}

								var allUsers = $(this).data("allusers");
								var maxUsers = $(this).data("maxusers");

								// Only shows the select everything button if not all of the records are shown on one page
								if (allUsers != maxUsers) {
									$('#select-everything').show();
								}
							}

							$("#select-all").hide();
							$("#unselect-all").css("display","block");
						});


						// Removes highlight from all rows and unselects them all
						$('#unselect-all').click(function() {
							$('.orgcheck').prop('checked', false);
							$('.org-row').css("background", "");
							$('#select-everything').hide();

							if ($("#addMembersSelected").length) {
								$("#addMembersSelected").hide();
								$("#addMembersSelect").show();
							}

							$("#unselect-all").hide();
							$("#select-all").css("display","block");
						});


						// Hides select all buttons, shows unselect buttons and selects everything
						$('#select-everything-btn').click(function() {
							var redirect = $(this).data("redirect") + " #org-table";
							$('#select-everything').hide();
							$('#select-all').hide();
							$('#unselect-everything').show();
							$('#unselect-all').hide();

							$('#org-table').load(redirect, function(){
								$('#unselect-everything').show();
								$.getScript(scriptURL); 
								if ($("#addMembersSelected").length) {
									$("#addMembersSelect").hide();
									$("#addMembersSelected").show();
								}
							});
						});

						// Hides unselect all buttons, shows select buttons and unselects everything
						$('.unselect-everything-btn').on("click", function() {
							$('#select-everything').hide();
							$('#unselect-everything').hide();
							var redirect = $(this).data("redirect") + " #org-table";
							$('#org-table').load(redirect, function(){
								$.getScript(scriptURL); 
							});

							if ($("#addMembersSelected").length) {
								$("#addMembersSelected").hide();
								$("#addMembersSelect").show();
							}

							$('#select-all').show();
							$('#unselect-all').show();
						});


						// Displays/hides select boxes based on whether or not the user selected them
						$('#filteroptions').change(function() {
							var e = document.getElementById("filteroptions");
							var filteroption = e.options[e.selectedIndex].value;


							if (filteroption == "optselect") {
								document.getElementsByName("status")[0].style.display = 'none';
								document.getElementsByName("division")[0].style.display = 'none';
								document.getElementsByName("medical")[0].style.display = 'none';
							} else if (filteroption == "optstatus") {
								document.getElementsByName("status")[0].style.display = 'block';
								document.getElementsByName("division")[0].style.display = 'none';
								document.getElementsByName("medical")[0].style.display = 'none';
							} else if (filteroption == "optdivision") {
								document.getElementsByName("status")[0].style.display = 'none';
								document.getElementsByName("division")[0].style.display = 'block';
								document.getElementsByName("medical")[0].style.display = 'none';
							}  else if (filteroption == "optmedical") {
								document.getElementsByName("status")[0].style.display = 'none';
								document.getElementsByName("division")[0].style.display = 'none';
								document.getElementsByName("medical")[0].style.display = 'block';
							}                	
						});

					}
					if (document.body.className ==  "orgMemberMessage organization orgMemberMessage") {}
					if (document.body.className ==  "previewInvite organization previewInvite" || 
							document.body.className ==  "previewOrganizationAdminUserInvites admin previewOrganizationAdminUserInvites") 
					{
						$("table a").css("cursor","default");
						$("table a").click(function(e){
							e.preventDefault();
						});
						$(".message-preview a").css("cursor","default");
						$(".message-preview a").click(function(e){
							e.preventDefault();
						});
					}
					if (document.body.className ==  "reviewInvite organization reviewInvite") {
						$("table a").css("cursor","default");
						$("table a").click(function(e){
							e.preventDefault();
						});
						$(".message-preview a").css("cursor","default");
						$(".message-preview a").click(function(e){
							e.preventDefault();
						});
					}


					if (document.body.className ==  "referFriends home referFriends") {

						var referFriend = 1;
						$("#refer-another-friend").click(function(e) {
							e.preventDefault();
							referFriend++;
							$("#refer-friend" + referFriend).show();
							$("#refer-friend" + referFriend + " input").attr("required","");
							if (referFriend == 5) {
								$(this).hide();
							}
						});

						$(".remove-refer-friend").click(function(e) {
							e.preventDefault();
							$(this).parents(".refer-block").hide();
							$(this).parents(".refer-block").find("input").val("");
							$(this).parents(".refer-block").find("label.error").hide();
							$(this).parents(".refer-block").find("input").removeAttr("required");
							referFriend--;
							$("#refer-another-friend").show();
						});

					} 

					if (document.body.className ==  "referOrganizations home referOrganizations") {

						var referOrg = 1;
						$("#refer-another-org").click(function(e) {
							e.preventDefault();
							referOrg++;
							$("#refer-org" + referOrg).show();
							$("#refer-org" + referOrg + " input:first").attr("required","");
							$("#refer-org" + referOrg + " select:first").attr("notEqual","select");           
							$("#refer-org" + referOrg + " .specificContact").attr("required","");

							if (referOrg == 5) {
								$(this).hide();
							}
						});

						$(".specificContact-true").click(function() {
							$(this).parents(".refer-block").find(".refer-section").show();
						});

						$(".specificContact-false").click(function() {
							$(this).parents(".refer-block").find(".refer-section").hide();
						});

						$(".remove-refer-org").click(function(e) {
							e.preventDefault();
							$(this).parents(".refer-block").hide();
							$(this).parents(".refer-block").find("input").val("");
							$(this).parents(".refer-block").find("label.error").hide();
							$(this).parents(".refer-block").find("input").removeAttr("required");
							$(this).parents(".refer-block").find("select").removeAttr("notEqual");
							$(this).parents(".refer-block").find("select").prop('selectedIndex',0);
							$(this).parents(".refer-block").find(".specificContact").removeAttr("required");

							referOrg--;
							$("#refer-another-org").show();
						});
					} 

					// Invite folder validations
					if (document.body.className ==  "createinvite invite createinvite") {}

					// Inviteredeem folder validations
					if (document.body.className ==  "redeeminvite inviteredeem redeeminvite") { }
					if (document.body.className ==  "redeemOrganizationAdminUserInvite inviteredeem redeemOrganizationAdminUserInvite") {}
					if (document.body.className ==  "redeemOrganizationMemberUserInvite inviteredeem redeemOrganizationMemberUserInvite") {}
					if (document.body.className ==  "redeemOrganizationMemberUserInviteDecline inviteredeem redeemOrganizationMemberUserInviteDecline") {}


					// Home folder validations
					if (document.body.className ==  "register home register") {}
					if (document.body.className ==  "forgotPassword home forgotPassword") {


					}      

					if (document.body.className ==  "submitCreateOrganizationGroup organization submitCreateOrganizationGroup") {
						// Submits form and proceeds to the next page
						$("#group-admin-add").click(function() {

							$('#epact-add-group-admins-form').data("redirectpage", $("#group-admin-add").data("redirect"));
							$('#epact-add-group-admins-form').validate();
							redirect = $("#group-admin-add").data("redirect");
							$('form#epact-add-group-admins-form').submit();
						});
					}

					if ($("body").hasClass("createCampaign") || $("body").hasClass("editCampaign")) {

						$("#campaignStartDateDisplay").datetimepicker({
							changeMonth: true,
							timeFormat: "h:mm tt",
							controlType: 'select',
							changeYear: true,
							yearRange: "-2:+7",
							onClose: function(dateText, inst) {
						      showPicker = false;
						    }
						});

						if ($("#campaignStartDateDisplay").val() != "") {
							$("input[name=campaignStartDate]").val(Date.parse($("#campaignStartDateDisplay").datepicker("getDate")));
							$("#campaignStartDateDisplay").datepicker('setDate', $("#campaignStartDateDisplay").val());
						}

						$("#campaignEndDateDisplay").datetimepicker({
							changeMonth: true,
							timeFormat: "h:mm tt",
							controlType: 'select',
							minDate: $("#campaignStartDateDisplay").datetimepicker("getDate"),
							yearRange: "-2:+7",
							onClose: function(dateText, inst) {
						      showPicker = false;
						    }
						});

						if ($("#campaignEndDateDisplay").val() != "") {
							$("input[name=campaignEndDate]").val(Date.parse($("#campaignEndDateDisplay").datepicker("getDate")));
							$("#campaignEndDateDisplay").datepicker('setDate', $("#campaignEndDateDisplay").val());
						}

						$("#campaignStartDateDisplay").on("change keypress paste", function(){
							$("#campaignEndDateDisplay").datetimepicker( "option", "minDate", $(this).datetimepicker("getDate"));
							$("input[name=campaignStartDate]").val(Date.parse($(this).datetimepicker("getDate")));
							$("label[for=campaignStartDate-field]").hide();
						});

						$("#campaignEndDateDisplay").on("change keypress paste", function(){
							$("input[name=campaignEndDate]").val(Date.parse($(this).datetimepicker("getDate")));
							$("label[for=campaignEndDate-field]").hide();
						});
					}

					if ($("body").hasClass("adminCreateOrganization") || $("body").hasClass("adminEditOrganization")) {
						
						$('#accordion-waivers').sortable({
						  containerSelector: 'div#accordion-waivers',
						  itemSelector: 'div.panel-default',
						  handle: 'i.icon-move',
						  onDrop: function($item, container, _super) {
								$item.removeClass("dragged").removeAttr("style");
								$("body").removeClass("dragging");
								$("#accordion-waivers .panel").each(function(index){
									$("input[name^=waiverWeight]",this).val(index);
								});
						  }
						});
						
						if (window.collapseId != undefined) 
						{
							$("#" + window.collapseId).addClass("in");
						}
						
						$("#organizationInfo-controls").css("display","none");
						$("#orgEmailTemplates-controls").css("display","none");
						$("#orgWaiver-controls").css("display","none");
						
						var activeFields = $("#dependant-info-tabs2 li.active a").data("show");
						$("#"+activeFields).css("display","block");
						
						$("#dependant-info-tabs2 li a").click(function(e){
							e.preventDefault();
							if ($("form#epact-admineditorg-form").valid())
							{
								$("#dependant-info-tabs2 li").removeClass("active");
								$(this).parent().addClass("active");
								var show = $(this).data("show");
								$("#organizationInfo-controls").css("display","none");
								$("#orgEmailTemplates-controls").css("display","none");
								$("#orgWaiver-controls").css("display","none");
								$("#"+show).css("display","block");
							}
						});

						$("a.deleteWaiverBtn").click(function(e){
							e.preventDefault();
							$.ajax({
								url: $(this).attr("href"),
								cache: false,
								success: function(data) {
										$("#dependant-info").load(document.URL+" #dependant-info", function(){
											$(this).children().unwrap();
											$.getScript(scriptURL);
										});
								}
							});
						});
						
						$("#epact-admineditorg-form-submit").click(function(e){
							e.preventDefault();
							$("#accordion-templates .panel-collapse").each(function(){
								if ($(this).hasClass("in"))
									window.collapseId = $(this).attr("id");
							});
							if ($("form#epact-admineditorg-form").valid())
							{
								$("form#epact-admineditorg-form").ajaxSubmit({
									success: function(data) {
										$("#dependant-info").load(document.URL+" #dependant-info", function(){
											$(this).children().unwrap();
											$.getScript(scriptURL);
										});
									}
								});
							}
							else
							{
								$("html, body").animate({ scrollTop: 0 }, "slow");
							}
						});
						
						$("#licenseStartDateDisplay").datepicker({
							changeMonth: true,
							changeYear: true,
							yearRange: "-2:+7",
							onClose: function(dateText, inst) {
						      showPicker = false;
						    }
						});

						$("#licenseEndDateDisplay, #licenseRenewalDateDisplay").datepicker({
							changeMonth: true,
							changeYear: true,
							minDate: $("#licenseStartDateDisplay").datepicker("getDate"),
							yearRange: "-2:+25",
							onClose: function(dateText, inst) {
						      showPicker = false;
						    }
						});

						if ($("#licenseStartDateDisplay").val() != "") {
							$("input[name=licenseStartDate]").val(Date.parse($("#licenseStartDateDisplay").datepicker("getDate")));
							$("#licenseStartDateDisplay").datepicker('setDate', $("#licenseStartDateDisplay").val());
						}
						if ($("#licenseEndDateDisplay").val() != "") {
							$("input[name=licenseEndDate]").val(Date.parse($("#licenseEndDateDisplay").datepicker("getDate")));
							$("#licenseEndDateDisplay").datepicker('setDate', $("#licenseEndDateDisplay").val());
						}
						if ($("#licenseRenewalDateDisplay").val() != "") {
							$("input[name=licenseRenewalDate]").val(Date.parse($("#licenseRenewalDateDisplay").datepicker("getDate")));
							$("#licenseRenewalDateDisplay").datepicker('setDate', $("#licenseRenewalDateDisplay").val());
						}

						$("#licenseEndDateDisplay").on("change keypress paste", function(){
							$("input[name=licenseEndDate]").val(Date.parse($(this).datepicker("getDate")));
						});

						$("#licenseRenewalDateDisplay").on("change keypress paste", function(){
							$("input[name=licenseRenewalDate]").val(Date.parse($(this).datepicker("getDate")));
						});

						$("#licenseStartDateDisplay").on("change keypress paste", function(){
							$("#licenseEndDateDisplay, #licenseRenewalDateDisplay").datepicker( "option", "minDate", $(this).datepicker("getDate"));
							$("input[name=licenseStartDate]").val(Date.parse($(this).datepicker("getDate")));
						});

					}


					if ((document.body.className ==  "createOrganizationGroup organization createOrganizationGroup") || 
							(document.body.className ==  "editOrganizationGroup organization editOrganizationGroup") ||
							(document.body.className == "addMembersToGroup organization addMembersToGroup")){

						$("#endTimeDisplay").each(function() {
							$("#endTimeDisplay" ).datepicker( "option", "minDate", $("#startTimeDisplay").datepicker("getDate"));
						});

						$("#startTimeDisplay").datepicker({
							changeMonth: true,
							changeYear: true,
							yearRange: "-2:+7",
							onClose: function(dateText, inst) {
						      showPicker = false;
						    }
						});

						$("#endTimeDisplay").datepicker({
							changeMonth: true,
							changeYear: true,
							minDate: $("#startTimeDisplay").datepicker("getDate"),
							yearRange: "-2:+7",
							onClose: function(dateText, inst) {
						      showPicker = false;
						    }
						});

						// Ensures that the minimum date of the end date is the date of the start time
						$("#startTimeDisplay").on("change keypress paste", function(){
							$("#endTimeDisplay" ).datepicker( "option", "minDate", $("#startTimeDisplay").datepicker("getDate"));


							if  ($("input[name=sameDate]").is(":checked")) {
								$("#endTimeDisplay").datepicker( "setDate", $("#startTimeDisplay").datepicker("getDate"));
								$("#endTime-field").val(Date.parse($("#startTimeDisplay").datepicker("getDate")));
							}

							$("#startTime-field").val(Date.parse($("#startTimeDisplay").datepicker("getDate")));

							if (isNaN($("#endTime-field").val())) {
								$("#endTime-field").val("");
							}

							if (isNaN($("#startTime-field").val())) {
								$("#startTime-field").val("");
							}
						});

						$("#endTimeDisplay").change(function() {
							if  ($("input[name=sameDate]").is(":checked")) {
								$("#startTimeDisplay").datepicker( "setDate", $("#endTimeDisplay").datepicker("getDate"));
								$("#startTime-field").val(Date.parse($("#startTimeDisplay").datepicker("getDate")));
							}

							$("#endTime-field").val(Date.parse($("#endTimeDisplay").datepicker("getDate")));
							$("#endTimeDisplay" ).datepicker( "option", "minDate", $("#startTimeDisplay").datepicker("getDate"));

							if (isNaN($("#endTime-field").val())) {
								$("#endTime-field").val("");
							}

							if (isNaN($("#startTime-field").val())) {
								$("#startTime-field").val("");
							}
						});



						// Makes the end and start dates the same
						$("input[name=sameDate]").click(function() {
							if ($(this).is(':checked')) {
								$("#endTimeDisplay").datepicker( "setDate", $("#startTimeDisplay").datepicker("getDate"));

								$("#startTime-field").val(Date.parse($("#startTimeDisplay").datepicker("getDate")));
								$("#endTime-field").val(Date.parse($("#startTimeDisplay").datepicker("getDate")));

								if (isNaN($("#endTime-field").val())) {
									$("#endTime-field").val("");
								}

								if (isNaN($("#startTime-field").val())) {
									$("#startTime-field").val("");
								}
							}
						});

					}


					if(document.body.className == 'profileContact profile contact' || document.body.className == 'profileConfirmContact profile confirmContact'){

						if ($(".parent-info").data("showwelcome") == "on") {
							$(window).load(function(){
								$('#welcome').modal('show');
							});
						}

						// Submits form and proceeds to the next page
						$("#contact-next").click(function() {
							$('#epact-contactinfo-form').data("redirectpage", $("#contact-next").data("redirect"));
							$('#epact-contactinfo-form').validate();
							redirect = $("#contact-next").data("redirect");
							$('form#epact-contactinfo-form').submit();
						});

						if ($("#workPhone-work").is(':checked')) {
							$("#altWorkPhoneNumber-field").addClass("validate");
						} 

					}


					if ($("body").hasClass("contact")) {  

						$("#contact-save").unbind("click").click(function() {
							if (!hasAttribute($(this),"disabled"))
							{
								var formToSave = $(this).data("saveform");
								$(formToSave).attr("data-saveform", "true");    	
	
								if (formToSave == "#epact-contactinfo-form") {
									$(formToSave).validate();
								} else {
									$(formToSave).submit();
								}
							}
						});
					}
					if(document.body.className == 'nondependantContact nondependantprofile contact'){

						if ($(".parent-info").data("showwelcome") == "on") {
							$(window).load(function(){
								$('#welcome').modal('show');
							});
						}

						$('#epact-nondependant-contactinfo-form').data("redirectpage", $("#contact-next").data("redirect"));

						// Submits form and proceeds to the next page
						$("#contact-next").click(function() {
							$('#epact-nondependant-contactinfo-form').validate();
							redirect = $("#contact-next").data("redirect");
							$('form#epact-nondependant-contactinfo-form').submit();
						});

						if ($("#workPhone-work").is(':checked')) {
							$("#altWorkPhoneNumber-field").addClass("validate");
						} 
					}

					if (document.body.className == "dependantContact dependantprofile contact") { 		

						// Submits form and proceeds to the next page
						$("#contact-next").click(function() {
							$('#epact-dependant-contact-form').data("redirectpage", $("#contact-next").data("redirect"));
							$('#epact-dependant-contact-form').validate();
							redirect = $("#contact-next").data("redirect");
							$('form#epact-dependant-contact-form').submit();
						});

						if (($(".dependant-info").data("showwelcome") == "on") || ($(".profile-right").data("showwelcome") == "on")){
							$(window).load(function(){
								$('#welcome').modal('show');
							});
						}		    				
					} 

					if ((document.body.className == "profileDetails profile details") || (document.body.className == "dependantDetails dependantprofile details")){ 

						$("#details-save").unbind("click").click(function() {
							if (!hasAttribute($(this),"disabled"))
							{
								var formToSave = $(this).data("saveform");
								$(formToSave).attr("data-saveform", "true");    					
								$(formToSave).submit();
							}
						});
					}

					if (document.body.className == "dependantDetails dependantprofile details") { 
						// Submits form and proceeds to the next page
						$("#details-next, #details-back").click(function() {
							$('#epact-dependant-details-form').data("redirectpage", $(this).data("redirect"));
							$('#epact-dependant-details-form').validate();
							redirect = $(this).data("redirect");
							$('form#epact-dependant-details-form').submit();
						});  
					}

					if ($("body.dependantMedical.dependantprofile.medical, body.profileMedical.profile.medical, body.dependantMedicalDetails.dependantprofile.medicalDetails, body.dependantMedicalAdditional.dependantprofile.medicalAdditional").length) { 

						// Validate the form to show errors if the medical form is incomplete
						if (($("body.dependantMedical.dependantprofile.medical, body.dependantMedicalDetails.dependantprofile.medicalDetails, body.dependantMedicalAdditional.dependantprofile.medicalAdditional, body.profileMedical.profile.medical").length) && ($("#medical-section").hasClass("incomplete")) ) {
							//Change it to a timer because we had a cache issue with $(window).load 
							setTimeout(function(){
								$("form").valid();  
								$("#medical-section").removeClass("incomplete");
							}, 500);
						} 

						if($("body.dependantMedicalDetails.dependantprofile.medicalDetails").length)
						{
							setTimeout(function(){
								var treatmentInfo = $("#treatment-info");
								var medicalMedication = $(".medical-medication");
								var medicalHistory = $(".medical-history");
								if (treatmentInfo.length && medicalMedication.length && medicalHistory.length)
								{
									var top = $("#treatment-info").position().top - $(".medical-medication").outerHeight() - $(".medical-history").outerHeight() - 10;
									$(".medical-medication").css("top", top);
								}
							},500);
							$("a.addMedicationBtn").click(function(e){
								e.preventDefault();
								add_medication();
							});
							$(".remove-medication span").click(function(e){
								var index = $(this).data("index");
								if (index && index != null)
									remove_medication(this, index);
							});
						}
						
						$(".medical-extra-true").each(function(){
							if ($(this).is(':checked')) {
								var description = "#" + $(this).attr("name") + "-note";
								$(description).show();
							} else {
								$(description).hide();
							}
						});

						$(".medical-extra").click(function(){
							var description = "#" + $(this).attr("name") + "-note";
							if ($(this).val() == "true") {
								$(description).show();
							} else {
								$(description).hide();
							}
						});


						// Hides/shows allergy field if checked
						if ($("input[name=MAJOR_ILLNESS_OR_INJURIES_IN_LAST_YEAR]:radio:checked").val() == 'true') {
							$(".RECENT_INJURIES_INFO-section").show();
							$("textarea[name=RECENT_INJURIES_INFO]").attr("required","");
						} else {
							$(".RECENT_INJURIES_INFO-section").hide();
							$("textarea[name=RECENT_INJURIES_INFO]").attr("required","");
						};

						$("input[name=MAJOR_ILLNESS_OR_INJURIES_IN_LAST_YEAR]:radio").click(function() {
							if ($(this).val() == 'true') {
								$(".RECENT_INJURIES_INFO-section").show();
								$("textarea[name=RECENT_INJURIES_INFO]").attr("required","");
							} else {
								$(".RECENT_INJURIES_INFO-section").hide();
								$("textarea[name=RECENT_INJURIES_INFO]").attr("required","");
							}
						});

						if($("input[name=OTHER_MEDICAL_HISTORY]").length) {
							if ($("input[name=OTHER_MEDICAL_HISTORY]").is(':checked')) {
								$("input[name=OTHER_MEDICAL_HISTORY_INFO]").show();
								$("input[name=OTHER_MEDICAL_HISTORY_INFO]").attr("required","");
							} else {
								$("input[name=OTHER_MEDICAL_HISTORY_INFO]").hide();
								$("input[name=OTHER_MEDICAL_HISTORY_INFO]").attr("required","");
							};
						};

						$("input[name=OTHER_MEDICAL_HISTORY]").click(function() {
							if ($(this).is(':checked')) {
								$("input[name=OTHER_MEDICAL_HISTORY_INFO]").show();
								$("input[name=OTHER_MEDICAL_HISTORY_INFO]").attr("required","");
							} else {
								$("input[name=OTHER_MEDICAL_HISTORY_INFO]").hide();
								$("input[name=OTHER_MEDICAL_HISTORY_INFO]").attr("required","");
							}
						});

						if ($("input[name='UNDER_TREATMENT']:radio:checked").val() == 'true') {
							$(".UNDER_TREATMENT_FOR-section").show();
							$("textarea[name='UNDER_TREATMENT_FOR']").attr("required","");
						} else {
							$(".UNDER_TREATMENT_FOR-section").hide();
							$("textarea[name='UNDER_TREATMENT_FOR']").attr("required","");
						};

						$("input[name='UNDER_TREATMENT']:radio").click(function() {
							if ($(this).val() == 'true') {
								$(".UNDER_TREATMENT_FOR-section").show();
								$("textarea[name='UNDER_TREATMENT_FOR']").attr("required","");
							} else {
								$(".UNDER_TREATMENT_FOR-section").hide();
								$("textarea[name='UNDER_TREATMENT_FOR']").attr("required","");
							}
						});

						if ($("input[name=FOOD_ALLERGIES]").is(':checked')) {
							$(".FOOD_ALLERGY_INFO-section").show();
							$("textarea[name=FOOD_ALLERGY_INFO]").attr("required","");
						} else {
							$(".FOOD_ALLERGY_INFO-section").hide();
							$("textarea[name=FOOD_ALLERGY_INFO]").attr("required","");
						};

						$("input[name=FOOD_ALLERGIES]").click(function() {
							if ($(this).is(':checked')) {
								$(".FOOD_ALLERGY_INFO-section").show();
								$("textarea[name=FOOD_ALLERGY_INFO]").attr("required","");
							} else {
								$(".FOOD_ALLERGY_INFO-section").hide();
								$("textarea[name=FOOD_ALLERGY_INFO]").attr("required","");
							}
						});

						// Hides/shows allergy field if checked
						if ($("input[name=ALLERGIES]").is(':checked')) {
							$(".ALLERGIES_INFO-section").show();
							$("textarea[name=ALLERGIES_INFO]").attr("required","");
						} else {
							$(".ALLERGIES_INFO-section").hide();
							$("textarea[name=ALLERGIES_INFO]").removeAttr("required");
						}

						$("input[name=ALLERGIES]").click(function() {
							if ($(this).is(':checked')) {
								$(".ALLERGIES_INFO-section").show();
								$("textarea[name=ALLERGIES_INFO]").attr("required","");
							} else {
								$(".ALLERGIES_INFO-section").hide();
								$("textarea[name=ALLERGIES_INFO]").removeAttr("required");
							}
						});


						// Hides/shows allergy field if checked
						if ($("input[name=MEDICATION]").is(':checked')) {
							$(".MEDICATIONS_INFO-section").show();
							$("textarea[name=MEDICATIONS_INFO]").attr("required","");
						} else {
							$(".MEDICATIONS_INFO-section").hide();
							$("textarea[name=MEDICATIONS_INFO]").removeAttr("required");
						}

						$("input[name=MEDICATION]").click(function() {  					
							if ($(this).is(':checked')) {
								$(".MEDICATIONS_INFO-section").show();
								$("textarea[name=MEDICATIONS_INFO]").attr("required","");
							} else {
								$(".MEDICATIONS_INFO-section").hide();
								$("textarea[name=MEDICATIONS_INFO]").removeAttr("required");
							}
						});


						// Hides/shows medical brace field if checked
						if ($("input[name=PRESENTLY_INJURED]").is(':checked')) {
							$(".PRESENTLY_INJURED_DESCRIPTION-section").show();
							$("#PRESENTLY_INJURED_DESCRIPTION").attr("required","");
						} else {
							$(".PRESENTLY_INJURED_DESCRIPTION-section").hide();
							$("#PRESENTLY_INJURED_DESCRIPTION").removeAttr("required");
						}

						$("input[name=PRESENTLY_INJURED]").click(function() {
							if ($(this).is(':checked')) {
								$(".PRESENTLY_INJURED_DESCRIPTION-section").show();
								$("#PRESENTLY_INJURED_DESCRIPTION").attr("required","");
							} else {
								$(".PRESENTLY_INJURED_DESCRIPTION-section").hide();
								$("#PRESENTLY_INJURED_DESCRIPTION").removeAttr("required");
							}
						});

						$('input[name=WEARS_GLASSES]').each(function() {
							if ($(this).is(':checked')) {
								$("#control-for-SHATTERPROOF_LENSES").show();
								//$("input[name=SHATTERPROOF_LENSES]").attr("required","");
							} else {
								$("#control-for-SHATTERPROOF_LENSES").hide();
								//$("input[name=SHATTERPROOF_LENSES]").removeAttr("required");
							}
						});
						$('input[name=WEARS_GLASSES]').click(function() {
							if ($(this).is(':checked')) {
								$("#control-for-SHATTERPROOF_LENSES").show();
								//$("input[name=SHATTERPROOF_LENSES]").attr("required","");
							} else {
								$("#control-for-SHATTERPROOF_LENSES").hide();
								//$("input[name=SHATTERPROOF_LENSES]").removeAttr("required");
							}
						});

						// Hides/shows medical brace field if checked
						if ($("input[name=MEDICAL_INFO_BRACELET]").is(':checked')) {
							$(".MEDICAL_INFO_BRACELET_WHY-section").show();
							$("#MEDICAL_INFO_BRACELET_WHY").attr("required","");
						} else {
							$(".MEDICAL_INFO_BRACELET_WHY-section").hide();
							$("#MEDICAL_INFO_BRACELET_WHY").removeAttr("required");
						}

						$("input[name=MEDICAL_INFO_BRACELET]").click(function() {
							if ($(this).is(':checked')) {
								$(".MEDICAL_INFO_BRACELET_WHY-section").show();
								$("#MEDICAL_INFO_BRACELET_WHY").attr("required","");
							} else {
								$(".MEDICAL_INFO_BRACELET_WHY-section").hide();
								$("#MEDICAL_INFO_BRACELET_WHY").removeAttr("required");
							}
						});

						// Hides/shows diabetes field if checked
						if ($("input[name=DIABETIC]").is(':checked')) {
							$(".DIABETIC_TYPE-section").show();				
							$('select[name=DIABETIC_TYPE]').attr("notEqual","none");
						} else {
							$(".DIABETIC_TYPE-section").hide();
							$('select[name=DIABETIC_TYPE]').removeAttr("notEqual");
						}

						$("input[name=DIABETIC]").click(function() {
							if ($(this).is(':checked')) {
								$(".DIABETIC_TYPE-section").show();				
								$('select[name=DIABETIC_TYPE]').attr("notEqual","none");
							} else {
								$(".DIABETIC_TYPE-section").hide();
								$('select[name=DIABETIC_TYPE]').removeAttr("notEqual");
							}
						});
					}

					if ($("body.dependantMedical.dependantprofile.medical, body.dependantMedicalDetails.dependantprofile.medicalDetails, body.dependantMedicalAdditional.dependantprofile.medicalAdditional").length) {      	

						$("#medical-save").click(function() {
							var formToSave = $(this).data("saveform");
							$("#medical-providers").removeClass("required-providers");
							$(formToSave).attr("data-saveform", "true");    					
							$(formToSave).submit();
							$("#medical-providers").addClass("required-providers");
							if (!$('#medical-providers.required-providers #medical-people').length) {
								$('#medical-providers.required-providers #medical-requirement').show();
							}
						});

						// Submits form and proceeds to the next page
						$("#medical-overview").click(function() {
							$('#epact-dependant-medical-form').data("redirectpage", $(this).data("redirect"));
							$('#epact-dependant-medical-form').validate();
							redirect = $(this).data("redirect");
							$('form#epact-dependant-medical-form').submit();
						});

					}

					if ($("body.dependantMedicalDetails.dependantprofile.medicalDetails").length) { 
						$("input[name^=dose], input[name^=instructions]").bind("input", function(){
							var medicationRow = $(this).data("index");
							if ($(".med-index-"+medicationRow+" input[name=dose"+medicationRow+"]").val() || $(".med-index-"+medicationRow+" input[name=instructions"+medicationRow+"]").val())
							{
								$(".med-index-"+medicationRow+" input[name=medication"+medicationRow+"]").attr("required","true");
							} 
							else
							{
								$(".med-index-"+medicationRow+" input[name=medication"+medicationRow+"]").removeAttr("required");
							}
						});

						if (!$(".med-index-1 input[name=medication1]").val() && !$(".med-index-1 input[name=dose1]").val() && !$(".med-index-1 input[name=instructions1]").val()) {
							$(".med-index-1 .remove-medication span").hide();
						}

						$(".med-index-1 input[name=medication1], .med-index-1 input[name=dose1], .med-index-1 input[name=instructions1]").bind("input", function(){
							if ($(".med-index-1 input[name=dose1]").val() || $(".med-index-1 input[name=instructions1]").val() || $(".med-index-1 input[name=medication1]").val())
							{
								$(".med-index-1 .remove-medication span").show();
							}
						});
					}

					if ($("body").hasClass("dependantContacts")) {

						$(".myepact-add-name a").click(function(e){
							e.preventDefault();
							$('#same-address-modal').modal('show');
						});
						
						// If the user answers "no" to the child authorization form, emergency contacts are required
						if ($('#epact-dependant-contacts-form').length) {
							if ($("#allowNoEmergContacts-false").is(':checked')) {
								if (!($("#alt-guardians").find('.myepact-person').length)) {
									$("#alt-guardians").addClass("required-contacts");
								} else {
									$("#alt-guardians").removeClass("required-contacts");
								}
							} else if ($("#allowNoEmergContacts-true").is(':checked')) {
								//
							} else {
								if ($("#contacts-section").hasClass("incomplete")) {
									$("#required-allNoEmergContacts").show();
								}
							}
						}

						$("#allowNoEmergContacts-true").each(function() {
							if ($(this).is(':checked')) {
								$("#alt-guardians").removeClass("required-contacts");
							}
						});

						$("#allowNoEmergContacts-false").each(function() {
							if ($(this).is(':checked')) {
								$("#alt-guardians").addClass("required-contacts");
							}
						});

						$("#allowNoEmergContacts-true").click(function() {
							$("#alt-guardians").removeClass("required-contacts");
							$("#emergency-requirement").hide();
							$("#required-allNoEmergContacts").hide();
						});

						$("#allowNoEmergContacts-false").click(function() {
							$("#alt-guardians").addClass("required-contacts");
							$("#required-allNoEmergContacts").hide();
						});

//						$('#epact-dependant-contacts-form').ajaxForm(function() { 
//						if (goahead == true) {   
//						window.destinationHref = redirect;
//						window.location.href = redirect;
//						}
//						});


						$("#contacts-back-noform, #contacts-next-noform").click(function() {

							if ($("#alt-guardians").length && (!($("#alt-guardians").find('.myepact-person').length))) {
								$('#emergency-requirement').show();
								$(window).scrollTop($('#ec-title').offset().top);
								$(".save-next-incomplete").show();

								if ($("#oop-contacts").length &&(!($("#oop-contacts").find('.myepact-person').length))) {
									$('#oop-requirement').show();
								}
							} else if ($("#oop-contacts").length && (!($("#oop-contacts").find('.myepact-person').length))) {
								$('#oop-requirement').show();
								$(".save-next-incomplete").show();
							} else {
								displaySavePopup("#epact-dependant-contacts-form", true);
								$(".btn-save").html("<span class='glyphicon glyphicon-ok'></span> Saved");     		
								$(".btn-save").attr("disabled","");
								$(".save-next-incomplete").hide();
								$(".save-next").show();
							}	            	
						});

						// Submits form and proceeds to the next page
						$("#contacts-next, #contacts-back").unbind("click").click(function() {
							var formToSave = $(this).data("saveform");

							if (!($("#allowNoEmergContacts-true").is(":checked")) && !($("#allowNoEmergContacts-false").is(":checked"))) {
								$("#required-allNoEmergContacts").show();
							} else {
								$("#required-allNoEmergContacts").hide();
							}

							if ($("#alt-guardians").length && (!($("#alt-guardians").find('.myepact-person').length))) {
								$('#emergency-requirement').show();
								$(window).scrollTop($('#ec-title').offset().top);
								$(".save-next-incomplete").show();

								if ($("#oop-contacts").length &&(!($("#oop-contacts").find('.myepact-person').length))) {
									$('#oop-requirement').show();
								}
							} else if ($("#oop-contacts").length && (!($("#oop-contacts").find('.myepact-person').length))) {
								$('#oop-requirement').show();
								$(".save-next-incomplete").show();
							} else {
								$(formToSave).attr("data-saveform", "true");    					
								$(formToSave).submit();
							}
						});	
					}

					if ((document.body.className == "dependantContacts dependantprofile contacts") || (document.body.className == "profileUserOrganization profile overviewUserOrganization")) { 

						// Only allows up to one out of area contact to be selected
						$(".oopSelect").each(function()
								{
							$(this).change(function()
									{
								if ($(this).prop('checked')) {
									$(".oopSelect").prop('checked',false);
									$(this).prop('checked',true);
								}
									});
								});
					}

					if ($("body").hasClass("landing")) {

						$("#landing-video1").click(function(e) {
							e.preventDefault();
							$("#vid-frame2").hide("slow");
							$("#vid-frame1").show("slow");
							$("#landing-video2").removeClass("active");
							$("#video-subtitle").html("<a target='_blank' href='https://www.youtube.com/watch?v=Ui3_ZgocpR4/'>What is ePACT</a>");
							$("#video-link").attr("href","https://www.youtube.com/watch?v=Ui3_ZgocpR4");
							$(this).addClass("active");
						});

						$("#landing-video2").click(function() {
							e.preventDefault();
							$("#vid-frame1").hide("slow");
							$("#vid-frame2").show("slow");
							$("#video-subtitle").html("<a target='_blank' href='https://www.youtube.com/watch?v=ywFfAqjYBaE'>ePACT for Organizations</a>");
							$("#landing-video1").removeClass("active");
							$("#video-link").attr("href","https://www.youtube.com/watch?v=ywFfAqjYBaE");
							$(this).addClass("active");
						});
					}

					function familyEditState() {

					}

					if ((document.body.className == "profileUserConnection profile overviewUserConnection")) {
						$("#connection-delete").click(function() {
							var r=confirm(languageStrings.getText("confirm_delete"));
							if (r==true) {
								var redirectPage = $(this).data("redirect");
								$("#epact-connection-delete-form").ajaxSubmit(function() {
									window.destinationHref = redirectPage;
									window.location.href = redirectPage;
								});
							}
						});
					}

					if ($("body").hasClass("viewCampaign")) {
						$("#campaign-delete").click(function() {
							var r=confirm(languageStrings.getText("confirm_delete"));
							if (r==true) {
								var redirectPage = $(this).data("redirect");
								$("#delete-campaign-form").ajaxSubmit(function() {
									window.destinationHref = redirectPage;
									window.location.href = redirectPage;
								});
							}
						});
					}

					if ($("body").hasClass("viewApplication")) {
						$("#application-delete").click(function() {
							var r=confirm(languageStrings.getText("confirm_delete"));
							if (r==true) {
								var redirectPage = $(this).data("redirect");
								$("#delete-application-form").ajaxSubmit(function() {
									window.destinationHref = redirectPage;
									window.location.href = redirectPage;
								});
							}
						});
					}
					
					
					
					if (document.body.className == "adminEditOrganization admin adminEditOrganization") {
						$("#control-for-allowMemberFileUpload label.checkbox input[type=checkbox]").insertAfter("#control-for-allowMemberFileUpload label.checkbox");
						$("#control-for-allowMemberFileUpload label.checkbox").insertBefore("#control-for-allowMemberFileUpload .controls");
						$("#control-for-allowMemberFileUpload label.checkbox").addClass("control-label");
						$("#control-for-allowMemberFileUpload label.checkbox").removeClass("checkbox");
						
						$("#control-for-allowSmsMessaging label.checkbox input[type=checkbox]").insertAfter("#control-for-allowSmsMessaging label.checkbox");
						$("#control-for-allowSmsMessaging label.checkbox").insertBefore("#control-for-allowSmsMessaging .controls");
						$("#control-for-allowSmsMessaging label.checkbox").addClass("control-label");
						$("#control-for-allowSmsMessaging label.checkbox").removeClass("checkbox");
						
						$("#org-delete").click(function() {
							var r=confirm(languageStrings.getText("confirm_delete_org"));
							if (r==true) {
								var redirectPage = $(this).data("redirect");
								$("#epact-admindeleteorg-form").ajaxSubmit(function() {
									window.destinationHref = redirectPage;
									window.location.href = redirectPage;
								});
							}
						});
					}		

					if (document.body.className == "profile profile overview") {
						$("#dependant-delete").click(function() {
							var r=confirm(languageStrings.getText("confirm_delete_dependant"));
							if (r==true) {
								var redirectPage = $(this).data("redirect");
								$("#epact-connection-delete-form").ajaxSubmit(function() {
									window.destinationHref = redirectPage;
									window.location.href = redirectPage;
								});
							}
						});
					}		


					if (document.body.className == "redeemOrganizationMemberUserInviteDecline inviteredeem redeemOrganizationMemberUserInviteDecline") {
						$("#declineReason-dropdown").change(function() {
							if ($(this).val() == "OTHER") {
								$(".declineReasonText-section").show();
							} else {
								$(".declineReasonText-section").hide();
							}
						});
					}

					// Ensures that phone numbers are required when editing adults in the household
					if ($("body").hasClass("profileFamily")) {
						$("#epact-adult-edit-form").each(function() {
							$(this).find("input[name=primaryPhoneOther]").attr("required","");
							$(this).find(".primaryPhoneOtherIsMobile-select").attr("notEqual","select");	
						});
					}

					if (document.body.className == "dependantFamily dependantprofile family") { 

						$('#epact-dependant-family-form').ajaxForm(function() {
							if (goahead == true) {
								window.destinationHref = redirect;
								window.location.href = redirect;
							}
						});

						// Submits two forms at once and proceeds to the previous page
						$("#submit-back, #submit-overview, #submit-next").click(function() {
							redirect = $(this).data("redirect");
							$('form#epact-dependant-family-form').submit();
						});
					}


					if ($("body.profileMedical.profile.medical").length || $("body.nondependantMedical.nondependantprofile.medical").length) {

						$("#medical-save").click(function() {
//							$(formToSave+" *").addClass("ignore");
							var formToSave = $(this).data("saveform");
//							$(formToSave+" *").addClass("ignore");
							$(formToSave).attr("data-saveform", "true"); 
							$(formToSave).submit();
//							$(formToSave+" *").removeClass("ignore");
//							$(formToSave).valid();
						});

						// Proceeds to the next page if the form has no validation errors
						$("#medical-next, #medical-back").click(function() {
							$("#epact-medicalinfo-form").data("redirectpage", $(this).data("redirect"));
							$('#epact-medicalinfo-form').validate();
							redirect = $(this).data("redirect");
							$('form#epact-medicalinfo-form').submit();
						});
					}



					/*** Begin additions for Content Security Policy ****/

					$(".account-switcher").click(function(e) {
						e.preventDefault();
						switchAccounts($(this).data('link'), $(this).data('redirect'));
					});

					$("button[data-validate=true]").click(function(e) {
						$(this).closest("form").validate();
					});

					$("[data-select]").click(function(ev) {
						ev.preventDefault();

						var select = $(this).data('select');
						var into = $(this).data('into');

						$('#' + into).load($(this).data('redirect') + ' #' + select, function(){
							$.getScript(scriptURL);
							$('#' + select).show();

							$(".cancel-select").click(function(e) {
								e.preventDefault();

								var reloadDiv = $(this).data("reloaddiv");
								var reloadURL = window.location.pathname + " " + reloadDiv;

								$(reloadDiv).load(reloadURL, function() {
									$.getScript(scriptURL); 
								});	
							});



							$(".select-button").click(function(event) {
								event.preventDefault();

								$("body").css("cursor", "progress");
								$(".select-button").attr("disabled","true");
								$(".popup-loader").show();

								var formName = $(this).data("formname");
								var reloadDiv = $(this).data("reloaddiv");

								if (!($(reloadDiv).length)) {
									reloadDiv = $(this).data("reloaddiv2");
								}

								$(formName).ajaxSubmit({

									error: function() {
										$(".select-warning").show();
										$("body").css("cursor", "default");
										$(".select-button").removeAttr("disabled");
										$(".popup-loader").hide();
									},         

									success: function() {    	      
										$(reloadDiv).load((window.location.pathname + " " + reloadDiv), function() {		            	
											$.getScript(scriptURL); 
											$("body").css("cursor", "default");
											$(".select-button").removeAttr("disabled");
											$(".select-warning").hide();
											$(".popup-loader").hide();

											if ($(".btn-save").length) {
												$(".btn-save").html("Save");     		
												$(".btn-save").removeAttr("disabled");
												$(".save-next,.save-next-incomplete").hide(); 
											}
										});    
									}
								});
							});

						});
					});

					$("[data-show-modal]").click(function(event) {
						event.preventDefault();
						$('#' + $(this).data('show-modal')).modal('show');
					});

					$("select[data-selected]").each(function() {
						$(this).children("option[value=" + $(this).data("selected") + "]").prop('selected', true);
					});

					$("input[data-form-ajax='true']").click(function(event)
							{
						event.preventDefault();
						$(this).ajaxSubmit();
							});

					$("[data-delete-confirm]").click(function(event)
							{
						if( !confirm(languageStrings.confirm_delete))
							event.preventDefault();
							});

					$("[data-refresh]").click(function(event){
						event.preventDefault();
						window.location = window.destinationHref;
					});

					$("[data-page-control]").click(function(e)
							{
						e.preventDefault();

						var page = $(this).data("page-control");

						var formName = $(this).data("page-form");

						if( formName.length )
						{
							var form = $(this).closest("#" + formName);
							var url = form.attr('') + "?page="+page;
							form.attr('action', url);
							form.submit();
						}
							});


					// Swap image
					$("img[data-img-swap='true']").on('mouseover', function(){

						if (!$(this).osrc) $(this).osrc = $(this).src; // keep an old copy
						var src = $(this).osrc;
						if (src) {
							$(this).src = src.replace(/\.png/, '-mo.png'); // use a pattern
							$(this).src = $(this).src.replace(/\.gif/, '-mo.gif');
							$(this).on('mouseout', function() {
								$(this).src = $(this).osrc; // restore
							});
						}

					});

					$("img.swap-this").on('mouseover', function(){
						swap($(this));
					});


					/**
					 *
					 * Wire up sitewide popover functionality
					 *
					 **/



					$('.popup-inline-help').popover({
						placement: 'bottom',
						html: true
					});

					$('.popup-inline-help').click(function (e) {
						e.stopPropagation();
					});


					$('.popup-inline-help').on('hide.bs.popover', function () {          
						$(".popover.fade").hide();
					});

					$('.popup-inline-help').on('shown.bs.popover', function () {
						$(".popover.fade.in").show();
					});

					$(document).click(function (e) {
						if (($('.popover').has(e.target).length == 0) || $(e.target).is('.close') || $(e.target).is('.close-popover')) {
							$('.popup-inline-help').popover('hide');
							$('.popup-ajax').popover('hide');
						}              
					});

					/*$(document).click(function (e) {
                    if (($('.popover').has(e.target).length == 0) || $(e.target).is('.close') || $(e.target).is('.close-popover')) {
                    	$('.popup-ajax').popover('hide');
                    }              
                });*/

					$('.popup-ajax').click(function (e) {
						e.stopPropagation();
					});

					$('.popup-ajax').on('hide.bs.popover', function () {          
						$(".popover.fade").hide();
					});

					/*$('.popup-ajax').on('show.bs.popover', function () {
                	$('.popup-ajax').not(this).popover('hide');
                	$(this).removeAttr("href");
                });*/


					$('.popup-ajax').on('shown.bs.popover', function (e) {

						$('.popup-ajax').not(this).popover('hide');
						$(this).removeAttr("href");

						$(".popover.fade.in").show();        	
						if($(this).data('loaded') == 'true')
						{
							return;
						}

						var popupContent = $(this).parents(".myepact-person,.myepact-person-right,.myepact-org,.org-row").find(".popover-content");
						var popupTitle = $(this).parents(".myepact-person,.myepact-person-right,.myepact-org,.org-row").find(".popover-title");

						if (($(popupContent).width() <= $(popupTitle).width() && $(popupContent).data("changed") != "true")) {
							$(popupContent).css("min-width",($(popupTitle).width() + 30));
							$(popupContent).data("changed","true");
						} 

						var popup = $(this);

						var link = $(this).data("overlay");

						$.ajax({
							url: link,
							cache: false,
							success: function(response) {
								$.getScript(scriptVarsURL).done(function() {
									if (!oldIE) {

										function hand () {
											// Once the request is received
											if (this.readyState == 2) {
												loggedIn = this.getResponseHeader('AUTHENTICATED');

												// Redirect to the homepage
												if (loggedIn == "false") {
													window.destinationHref = window.location.protocol + "//" + window.location.hostname + "/us/login?timeout=true";
													window.location.href = window.location.protocol + "//" + window.location.hostname + "/us/login?timeout=true";
												} /*else {
	                            	    		$(popup).data('bs.popover').options.content = '<div class="details">' + response + '</div>';

	                            	    	}*/

											}

										}
										$(popup).data('bs.popover').options.content = '<div class="details">' + response + '</div>';
										/*var x = new XMLHttpRequest();
	                            	x.onreadystatechange = hand;

	                            	x.open('GET', link, true);
	                            	x.send();*/

									} else {

										//fill in the notes in the clickover....
										$(popup).data('bs.popover').options.content = '<div class="details">' + response + '</div>';                                
									}
									$('.popover').addClass('fade'); // for the epact javascript
									$(popup).data('bs.popover').options.animation = true;
									$(popup).data('loaded', 'true');
									$(popup).popover('show');
								});
							}

						});
					});


					$('.popup-ajax').popover({
						html: true,
						content: "<div class='details'><img src='" + loadGIF + "'></div>"
					});

					//Run form validation if share is the refferer page
					if (document.referrer.indexOf("/share") != -1 && (document.referrer.indexOf("profile/") != -1 || document.referrer.indexOf("dependant/") != -1)) {
//						$('form').valid();
						if (!$('#medical-providers.required-providers #medical-people').length) {
							$('#medical-providers.required-providers #medical-requirement').show();
						}
						if (!$('#alt-guardians.required-contacts .myepact-people .myepact-person').length) {
							$('#alt-guardians.required-contacts #emergency-requirement').show();
						}
						if (!$('#oop-contacts.required-oopcontacts .myepact-people .myepact-person').length) {
							$('#oop-contacts.required-oopcontacts #oop-requirement').show();
						}
						if (!($("#allowNoEmergContacts-true").is(":checked")) && !($("#allowNoEmergContacts-false").is(":checked"))) {
							$("#required-allNoEmergContacts").show();
						}
					}

					if (typeof wizardLinks !== "undefined" && $.isFunction(wizardLinks)) {
						wizardLinks();
					}

					//Fix for ES-1687 shared address pop up issue
					$("#shared-address-modal button.close").click(function(){
						$(".btn-save").html("Save");        
						$(".btn-save").removeAttr("disabled");
					});

					$(".message-alert-content:visible").slideDown("fast", function() {
						setTimeout(function(){
							$(".message-alert-content").slideUp();
						},7000);
					});
					
					//Reverse years on datepicker
					$(".ui-datepicker").on("mouseenter", function() {
						if (!showPicker) {
							showPicker = true;
							
							//Reverse the years
							var dropYear = $("select.ui-datepicker-year");
							
							dropYear.find('option').each(function() {
								dropYear.prepend(this);
							});
						}	
					});
					
					if ($("#leave-message").length) {
						head.ready( function() {	
							window.completeWiz = "${completedWizard?c}";
							function wizardLinks() {
								$("a").on('click', function(e) {
									if($(this).attr("target") != "_blank")
									{
										if ($(this).attr("href") != undefined && $(this).attr("href") != null) {
											window.destinationHref = $(this).attr("href");
										} else if ($(this).attr("onClick") != undefined && $(this).attr("onClick") != null) {
											window.destinationHref = window.location.href;
										} else if ($(this).data("redirect") != undefined && $(this).data("redirect") != null) {
											window.destinationHref = $(this).data("redirect");
										} else {
											window.destinationHref = window.location.href;
										}
										showWizardMessage(e);
									}
								});
								$('input[type=submit]').on('click', function(e){
									var form = $(this).data('formname');
									window.destinationHref = $(form).attr('action');
									showWizardMessage(e);
								});
								$('.btn-save').on('click', function(e){
									window.destinationHref = window.location.href;
									showWizardMessage(e);
								});
							}
							
							function showWizardMessage(e) {
							
								if (window.completeWiz == "false" && !isWizUrl(window.destinationHref) ) {
									if (e != undefined && e != null)
										e.preventDefault();
									$("#leave-message").modal("show");
								}
							}
							
							function isWizUrl(url) {
								var wizUrls = ["/contact", "/medical", "/family", "/contacts", "/share", "/details", "/submitshareinfo", "/login", "/userfile"];
								if (url == undefined) {
									return true;
								}
								for (key in wizUrls) {
									if (url.indexOf(wizUrls[key]) != -1) {
										return true;
									} else if (url.indexOf("javascrip") == 0 || url.indexOf("#") == 0) {
										return true;
									}
								}
								
								return false;
							}
							
							wizardLinks();
						});
					}
					
					$("#loading-overlay").hide();
					
				}

		};

		// Trigger the init method
		epact.init();


	});

});


					// Loop and apply standard form validation
					$('.frm-srch-adviser').each(function() {

						$(this).validate({submitHandler: function(form) { 

							var formName = "#" + form.name;
							var redirectPage= $(formName).data("redirectpage");

							$("#login-loader").show();
							$("body").css("cursor", "progress");
							$(formName).ajaxSubmit({

								error: function() {
									$("body").css("cursor", "default");
									$("#login-loader").hide();
								},         

								success: function(response) {  	
									// Directs them to the lockout page if their account is locked out
									if (response.indexOf("accountLockout home accountLockout") != -1) {
										window.destinationHref =  lockoutLink;
										window.location.href = lockoutLink;
									}
									$.getScript(scriptVarsURL).done(function() {
									var loggedIn = false;
									$("body").css("cursor", "default");
									$("#login-loader").hide();

									var $data=$(response);
									var strippedResponse = $data.filter('#switchAccountVars').html();

									$("#hiddendiv").html(strippedResponse);      
									
									if (outOfDate) {
										$("#login-error-text").text("Page was out of date. Please try again.");
										$("#login-error").show();
										$("#login-form-hidden").load(document.URL + " #login-form-hidden");
									} else if (loggedInUserEmail == "unavailable") {
										$("#login-error-text").text("The username or password is incorrect.");
										$("#login-error").show();
									} else if (loggedInUserEmail == "admin@epactnetwork.com") {	
										switchAccounts(switchAdminLink,listOrgsLink);    
									} else {
										// If the user is an admin of any orgs, display the switch account overlay

										if (adminMode) {
											// Calls the switch acconut overlay if the user is an admin for an org
											$.ajax({
												url: switchAccountLink,
												cache: false,
												success: function(data) {
														var failure = null;

														$("#switch-account .modal-body").load(switchAccountLink, '', function(){
															$.getScript(scriptURL);
															$("#switch-account").modal('show');
															$("#full-menu").css("left","-9px");
															$(".navbar-static-top").css("position","relative");
															$(".navbar-static-top").css("left","-9px");
														});
												}
											});
										} else { 
											window.destinationHref = defaultLink;
											window.location.href = defaultLink;
										}
									}
									});
								}
							});
						}
						});          
					});
					
//$.validator.setDefaults({
//    highlight: function(element) {
//        $(element).closest('.form-group').addClass('has-error');
//    },
//    unhighlight: function(element) {
//        $(element).closest('.form-group').removeClass('has-error');
//    },
//    errorElement: 'span',
//    errorClass: 'help-block',
//    errorPlacement: function(error, element) {
//        if(element.parent('.input-group').length) {
//            error.insertAfter(element.parent());
//        } else {
//            error.insertAfter(element);
//        }
//    }
//});
//

$(document).ready(function(e) {
		
	/**validation request demo**/
//	 $("#my_form").validate({
//		rules: {
//		inputName: {required: true},
//		inputContactNo: {required: true},
//		inputEmail: {email: true, required: true},
//		inputCompany: {required: true},
//		inputCountry: {required: true},
//		'checkboxes[]': {required: true}
//		},
//		/*messages: {
//		inputName: "Name is Required.",
//		inputContactNo: "Contact No is Required.",
//		inputCompany: "Company is Required.",
//		inputCountry: "Country is Required.",
//		},*/
//		tooltip_options: {
//		inputName: {trigger:'focus'},
//		inputName: {placement:'right',html:true}
//		},
//		errorPlacement: function(error, element) {
//			 error.appendTo(element.closest('.form-group'));
//		},	
//		submitHandler: function() {
//	 
//			$.post(
//				base_url+'frontpage/anewskin/reqdemo', 
//				$('form#my_form').serialize() , 
//				function(data){
//					$('.modal_form').fadeOut(500, function(){$('.start_success').fadeIn(300);});
//					console.log(data);
//				},
//				"json"
//			);
//	
//		}
//	});
//	
//	/**validation request demo**/
//	 $("#contact_form").validate({
//		rules: {
//		inputname: {required: true},
//		inputwebsite: {required: true},
//		inputemail: {email: true, required: true},
//		inputmessage: {required: true},
//		inputCountry: {required: true}
//		},
//		messages: {
//		inputname: "Name is Required.",
//		inputwebsite: "Company is Required.",
//		inputmessage: "Message is Required.",
//		inputCountry: "Country is Required.",
//		},
//		tooltip_options: {
//		inputName: {trigger:'focus'},
//		inputName: {placement:'right',html:true}
//		},
//		submitHandler: function() {
//	 
//			$.post('frontpage/anewskin/contactus', 
//			$('form#contact_form').serialize() , 
//			function(data){
//				
//				$('.cont_success').fadeIn(300);
//				console.log(data.msg);
//	
//				$('.cont_success').fadeIn(300);
//				$('#inputname').val('');
//				$('#inputwebsite').val('');
//				$('#inputemail').val('');
//				$('#inputsubject').val('');
//				$('#inputmessage').val('');
//				$('select#inputCountry option:first-child').attr("selected", "selected");
//				
//				
//			}, "json");
//		}
//	});
	
	//price click
	$('.prices_btn').find('input').on('change', function(){
		var price = $(this).val();
		
		var quarterly = (price * 3) * 0.1;
		var yearly = (price * 12) * 0.2;
		quarterly = (price * 3) - quarterly;
		yearly = (price * 12) - yearly;
		
		if(price != 0){
			price = Math.round(price * 100) / 100;
			price = price.toFixed(0);		
		}
		
		quarterly = Math.round(quarterly * 100) / 100;
		yearly = Math.round(yearly * 100) / 100;

		if(price == '138' || price == '215'){
			quarterly = quarterly.toFixed(0) - 2;
			quarterly = quarterly.format(); //+ '.00';
			yearly = yearly - 5;
			yearly = yearly.format(); //replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
		}
		else if (price == '172'){
			quarterly = quarterly.toFixed(0) - 1;
			quarterly = quarterly.format(); // + '.00';
			yearly = yearly - 4;
			yearly = yearly.format(); //.replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
		}
		else{
			quarterly = quarterly.format(); //.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'); //.toFixed(2);
			yearly = yearly.format(); //.replace(/(\d)(?=(\d{3})+\.)/g, '$1,'); //.toFixed(2);
		}
		
		
		
		if(price != '0'){
			$('.pricing_table').show();
			$('.price_contact_us').hide();
			$('.monthly_price').html(price);
			$('.quarterly_price').html(quarterly);
			$('.yearly_price').html(yearly);
			$('#price_selected').val(price);
		}
		else{
			$('.pricing_table').hide();
			$('.price_contact_us').show();
		}
	});


	$('.buy_btn').on('click', function(){
		var $self = $(this);
		var plan = $self.data('plan');
		var price = $self.closest('.panel').find('.the_price').html();
//		var total_price = price * 0.1;
//		total_price = total_price + price;
		
		//auto select plan
		$('#select_plan').find('[data-price="'+price+'"]').attr('selected','selected');
		$('#select_plan').change();
		
		//var url = base_url+'signup?plan='+plan+'&'+'price='+price;
		//window.location.href = url;
		$('#buyNowModal').modal('show');
		return false;
	});
	
	
	$(document).on('focusin', '.input_location', function(){
		var $self = $(this);
		var $form = $self.closest('form');
		var form_id = $form.attr('id');

		$self.geocomplete()
		.bind("geocode:result", function(event, result){
	
			//var $form = $('#signup');
			var ac_count = result.address_components.length - 1;
			var country_short = result.address_components[ac_count]['short_name'];
			//var country_short = $form.find('[name="country_short"]').val();
			var location_lat = result.geometry.location.lat();
			var location_long = result.geometry.location.lng();
			$self.siblings('[name="lat"]').val(location_lat);
			$self.siblings('[name="lng"]').val(location_long);
			
			
			console.log(country_short);
			console.log(result);
			$.ajax({
				  type: "POST",
				  
				  url: base_url+"home/show_country_code",
				  
				  data: { country_short: country_short },
				  
				  dataType: 'json',
				  
				  success: function(data) {
					console.log(data);
	
					$form.find('.addon-shortcode').html('+'+data.calling_code);
					$form.find('[name="country_code"]').val(data.country_id);
					$form.find('[name="country_code"]').selectpicker('refresh');
					//$('[name="country_short"]').val(data);
	
				  },
				  error: function(err){
					  console.log(err);
				  }
			});
			
		});
	});
	
	
	//get country code
	$('.geocomplete').geocomplete({ details: "form#trial_form" }).bind("geocode:result", function(event, result){
		var $form = $('#trial_form');
		var country_short = $form.find('[name="country_short"]').val();
		
		console.log(result);
		$.ajax({
			  type: "POST",
			  
			  url: base_url+"frontpage/anewskin/show_country_code",
			  
			  data: { country_short: country_short },
			  
			  dataType: 'json',
			  
			  success: function(data) {
				console.log(data);

				$form.find('#addon-shortcode').html('+'+data.calling_code);
				$form.find('[name="country_code"]').val(data.country_id);
				//$('[name="country_short"]').val(data);

			  },
			  error: function(err){
				  console.log(err);
			  }
		});

	});
	
	
	$('[name="rc_is_ap"]').on('change', function(){
		var $form = $('#signup');
		if($('[name="rc_is_ap"]:checked').val() == 'Y'){
			var first_name = $form.find('[name="first_name"]').val();
			var last_name = $form.find('[name="last_name"]').val();
			var email = $form.find('[name="email"]').val();
			var mobile = $form.find('[name="mobile"]').val();
			var country_code = $form.find('[name="country_code"]').val();
			var the_code = $form.find('[name="country_code"]').find(':selected').attr('data-code');
			
			$form.find('[name="ccfirst_name"]').val(first_name);
			$form.find('[name="cclast_name"]').val(last_name);
			$form.find('[name="ccemail"]').val(email);
			$form.find('[name="ccmobile"]').val(mobile);
			$form.find('[name="cccountry_code"]').val(country_code);
			$('#ccaddon-shortcode').html('+'+the_code);
			
		}
		else{
			$form.find('[name="ccfirst_name"]').val('');
			$form.find('[name="cclast_name"]').val('');
			$form.find('[name="ccemail"]').val('');
			$form.find('[name="ccmobile"]').val('');
			$form.find('[name="cccountry_code"]').val('');
			$('#ccaddon-shortcode').html('+');
		}
	});
	
	
	$(document).on('change', '[name="plan"]', function(){
		var $self = $(this);
		var $form = $('#signup');
		var value = $self.val();
		var plan_desc = $self.find(':selected').text();
		
		$form.find('[name="plan"]').val(value);
		
		if(value == ''){
			$('.bill_info').addClass('hidden');
		}
		else{
			$('.bill_info').removeClass('hidden');
			var ssplit = value.split('__');
			var price = ssplit[2];
			var gst = ssplit[3];
			var total = ssplit[4];
			
			$('.the_total').html(total);
			$('.the_gst').html(gst);
			$('.thee_price').html(price);
			$('.plan_desc').html(plan_desc);
		}
	});
	
	$(document).on('change', '[name="country_code"]', function(){
		var ccode = $(this).find(':selected').attr('data-code');
		var country_id = $(this).val();
		$('.addon-shortcode').html('+'+ccode);
		$('[name="country_code"]').val(country_id);
		console.log('arr');
	});
	
//	$('[name="country_code"]').on('change', function(){
//		console.log('sdf');
//		var ccode = $(this).find(':selected').attr('data-code');
//		$('.addon-shortcode').html('+'+ccode);
//	});

	$('.continue_btn').on('click', function(){
		$(this).closest('form').submit();
		return false;
	});
	
	
	$('.payment_bck_btn').on('click', function(){
		$('#progressbar').children('.active').removeClass('active').prev().removeClass('done').addClass('active');
		$('.step-details-btn').click();
		$('.step-details-btn').addClass('active').siblings().removeClass('active');

	});
	
	$('.confirm_bck_btn').on('click', function(){
		$('#progressbar').children('.active').removeClass('active').prev().removeClass('done').addClass('active');
		$('.step-payment-btn').click();
		$('.step-payment-btn').addClass('active').siblings().removeClass('active');

	});

	$('.update_auth_btn').on('click', function(){
		$self = $(this);
		if($(this).html() == 'Update'){
			$('.auth_table').fadeOut('slow', function(){
				$('.the_clone_authfields').removeClass('hidden');
				$self.html('Save');
				$self.toggleClass('btn-default btn-primary');
			});
		}
		else{
			var form = '.the_clone_authfields';
			var first_name = $(form).find('[name="first_name"]').val();
			var last_name = $(form).find('[name="last_name"]').val();
			var address = $(form).find('[name="address"]').val();
			var organization = $(form).find('[name="organization"]').val();
			var email = $(form).find('[name="email"]').val();
			var mobile = $(form).find('[name="mobile"]').val();
			var country_code = $(form).find('[name="country_code"]').val();
			var the_code = $(form).find('[name="country_code"]').find(':selected').attr('data-code');

			var $signup = $('#signup');
			$signup.find('[name="first_name"]').first().val(first_name);
			$signup.find('[name="last_name"]').first().val(last_name);
			$signup.find('[name="address"]').first().val(address);
			$signup.find('[name="organization"]').first().val(organization);
			$signup.find('[name="email"]').first().val(email);
			$signup.find('[name="mobile"]').first().val(mobile);
			$signup.find('[name="country_code"]').first().val(country_code);

			$('.coo_name').html(first_name+ ' '+last_name);
			$('.coo_email').html(email);
			$('.coo_mobile').html('+'+the_code+''+mobile);
			$('.coo_bssadd').html(address);
			$('.coo_bssname').html(organization);
			
			
			$('.the_clone_authfields').addClass('hidden');
			$('.auth_table').fadeIn('slow', function(){
				$self.html('Update');
				$self.toggleClass('btn-default btn-primary');
			});
			
		}
		
	});
	
	$(document).on('hidden.bs.modal', '.modal', function () {
		if($('.modal:visible').length > 0){
			setTimeout(function(){
				$('body').addClass('modal-open');
			},200);
		}

	});
	
	$('[data-toggle="popover"]').popover();
	
	$('.security_info').popover({
		container: 'body',
		html : true,
        content: function() {
          return $('#security_info_content').html();
        },
		placement: 'top',
		trigger: 'click hover'
	});
	
});//document.ready

Number.prototype.format = function(n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};